# Translation of video_subtitles--gcompris--count_the_items.po to Catalan (Valencian)
# Copyright (C) 2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
# or the same license as the source of its messages in English.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
#. extracted from gcompris/count_the_items.srt
msgid ""
msgstr ""
"Project-Id-Version: websites-video-subtitles\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-09-06 12:11+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. visible for 10 seconds
#: 00:00:02.566--%3E00:00:13.466
msgid "Count the Items - Learning to group then count objects"
msgstr "Compta els elements - Aprendre a agrupar i comptar objectes"

#. visible for 6 seconds
#: 00:00:16.400--%3E00:00:22.400
msgid ""
"Choose \"Maths\" (Edward the sheep) > \"Numeration\"\n"
"and click on the activity"
msgstr ""
"Seleccioneu «Matemàtiques» (l'ovella Eduard) > «Numeració»\n"
"i feu clic en l'activitat"

#. visible for 5 seconds
#: 00:00:33.200--%3E00:00:38.300
msgid "Choose the difficulty"
msgstr "Seleccioneu la dificultat"

#. visible for 5 seconds
#: 00:00:52.133--%3E00:00:57.133
msgid "Grouping is an important part of learning to count"
msgstr "Agrupar és una part important d'aprendre a comptar"

#. visible for 4 seconds
#: 00:00:57.133--%3E00:01:01.766
msgid ""
"It helps to not forget any objects\n"
"and to count them only once"
msgstr ""
"Ajuda a no oblidar cap objecte\n"
"i a comptar-los només una vegada"

#. visible for 6 seconds
#: 00:01:51.100--%3E00:01:57.533
msgid ""
"Once all the questions of a level have been\n"
"answered, the next level starts"
msgstr ""
"Una vegada s'han respost totes les preguntes\n"
"d'un nivell, s'inicia el nivell següent"

#. visible for 4 seconds
#: 00:01:57.533--%3E00:02:01.700
msgid ""
"You can choose your level\n"
"by clicking on the level number"
msgstr ""
"Podeu triar el nivell\n"
"fent clic en el número de nivell"

#. visible for 16 seconds
#: 00:02:09.133--%3E00:02:25.533
msgid "We hope you enjoyed this activity!"
msgstr "Esperem que gaudiu d'esta activitat!"
