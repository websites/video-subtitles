# Translation of video_subtitles--kde--QtWayland_Robustness.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
# or the same license as the source of its messages in English.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
#. extracted from kde/QtWayland_Robustness.srt
msgid ""
msgstr ""
"Project-Id-Version: websites-video-subtitles\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-09-15 19:09+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#. visible for 4 seconds
#: 00:00:00.000--%3E00:00:04.000
msgid "Okay, so we've got some applications go open, movie playing,"
msgstr "D'acord, tenim algunes aplicacions obertes, reproduint una pel·lícula,"

#. visible for 6 seconds
#: 00:00:05.000--%3E00:00:11.000
msgid "Tetris playing in the corner, multi-tasking, and then all of a sudden,"
msgstr "jugant al Tetris a la cantonada, fent multitasca, i de sobte,"

#. visible for 2 seconds
#: 00:00:12.000--%3E00:00:14.000
msgid "Our KWin crashes."
msgstr "El KWin ha fallat."

#. visible for 5 seconds
#: 00:00:17.000--%3E00:00:22.000
msgid ""
"And the audio didn't stop, Tetris is continuing as soon as it gets over it "
"again."
msgstr "I l'àudio no s'atura, el Tetris continuarà tan aviat com ho superi."

#. visible for 3 seconds
#: 00:00:26.000--%3E00:00:29.000
msgid "And our IDE is still available."
msgstr "I el nostre IDE encara està disponible."

#. visible for 4 seconds
#: 00:00:29.000--%3E00:00:33.000
msgid "And I can run this multiple times, simulate multiple crashes,"
msgstr "I puc executar això diverses vegades, simulant fallades diverses,"

#. visible for 2 seconds
#: 00:00:37.000--%3E00:00:39.000
msgid "and we didn't skip a frame."
msgstr "i no perdem ni un fotograma."

#. visible for 5 seconds
#: 00:00:40.000--%3E00:00:45.000
msgid "We can even copy some text, copy this, restart KWin."
msgstr "Fins i tot podem copiar text, copiar això, reiniciar el KWin."

#. visible for 3 seconds
#: 00:00:46.000--%3E00:00:49.000
msgid "And now, it's still on my collect board."
msgstr "I ara, encara és al meu porta-retalls."

#. visible for 1 seconds
#: 00:00:52.000--%3E00:00:53.000
msgid "Robust."
msgstr "Robust."
