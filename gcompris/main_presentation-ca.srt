1
00:00:00,000 --> 00:00:07,166
GCompris: Una suite de programari educatiu d'alta qualitat

2
00:00:08,633 --> 00:00:12,533
El GCompris inclou més de 180 activitats per a infants de 2 a 10 anys

3
00:00:13,966 --> 00:00:17,633
Totes les activitats són divertides pel grup d'edat per al qual estan dissenyades

4
00:00:19,066 --> 00:00:23,633
Aquí hi ha una llista de categories d'activitat amb exemples

5
00:00:23,633 --> 00:00:28,566
Descobrir l'ordinador: apreneu a utilitzar el teclat, el ratolí, la pantalla tàctil...

6
00:00:28,566 --> 00:00:31,900
Teclat: escriviu la paraula abans que arribi a terra

7
00:00:32,233 --> 00:00:33,900
Ratolí: aprendre a fer servir els botons

8
00:00:35,233 --> 00:00:37,233
Pràctica de la coordinació pantalla tàctil/ratolí tàctil

9
00:00:38,566 --> 00:00:42,800
Lectura/escriptura: reconèixer lletres...

10
00:00:45,233 --> 00:00:50,966
...llegir paraules...

11
00:00:51,933 --> 00:00:57,566
...i amplieu el vostre vocabulari.

12
00:00:58,633 --> 00:01:02,800
Milloreu les vostres habilitats matemàtiques

13
00:01:02,800 --> 00:01:06,966
Entrenament de numeració i càlcul, resolució de problemes de mesura

14
00:01:08,566 --> 00:01:12,766
Ciència i humanitats: feu experiments, apreneu història i geografia

15
00:01:18,633 --> 00:01:22,800
Partides de joc: escacs, connecta'n quatre, aualé i molts més...

16
00:01:28,533 --> 00:01:33,566
Podeu descarregar el GCompris de franc per al Windows, Android, macOS i Linux

17
00:01:38,600 --> 00:01:48,633
GCompris: creat per la comunitat KDE
