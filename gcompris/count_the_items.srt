1
00:00:02,566 --> 00:00:13,466
Count the Items - Learning to group then count objects

2
00:00:16,400 --> 00:00:22,400
Choose "Maths" (Edward the sheep) > "Numeration"
and click on the activity

3
00:00:33,200 --> 00:00:38,300
Choose the difficulty

4
00:00:52,133 --> 00:00:57,133
Grouping is an important part of learning to count

5
00:00:57,133 --> 00:01:01,766
It helps to not forget any objects
and to count them only once

6
00:01:51,100 --> 00:01:57,533
Once all the questions of a level have been
answered, the next level starts

7
00:01:57,533 --> 00:02:01,700
You can choose your level
by clicking on the level number

8
00:02:09,133 --> 00:02:25,533
We hope you enjoyed this activity!

