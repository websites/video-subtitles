1
00:00:02,566 --> 00:00:13,466
Contare gli oggetti - Imparare a raggruppare e poi contare gli oggetti

2
00:00:16,400 --> 00:00:22,400
Scegli «Matematica» (Edoardo la pecora) > «Numerazione»
e fai clic sull'attività

3
00:00:33,200 --> 00:00:38,300
Scegli la difficoltà

4
00:00:52,133 --> 00:00:57,133
Il raggruppamento è una parte importante per imparare a contare

5
00:00:57,133 --> 00:01:01,766
Aiuta a non dimenticare nessun oggetto
e contarli una sola volta

6
00:01:51,100 --> 00:01:57,533
Una volta che tutte le domande di un livello sono state
risposte, inizia il livello successivo

7
00:01:57,533 --> 00:02:01,700
Puoi scegliere il tuo livello
facendo cli sul numero del livello

8
00:02:09,133 --> 00:02:25,533
Ci auguriamo che questa attività ti sia piaciuta!
