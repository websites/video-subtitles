1
00:00:02,566 --> 00:00:13,466
Zenbatu elementuak - Objektuak taldekatu eta ondoren haiek zenbatzen ikastea

2
00:00:16,400 --> 00:00:22,400
Aukeratu "Matematika" (Txuri ardia) > "Zenbakitzea"
eta egin klik jardueran

3
00:00:33,200 --> 00:00:38,300
Aukeratu zailtasuna

4
00:00:52,133 --> 00:00:57,133
Taldekatzea zenbatzen ikasteko atal garrantzitsu bat da

5
00:00:57,133 --> 00:01:01,766
Objekturik ez ahazten eta haiek
behin bakarrik zenbatzen laguntzen du

6
00:01:51,100 --> 00:01:57,533
Maila bateko galdera guztiak erantzun direnean,
hurrengo maila hasten da

7
00:01:57,533 --> 00:02:01,700
Zure maila aukera dezakezu
maila zenbakian klik eginda

8
00:02:09,133 --> 00:02:25,533
Jarduera honekin gozatu duzulakoan gaude!
