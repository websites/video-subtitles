1
00:00:00,000 --> 00:00:07,166
GCompris: A high quality educational software suite

2
00:00:08,633 --> 00:00:12,533
GCompris comes with more than 180 activities for children aged 2 to 10

3
00:00:13,966 --> 00:00:17,633
All activities are fun for the age group they are designed for

4
00:00:19,066 --> 00:00:23,633
Here is a list of activity categories with examples

5
00:00:23,633 --> 00:00:28,566
Computer discovery: Learn how to use a keyboard, mouse, touchscreen...

6
00:00:28,566 --> 00:00:31,900
Keyboard: Type the word before it reaches the ground

7
00:00:32,233 --> 00:00:33,900
Mouse: Learn to use the buttons

8
00:00:35,233 --> 00:00:37,233
Touchscreen/touchpad coordination practice

9
00:00:38,566 --> 00:00:42,800
Reading/Writing: Recognize letters...

10
00:00:45,233 --> 00:00:50,966
... read words, ...

11
00:00:51,933 --> 00:00:57,566
... and expand your vocabulary.

12
00:00:58,633 --> 00:01:02,800
Improve your maths skills

13
00:01:02,800 --> 00:01:06,966
Numeration and calculation training, solving measurement problems

14
00:01:08,566 --> 00:01:12,766
Science and Humanities: Carry out experiments, learn history and geography

15
00:01:18,633 --> 00:01:22,800
Play games: Chess, connect Four, Oware and many more...

16
00:01:28,533 --> 00:01:33,566
You can download GCompris for free for Windows, Android, macOS, and Linux

17
00:01:38,600 --> 00:01:48,633
Gcompris: Made by KDE

