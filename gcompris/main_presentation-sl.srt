1
00:00:00,000 --> 00:00:07,166
GCompris: Visokokakovostna izobraževalna programska oprema

2
00:00:08,633 --> 00:00:12,533
GCompris ponuja več kot 180 dejavnosti za otroke od 2 do 10 let

3
00:00:13,966 --> 00:00:17,633
Vse dejavnosti so zabavne za starostno skupino, ki so ji namenjene

4
00:00:19,066 --> 00:00:23,633
Tukaj je seznam kategorij dejavnosti s primeri

5
00:00:23,633 --> 00:00:28,566
Odkrivanje računalnika: naučite se uporabljati tipkovnico, miško, zaslon na dotik...

6
00:00:28,566 --> 00:00:31,900
Tipkovnica: Vnesite besedo, preden doseže podlago

7
00:00:32,233 --> 00:00:33,900
Miška: Naučite se uporabljati gumbe

8
00:00:35,233 --> 00:00:37,233
Vaja koordinacije zaslona na dotik/sledilne ploščice

9
00:00:38,566 --> 00:00:42,800
Branje/pisanje: Prepoznavanje črk...

10
00:00:45,233 --> 00:00:50,966
.. preberite besede, ...

11
00:00:51,933 --> 00:00:57,566
.. in razširite svoj besedni zaklad.

12
00:00:58,633 --> 00:01:02,800
Izboljšajte svoje matematične sposobnosti

13
00:01:02,800 --> 00:01:06,966
Učenje številčenja in računanja, reševanje merskih problemov

14
00:01:08,566 --> 00:01:12,766
Znanost in humanistika: Izvajajte poskuse, učite se zgodovine in geografije

15
00:01:18,633 --> 00:01:22,800
Igrajte igre: Šah, Poveži štiri, Oware in še druge...

16
00:01:28,533 --> 00:01:33,566
GCompris lahko prosto prenesete za Windows, Android, macOS in Linux

17
00:01:38,600 --> 00:01:48,633
Gcompris: Izdelal KDE
