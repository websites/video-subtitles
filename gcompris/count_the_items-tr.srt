1
00:00:02,566 --> 00:00:13,466
Ögeleri Say - Nesneleri gruplayıp saymayı öğrenmek

2
00:00:16,400 --> 00:00:22,400
"Matematik" > "Numaralandırma" seç
ve etkinliğe tıkla

3
00:00:33,200 --> 00:00:38,300
Zorluğu seç

4
00:00:52,133 --> 00:00:57,133
Gruplamak, saymayı öğrenmenin önemli bir bölümüdür

5
00:00:57,133 --> 00:01:01,766
Hiçbir nesneyi unutmamana ve onları
yalnızca bir kez saymana yardımcı olur

6
00:01:51,100 --> 00:01:57,533
Bir düzeydeki tüm sorulan yanıtlandığında
sonraki düzey başlar

7
00:01:57,533 --> 00:02:01,700
Düzey numarasına tıklayarak istediğin
düzeyi seçebilirsin

8
00:02:09,133 --> 00:02:25,533
Bu etkinliğin hoşuna gittiğini umuyoruz!
