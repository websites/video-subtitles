1
00:00:02,566 --> 00:00:13,466
Cuenta los objetos: aprende a agrupar y contar objetos

2
00:00:16,400 --> 00:00:22,400
Elige «Matemáticas» (la oveja Eduardo) > «Numeración»
y pulsa sobre la actividad

3
00:00:33,200 --> 00:00:38,300
Elige la dificultad

4
00:00:52,133 --> 00:00:57,133
Agrupar es una parte importante para aprender a contar.

5
00:00:57,133 --> 00:01:01,766
Ayuda a no olvidar ningún objeto
y a contarlos solo una vez

6
00:01:51,100 --> 00:01:57,533
Cuando hayas respondido a todas las preguntas
de un nivel, empezará el siguiente nivel

7
00:01:57,533 --> 00:02:01,700
Puedes escoger el nivel
pulsando el número del nivel

8
00:02:09,133 --> 00:02:25,533
¡Esperamos que te haya gustado esta actividad!
