1
00:00:02,566 --> 00:00:13,466
Štetje predmetov - Učenje združevanja in štetja predmetov

2
00:00:16,400 --> 00:00:22,400
Izberite "Matematika" (Oven Edward) > "Štetje"
in kliknite na dejavnost

3
00:00:33,200 --> 00:00:38,300
Izberite težavnost

4
00:00:52,133 --> 00:00:57,133
Razvrščanje v skupine je pomemben del učenja štetja

5
00:00:57,133 --> 00:01:01,766
Pomaga, da ne pozabite nobenega predmeta
in jih preštejete samo enkrat

6
00:01:51,100 --> 00:01:57,533
Ko so odgovorjena vsa vprašanja ene ravni
se začne naslednja raven

7
00:01:57,533 --> 00:02:01,700
Izberete lahko svojo raven
s klikom na številko ravni

8
00:02:09,133 --> 00:02:25,533
Upamo, da ste uživali v tej dejavnosti!
