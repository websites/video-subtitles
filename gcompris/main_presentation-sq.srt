1
00:00:00,000 --> 00:00:07,166
GCompris- Një suitë software-i edukativ i cilësisë së lartë

2
00:00:08,633 --> 00:00:12,533
GCompris vjen me më shumë se 180 veprimtari për fëmijë të moshës 2 deri në 10 vjeç

3
00:00:13,966 --> 00:00:17,633
Krejt veprimtaritë kanë zbavitëse për grup-moshën për të cilën janë menduar

4
00:00:19,066 --> 00:00:23,633
Ja një listë e kategorive të veprimtarive, me shembuj

5
00:00:23,633 --> 00:00:28,566
Njihni kompjuterat: Mësoni si të përdorni një tastierë, mi, ekran me prekje…

6
00:00:28,566 --> 00:00:31,900
Tastierë: Shtypni fjalën, përpara se bjerë përdhe

7
00:00:32,233 --> 00:00:33,900
Mi: Mësoni si të përdorni butonat

8
00:00:35,233 --> 00:00:37,233
Praktikë bashkërendimi në ekran me prekje/touchpad

9
00:00:38,566 --> 00:00:42,800
Lexim/Shkrim: Njohje shkronjash…

10
00:00:45,233 --> 00:00:50,966
... lexoni fjalë, ...

11
00:00:51,933 --> 00:00:57,566
… dhe zgjeroni fjalorin tuaj.

12
00:00:58,633 --> 00:01:02,800
Përmirësoni aftësitë tuaja matematikore

13
00:01:02,800 --> 00:01:06,966
Stërvitje në numërim dhe llogaritje, zgjidhje problemesh matjeje

14
00:01:08,566 --> 00:01:12,766
Shkenca dhe Shoqërore: Kryeni eksperimente, mësoni histori dhe gjeografi

15
00:01:18,633 --> 00:01:22,800
Luani lojëra: Shah, katërsh, Oware dhe plot të tjera…

16
00:01:28,533 --> 00:01:33,566
GCompris-në mund ta shkarkoni falas për Windows, Android, macOS dhe Linux

17
00:01:38,600 --> 00:01:48,633
GCompris: Prodhuar nga KDE
