1
00:00:02,566 --> 00:00:13,466
Tel het aantal items - leren om ze te groeperen en daarna objecten tellen

2
00:00:16,400 --> 00:00:22,400
Kies "Maths" (Edward het schaap) > "Nummers geven"
en klik op de activiteit

3
00:00:33,200 --> 00:00:38,300
Kies de moeilijkheidsgraad

4
00:00:52,133 --> 00:00:57,133
Groeperen is een belangrijk onderdeel van leren te tellen

5
00:00:57,133 --> 00:01:01,766
Het helpt om geen enkel object te vergeten
en ze slechts één keer te tellen

6
00:01:51,100 --> 00:01:57,533
Nadat alle vragen van een niveau zijn
beantwoord, zal het volgende niveau beginnen

7
00:01:57,533 --> 00:02:01,700
U kunt uw niveau kiezen
door te klikken op het niveaunummer

8
00:02:09,133 --> 00:02:25,533
We hopen dat u plezier had met deze activiteit!
