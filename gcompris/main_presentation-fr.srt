1
00:00:00,000 --> 00:00:07,166
GCompris : une suite logicielle éducative de haute qualité

2
00:00:08,633 --> 00:00:12,533
GCompris est livré avec plus de 180 activités pour les enfants de 2 à 10 ans

3
00:00:13,966 --> 00:00:17,633
Toutes les activités sont amusantes pour le groupe d'âge pour lequel ils sont conçus

4
00:00:19,066 --> 00:00:23,633
Voici une liste des catégories d'activités avec des exemples

5
00:00:23,633 --> 00:00:28,566
Découverte de l'ordinateur : apprendre à utiliser un clavier, une souris, un écran tactile…

6
00:00:28,566 --> 00:00:31,900
Clavier : tapez le mot avant qu'il n'atteigne le sol

7
00:00:32,233 --> 00:00:33,900
Souris : apprenez à utiliser les boutons

8
00:00:35,233 --> 00:00:37,233
Entraînement à la coordination de l'écran tactile / pavé tactile

9
00:00:38,566 --> 00:00:42,800
Lecture / écriture : reconnaître les lettres…

10
00:00:45,233 --> 00:00:50,966
… lire les mots, …

11
00:00:51,933 --> 00:00:57,566
… et développez votre vocabulaire.

12
00:00:58,633 --> 00:01:02,800
Améliorez vos compétences en mathématiques

13
00:01:02,800 --> 00:01:06,966
Entraînement à la numérotation et au calcul, à la résolution des problèmes de mesure

14
00:01:08,566 --> 00:01:12,766
Science et sciences humaines : effectuer des expériences, apprendre l'histoire et la géographie

15
00:01:18,633 --> 00:01:22,800
Jouez à des jeux : échecs, Puissance 4, Oware et bien d'autres…

16
00:01:28,533 --> 00:01:33,566
Vous pouvez télécharger gratuitement GCompris pour Windows, Android, macOS et Linux

17
00:01:38,600 --> 00:01:48,633
GCompris : développé par KDE
