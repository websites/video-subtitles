1
00:00:02,566 --> 00:00:13,466
Numëroni Gjëra - Mësoni të gruponi, mandej të numëroni objekte

2
00:00:16,400 --> 00:00:22,400
Zgjidhni “Matematikë” (dashin Eduard > “Numërim”
dhe klikoni mbi veprimtarinë

3
00:00:33,200 --> 00:00:38,300
Zgjidhni vështirësi

4
00:00:52,133 --> 00:00:57,133
Grupimi është pjesë e rëndësishme e mësimit si të numëroni

5
00:00:57,133 --> 00:01:01,766
Ndihmon të mos harrohet ndonjë objekt
dhe të numërohet vetëm një herë

6
00:01:51,100 --> 00:01:57,533
Pasi t’u jeni përgjigjur krejt pyetjeve
të një niveli, fillon niveli tjetër

7
00:01:57,533 --> 00:02:01,700
Nivelin tuaj mund ta zgjidhni
duke klikuar mbi numrin e nivelit

8
00:02:09,133 --> 00:02:25,533
Shpresojmë t’ju ketë shijuar kjo veprimtari!
