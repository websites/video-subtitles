1
00:00:02,566 --> 00:00:13,466
Räkna sakerna: lär dig att gruppera och sedan räkna objekt

2
00:00:16,400 --> 00:00:22,400
Välj "Matte" (fåret Edward) > "Räkning"
och klicka på aktiviteten

3
00:00:33,200 --> 00:00:38,300
Välj svårighetsgraden

4
00:00:52,133 --> 00:00:57,133
Gruppering är en viktig del av att lära sig räkna

5
00:00:57,133 --> 00:01:01,766
Det hjälper att inte glömma några saker
och att bara räkna dem en gång

6
00:01:51,100 --> 00:01:57,533
När alla frågor på en nivå har
besvarats, börjar nästa nivå

7
00:01:57,533 --> 00:02:01,700
Du kan välja nivå genom
att klicka på nivånumret

8
00:02:09,133 --> 00:02:25,533
Vi hoppas att du tycker att aktiviteten är rolig!
