1
00:00:02,566 --> 00:00:13,466
Comptez les éléments - Apprendre à regrouper puis à compter les objets

2
00:00:16,400 --> 00:00:22,400
Sélectionnez « Maths » (Edward le mouton)> « Numération »
et faites un clic sur l'activité.

3
00:00:33,200 --> 00:00:38,300
Sélectionnez le niveau de difficulté

4
00:00:52,133 --> 00:00:57,133
Le regroupement est une partie importante de l'apprentissage du calcul.

5
00:00:57,133 --> 00:01:01,766
Il aide à n'oublier aucun objet
Et pour les compter une seule fois.

6
00:01:51,100 --> 00:01:57,533
Une fois que les réponses à toutes les questions d'un niveau
ont été fournies, le niveau suivant commence.

7
00:01:57,533 --> 00:02:01,700
Vous pouvez sélectionner votre niveau
par un clic sur le numéro de niveau.

8
00:02:09,133 --> 00:02:25,533
Nous espérons que vous avez apprécié cette activité !
