#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: MIT

import json

def get_video_id(json_file, key):
    """
    Get the id of the video depending on the platform (PeerTube or YouTube)
    """
    with open(json_file, encoding="utf-8") as file:
        json_data = json.load(file)
        return json_data[key] if key in json_data else ""
