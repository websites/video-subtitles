#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: MIT

"""
**upload_peertube** allows you to push all the subtitles for a video.
"""
import sys
import os
import argparse
import time
import json
import requests

import common

API_URL = 'https://tube.kockatoo.org/api/v1'
verbose = False

# pt is Brazilian Portuguese
specific_languages_codes = {"ca@valencia": "ca-valencia",
                            "pt": "pt-PT",
                            "pt_BR": "pt",
                            "zh_CN": "zh-Hans",
                            "zh_TW": "zh-Hant"}

access_tokens = {}

def upload_video(srt_filename, locale, video_id):
    token = get_access_token_for_folder(os.path.dirname(srt_filename))
    if token is None:
        if verbose:
            print(f"missing credentials.json file in folder of {srt_filename}")
        return
    [token_type, access_token] = token
    headers = {'Authorization': token_type + ' ' + access_token}

    if verbose:
        print(f"Request {API_URL}/videos/{video_id}/captions/{locale}")
    with open(srt_filename, 'rb') as srt_data:
        files = {
            'captionfile': (srt_filename, srt_data)
        }
        if not dry_run:
            print(f"Pushing subtitles for {video_id} and language {locale}")
            response = requests.put(f"{API_URL}/videos/{video_id}/captions/{locale}", headers=headers, files=files, timeout=2)
            print(f"Upload: response={response.status_code}, {response.content}")
        # Sleep to avoid too many requests at same time
        time.sleep(1)


def handle_video(file_to_upload):
    if verbose:
        print("handle", file_to_upload)

    no_extension_file = file_to_upload.replace(".srt", "")
    if (locale_index := no_extension_file.find("-")) != -1:
        no_extension_file = no_extension_file[0:locale_index]
    json_file = no_extension_file + ".json"

    if not os.path.exists(json_file):
        if verbose:
            print(f"{json_file} does not exist, skip {file_to_upload} file")
            return
    peertube_id = common.get_video_id(json_file, "pt_id")

    if peertube_id == "":
        if verbose:
            print(f"Skip subtitle {file_to_upload} because it has no peertube video")
        return
    # Check if it is the original srt (English) or a translated one
    if (locale_index := file_to_upload.find("-")) != -1:
        locale = file_to_upload[locale_index+1:].removesuffix(".srt")
        locale = specific_languages_codes[locale] if locale in specific_languages_codes else locale
        upload_video(file_to_upload, locale, peertube_id)
    else:
        upload_video(file_to_upload, "en", peertube_id)

    # To get existing subtitles
    # response = requests.get(f"{API_URL}/videos/{peertube_id}/captions/", timeout=2, headers=headers)
    # data = response.json()
    # print(data)

def handle_folder(folder):
    """
    Browse recursively all the files in the folder.
    For each srt file, if the filename contains a locale and if there is a json file
    for the original srt file, we upload the file to peertube
    """
    if verbose:
        print(f"Recursive find in {folder} folder")
    for root, dirs, files in os.walk(folder):
        dirs[:] = [d for d in dirs if d not in ['.git', 'po', 'poqm']]
        for name in files:
            if name.endswith("srt"):
                handle_video(os.path.join(root, name))


def get_access_token_for_folder(folder):
    credentials_file = os.path.join(folder, "credentials.json")
    if not os.path.exists(credentials_file):
        if verbose:
            print(f"Credentials file does not exist, cannot upload for {folder}")
        return
    with open(credentials_file, encoding="utf-8") as file:
        json_data = json.load(file)
        api_user = os.environ[json_data["user"]]
        api_pass = os.environ[json_data["password"]]

        if api_user in access_tokens:
            if verbose:
                print(f"Reusing existing token for folder {folder} (user {api_user})")
            return access_tokens[api_user]

        access_tokens[api_user] = get_access_token(api_user, api_pass)
        return access_tokens[api_user]


def get_access_token(api_user, api_pass):
    """
    Get the peertube token access for the user
    """
    response = requests.get(API_URL + '/oauth-clients/local', timeout=2)
    data = response.json()
    client_id = data['client_id']
    client_secret = data['client_secret']

    data = {
        'client_id': client_id,
        'client_secret': client_secret,
        'grant_type': 'password',
        'response_type': 'code',
        'username': api_user,
        'password': api_pass
    }

    response = requests.post(API_URL + '/users/token', data=data, timeout=2)
    if response.status_code != 200:
        print(f"Error: {response.text}")
        sys.exit(-1)
    data = response.json()

    if "status" in data:
        print(f"Error when requesting to log in: {data['detail']}")
        sys.exit(1)

    return [data['token_type'], data['access_token']]


parser = argparse.ArgumentParser()
parser.add_argument("--input", "-i", help="srt input folders",
                    nargs='*', default=["."])
parser.add_argument("--verbose", "-v", help="verbose mode",
                    action="store_true")
parser.add_argument("--dry-run", "-d",
                    help="dry run mode (no upload)", action="store_true")

args = parser.parse_args()
verbose = args.verbose
dry_run = args.dry_run

for input_folder in args.input:
    if os.path.isdir(input_folder):
        handle_folder(input_folder)
    elif os.path.isfile(input_folder):
        handle_video(input_folder)
