<!-- SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
     SPDX-License-Identifier: CC0-1.0
-->

# video-subtitles


## Getting started

This repository contains subtitles files (in srt format) to be translated for various KDE videos that are uploaded to PeerTube or YouTube.

For each subtitle file, translators will have a po file generated that can be translated as all KDE po files. Then, the nightly scripts will generate a translated subtitle file for each. The output filename will be `<video>-locale.srt`.

If a translator prefers to directly translate the srt file (for example, to update the timestamp, add new texts...), coordinate with your translation team to ensure the po won't be translated (if the po is 100% translated, it will override the srt file) and you can directly commit the file `<video>-locale.srt` in git.

## Adding a new subtitle
When adding a new srt file, ensure the filename does not have any dash in it.
If we plan to upload the file on PeerTube or YouTube, you need to add a <srt>.json file containing an id for PeerTube `pt_id` and/or an id for YouTube `yt_id`.

Don't forget to add the licence and copyright in the .reuse/dep5 for the added files (`<video>.srt` and `<video>.json`). Also, add a line for the future translations (`<video>-*.json`). The licence is the same as the srt file.

You should prefer doing a Merge Request so the pipeline will check that proper licencing/copyright has been done.

## Uploading subtitles on PeerTube/YouTube
You need to have the credentials of the owner of the videos you want to publish the subtitles. Ensure the environment variables are well set according to their names in the credentials.json file of each subfolder.
As of now, 2 different accounts exist, one for krita (`VIDEO_SUB_KRITA_ACCOUNT_USER` and `VIDEO_SUB_KRITA_ACCOUNT_PASS` for user/password variables) and one for all the other KDE videos (`VIDEO_SUB_KDE_ACCOUNT_USER` and `VIDEO_SUB_KDE_ACCOUNT_PASS`).

* For [PeerTube](https://docs.joinpeertube.org/api-rest-reference.html#tag/Video-Captions).
To upload all subtitles: 
```bash
python ./upload_peertube.py 
```
To upload subtitles for a specific video or folder:
```bash
python ./upload_peertube.py -i <path_to_srt_folder> -i <path_to_srt_file>
```
Help is available using `-h` option:
```bash
python ./upload_peertube.py -h
```
       
* For [YouTube](https://developers.google.com/youtube/v3/docs/captions?hl=en).
To upload all subtitles: 
```bash
export API_SECRET="xxxx.json"; python ./upload_youtube.py 
```
To upload subtitles for a specific video or folder:
```bash
export API_SECRET="xxxx.json"; python ./upload_youtube.py -i <path_to_srt_folder> -i <path_to_srt_file>
```
Help is available using `-h` option:
```bash
python ./upload_youtube.py -h
```
   
To generate the JSON file, you need to create an application in https://console.cloud.google.com/, then create an OATH 2.0 identifier as `Desktop` type. Then, you need to generate the secret code file.
