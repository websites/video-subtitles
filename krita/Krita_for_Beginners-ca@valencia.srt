1
00:00:00,130 --> 00:00:02,040
Hola a tothom, primer de tot

2
00:00:02,040 --> 00:00:07,140
Vull explicar-vos que este vídeo és per a les persones que comencen amb la pintura digital i

3
00:00:07,330 --> 00:00:13,620
trien <b><font color=#ff0000>Krita</b></font> com a primera opció per a aprendre. Així que si eres un usuari de nivell mitjà o avançat

4
00:00:14,139 --> 00:00:18,839
<i>Probablement veuràs coses en este vídeo que ja saps. En este vídeo,</i>

5
00:00:18,840 --> 00:00:24,719
parlarem de: descarregar Krita, controladors, com instal·lar Krita correctament,

6
00:00:24,789 --> 00:00:28,079
la pantalla de benvinguda, com crear la nostra primera imatge,

7
00:00:28,420 --> 00:00:35,790
pinzells, l'espai de treball i el color, com guardar les imatges i treballar de manera segura. Així que estàs preparat?

8
00:00:36,850 --> 00:00:38,850
Comencem

9
00:00:44,560 --> 00:00:47,940
Tot ha canviat molt des dels primers programes de pintura digital

10
00:00:48,550 --> 00:00:56,430
Hui dia, vivim moments apassionants perquè estem envoltats d'aplicacions creatives. Hui més que mai podem

11
00:00:56,430 --> 00:00:59,159
pintar amb moltes eines diferents i açò és genial.

12
00:00:59,160 --> 00:01:03,360
Però a vegades ens satura la quantitat d'opcions disponibles.

13
00:01:03,790 --> 00:01:11,069
Si estàs ací, és perquè estàs interessat en Krita i vols començar a pintar i crear el teu propi món

14
00:01:11,260 --> 00:01:16,259
Potser has vist una imatge bonica feta amb Krita i açò

15
00:01:16,570 --> 00:01:19,410
augmenta la teua curiositat per este programari

16
00:01:19,660 --> 00:01:23,160
Bé, este vídeo és per a fer-te més fàcil la primera

17
00:01:23,380 --> 00:01:28,499
experiència amb Krita. Des de les primeres etapes, Krita ha sigut un programa d'un nivell mitjà

18
00:01:28,780 --> 00:01:34,619
Una interfície sense massa botons que et distraguen o t'intimiden, açò sempre s'agraïx

19
00:01:35,259 --> 00:01:38,489
Perquè no hem d'aprendre una interfície complexa

20
00:01:38,490 --> 00:01:44,580
Però podem ajustar-ho a les nostres necessitats. Si no has instal·lat el programa, ací tens alguns consells.

21
00:01:44,619 --> 00:01:45,520
En primer lloc,

22
00:01:45,520 --> 00:01:47,520
On puc obtindre el programari de Krita?

23
00:01:47,680 --> 00:01:53,610
Molta gent busca programari en pàgines que oferixen molt programari. Açò no és recomanable

24
00:01:53,800 --> 00:01:58,559
perquè podem trobar males sorpreses com programari maliciós o espia.

25
00:01:59,170 --> 00:02:01,170
Així que si vols pintar de manera segura

26
00:02:01,390 --> 00:02:08,970
el millor lloc per a descarregar Krita és el lloc web oficial Krita.org. Els enllaços estan en la

27
00:02:09,280 --> 00:02:13,830
descripció a continuació. Podem buscar el botó de baixada o senzillament prémer el botó gran

28
00:02:14,549 --> 00:02:20,909
«Aconseguix ara Krita» un botó molt descriptiu. A continuació, pots triar la plataforma que vols per a Krita.

29
00:02:21,010 --> 00:02:23,010
Açò és, si utilitze per exemple

30
00:02:23,500 --> 00:02:28,139
Linux, Windows o Macintosh. Podem baixar Krita i mentre es baixa

31
00:02:28,540 --> 00:02:35,249
Fem un colp d'ull a què tenim ací. Com pots veure, també podem anar fins a la Windows Store o a una plataforma Steam

32
00:02:35,590 --> 00:02:39,479
Si volem obtindre Krita com a aplicació. És contingut de pagament

33
00:02:39,480 --> 00:02:46,259
però ajuda a pagar al principal desenvolupador de Krita, Boudewijn, és força important. També pots veure que hi ha alguna cosa anomenada

34
00:02:46,630 --> 00:02:51,540
«nightly builds». Krita té dues versions diferents. Què preferixes?

35
00:02:52,380 --> 00:02:54,859
una versió estable o una versió de desenvolupament?

36
00:02:55,500 --> 00:02:58,130
Quin Krita és el millor per a mi?

37
00:02:58,740 --> 00:03:00,380
Una pregunta estranya, oi?

38
00:03:00,380 --> 00:03:00,870
bé…

39
00:03:00,870 --> 00:03:03,740
Si necessites estabilitat per a acabar el teu projecte

40
00:03:04,140 --> 00:03:08,509
còmodament, l'elecció més habitual és descarregar la versió estable

41
00:03:08,640 --> 00:03:12,830
És la que té les principals característiques per a acabar el treball

42
00:03:12,890 --> 00:03:20,330
Però si vols provar les últimes característiques i fins i tot donar els teus comentaris després d'açò, pots baixar la

43
00:03:21,030 --> 00:03:27,020
Versió en desenvolupament. I sí, el llançament del desenvolupament pot fallar, i sí, açò és bo

44
00:03:30,060 --> 00:03:36,229
És bo perquè açò ens permet refinar el desenvolupament i deixar Krita tant polit com siga

45
00:03:36,360 --> 00:03:40,190
possible per al pròxim llançament i el més bo és que puc descarregar

46
00:03:40,560 --> 00:03:46,970
les «Nightly builds» i no suprimix el meu llançament estable. Així que tu tries. Ja hem baixat Krita

47
00:03:47,120 --> 00:03:49,120
I ara què? Controladors

48
00:03:50,280 --> 00:03:56,179
És còmode descarregar els últims controladors per a la nostra tauleta gràfica. Tindre el nostre equip

49
00:03:56,430 --> 00:04:02,810
actualitzat sempre és una bona idea i podem estalviar molts problemes tant en Krita com en un altre programari

50
00:04:03,299 --> 00:04:06,739
Així que invertix una mica de temps en preparar-ho tot.

51
00:04:06,739 --> 00:04:11,449
T'espere. Una vegada descarregat Krita i hages actualitzat els controladors,

52
00:04:11,450 --> 00:04:18,169
pots instal·lar Krita. Si trobes problemes amb la tauleta, verifica que és compatible amb Krita

53
00:04:18,810 --> 00:04:24,649
Krita admet una àmplia varietat de dispositius, però de vegades podem trobar comportaments estranys

54
00:04:24,690 --> 00:04:28,910
Ací tens l'enllaç per a comprovar-ho. També en la descripció de davall

55
00:04:29,370 --> 00:04:35,419
Alguns usuaris han informat de problemes amb alguns «anells» al voltant del cursor en Windows 10

56
00:04:35,419 --> 00:04:38,299
Per tant, mira este vídeo si estàs en esta situació

57
00:04:39,120 --> 00:04:44,929
La instal·lació de Krita no és complicada. Una vegada descarregat, fes clic en el fitxer per a instal·lar-lo

58
00:04:45,810 --> 00:04:52,070
A més, assegura't que l'intèrpret d'ordres de Windows està marcat per a veure els fitxers de Krita en el navegador de fitxers en Windows

59
00:04:52,919 --> 00:04:56,809
Si eres un usuari de Linux, pots baixar una «appimage»

60
00:04:57,570 --> 00:05:00,830
Una vegada descarregat, has de fer que el fitxer siga executable

61
00:05:01,740 --> 00:05:06,569
comprovant els permisos de control. Fes clic dret en el fitxer «appimage» i

62
00:05:06,879 --> 00:05:09,869
selecciona «Propietats» i després fes-lo executable

63
00:05:10,509 --> 00:05:16,619
D'acord, obri Krita i apareixerà la pantalla de presentació carregant els recursos. Després d'alguns segons

64
00:05:16,620 --> 00:05:23,969
Ara pots veure la interfície del programa. Tenim una espècie de pantalla de benvinguda. Esta pantalla dona molta informació important

65
00:05:24,610 --> 00:05:29,550
Per exemple, podem veure les últimes notícies de Krita. Gràcies a este quadro

66
00:05:30,069 --> 00:05:32,968
enllaçat amb el compte oficial de pintura de Krita

67
00:05:33,400 --> 00:05:38,159
I també enllaços molt interessants que t'ajuden a saber més sobre

68
00:05:38,319 --> 00:05:45,028
el món de Krita i la seua comunitat. També podem anar directament al manual i aprendre molt llegint bon

69
00:05:45,159 --> 00:05:50,729
contingut, creat pels desenvolupadors de Krita. Podem anar directament al fòrum i buscar ajuda.

70
00:05:50,849 --> 00:05:55,199
Només has de registrar un compte, iniciar la sessió i parlar directament amb

71
00:05:55,569 --> 00:06:00,929
els desenvolupadors i compartir les teues idees o afegir obres d'art per a obtindre comentaris d'altres usuaris.

72
00:06:01,210 --> 00:06:06,060
Contacte directe amb programadors i artistes. Molt bé! Ací en la primera columna

73
00:06:06,060 --> 00:06:10,259
veem «Fitxer nou» amb la drecera habitual «Control+N»

74
00:06:10,539 --> 00:06:15,869
Si hi fas clic apareixerà una finestra nova i hi veuràs moltes opcions. Tu

75
00:06:16,270 --> 00:06:22,259
pots triar una plantilla o crear el teu propi fitxer amb la mida que necessites. Si no crees una imatge

76
00:06:22,479 --> 00:06:26,098
algunes parts de la interfície de Krita no funcionaran, com…

77
00:06:26,589 --> 00:06:31,199
La selecció del pinzell, la selecció del color, etc. Només hi ha menús disponibles.

78
00:06:31,629 --> 00:06:34,619
Açò és lògic perquè no hi ha res amb què treballar.

79
00:06:34,719 --> 00:06:40,889
Així que fem la nostra primera imatge en Krita. Ves fins al document personalitzat i tria la mida del fitxer

80
00:06:40,889 --> 00:06:44,758
vols crear i no preocupar-te de totes les opcions.

81
00:06:44,759 --> 00:06:49,679
Hi donarem un colp d'ull en un altre vídeo. Indica una mida i fes clic en el botó «Crea»

82
00:06:49,960 --> 00:06:53,279
Una vegada crees una imatge pots començar a treballar-hi.

83
00:06:53,589 --> 00:06:58,919
Hi ha interfícies que són molt complexes i que ens superen.

84
00:06:59,469 --> 00:07:01,469
també m'ha passat

85
00:07:01,569 --> 00:07:07,528
Però, què necessitem realment per a pintar? Necessitem pinzells de colors i un

86
00:07:08,050 --> 00:07:09,189
espai de treball confortable

87
00:07:09,189 --> 00:07:11,549
Pinzells, espai de treball i color

88
00:07:12,250 --> 00:07:13,509
Així que pinzells…

89
00:07:13,509 --> 00:07:19,809
on són els pinzells, i com pintar i esborrar les coses, com canviar la seua mida ràpidament i

90
00:07:19,970 --> 00:07:23,470
Com no perdre's entre tantes opcions de pinzell.

91
00:07:24,110 --> 00:07:28,330
El conjunt de pinzells de Krita està dissenyat per a adaptar-se a les necessitats bàsiques de qualsevol persona

92
00:07:28,460 --> 00:07:29,289
primer de tot

93
00:07:29,289 --> 00:07:32,229
Està bé assegurar-se que tot és correcte

94
00:07:32,570 --> 00:07:40,329
perquè pot passar que activem el «mode esborrador» per error i no ho recordem i de sobte

95
00:07:40,639 --> 00:07:45,099
els pinzells no pinten i ens podem confondre. Així que

96
00:07:45,740 --> 00:07:48,820
verifica que el botó del mode esborrador estiga desactivat

97
00:07:48,949 --> 00:07:53,348
D'acord! D'entrada, els pinzells es troben en la part dreta de la interfície

98
00:07:53,690 --> 00:08:01,479
Però pots canviar on es troben senzillament arrossegant l'acoblador. Si tanques l'acoblador «Pinzells» pots restaurar-lo amb facilitat. Ves fins a

99
00:08:01,759 --> 00:08:08,589
«Configuració -> Acobladors -> Pinzells predefinits» i Krita recordarà la seua última ubicació on s'ha visualitzat.

100
00:08:10,400 --> 00:08:12,519
Els pinzells poden paréixer excessius

101
00:08:13,340 --> 00:08:17,440
Però potser en voldràs més d'ací a alguns dies o setmanes.

102
00:08:18,470 --> 00:08:25,209
He dit que a la botiga de Krita hi ha tot un conjunt per a emular l'oli, el pastel i l'aquarel·la

103
00:08:27,500 --> 00:08:31,720
Krita sap que ub munt de pinzells pot desbordar a l'usuari.

104
00:08:32,180 --> 00:08:37,839
Així que ha creat un sistema d'etiquetes que es pot utilitzar per a veure'n només alguns

105
00:08:38,479 --> 00:08:40,479
orientat a fluxos de treball específics

106
00:08:41,120 --> 00:08:45,039
Vegem com funciona açò. Per exemple, tenim la «Pintura»

107
00:08:45,709 --> 00:08:47,029
«Esbós»

108
00:08:47,029 --> 00:08:54,789
«Textura», etc. Cada etiqueta té uns quants pinzells relacionats amb esta tasca. Per exemple, l'etiqueta «Textures»

109
00:08:54,980 --> 00:09:02,380
Té alguns pinzells per a ajudar-te a crear efectes ràpids amb textures. En vídeos futurs veurem més coses sobre els pinzells.

110
00:09:03,440 --> 00:09:08,020
Una altra manera de buscar pinzells és mitjançant el filtratge, podem anar més enllà

111
00:09:08,570 --> 00:09:11,770
explorar els pinzells utilitzant l'editor de pinzells, per exemple,

112
00:09:12,230 --> 00:09:19,480
Per a aprendre més sobre els motors de pinzell, ho veurem en un vídeo que vindrà. Però potser no t'agrada tindre els pinzells

113
00:09:19,480 --> 00:09:23,980
allà, sempre visibles, com observant-te. En este cas es pot utilitzar la

114
00:09:24,350 --> 00:09:31,410
drecera de teclat «F6» o anar fins a la finestra desplegable de la part superior que obrirà la finestra amb tots els pinzells

115
00:09:32,410 --> 00:09:37,439
Ací pots utilitzar el sistema d'etiquetes. Encara tens una altra opció molt interessant.

116
00:09:38,140 --> 00:09:41,009
A vegades no fem servir més de 10 pinzells

117
00:09:41,830 --> 00:09:46,770
Imagina, per exemple, que estem dibuixant a pantalla completa sense cap interfície

118
00:09:47,050 --> 00:09:54,540
prement la drecera «Tab» i després prems el botó dret del ratolí o el botó del llapis que tens associat amb el clic dret

119
00:09:55,450 --> 00:10:00,150
Permet veure coses com els pinzells, el color, els últims colors utilitzats.

120
00:10:00,150 --> 00:10:06,720
I també emmirallar la imatge, girar-la i més. Este menú ràpid és realment potent.

121
00:10:06,850 --> 00:10:12,119
Ho veurem també en un altre vídeo. Has triat un pinzell. Per a canviar la mida

122
00:10:12,360 --> 00:10:20,320
prem la tecla de «majúscules» i arrossega el cursor cap a la dreta per a augmentar la mida o cap a l'esquerra per a disminuir-la

123
00:10:20,520 --> 00:10:26,220
També podem fer-ho a través de les dreceres de teclat. Bé, no és tan còmode, o

124
00:10:26,920 --> 00:10:33,020
a través del control lliscant a la part superior de la interfície, el qual és una bona idea. Una cosa que notaràs,

125
00:10:33,030 --> 00:10:35,309
és que si tries un pinzell i

126
00:10:35,890 --> 00:10:37,810
canvies la mida, per exemple

127
00:10:37,810 --> 00:10:45,510
Fas algunes pinzellades i penses que es veu molt bé, és molt bo i m'agrada tal com és.

128
00:10:46,090 --> 00:10:50,639
però si selecciones un altre pinzell i tornes a seleccionar l'anterior

129
00:10:51,400 --> 00:10:55,800
Veuràs que el pinzell ha tornat al seu estat anterior

130
00:10:56,740 --> 00:10:59,820
En este cas, els teus canvis no han sigut massa.

131
00:11:00,370 --> 00:11:07,650
Però imagina que comences a jugar amb els paràmetres del pinzell i al final no saps què has canviat exactament,

132
00:11:07,650 --> 00:11:12,869
però el resultat del pinzell és molt xulo. En este cas

133
00:11:12,880 --> 00:11:15,460
Açò pot ser realment molest. Oi?

134
00:11:15,580 --> 00:11:22,300
Açò es fa per a les persones que preferixen utilitzar pinzells predeterminats com són i sense canviar-los massa.

135
00:11:22,440 --> 00:11:29,240
Esta és una bona pràctica al començament perquè t'ajuda a entendre millor la diferència entre ells i

136
00:11:29,680 --> 00:11:35,310
les possibilitats que pots tindre amb un sol pinzell i esta és la raó per la qual s'ha creat Krita

137
00:11:35,560 --> 00:11:42,859
Els «pinzells bruts». Pinzells que poden recordar els canvis fins i tot si canvies els pinzells una vegada i una altra,

138
00:11:43,350 --> 00:11:45,890
Per a utilitzar esta característica, cal activar

139
00:11:46,410 --> 00:11:53,480
«Guarda temporalment les deformacions al predefinit» en l'editor de pinzells, Krita recordarà la configuració fins que es tanque.

140
00:11:53,640 --> 00:11:58,849
D'acord, si vols guardar una versió modificada senzillament fes clic en el botó de sobreescriptura

141
00:11:59,040 --> 00:12:01,040
I açò és tot

142
00:12:01,140 --> 00:12:04,969
Ja tens el primer pinzell modificat a punt per a ser utilitzat

143
00:12:05,269 --> 00:12:11,479
Fins i tot si tanques Krita. Per a estar en un espai de treball còmode, necessitem saber com fer la navegació bàsica

144
00:12:12,000 --> 00:12:17,779
El zoom, la panoràmica, el gir i també l'emmirallament horitzontal poden ser útils

145
00:12:18,029 --> 00:12:18,890
en Krita

146
00:12:18,890 --> 00:12:23,059
utilitzem de manera predeterminada la tecla «M» com a drecera per a emmirallar la vista

147
00:12:23,370 --> 00:12:30,200
Açò és realment útil quan pintem durant molt temps. Una drecera senzilla, però una característica potent. El color

148
00:12:31,110 --> 00:12:37,550
Quan pintem, necessitem un lloc per a triar els colors. Un lloc on es poden triar colors diferents.

149
00:12:37,920 --> 00:12:43,490
Per exemple, com faç que el meu color siga menys saturat o més brillant?

150
00:12:44,190 --> 00:12:49,909
Podem utilitzar «Selector avançat del color» o utilitzar una «Paleta» personalitzada. Per ara

151
00:12:49,950 --> 00:12:56,390
Deixarem açò tal com és d'entrada perquè veurem el color en els vídeos següents.

152
00:12:57,240 --> 00:13:03,109
Guardeu les imatges i treballeu amb seguretat. Una pràctica molt recomanada és guardar sovint

153
00:13:03,570 --> 00:13:08,539
Així que, per favor, fes-ho. De vegades, el programari es bloqueja. Bé…

154
00:13:08,540 --> 00:13:12,740
Supose que has estat dibuixant i provant els pinzells que venien d'entrada

155
00:13:12,899 --> 00:13:17,659
però què passa si estem tan il·lusionats que ens oblidem de guardar i

156
00:13:18,000 --> 00:13:21,859
de sobte, la llum s'apaga? Fa anys, açò era un problema gran

157
00:13:21,930 --> 00:13:27,770
Et fa tornar a fer-ho tot des de l'última vegada que has guardat el treball

158
00:13:28,740 --> 00:13:35,990
Però estem en una era tecnològica, no? Per açò, Krita va més enllà i ens ajuda?

159
00:13:36,750 --> 00:13:40,039
Cada vegada que obris una imatge i hi estàs treballant

160
00:13:40,649 --> 00:13:42,889
Krita guardarà el vostre treball en silenci

161
00:13:43,260 --> 00:13:50,179
El programa realitza una còpia del fitxer i s'encarrega de guardar el treball cada 15 minuts. És genial o no?

162
00:13:51,059 --> 00:13:58,439
Crec que ho és. Si volem canviar este període, hem d'anar fins al menú «Configuració -> Configura Krita… -> General

163
00:13:59,319 --> 00:14:02,218
-> pestanya Gestió de fitxers» i allí canviem el període

164
00:14:02,889 --> 00:14:04,719
Fem-ho.

165
00:14:04,719 --> 00:14:11,489
Considere que cinc minuts és un temps no intrusiu per a guardar en el cas que hi haja algun motiu estrany

166
00:14:12,099 --> 00:14:18,959
Vols desmarcar esta opció, fes clic en el botó d'opció «Activa la guardada automàtica»

167
00:14:19,389 --> 00:14:22,108
Jo no ho faria. No ho faré.

168
00:14:22,109 --> 00:14:26,098
Puc assegurar-ho. I si vull guardar etapes diferents del meu treball

169
00:14:26,529 --> 00:14:31,859
llavors utilitzem l'opció de guardar de manera incremental. Un nom genial per a una característica molt útil

170
00:14:32,319 --> 00:14:34,348
I per què crec que és tan útil?

171
00:14:34,719 --> 00:14:38,369
Perquè si vos adoneu que té una drecera associada

172
00:14:38,529 --> 00:14:44,669
la qual és «Ctrl+Alt+S» i d'esta manera només prement la drecera crearem una

173
00:14:44,949 --> 00:14:51,478
Versió incremental del fitxer en el qual estem treballant. Si t'agrada este tipus d'eines que ajuden a la productivitat,

174
00:14:51,669 --> 00:14:56,759
feu-m'ho saber en els comentaris i feu-li un «m'agrada» a Krita. D'acord! Açò és tot de moment!

175
00:14:56,759 --> 00:14:59,878
Ací tens els fonaments, però hi ha molt més.

176
00:15:00,189 --> 00:15:03,898
No oblides subscriure't i compartir este vídeo en les teues xarxes socials

177
00:15:03,899 --> 00:15:09,719
I si penses que açò podria ajudar els teus amics i més usuaris de Krita, llavors compartix-ho.

178
00:15:10,179 --> 00:15:15,119
Si t'interessa algun tema concret o vols suggerir temes nous

179
00:15:15,879 --> 00:15:21,028
Fes-m'ho saber en els comentaris de davall i potser este podria ser el vídeo següent

180
00:15:21,969 --> 00:15:28,169
Moltes gràcies per veure'l i estar preparat per a vídeos nous on veurem com utilitzar Krita

181
00:15:28,239 --> 00:15:30,239
amb característiques més avançades

182
00:15:30,789 --> 00:15:32,789
Adeu
