1
00:00:00,130 --> 00:00:02,040
Ten eerste: Hallo iedereen

2
00:00:02,040 --> 00:00:07,140
Ik wil u graag vertellen over deze video die is voor de mensen die beginnen met digitaal schilderen en

3
00:00:07,330 --> 00:00:13,620
<b><font color=#ff0000>Krita</b></font> kiezen als de eerste optie om dat te leren. Dus als u een gebruiker op het middenniveau of een geavanceerde gebruiker

4
00:00:14,139 --> 00:00:18,839
<i>dan zult u dingen in deze video zien die u al weet. In deze video,</i>

5
00:00:18,840 --> 00:00:24,719
gaan we praten over: Krita downloaden, stuurprogramma's, hoe Krita juist te installeren,

6
00:00:24,789 --> 00:00:28,079
het openingsscherm,hoe uw eerste afbeelding te creëren,

7
00:00:28,420 --> 00:00:35,790
penselen, de werkruimte en kleur, hoe uw afbeeldingen en werk veilig op te slaan. Dus bent u klaar?

8
00:00:36,850 --> 00:00:38,850
Laten we beginnen

9
00:00:44,560 --> 00:00:47,940
Alles is heel erg veranderd sinds de eerste digitale programma's voor schilderen

10
00:00:48,550 --> 00:00:56,430
We leven nu in opwindende tijden omdat we omringt zijn door creatieve toepassingen. Vandaag meer dan ooit kunnen we

11
00:00:56,430 --> 00:00:59,159
schilderen met heel veel verschillend gereedschap en dat is geweldig.

12
00:00:59,160 --> 00:01:03,360
Maar soms zijn we lam geslagen door de hoeveelheid opties die we beschikbaar hebben.

13
00:01:03,790 --> 00:01:11,069
Als u hier bent, is dat omdat u geïnteresseerd bent in Krita en u wilt beginnen met schilderen en uw eigen wereld creëren

14
00:01:11,260 --> 00:01:16,259
Misschien hebt u een prachtige afbeelding, gemaakt met Krita, gezien en dat

15
00:01:16,570 --> 00:01:19,410
Heeft uw nieuwsgierigheid over de software van Krita verhoogd

16
00:01:19,660 --> 00:01:23,160
Wel, deze video is er om uw eerste ervaring met Krita

17
00:01:23,380 --> 00:01:28,499
gemakkelijker te maken. Sinds de vroegste stadia is Krita een programma geweest met een middenniveau

18
00:01:28,780 --> 00:01:34,619
Een interface zonder te veel knoppen die u afleiden of intimideren, wordt altijd gewaardeerd

19
00:01:35,259 --> 00:01:38,489
Omdat we geen complex interface hoeven te leren

20
00:01:38,490 --> 00:01:44,580
Maar we kunnen het aanpassen, om aan uw behoeften te voldoen. Als u het programma nog niet hebt geïnstalleerd dan zijn hier een paar tips.

21
00:01:44,619 --> 00:01:45,520
ten eerste,

22
00:01:45,520 --> 00:01:47,520
Waar haal ik de Krita software?

23
00:01:47,680 --> 00:01:53,610
Veel mensen zoeken naar software in pagina's die veel software aanbieden. Dat is niet aanbevolen

24
00:01:53,800 --> 00:01:58,559
omdat we slechte verrassingen kunnen vinden zoals malware of spyware.

25
00:01:59,170 --> 00:02:01,170
Dus als u veilig wilt schilderen

26
00:02:01,390 --> 00:02:08,970
dan is de beste site voor downloaden van Krita de officiële website van Krita.org. De koppelingen zijn in de

27
00:02:09,280 --> 00:02:13,830
onderstaande beschrijving. We kunnen zoeken naar de downloadknop of op de grote knop drukken

28
00:02:14,549 --> 00:02:20,909
"Haal Krita nu op" een erg duidelijke knop. Daarna kunnen we het platform kiezen waarvoor we Krita willen.

29
00:02:21,010 --> 00:02:23,010
Dat wil zeggen, als ik bijvoorbeeld gebruik

30
00:02:23,500 --> 00:02:28,139
Linux, Windows of Macintosh. We kunnen Krita downloaden en terwijl het wordt gedownload

31
00:02:28,540 --> 00:02:35,249
Laten we een kijkje nemen op wat we hier hebben. Zoals u kunt zien kunnen we ook naar Windows Store gaan of een steamplatform

32
00:02:35,590 --> 00:02:39,479
Als we Krita als een toepassing willen hebben. Is het betaalde inhoud

33
00:02:39,480 --> 00:02:46,259
maar het helpt bij het betalen van de hoofdontwikkelaar van Krita Boudewijn, echt belangrijk. U kunt ook zien dat er iets is genaamd

34
00:02:46,630 --> 00:02:51,540
nachtelijke bouwsels. Krita heeft twee verschillende versies. Dus wat heeft uw voorkeur?

35
00:02:52,380 --> 00:02:54,859
een stabiele versie of ontwikkelversie?

36
00:02:55,500 --> 00:02:58,130
Welke Krita is het beste voor mij?

37
00:02:58,740 --> 00:03:00,380
Vreemde vraag, niet?

38
00:03:00,380 --> 00:03:00,870
wel...

39
00:03:00,870 --> 00:03:03,740
Als u stabiliteit wilt om uw project te comfortabel

40
00:03:04,140 --> 00:03:08,509
te beëindigen, de meest gebruikelijke keuze is het downloaden van de stabiele versie

41
00:03:08,640 --> 00:03:12,830
Het is diegene die de hoofdfuncties heeft om de job beëindigen

42
00:03:12,890 --> 00:03:20,330
Maar als u de laatste mogelijkheden wilt proberen en zelfs uw terugkoppeling daarna wilt geven, dan kunt u downloaden

43
00:03:21,030 --> 00:03:27,020
de ontwikkelversie. En ja, de ontwikkelversie kan crashen, en ja, dat is goed

44
00:03:30,060 --> 00:03:36,229
Dat is goed omdat dit ons in staat stelt de ontwikkeling te verfijnen en Krita zo goed mogelijk te polijsten

45
00:03:36,360 --> 00:03:40,190
Mogelijk voor de volgende uitgave en het goede is dat ik kan downloaden

46
00:03:40,560 --> 00:03:46,970
Nachtelijke bouwsels en mijn stabiele uitgave niet verwijderen. U kunt kiezen. We hebben Krita al gedownload

47
00:03:47,120 --> 00:03:49,120
En nu wat verder? Stuurprogramma's

48
00:03:50,280 --> 00:03:56,179
Het is handig om de laatste stuurprogramma's voor uw grafische tablet te downloaden. Uw apparatuur

49
00:03:56,430 --> 00:04:02,810
bijgewerkt houden is altijd een goed idee en we kunnen veel problemen vermijden zowel in Krita als in andere software

50
00:04:03,299 --> 00:04:06,739
Investeer dus een beetje tijd in alles gereed maken.

51
00:04:06,739 --> 00:04:11,449
Ik wacht op u. Nadat Krita is gedownload en u hebt de stuurprogramma's bijgewerkt,

52
00:04:11,450 --> 00:04:18,169
kunt u Krita installeren. Als u problemen vindt met het tablet ga dan na dat uw tablet compatibel is met Krita

53
00:04:18,810 --> 00:04:24,649
Krita dekt een brede reeks apparaten, maar soms vinden vreemde gedragingen

54
00:04:24,690 --> 00:04:28,910
Hier is dus de koppeling om het te bekijken. Ook in de onderstaande beschrijving

55
00:04:29,370 --> 00:04:35,419
Sommige gebruikers hebben problemen gerapporteerd met "ringen" rond de cursor in Windows 10

56
00:04:35,419 --> 00:04:38,299
Bekijk dus deze video als u in die situatie bent

57
00:04:39,120 --> 00:04:44,929
Krita installeren is niet gecompliceerd nadat het gedownload is, klik op het bestand om het te installeren

58
00:04:45,810 --> 00:04:52,070
Ga na dat Windows shell gemarkeerd is om de krita-bestanden in onze bestandsbrowser in Windows te zien

59
00:04:52,919 --> 00:04:56,809
Als u een Linux gebruiker bent kunt u een appimage downloaden

60
00:04:57,570 --> 00:05:00,830
Nadat het is gedownload moet u het bestand daarna uitvoerbaar maken

61
00:05:01,740 --> 00:05:06,569
De rechten controleren. Klik rechts op uw appimage-bestand en

62
00:05:06,879 --> 00:05:09,869
selecteer eigenschappen en maak het daarna uitvoerbaar

63
00:05:10,509 --> 00:05:16,619
Okay, open Krita en het startscherm verschijnt die hulpbronnen laadt. Na een paar seconden

64
00:05:16,620 --> 00:05:23,969
Nu kunt u het programma-interface zien. We hebben een soort van welkomstscherm. Dit scherm geeft heel wat belangrijke informatie

65
00:05:24,610 --> 00:05:29,550
We kunnen, bijvoorbeeld, het laatste nieuws van Krita zien. Dankzij dit vak

66
00:05:30,069 --> 00:05:32,968
Gekoppeld aan het officiële account van Krita schilderen

67
00:05:33,400 --> 00:05:38,159
En ook erg interessante koppelingen die u helpen er meer over te leren

68
00:05:38,319 --> 00:05:45,028
De wereld van Krita en zijn gemeenschap. Ook kunnen we direct naar de handleiding gaan en veel leren na goed lezen

69
00:05:45,159 --> 00:05:50,729
inhoud, gemaakt door Krita ontwikkelaars. We kunnen direct naar het forum gaan en hulp zoeken.

70
00:05:50,849 --> 00:05:55,199
Registreer een account, meldt u aan en u kunt direct praten met

71
00:05:55,569 --> 00:06:00,929
ontwikkelaars en uw ideeën delen of kunstwerk toevoegen om terugkoppeling te krijgen van andere gebruikers.

72
00:06:01,210 --> 00:06:06,060
Direct contact met programmeurs en artiesten. fijn! Hier op de eerste kolom

73
00:06:06,060 --> 00:06:10,259
we zien "nieuw bestand" met de algemene sneltoets "Ctrl+N"

74
00:06:10,539 --> 00:06:15,869
Als ik erop klik verschijnt een nieuw venster en zie ik vele opties. U

75
00:06:16,270 --> 00:06:22,259
kunt een sjabloon kiezen of uw eigen bestand aanmaken met de benodigde grootte. Als u geen afbeelding aanmaakt

76
00:06:22,479 --> 00:06:26,098
werken sommige delen van het interface van Krita niet, zoals...

77
00:06:26,589 --> 00:06:31,199
Selectie van penseel, kleur enzovoort. alleen menu's zijn beschikbaar.

78
00:06:31,629 --> 00:06:34,619
Dit is logisch omdat er niets is om mee te werken.

79
00:06:34,719 --> 00:06:40,889
Laten we onze eerste afbeelding in Krita maken. Ga naar aangepast document en kies de bestandsgrootte

80
00:06:40,889 --> 00:06:44,758
die u wilt aanmaken en maak u geen zorgen over alle opties.

81
00:06:44,759 --> 00:06:49,679
We zullen een kijkje op ze nemen in een andere video. Geef een grootte en klik op de knop Aanmaken

82
00:06:49,960 --> 00:06:53,279
Nadat u een afbeelding hebt gemaakt kunt u erop beginnen te werken.

83
00:06:53,589 --> 00:06:58,919
Er zijn interfaces die erg complex zijn en ons overweldigen.

84
00:06:59,469 --> 00:07:01,469
het is ook met mij gebeurd

85
00:07:01,569 --> 00:07:07,528
Maar wat hebben we echt nodig voor schilderen? We hebben penseelkleuren nodig en een comfortabele

86
00:07:08,050 --> 00:07:09,189
werkruimte

87
00:07:09,189 --> 00:07:11,549
Penselen, werkruimte en kleur

88
00:07:12,250 --> 00:07:13,509
Dus penselen...

89
00:07:13,509 --> 00:07:19,809
waar zijn de penselen en hoe dingen te schilderen en te wissen, hoe hun grootte snel te wijzigen en

90
00:07:19,970 --> 00:07:23,470
Hoe niet verloren te geraken tussen zoveel penseelopties.

91
00:07:24,110 --> 00:07:28,330
De set penselen van Krita is ontworpen om te passen in de basis behoeften van iedereen

92
00:07:28,460 --> 00:07:29,289
ten eerste

93
00:07:29,289 --> 00:07:32,229
Het is goed er zeker van te zijn dat alles correct is

94
00:07:32,570 --> 00:07:40,329
omdat het kan gebeuren dat we de "wismodus" per ongeluk activeren en we het niet herinneren en plotseling

95
00:07:40,639 --> 00:07:45,099
de penselen niet meer schilderen en we verward kunnen raken. Dus

96
00:07:45,740 --> 00:07:48,820
Ga na dat de knop wismodus uit staat

97
00:07:48,949 --> 00:07:53,348
Ok! penselen staan standaard aan de rechterkant van het interface

98
00:07:53,690 --> 00:08:01,479
Maar u kunt wijzigen waar ze staan gewoon door de vastzetter te verslepen. Als u de vastzetter penselen sluit dan kunt u het gemakkelijk herstellen. Ga naar

99
00:08:01,759 --> 00:08:08,589
voorinstelling instellingen/vastzetter/penseel en Krita herinnert zijn laatste locatie van verschijnen.

100
00:08:10,400 --> 00:08:12,519
De penselen kunnen er erg gelijk uitzien

101
00:08:13,340 --> 00:08:17,440
Maar u zult er zelfs meer willen misschien na enige dagen of weken.

102
00:08:18,470 --> 00:08:25,209
Heb ik als vertelt dat in de winkel van Krita er een gehele set is om olie, pastel en waterkleuren te emuleren

103
00:08:27,500 --> 00:08:31,720
Krita weet dat veel penselen de gebruiker kan overweldigen.

104
00:08:32,180 --> 00:08:37,839
Dus heeft het een systeem met labels gemaakt die gebruikt kan worden om alleen enige penselen te zien

105
00:08:38,479 --> 00:08:40,479
Georiënteerd op een specifieke werkmethode

106
00:08:41,120 --> 00:08:45,039
Laten we eens zien hoe dat werkt. bijvoorbeeld, we hebben het "Schilderen"

107
00:08:45,709 --> 00:08:47,029
"Schets"

108
00:08:47,029 --> 00:08:54,789
"Textuur", enzovoort. Elk label heeft een paar penselen gerelateerd aan die taak. Bijvoorbeeld het label "Texturen"

109
00:08:54,980 --> 00:09:02,380
Heeft enige penselen om u te helpen snel effecten met textuur te maken. In de toekomst video's. We zullen meer zien over penselen.

110
00:09:03,440 --> 00:09:08,020
Een andere manier om naar penselen te kijken is filteren gebruiken, we kunnen dieper gaan

111
00:09:08,570 --> 00:09:11,770
penselen bekijken met de penselenbewerker, bijvoorbeeld,

112
00:09:12,230 --> 00:09:19,480
Om meer te leren over penseel-engines. We zullen dat laten zien in een volgende video. Maar misschien houdt u er niet van om daar penselen te hebben,

113
00:09:19,480 --> 00:09:23,980
altijd zichtbaar, alsof ze u bekijken. In dat geval kunt u de toets F6

114
00:09:24,350 --> 00:09:31,410
gebruiken of gaan naar het afrolvenster bovenaan dat het venster met al uw penselen zal openen

115
00:09:32,410 --> 00:09:37,439
Hier kunt u het labelsysteem gebruiken. U hebt nog een andere erg interessante optie.

116
00:09:38,140 --> 00:09:41,009
Soms gebruiken we niet meer dan 10 penselen

117
00:09:41,830 --> 00:09:46,770
Stel u bijvoorbeeld voor dat we in volledig scherm tekenen zonder een interface

118
00:09:47,050 --> 00:09:54,540
op de toets "Tab" drukken, daarna drukt u de rechter muisknop in of de knop van de stylus die u hebt geassocieerd met rechts klikken

119
00:09:55,450 --> 00:10:00,150
Dit biedt het zien van dingen zoals penselen, kleur, laatst gebruikte kleuren.

120
00:10:00,150 --> 00:10:06,720
En ook de afbeelding spiegelen, draaien en meer. Dit snelmenu is echt krachtig.

121
00:10:06,850 --> 00:10:12,119
We zullen dat ook in een andere video zien. U hebt een penseel gekozen. Om de grootte te wijzigen

122
00:10:12,360 --> 00:10:20,320
druk op de toets "Shift" en sleep de cursor naar rechts om de grootte te verhogen of naar links om de grootte te verkleinen

123
00:10:20,520 --> 00:10:26,220
We kunnen het ook doen via sneltoetsen. Wel, het is gemakkelijker

124
00:10:26,920 --> 00:10:33,020
via de schuifregelaar bovenaan het interface, wat een goed idee is. Iets wat u gaat opmerken,

125
00:10:33,030 --> 00:10:35,309
is dat als u een penseel kiest en

126
00:10:35,890 --> 00:10:37,810
de grootte wijzigt, bijvoorbeeld

127
00:10:37,810 --> 00:10:45,510
u maakt een paar streken en u zegt hmm dit ziet er erg goed uit een erg goed penseel en ik vind het prima zoals het is.

128
00:10:46,090 --> 00:10:50,639
maar als u een ander penseel kiest en u selecteert opnieuw het vorige penseel

129
00:10:51,400 --> 00:10:55,800
Dan zult u zien dat het penseel teruggekeerd is naar zijn vorige status

130
00:10:56,740 --> 00:10:59,820
In dit geval waren uw wijzigingen niet al te veel.

131
00:11:00,370 --> 00:11:07,650
Maar stel u voor dat u begon te spelen met de penseelparameters en aan het eind wist u niet meer wat u precies hebt hebt gewijzigd,

132
00:11:07,650 --> 00:11:12,869
maar het resulterende penseel is erg cool. In dat geval

133
00:11:12,880 --> 00:11:15,460
Dit kan echt vervelend zijn, niet waar?

134
00:11:15,580 --> 00:11:22,300
Dit is gedaan voor mensen die aan het gebruiken van standaard penselen, zoals ze zijn, de voorkeur geven en zonder ze teveel te wijzigen.

135
00:11:22,440 --> 00:11:29,240
Dit is in het begin praktisch omdat het u helpt om beter het verschil ertussen te begrijpen en

136
00:11:29,680 --> 00:11:35,310
de mogelijkheden die u kunt hebben met een enkel penseel en dat is de reden waarom Krita is gemaakt

137
00:11:35,560 --> 00:11:42,859
de "Vuile penselen". Penselen die uw wijzigingen kunnen onthouden zelfs als u penselen opnieuw en opnieuw wijzigt,

138
00:11:43,350 --> 00:11:45,890
Om deze functie te gebruiken. We moeten activeren

139
00:11:46,410 --> 00:11:53,480
"Tijdelijk tweaks opslaan naar voorinstellingen" in de penseelbewerker van Krita zal de instellingen bewaren totdat deze sluit.

140
00:11:53,640 --> 00:11:58,849
Okay, als u een gewijzigde versie wilt opslaan klik gewoon op knop overschrijven

141
00:11:59,040 --> 00:12:01,040
En dat is het

142
00:12:01,140 --> 00:12:04,969
U hebt uw eerste gewijzigde penseel gereed om te worden gebruikt

143
00:12:05,269 --> 00:12:11,479
Zelfs als u Krita sluit. Om in een comfortabele werkruimte te zijn moeten we lfs kennen over hoe de basis navigatie te doen

144
00:12:12,000 --> 00:12:17,779
Zoals zoomen, heen en weer draaien en ronddraaien en ook horizontaal spiegelen kan nuttig zijn

145
00:12:18,029 --> 00:12:18,890
in Krita

146
00:12:18,890 --> 00:12:23,059
we gebruiken standaard de toets "M" als een sneltoets om het beeld te spiegelen

147
00:12:23,370 --> 00:12:30,200
Dat is echt nuttig wanneer we gedurende een lange tijd schilderen. Een eenvoudige sneltoets, maar een krachtige functie. De kleur

148
00:12:31,110 --> 00:12:37,550
Wanneer we schilderen hebben we een plek nodig om kleuren te kiezen. Een plek waar u verschillende kleuren kunt kiezen.

149
00:12:37,920 --> 00:12:43,490
Bijvoorbeeld, hoe maak ik mijn kleur minder verzadigd of helderder? Bijvoorbeeld

150
00:12:44,190 --> 00:12:49,909
We kunnen "Geavanceerde kleurenkiezer" gebruiken of een aangepast "Palet". Voor nu

151
00:12:49,950 --> 00:12:56,390
Laten we dat zoals het standaard is omdat we kleur in komende video's zullen dekken.

152
00:12:57,240 --> 00:13:03,109
Opslaan van afbeeldingen en veilig werken. Een zeer aanbevolen praktijk is om vaal op te slaan

153
00:13:03,570 --> 00:13:08,539
Doe dat dus. Alle software crasht soms. Dus...

154
00:13:08,540 --> 00:13:12,740
Ik neem aan dat u getekend hebt en getest met de penselen die standaard meekwamen

155
00:13:12,899 --> 00:13:17,659
Maar wat gebeurt als we zo opgewonden zijn dat we zijn vergeten om op te slaan en

156
00:13:18,000 --> 00:13:21,859
plotseling gaat het licht uit? Jaren geleden was dat een groot probleem

157
00:13:21,930 --> 00:13:27,770
Het liet u alles opnieuw doen sinds de laatste keer dat u uw werk opsloeg

158
00:13:28,740 --> 00:13:35,990
Maar we zijn nu toch in een technologisch tijd? Dus dat is waarom Krita verder gaat en ons hoe helpt?

159
00:13:36,750 --> 00:13:40,039
Elke keer dat u een afbeelding opent en u werkt eraan

160
00:13:40,649 --> 00:13:42,889
Slaat Krita stilletjes uw werk op

161
00:13:43,260 --> 00:13:50,179
Het programma maakt een kopie van het bestand en zorgt ervoor het werk elke 15 minuten op te slaan. Is dat cool of niet?

162
00:13:51,059 --> 00:13:58,439
Ik denk dat het zo is. Als we die tijd willen wijzigen, moeten we naar het menu instellingen/configureren Krita/Algemeen/sectie en

163
00:13:59,319 --> 00:14:02,218
/Bestandsbehandeling en daar wijzigen we de tijd

164
00:14:02,889 --> 00:14:04,719
Laten we dat doen.

165
00:14:04,719 --> 00:14:11,489
Ik vind dat vijf minuten geen opdringerige tijd is voor opslaan in het geval dat om een vreemde reden

166
00:14:12,099 --> 00:14:18,959
U deze optie wilt deactiveren klik eenvoudig op het keuzerondje "Automatisch opslaan inschakelen"

167
00:14:19,389 --> 00:14:22,108
Ik zou het niet doen. Ik ga het niet doen.

168
00:14:22,109 --> 00:14:26,098
Dat kan ik verzekeren. Wat als ik verschillende stadia van mijn werk wil opslaan

169
00:14:26,529 --> 00:14:31,859
Dan gebruiken we de incrementele opslagoptie een coole naam voor een erg nuttige functie

170
00:14:32,319 --> 00:14:34,348
En waarom denk ik dat het zo nuttig is?

171
00:14:34,719 --> 00:14:38,369
Omdat, als u het opmerkte, er een sneltoets aan gekoppeld is

172
00:14:38,529 --> 00:14:44,669
wat "Ctrl+Alt+S" is en op die manier drukken we de sneltoets en we maken een

173
00:14:44,949 --> 00:14:51,478
Incrementele versie van het bestand waar we aan werken. Als u dit type hulpmiddelen prettig vindt die helpen bij productiviteit,

174
00:14:51,669 --> 00:14:56,759
laat het me weten in het commentaar en geef Krita een like. Ok! Dat is voor nu het einde!

175
00:14:56,759 --> 00:14:59,878
U hebt de basis maar er is veel veel meer.

176
00:15:00,189 --> 00:15:03,898
Vergeet niet om in te schrijven en deel deze video in uw sociale media

177
00:15:03,899 --> 00:15:09,719
En als u denkt dat dit uw vrienden en meer gebruikers van Krita kan helpen, deel het dan.

178
00:15:10,179 --> 00:15:15,119
Als u geïnteresseerd bent in een specifiek onderwerp of u wilt nieuwe onderwerpen suggereren

179
00:15:15,879 --> 00:15:21,028
Laat het me weten in het onderstaande commentaar en misschien zou dat de volgende video kunnen zijn

180
00:15:21,969 --> 00:15:28,169
Veel dankvoor het kijken en maak u klaar voor nieuwe video's waarin ik het gebruiken van Krita wil dekken

181
00:15:28,239 --> 00:15:30,239
met meer geavanceerde functies

182
00:15:30,789 --> 00:15:32,789
Tot ziens
