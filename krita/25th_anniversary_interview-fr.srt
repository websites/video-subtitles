﻿1
00:00:01,120 --> 00:00:04,520
Bonjour à vous ! Bienvenu dans cette nouvelle vidéo !

2
00:00:04,520 --> 00:00:08,200
Nous célébrons le 25 ième anniversaire de Krita

3
00:00:08,200 --> 00:00:13,960
et aujourd'hui ! Ici ! Nous avons un invité très, très spécial.

4
00:00:14,000 --> 00:00:16,600
Le mainteneur de Krita... Halla Rempt

5
00:00:16,600 --> 00:00:21,600
Et Halla a été le mainteneur durant plus de 20 ans !

6
00:00:21,600 --> 00:00:22,960
C'est extraordinaire !

7
00:00:22,960 --> 00:00:25,880
Une journée très spéciale, profitez-en !

8
00:00:25,880 --> 00:00:28,120
Salut Halla, comment ça va ?

9
00:00:28,400 --> 00:00:30,200
Bonjour Ramon, ravi de vous voir !

10
00:00:30,440 --> 00:00:34,960
La célébration de ses 25 ans est une étape importante.

11
00:00:34,960 --> 00:00:39,200
Qu'est-ce que la communauté « Open Source » signifiait pour le développement de Krita ?

12
00:00:39,800 --> 00:00:45,120
Qu'est-ce que la communauté « Open source » a signifié vis-à-vis du développement de Krita au cours des 25 dernières années ? Eh bien, je dirais

13
00:00:45,120 --> 00:00:50,520
c'est absolument essentiel et cela pour deux raisons. Tout d'abord, bien sûr, être un logiciel « Open source »

14
00:00:50,520 --> 00:00:55,680
ont fait appel aux contributions de centaines de personnes. Il y en a plus de 600

15
00:00:55,680 --> 00:01:00,440
les personnes de la liste d'engagement de Krita et ce n'est que la partie développement. Hormis les sites Internet,

16
00:01:00,440 --> 00:01:05,120
pas la documentation, pas tout le reste et c'est génial ! Chaque fois que nous étions coincés, quelque part,

17
00:01:05,120 --> 00:01:10,720
les personnes pouvaient nous aider ! Et beaucoup de personnes l'ont fait. Enfin, peut-être encore plus important, maintenant que

18
00:01:10,720 --> 00:01:17,200
Krita est un logiciel libre « Open source » sous licence « GPL », le code source ne peut que resté ouvert. Lorsque sur le

19
00:01:17,200 --> 00:01:22,800
années, nous avons vu des concurrents disparaître parce qu'ils ont été rachetés par des entreprises comme Adobe,

20
00:01:22,800 --> 00:01:27,440
nous savions juste que nous pouvions avoir confiance dans le fait que Krita continuerait tant qu'il y aurait

21
00:01:27,440 --> 00:01:32,680
personnes contributrices. Donc faire partie de la communauté « Open source » est totalement fondamental. Et cela ne changera jamais !

22
00:01:32,760 --> 00:01:35,240
Waouh ! Ceci est une information très intéressante.

23
00:01:35,240 --> 00:01:40,240
Krita a été au cours de ces 25 années,

24
00:01:40,240 --> 00:01:42,200
ce qui est un voyage extraordinaire.

25
00:01:42,200 --> 00:01:46,160
Comment Krita a évolué au cours de ces 25 années ?

26
00:01:46,200 --> 00:01:50,240
25 ans, c'est un assez long voyage... Je n'ai pas été là durant tout ce voyage.

27
00:01:50,240 --> 00:01:54,840
Peut-être seulement 21 ans, ce qui est encore long. Quand je regarde en arrière vers

28
00:01:54,840 --> 00:01:58,960
à quoi ressemblait Krita quand j'ai rejoint le projet,. Tu ne pouvais même pas dessiner.

29
00:01:58,960 --> 00:02:02,720
Vous pouviez charger des images, ajouter des calques et déplacer des calques tout autour. Et, c'était tout.

30
00:02:02,720 --> 00:02:08,320
Donc, nous avons commencé à construire là-dessus. Nous avons ré-écrit la totalité du noyau de Krita. Le noyau a été ré-écrit 4 fois.

31
00:02:08,320 --> 00:02:15,680
et chaque fois que Krita est devenu plus performant, nous avons commencé à ajouter des outils, nous avons commencé à ajouter des phases

32
00:02:15,680 --> 00:02:23,640
comme Krita a pris en charge un canevas reposant sur OpenGL, le processeur graphique « GPU » a pu l'accélérer avant que cela soit le cas avec Photoshop. CMJN depuis longtemps. Nous

33
00:02:23,640 --> 00:02:28,040
a commencé à mettre en œuvre de plus en plus de trucs amusants, de plus en plus de trucs utiles et puis est venu le

34
00:02:28,040 --> 00:02:33,800
grande pause. Nous avons commencé à écouter nos personnes utilisatrices grâce à ces sessions. Des sprints de développement,

35
00:02:33,800 --> 00:02:38,960
où les personnes de l'équipe de développement et contributrices de Krita se réunissent, la plupart du temps nous travaillons ensemble pour discuter

36
00:02:38,960 --> 00:02:44,000
ce que nous allons faire au cours de la prochaine année
depuis notre sprint avec les studios de Blender et

37
00:02:44,000 --> 00:02:49,560
puis nous avons demandé aux artistes de faire une « démonstration » en travaillant avec 
Krita. Nous venons de leur demander et... voici une heure

38
00:02:49,560 --> 00:02:55,000
pour vous et nous allons vous regarder peindre, espérons que
cela ne te dérange pas. Tu peux dire tout ce que tu veux,

39
00:02:55,000 --> 00:03:00,320
se plaindre de tout ce dont vous avez à vous plaindre 
Tout passe. Et nous, l'équipe de développement, ne sommes pas

40
00:03:00,320 --> 00:03:03,960
autorisé à répondre. Nous ne sommes pas non plus autorisés à vous aider, car nous voulons voir ce que

41
00:03:03,960 --> 00:03:11,080
les problèmes. Et ce genre d'observation des personnes utilisatrices nous a vraiment aidé à améliorer la créativité à pas de géant.

42
00:03:11,080 --> 00:03:18,000
Quelle a été l'utilisation la plus créative de Krita qui vous a fait réfléchir...

43
00:03:18,000 --> 00:03:20,680
Ouah. Je n'y avais jamais pensé !

44
00:03:20,680 --> 00:03:27,120
l'utilisation de Krita la plus extravagante ? Je suppose que c'est la personne qui voulait utiliser une partie de Krita.

45
00:03:27,120 --> 00:03:34,120
capacités de profondeur pour la création de cartes stellaires. Utilisation d'énormes photographies astronomiques et

46
00:03:34,120 --> 00:03:43,680
voulait travailler avec eux et honnêtement, j'aurais voulu (Eh bien, j'ai dit ça en fait), avoir des contacts avec des personnes très importantes pour ça. Mais de tels contacts étaient assez courants.

47
00:03:43,880 --> 00:03:49,440
De nombreux utilisateurs pourraient se demander ce qui motive l'équipe constituant Krita ?

48
00:03:50,440 --> 00:03:56,520
Qu'est-ce qui nous motive ? Je suppose une raison égoïste et désintéressée. La raison égoïste est

49
00:03:56,520 --> 00:04:02,320
c'est le travail le plus amusant que j'ai jamais fait de ma vie. J'ai travaillé pour toutes sortes d'entreprises au moins

50
00:04:02,320 --> 00:04:09,000
six ou sept et dans une seule de ces entreprises, nous avons effectivement publié le logiciel que je développais,

51
00:04:09,000 --> 00:04:15,600
certaines entreprises développent juste des logiciels pour dépenser de l'argent. C'est fou mais Krita s'en sort

52
00:04:15,600 --> 00:04:21,920
la main des personnes utilisatrices. C'est là que la partie désintéressée intervient, parce que ces personnes nous disent le

53
00:04:21,920 --> 00:04:29,200
les choses les plus douces, comme cette fois, où j'ai reçu un courriel d'une personne de Malaisie. Ses parents lui ont dit à elle

54
00:04:29,200 --> 00:04:33,840
que « les filles n'ont pas besoin de dessiner. Donc non, nous ne vous obtenons aucun logiciel de dessin » son frère

55
00:04:33,840 --> 00:04:39,520
lui a donné une tablette de dessin, alors elle a découvert 
Krita et elle pouvaient commencer à faire de l'art et elle

56
00:04:39,520 --> 00:04:46,120
m'a envoyé un message avec des déclarations d'extase totale : « Je peux faire
des œuvres d'art, merci, merci, merci » et c'est juste

57
00:04:46,120 --> 00:04:51,800
si motivant, donnez-nous un sentiment incroyable. Donc, si vous aimez Krita, pensez à nous dire que vous l'aimez

58
00:04:51,800 --> 00:04:58,320
parce que normalement, nous ne voyons que des rapports de bogues. Ces derniers sont utiles. Si vous faites des rapports de bogues, encore merci !

59
00:04:58,320 --> 00:05:03,080
Mais parfois, ils peuvent être un peu démotivants parce qu'ils sont si nombreux.

60
00:05:03,200 --> 00:05:09,400
Pouvez-vous nous en dire plus sur l'utilisation commerciale de Krita ?

61
00:05:09,400 --> 00:05:16,240
Utilisation commerciale de Krita. C'est bien sûr tout à fait bien. C'est ce que nous disons explicitement dans tous nos

62
00:05:16,240 --> 00:05:22,280
Listes de questions fréquemment posées (FAQ) dans la boîte à propos de partout. Mais commercial

63
00:05:22,280 --> 00:05:29,160
l'utilisation se produit réellement. La première fois que j'en ai entendu parler, c'était lorsque nous présentions Krita à la conférence « SIGGRAPH » à Vancouver.

64
00:05:29,160 --> 00:05:35,694
Nous avions un stand à côté de Blender grâce à l'aide, vraiment utile, de Ton Roosendaal. Nous étions comme...

65
00:05:35,694 --> 00:05:41,600
il y a tellement, tellement d'intérêt, tellement de gens veulent nous parler et puis ce gars est venu

66
00:05:41,600 --> 00:05:49,680
jusqu'à moi et il a dit de ne le dire à personne d'autre au moins pour l'instant, mais -psst- nous avons utilisé Krita

67
00:05:49,680 --> 00:05:55,200
pour l'introduction de « Little, Big », qui était un film d'animation, je pense, fait par Disney,

68
00:05:55,200 --> 00:06:01,560
Je ne suis plus sûr. Donc, c'était la première fois que j'entendais parler de l'utilisation commerciale de Krita. Cela était en 2014, je pense.

69
00:06:01,560 --> 00:06:06,480
Depuis, nous obtenons des parrainages. Nous sommes en contact avec des personnes

70
00:06:06,480 --> 00:06:14,360
utiliser Krita commercialement pour des jeux, des films, des livres. C'est tout simplement très stimulant.

71
00:06:14,360 --> 00:06:23,240
Ok. Donc, nous voulons aider Krita, comment Krita est-il financé, à quel point est-il durable dans l'avenir ?

72
00:06:23,240 --> 00:06:28,160
Nous en avons déjà parlé un peu. Fondamentalement, il y a deux entités. D'une part, il y a le

73
00:06:28,160 --> 00:06:36,600
fondation Krita à but non lucratif. D'autre part, il y a le logiciel à but lucratif de Halla Rempt, c'est moi.

74
00:06:36,600 --> 00:06:44,000
et nous avons réparti les activités entre les deux. La fondation sollicite des dons ce qui signifie que cela concerne l'impôt

75
00:06:44,000 --> 00:06:50,400
exempté, le logiciel de Halla Rempt vend des DVD et d'autres choses. Mais bien sûr, ce ne génère presque plus de revenus

76
00:06:50,400 --> 00:06:58,080
ces jours-ci parce que nous avons ces excellents tutoriels sur YouTube❤. Krita se vend grâce à diverses boutiques d'applications.

77
00:06:58,080 --> 00:07:06,400
Les deux flux de revenus combinés, les dons ponctuels au fonds de développement et les revenus des boutiques sont suffisants pour parrainer

78
00:07:06,400 --> 00:07:13,000
les personnes actuellement parrainées de notre équipe de développement et les vidéos de Ramon. C'est donc assez durable à

79
00:07:13,000 --> 00:07:21,160
au moins nous le faisons depuis 2015 avec 
des boutiques ajoutées en 2017. Nous faisons donc du bon travail.

80
00:07:21,160 --> 00:07:28,520
Comment la communauté a-t-elle aidé Krita et comment Krita a-t-il aidé par sa communauté ?

81
00:07:28,520 --> 00:07:33,360
Il y a bien longtemps ! Quand Krita n'était qu'un passe-temps pour toutes les personnes qui étaient impliquées,

82
00:07:33,360 --> 00:07:39,120
la plupart d'entre nous n'avaient pas accès aux tablettes de dessin numériques. J'ai commencé à travailler avec Krita parce que j'ai

83
00:07:39,120 --> 00:07:47,362
un pour mon anniversaire. Mais, il a commencé à mal fonctionner. Alors, nous avons fait une collecte de fonds pour obtenir deux tablettes Wacom et un stylet graphique !

84
00:07:47,362 --> 00:07:54,280
c'est donc à ce moment-là que nous avons remarqué que, en particulier dans la communauté des logiciels libres, il n'y avait même aucun artiste

85
00:07:54,280 --> 00:08:03,560
à l'époque, étaient vraiment favorables, et un peu plus tard, c'était Lukas, nous avions ce Slovaque

86
00:08:03,560 --> 00:08:09,800
étudiant en code, qui est devenu un contributeur régulier. Il a même fait sa thèse sur Krita

87
00:08:09,800 --> 00:08:15,400
moteurs de brosse. Il a obtenu 10 points sur 10 pour cela. Parce que c'était un travail brillant et ses professeurs

88
00:08:15,400 --> 00:08:21,520
étaient tellement étonnés ! Que leur étudiant a fait des choses qui ont été utilisées dans le monde réel !

89
00:08:21,520 --> 00:08:28,320
En même temps, nous étions enthousiastes car nous portions Krita de Qt3 à Qt4 et nous ré-écrivions tout

90
00:08:28,320 --> 00:08:34,720
rien ne marche, tout est trop « bogué » pour les mots c'est vraiment horrible ! A ce moment-là, Lukas a dit :

91
00:08:34,720 --> 00:08:40,960
« Mes trois derniers mois à l'université étaient censés être un stage. Je dois faire des choses pertinentes,

92
00:08:40,960 --> 00:08:47,160
utile, je préférerais être payé un petit peu » 
Et parce que Krita à cette époque était si incroyablement

93
00:08:47,160 --> 00:08:55,720
plein de bogues. Nous nous battions toujours en tant que bénévoles en tant que codeurs amateurs pour mettre Krita dans un état fonctionnel,

94
00:08:55,720 --> 00:09:04,480
financer Lukas pour corriger les bogues pendant 3 mois. Ce fut un ÉNORME succès ! Au départ, Lukas voulait principalement

95
00:09:04,480 --> 00:09:12,840
de l'argent pour payer son loyer mais nous pourrions alors le payer. Pour nous, une somme d'argent correcte chaque mois,

96
00:09:12,840 --> 00:09:20,400
avec le résultat de la collecte de fonds, tant d'artistes, tant de personnes enthousiastes, ont soutenu Krita.

97
00:09:20,400 --> 00:09:27,520
Les trois mois sont devenus six mois, puis un peu plus tard, 
Dmitry Kazakov a fait un travail brillant comme jamais

98
00:09:27,520 --> 00:09:34,160
depuis sa première conférence « GSOC » avec Krita. Il allait finir l'université et je ne voulais pas qu'il

99
00:09:34,160 --> 00:09:39,960
prendre un autre emploi et le perdre pour le projet. Nous avons donc commencé à relancer régulièrement des collectes de fonds

100
00:09:39,960 --> 00:09:46,400
ce qui nous a valu beaucoup de publicité et était épuisant. Mais, amusant à faire. Eh bien, Dmitry est toujours

101
00:09:46,400 --> 00:09:49,880
à temps plein travaillant sur Krita, en tant que membre parrainé de l'équipe de développement, 
donc ça a marché.

102
00:09:50,000 --> 00:09:57,360
Vous souvenez-vous d'une époque où la communauté de Krita était venue aider au développement de Krita ?

103
00:09:57,600 --> 00:10:01,600
Le pire moment a été lorsque le bureau des impôts néerlandais...

104
00:10:01,600 --> 00:10:07,040
a remarqué que... sautons les trucs compliqués, ça me donne mal à la tête, de toute façon

105
00:10:07,040 --> 00:10:10,720
ils voulaient avoir 24 000 € € en une seule fois provenant de

106
00:10:10,720 --> 00:10:14,800
La fondation Krita, de l'argent que nous n'avions pas quand nous avons dit

107
00:10:14,800 --> 00:10:21,000
monde à ce sujet. Le monde a couru pour nous soutenir ! 
Ce qui était génial, nous avons en fait de

108
00:10:21,000 --> 00:10:27,320
ce moment, que nous n'avons jamais plus regardé en arrière. Nous avons créé une communauté d'aide et nous avons un développement

109
00:10:27,320 --> 00:10:35,280
fonds, que je vous incite fortement à rejoindre, désolé pour la prise. Nous vendons Krita dans diverses boutiques d'applications.

110
00:10:35,280 --> 00:10:43,480
sont un enfer absolu sur lequel travailler. Mais, il paie les nôtres mensuellement. Merci à la communauté de nous aider !

111
00:10:43,480 --> 00:10:50,840
Comment Krita peut-il  aider les personnes utilisatrices, dans l'apprentissage du logiciel ou même dans le codage du logiciel ?

112
00:10:51,280 --> 00:10:56,520
Nous essayons de rendre Krita accessible ou même facilement accessible pour les personnes nouvelles,

113
00:10:56,520 --> 00:11:03,640
mais en premier lieu, bien sûr, nous essayons simplement de maintenir 
les standards existants d'interface utilisateur. Nous n'essayons pas de faire

114
00:11:03,640 --> 00:11:09,488
nouvelles choses bizarres. Nous voulons juste que les personnes puissent trouver toute chose à l'emplacement auquel ils sont habitués.

115
00:11:09,488 --> 00:11:15,920
Si vous avez utilisé un ordinateur au cours des dernières 5, 10, 20 ou 30 années, rien en Krita ne sera vraiment

116
00:11:15,920 --> 00:11:22,600
vous surprendre qui rend Krita en soi utilisable. Bien sûr, nous travaillons également avec nos

117
00:11:22,600 --> 00:11:29,840
l'expert en interface utilisateur (UI), Scott Petrovic. Il nous aide 
à concevoir de nouvelles fonctionnalités. Maintenant, si vous voulez en savoir plus

118
00:11:29,840 --> 00:11:36,560
niveau de l'application elle-même, alors la documentation est vraiment importante. Nous passons pas mal de temps

119
00:11:36,560 --> 00:11:44,440
et d'effort sur notre site de documentation : le site « docs.krita.org » 
ne répertorie pas seulement toutes les options de menu. Il donne également

120
00:11:44,440 --> 00:11:51,600
vos tutoriels sur les choses concernant les flux de travail. Des choses qui pourraient ne pas être apparentes si vous êtes un personne de bureau de

121
00:11:51,600 --> 00:11:57,280
applications de peinture numérique car soyons clairs sur ce point, la peinture numérique est un outil

122
00:11:57,840 --> 00:12:05,720
séparé de tout le reste. Je veux dire oui. Je peins moi-même en analogique

123
00:12:05,720 --> 00:12:12,240
en utilisant des peintures à l'huile. Je dessine un croquis et une sculpture et,puis, quand j'essaie de dessiner en utilisant Krita, je sais que

124
00:12:12,240 --> 00:12:18,600
que la plupart des habitudes apprises ne sont pas transférées vers une application de peinture numérique. Nous avons donc

125
00:12:18,600 --> 00:12:24,920
un site Internet complet avec des tutoriels, des directives, 
des conseils. Nous avons aussi une grande communauté et merci

126
00:12:24,960 --> 00:12:34,202
à Raghavendra Kamath (raghukamath), nous avons un forum Internet pour personnes utilisatrices nommé : krita-artists.org, tout comme le forum des artistes avec Blender

127
00:12:34,202 --> 00:12:38,120
cela repose totalement là-dessus. C'est un thème récurrent. Chaque fois que je pense que nous avons besoin

128
00:12:38,120 --> 00:12:43,320
quelque chose de nouveau. Je regarde d'abord ce que Ton fait pour Blender, puis je le copie et je ne suis pas

129
00:12:43,320 --> 00:12:49,040
honteux de l'admettre. Notre forum d'artistes Krita est une énorme ressource où les personnes peuvent poser des questions

130
00:12:49,040 --> 00:12:54,640
mais aussi trouver des réponses aux questions ayant été posées auparavant. Enfin, nous investissons beaucoup de

131
00:12:54,640 --> 00:13:01,280
temps et argent, dans la série de vidéos de Ramón pour notre chaîne Krita. Alors, merci à Ramón d'avoir travaillé avec nous !

132
00:13:01,360 --> 00:13:11,160
Dans quelle mesure Krita est-il personnalisable et quel type de modules externes ou d'extensions les personnes utilisatrices peuvent-ils créer ou utiliser ?

133
00:13:11,840 --> 00:13:18,760
Lorsque Krita était vraiment jeune, Cyrille Berger a créé la première interface de scripts pour Krita

134
00:13:18,760 --> 00:13:24,960
être possible d'écrire des scripts en JavaScript, Ruby et Python. Attendez ! ce n'est pas vraiment l'écriture de votre premier script

135
00:13:24,960 --> 00:13:30,120
interface. Interface avec JavaScript de KDE. Je veux dire que
vous savez que, presque tous les navigateurs, sauf pour

136
00:13:30,120 --> 00:13:39,640
Firefox, de nos jours, repose sur le travail de KDE sur le langage « KHTML », un navigateur Internet reposant sur KDE. Cela est venu bien sûr avec le moteur JavaScript.

137
00:13:39,640 --> 00:13:47,720
Et ce moteur en JavaScript était un truc séparé que nous pouvions utiliser pour rendre Krita utilisable avec des scripts,

138
00:13:47,720 --> 00:13:52,560
donc c'était la toute première chose. Donc, nous avions d'abord commencé avec JavaScript puis nous l'avons remplacé

139
00:13:52,560 --> 00:14:02,720
avec Kross, qui est la chose qui a rendu 
Ruby, Python, et d'autres disponibles. Ensuite, nous avons ajouté Open GTL

140
00:14:02,720 --> 00:14:09,360
presque oublié ces jours-ci, un clone parfait de quelque chose utilisé à Hollywood a ré-implémenté cela ou

141
00:14:09,360 --> 00:14:15,920
plutôt Cyrille Berger l'a ré-implémenté. Il pourrait être utilisé 
pour beaucoup de choses, y compris la création d'espaces colorimétriques entièrement nouveaux.

142
00:14:15,920 --> 00:14:22,920
C'était vraiment agréable. Mais, Cyrille s'est marié et a quitté le projet. 
C'était fondamentalement impossible à maintenir. Alors, nous avons laissé tomber cela.

143
00:14:22,920 --> 00:14:29,400
Dans le même temps, nous avons porté Krita sur une nouvelle version des environnements de développement de KDE. Kross a cessé de fonctionner correctement

144
00:14:29,400 --> 00:14:35,760
nous avons donc abandonné la prise en charge de cela. Heureusement, à l'époque, Krita avait très peu de personnes utilisatrices. Donc, personne n'était vraiment impacté.

145
00:14:35,760 --> 00:14:43,920
Ensuite, nous avons porté Krita sur une autre version de Qt et KDE, ce qui représentait beaucoup de travail

146
00:14:43,920 --> 00:14:51,440
Un été sur le toit-terrasse quand il faisait très chaud, 
j'ai pris le code de scripts en Python pour le texte de KDE

147
00:14:51,440 --> 00:14:58,280
l'éditeur Kate et a copié tout cela et l'a corrigé pour rendre Krita utilisable avec des scripts. C'est le fondement de notre

148
00:14:58,280 --> 00:15:05,760
API actuelle pour des scripts en Python, qui est un peu limitée. Mais, elle se développe et elle est toujours là.

149
00:15:05,760 --> 00:15:11,920
Vous pouvez y faire tout ce que vous voulez en termes d'interface utilisateur et de scripts. Et puis, bien sûr,

150
00:15:11,920 --> 00:15:19,200
nous avons eu cet énorme projet « Google Summer Of Code » (GSOC) où nous 
a mis en place une prise en charge de SeExpr de Disney.

151
00:15:19,200 --> 00:15:25,699
Cela permet vraiment d'écrire des scripts, de générer des choses qui rempliront vos... vos calques avec tout ce que vous voulez

152
00:15:25,699 --> 00:15:31,240
Nous ne savons pas à quel point il est  utilisé. Mais, il est extrêmement puissant. Alors oui ! Krita est très

153
00:15:31,240 --> 00:15:37,120
personnalisable et même en dehors de cela, la plus grande partie de Krita est constituée de modules externes. Chaque outil est un module externe. si vous connaissez un

154
00:15:37,120 --> 00:15:43,560
un peu de code C++. Je veux dire que je ne connaissais pas le langage C++ quand j'ai commencé à bidouiller Krita et je me suis bien débrouillé avec des échecs

155
00:15:43,560 --> 00:15:50,560
pour faire quelque chose d'utile qui, à son tour, a attiré d'autres personnes contributrices. Krita a décollé alors commençons à bidouiller !

156
00:15:50,880 --> 00:15:56,560
Pour les derniers mots, merci beaucoup d'être disponible pour cet entretien,

157
00:15:56,560 --> 00:16:02,240
Je sais que vous être vraiment beaucoup occupé. Donc, j'apprécie beaucoup.

158
00:16:02,240 --> 00:16:05,440
Quel message voulez-vous envoyer aux personnes utilisatrices de Krita ?

159
00:16:05,440 --> 00:16:09,000
Merci Ramón pour cet entretien. Un dernier message pour vous qui utilisez Krita.

160
00:16:09,000 --> 00:16:13,520
Utilisez Krita encore plus ! Faites de l'art, faites de l'art, faites de l'art ! J'adore voir tout ce qui est artistique réalisé avec Krita.

161
00:16:13,520 --> 00:16:18,116
Tout ce que vous faites avec Krita me convient. Au revoir !

162
00:16:18,116 --> 00:16:24,384
Célébrez avec nous le 25 ième anniversaire du développement de Krita

163
00:16:24,384 --> 00:16:32,312
Tout commentaire est le bienvenu. Ramon ici comme d'habitude, disant au revoir, passe une bonne journée... ou une bonne nuit. Merci beaucoup d'avoir regardé. ❤
