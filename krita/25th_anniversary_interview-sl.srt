﻿1
00:00:01,120 --> 00:00:04,520
Pozdrav vsem in dobrodošli v tem novem video posnetku!

2
00:00:04,520 --> 00:00:08,200
Praznujemo 25. obletnico Krite

3
00:00:08,200 --> 00:00:13,960
Danes je z nami zelo, zelo poseben gost.

4
00:00:14,000 --> 00:00:16,600
Vzdrževalec Krite ... Halla Rempt.

5
00:00:16,600 --> 00:00:21,600
Halla je vzdrževalec že več kot 20 let!

6
00:00:21,600 --> 00:00:22,960
To je neverjetno!

7
00:00:22,960 --> 00:00:25,880
Prav poseben dan, uživajte v njem!

8
00:00:25,880 --> 00:00:28,120
Pozdravljeni, Halla, kako ste?

9
00:00:28,400 --> 00:00:30,200
Zdravo, Ramon, lepo te je videti!

10
00:00:30,440 --> 00:00:34,960
Praznovanje 25. obletnice je pomemben mejnik.

11
00:00:34,960 --> 00:00:39,200
Kaj je odprta-koda pomenila za razvoj Krite?

12
00:00:39,800 --> 00:00:45,120
Kaj je odprta-koda pomenila za razvoj Krite v zadnjih 25 letih? No, rekel bi,

13
00:00:45,120 --> 00:00:50,520
da je to bistveno - in sicer iz dveh razlogov. Prvič, odprto-kodnost

14
00:00:50,520 --> 00:00:55,680
je črpala iz prispevkov več sto ljudi. Obstaja več kot 600

15
00:00:55,680 --> 00:01:00,440
oseb na Kritinem seznamu prispevkov in tu gre samo za razvoj. Ni spletnih strani,

16
00:01:00,440 --> 00:01:05,120
dokumentacija in vsega ostalega - in to je super! Kadarkoli smo obtičali,

17
00:01:05,120 --> 00:01:10,720
so nam ljudje lahko pomagali! In veliko ljudi je to storilo. Nenazadnje, morda še pomembneje, zdaj,

18
00:01:10,720 --> 00:01:17,200
ko je Krita odprto-kodno prosto programje pod GPL, ga nikoli ni mogoče zapreti. Ko smo skozi

19
00:01:17,200 --> 00:01:22,800
leta videli izginjati konkurente, ker so jih odkupila podjetja, kot je Adobe,

20
00:01:22,800 --> 00:01:27,440
smo vedeli, da lahko zaupamo, da bo Krita živela naprej, dokler bo

21
00:01:27,440 --> 00:01:32,680
imela avtorje prispevkov. Torej biti odprto-koden je popolnoma temeljno, to se ne bo nikoli spremenilo!

22
00:01:32,760 --> 00:01:35,240
To je zelo zanimiva informacija.

23
00:01:35,240 --> 00:01:40,240
Krita je bila v teh 25 letih prisotna,

24
00:01:40,240 --> 00:01:42,200
kar je neverjetno potovanje.

25
00:01:42,200 --> 00:01:46,160
Kako se je Krita razvila v teh 25 letih?

26
00:01:46,200 --> 00:01:50,240
25 let, to je precej dolgo potovanje ... Nisem bil zraven ves čas.

27
00:01:50,240 --> 00:01:54,840
Morda le 21 let, kar je še vedno veliko. Ko se ozrem nazaj,

28
00:01:54,840 --> 00:01:58,960
kaj je bila Krita, ko sem se pridružil projektu - z njo nisi mogel niti risati.

29
00:01:58,960 --> 00:02:02,720
Lahko si naložil slike, dodal in premikal plasti naokoli - in to je bilo to.

30
00:02:02,720 --> 00:02:08,320
Zato smo začeli graditi na tem. Na novo smo napisali celotno jedro Krite. Jedro smo prepisali 4-krat

31
00:02:08,320 --> 00:02:15,680
in vsakič, ko je Krita postala uspešnejša, smo začeli dodajati orodja, začeli smo dodajati faze,

32
00:02:15,680 --> 00:02:23,640
da je Krita podpirala platno openGL, pospeševanje z GPE še pred Photoshopom, dolga leta CMYK.

33
00:02:23,640 --> 00:02:28,040
Začeli smo uvajati vedno več zabavnih stvari, vedno več uporabnih stvari in nato je prišel

34
00:02:28,040 --> 00:02:33,800
velik prelom. Začeli smo poslušati naše uporabnike, imeli smo te seanse. razvojne šprinte,

35
00:02:33,800 --> 00:02:38,960
kjer se srečujejo Kritini razvijalci in sodelavci. Večinoma sodelujemo pri razpravi,

36
00:02:38,960 --> 00:02:44,000
kaj bomo storili v naslednjem letu,
vse od našega sprinta v Blender Studios naprej ...

37
00:02:44,000 --> 00:02:49,560
Potem prosimo ustvarjalce, na uporabijo »demo«
tega o Kriti, o čemer smo pravkar govorili ...

38
00:02:49,560 --> 00:02:55,000
Eno uro vas bomo gledali slikati, upamo, 
da vas ne moti, lahko rečete, kar želite.

39
00:02:55,000 --> 00:03:00,320
Pritožite se nad vsem, nad čemer se želite pritožiti. 
Vse je dovoljeno in mi, razvijalci, ne smemo

40
00:03:00,320 --> 00:03:03,960
odgovoriti nazaj. Tudi pomagati jim ne smemo, ker želimo videti kaj so težave.

41
00:03:03,960 --> 00:03:11,080
In takšno opazovanje uporabnikov nam je resnično pomagalo izboljšati Krito v velikih korakih.

42
00:03:11,080 --> 00:03:18,000
Kaj je bila najbolj ustvarjalna uporaba Krite, zaradi katere ste pomislili...

43
00:03:18,000 --> 00:03:20,680
Ojoj! Nikoli nisem razmišljal o tem!

44
00:03:20,680 --> 00:03:27,120
Najbolj izjemna uporaba Krite? Mislim, da je to oseba, ki je želela uporabiti Kritino visoko

45
00:03:27,120 --> 00:03:34,120
bitno globino za izdelavo zvezdnih zemljevidov. Uporabili so ogromne astronomske fotografije

46
00:03:34,120 --> 00:03:43,680
in želeli delati z njimi in, iskreno - (no, to sem dejansko rekel), za to uporabili VIPS. Vendar je bilo precej posebno.

47
00:03:43,880 --> 00:03:49,440
Mnogi uporabniki se morda sprašujejo, kaj motivira ekipo, ki stoji za Krito?

48
00:03:50,440 --> 00:03:56,520
Kaj nas motivira? Mislim, da obstaja sebičen in nesebičen razlog. Sebični razlog je,

49
00:03:56,520 --> 00:04:02,320
da gre za najbolj zabavno delo, kar sem jih kdaj opravljal v življenju. Delal sem za vse vrste podjetij,

50
00:04:02,320 --> 00:04:09,000
za šest ali sedem, in samo v enem od teh podjetij smo dejansko izdali programsko opremo, katero sem razvijal,

51
00:04:09,000 --> 00:04:15,600
nekatera podjetja samo razvijajo programsko opremo za porabo denarja, noro.

52
00:04:15,600 --> 00:04:21,920
Krita pa konča v rokah uporabnikov in tu nastopi nesebični del, ker nam ti uporabniki povedo

53
00:04:21,920 --> 00:04:29,200
najslajše stvari, kot tistikrat, ko sem dobil e-pošto od nekoga iz Malezije. Starši so ji rekli,

54
00:04:29,200 --> 00:04:33,840
da »dekletom ni treba risati, zato ne, ne bomo ti kupili programa za risanje«, njen brat

55
00:04:33,840 --> 00:04:39,520
dala ji je risalno tablico, zato je odkrila 
Krito in lahko je začela ustvarjati.

56
00:04:39,520 --> 00:04:46,120
Pisala mi je v popolni ekstazi: »Lahko slikam. 
Hvala, hvala, hvala!« In to je tako

57
00:04:46,120 --> 00:04:51,800
visoka motivacija, ki nam daje neverjeten občutek, zato, če vam je všeč Krita, nam to povejte.

58
00:04:51,800 --> 00:04:58,320
Ker običajno vidimo samo poročila o hroščih. Ki so koristna, zato - če pripravljate poročila o napakah - hvala!

59
00:04:58,320 --> 00:05:03,080
Ampak včasih so lahko malo demotivirajoča, ker jih je toliko.

60
00:05:03,200 --> 00:05:09,400
Nam lahko poveste več o komercialni uporabi Krite?

61
00:05:09,400 --> 00:05:16,240
Seveda je to popolnoma v redu. To izrecno povemo v vseh naših

62
00:05:16,240 --> 00:05:22,280
Pogosto zastavljenih vprašanjih (FAQ) v polju O programu povsod. Toda komercialna

63
00:05:22,280 --> 00:05:29,160
uporaba se dejansko dogaja. Zanjo sem prvič slišal, ko smo predstavili Krito na SIGGRAPHU v Vancouvru.

64
00:05:29,160 --> 00:05:35,694
Zahvaljujoč pomoči Tona Roosendaala smo imeli stojnico poleg Blenderja ...

65
00:05:35,694 --> 00:05:41,600
Toliko zanimanja je bilo, toliko ljudi se je želelo pogovoriti z nami in potem je prišel ta tip

66
00:05:41,600 --> 00:05:49,680
do mene in rekel, da vsaj za zdaj ne povem nikomur drugemu, ampak - psst - uporabljamo Krito

67
00:05:49,680 --> 00:05:55,200
za uvod v »Little, Big«, animirani film - Disneyja, se mi zdi.

68
00:05:55,200 --> 00:06:01,560
Nisem več prepričan, tako da sem prvič slišal za komercialno uporabo Krite, in to je bilo leta 2014.

69
00:06:01,560 --> 00:06:06,480
Odtlej pridobivamo sponzorstva, smo v stiku z ljudmi,

70
00:06:06,480 --> 00:06:14,360
ki komercialno uporabljajo Krito za igre, filme, knjige - in to je zelo spodbudno.

71
00:06:14,360 --> 00:06:23,240
Prav, torej želimo pomagati Kriti. Kako se financira Krita? Kako trajnostna je za prihodnost?

72
00:06:23,240 --> 00:06:28,160
O tem smo že malo govorili. V bistvu obstajata dve entiteti. Na eni strani je

73
00:06:28,160 --> 00:06:36,600
neprofitna fundacija Krita, na drugi strani pa obstaja profitna Halla Rempt Software, to sem jaz.

74
00:06:36,600 --> 00:06:44,000
Dejavnosti smo razdelili med obema. Fundacija prosi za donacije, kar pomeni, da je opravičena

75
00:06:44,000 --> 00:06:50,400
davkov, Halla Rempt Software pa prodaja DVD-je in stvari, seveda pa to skoraj ne prinaša več dohodkov,

76
00:06:50,400 --> 00:06:58,080
ker imamo te odlične vadnice na YouTubu ❤ . Krita se prodaja v različnih trgovinah s programi.

77
00:06:58,080 --> 00:07:06,400
Dva dohodkovna toka se združita, enkratne donacije iz sklada in dohodek iz trgovin je dovolj za sponzoriranje

78
00:07:06,400 --> 00:07:13,000
naše trenutno ekipe razvijalcev in Ramonove videoposnetke, tako da je to precej trajnostno.

79
00:07:13,000 --> 00:07:21,160
To počnemo vse od 2015, trgovine smo 
dodali 2017, zato dobro opravljamo svoje delo.

80
00:07:21,160 --> 00:07:28,520
Kako skupnost podpira Krito in kako Krita podpira svojo skupnost?

81
00:07:28,520 --> 00:07:33,360
Dolgo nazaj! Ko je bila Krita samo hobi za vse, ki so bili vpleteni,

82
00:07:33,360 --> 00:07:39,120
večina nas ni imela dostopa do digitalnih tablic za risanje. Na Kriti sem začel delati, ker sem dobil

83
00:07:39,120 --> 00:07:47,362
eno za rojstni dan, vendar je začela nagajati, zato smo zbirali sredstva za dve tablici Wacom in pero!

84
00:07:47,362 --> 00:07:54,280
Tako smo takrat opazili, da še v skupnosti prostega programja ni bilo umetnikov,

85
00:07:54,280 --> 00:08:03,560
takrat smo resnično podpirali in malo kasneje, to je bil Lukas, imeli smo tega slovaškega

86
00:08:03,560 --> 00:08:09,800
študenta GSOC, ki je kasneje postal redni sodelavec, je celo opravil svojo diplomo o čopičih Krite.

87
00:08:09,800 --> 00:08:15,400
Za to je dobil oceno 10 od 10, ker je bilo to sijajno delo in njegovi profesorji

88
00:08:15,400 --> 00:08:21,520
so bili presenečeni, da je študent dejansko počel stvari, uporabljene v resničnem svetu!

89
00:08:21,520 --> 00:08:28,320
Hkrati smo prenašali Krito iz Qt3 v Qt4, vse je bilo treba napisati na novo,

90
00:08:28,320 --> 00:08:34,720
nič ni delovalo, vse je bilo preveč »hroščavo«, res je bila grozno! Takrat je Lukas dejal:

91
00:08:34,720 --> 00:08:40,960
»Moji zadnji trije meseci na univerzi so pripravništvo. Moram narediti pomembne stvari,

92
00:08:40,960 --> 00:08:47,160
nekaj koristnega, rad bi bil za to nekaj malega plačan.« 
In ker je bila Krita takrat tako neverjetno

93
00:08:47,160 --> 00:08:55,720
hroščava, in borili smo se prostovoljno kot hobi programerji, da bi Krita spravili v uporabno stanje,

94
00:08:55,720 --> 00:09:04,480
zato smo financirali Lukasa, da 3 mesece samo popravljal hrošče. To je bil VELIK uspeh! Sprva je Lukas

95
00:09:04,480 --> 00:09:12,840
želel zaslužiti za plačilo najemnine, vendar smo mu lahko plačali za takrat, za nas, dostojno plačilo vsak mesec,

96
00:09:12,840 --> 00:09:20,400
z nabirko akcije zbiranja sredstev je Krito podprlo toliko umetnikov, toliko navdušenih ljudi.

97
00:09:20,400 --> 00:09:27,520
Iz treh mesecev je nastalo šest, odtlej pa je, malo kasneje, 
Dmitrij Kazakov opravljal briljantno delo

98
00:09:27,520 --> 00:09:34,160
od svojega prvega GSOC s Krito. Nameraval je zaključiti študij in nisem hotel,

99
00:09:34,160 --> 00:09:39,960
da vzame drugo službo in ga izgubimo pri projektu. Tako smo začeli redno zbirati sredstva

100
00:09:39,960 --> 00:09:46,400
kar nam je dejansko prineslo veliko publicitete in je bilo naporno, a zabavno in Dmitrij je še vedno

101
00:09:46,400 --> 00:09:49,880
polno zaposlen na Kriti, kot sponzorirani razvijalec. 
Torej je to delovalo.

102
00:09:50,000 --> 00:09:57,360
Se spomnite časa, ko je skupnost Krita prišla pomagati pri razvoju Krite?

103
00:09:57,600 --> 00:10:01,600
Najhujši trenutek je bil, ko je nizozemski davčni urad ...

104
00:10:01,600 --> 00:10:07,040
opazil, da ... Preskočimo zapletene stvari, ki mi tako ali tako povzročajo glavobol.

105
00:10:07,040 --> 00:10:10,720
Želeli so imeti 24.000 € naenkrat

106
00:10:10,720 --> 00:10:14,800
od Fundacije Krita, denar, ki ga nismo imeli.

107
00:10:14,800 --> 00:10:21,000
Svet smo obvestili o tem, vsi so na želeli 
podpreti! kar je bilo super, pravzaprav

108
00:10:21,000 --> 00:10:27,320
od tistega trenutka se nismo nikoli ozrli nazaj, imamo podporno skupnost in imamo razvojni

109
00:10:27,320 --> 00:10:35,280
sklad. Pozivam vas, da se mu pridružite, prodajamo Krito v različnih trgovinah s programi. Trgovine

110
00:10:35,280 --> 00:10:43,480
so absolutna nadloga, vendar nam plačujejo mesečne stroške, hvala skupnosti, da nas podpirate!

111
00:10:43,480 --> 00:10:50,840
Kako Krita pomaga uporabnikom, pri učenju programa ali celo pri programiranju?

112
00:10:51,280 --> 00:10:56,520
Trudimo se, da bi bila Krita dostopna ali celo učljiva za novince,

113
00:10:56,520 --> 00:11:03,640
Toda na prvem mestu seveda preprosto poskušamo ohraniti obstoječe 
standarde uporabniškega vmesnika. Ne posčnemo

114
00:11:03,640 --> 00:11:09,488
čudnih novih stvari. Želimo samo, da bi ljudje lahko našli vse na mestu, na katerega so navajeni.

115
00:11:09,488 --> 00:11:15,920
Če uporabljate računalnik zadnjih 5, 10, 20 ali 30 let, potem vas nič v Kriti ne bo zares

116
00:11:15,920 --> 00:11:22,600
presenetilo, kar dela Krito uporabno. Seveda sodelujemo tudi z našim

117
00:11:22,600 --> 00:11:29,840
strokovnjakom za uporabniški vmesnik Scottom Petrovićem. 
Pomaga nam oblikovati nove funkcionalnosti. Če gremo dlje

118
00:11:29,840 --> 00:11:36,560
od ravni samega programa, moramo izpostaviti pomen dokumentacije. Kar nekaj časa in naporov

119
00:11:36,560 --> 00:11:44,440
porabimo za našo dokumentacijsko spletno stran: docs.krita.org 
Ta ne navaja samo vseh možnosti v menijih, ampak ponuja

120
00:11:44,440 --> 00:11:51,600
tudi vadnice o poteku dela. Stvari, ki morda niso očitne, če ste uporabnik pisarniških

121
00:11:51,600 --> 00:11:57,280
programov za digitalno slikanje, ker - bodimo jasni - digitalno slikanje je ločeno

122
00:11:57,840 --> 00:12:05,720
od vsega drugega. Tudi sam slikam analogno

123
00:12:05,720 --> 00:12:12,240
z oljnimi barvami, skiciram in kiparim, potem pa, ko poskušam risati s Krito, vem,

124
00:12:12,240 --> 00:12:18,600
da se večina naučenega ne prenese v program za digitalno slikarstvo, zato imamo

125
00:12:18,600 --> 00:12:24,920
obsežno spletna stran z vadnicami, smernicami, 
nasveti. Imamo tudi veliko skupnost in zahvaljujoč

126
00:12:24,960 --> 00:12:34,202
Raghavendri Kamathu (raghukamath) imamo uporabniško spletno stran: krita-artists.org, podobno Blender Artists.

127
00:12:34,202 --> 00:12:38,120
V celoti temelji na tem. To je ponavljajoča se tema. Kadarkoli mislim, da potrebujemo

128
00:12:38,120 --> 00:12:43,320
nekaj novega, najprej pogledam, kaj Ton počne za Blender, nato pa to samo kopiram in ni me

129
00:12:43,320 --> 00:12:49,040
sram to priznati. Naš umetniški forum Krita je ogromen vir, kjer lahko ljudje postavljajo vprašanja,

130
00:12:49,040 --> 00:12:54,640
pa tudi poiščejo odgovore na poprej zastavljena vprašanja. Končno vlagamo veliko

131
00:12:54,640 --> 00:13:01,280
časa in denarja v serijo videoposnetkov Ramóna za naš kanal Krita, zato se vam za sodelovanje zahvaljujem, Ramón.

132
00:13:01,360 --> 00:13:11,160
Kako prilagodljiva je Krita in kakšne vstavke ali razširitve lahko uporabniki ustvarijo ali uporabljajo?

133
00:13:11,840 --> 00:13:18,760
Ko je bila Krita še mlada, je Cyrille Berger prvič ustvaril prvi vmesnik za skriptanje Krite

134
00:13:18,760 --> 00:13:24,960
v JavaScriptu, Rubyju in Pythonu. Čakajte! To pravzaprav ni prvi naš prvi skriptni

135
00:13:24,960 --> 00:13:30,120
vmesnik. KDE-jev vmesnik JavaScript. 
Skoraj vsak brskalnik, razen

136
00:13:30,120 --> 00:13:39,640
Firefoxa danes temelji na KDE-jevem delu na KHTML-ju, KDE-jevem spletnem brskalniku. Ta je vseboval pogon za JavaScript.

137
00:13:39,640 --> 00:13:47,720
In ta pogon JavaScript je bila ločena stvar, ki bi jo lahko uporabili, da bi Krito naredili skriptno,

138
00:13:47,720 --> 00:13:52,560
to je bila torej prva stvar. Najprej smo imeli JavaScript, nato pa smo ga zamenjali

139
00:13:52,560 --> 00:14:02,720
s Krossom, kar je tisto, kar je omogočilo 
Ruby, Python itn. Nato smo dodali Open GTL,

140
00:14:02,720 --> 00:14:09,360
dandanes večinoma pozabljen, klon nečesa, kar se uporablja v Hollywoodu, ki je bil ponovno izdelan oz.

141
00:14:09,360 --> 00:14:15,920
Cyrille Berger je to ponovno izdelala in lahko bi ga uporabili 
za veliko stvari, vključno z ustvarjanjem popolnoma novih barvnih prostorov.

142
00:14:15,920 --> 00:14:22,920
Bilo je res kul, toda Cyrille se je poročila in zapustila projekt. 
In bilo je v bistvu nevzdržno, zato smo to opustili.

143
00:14:22,920 --> 00:14:29,400
Hkrati smo Krito prenesli na novo verzijo KDE-jevih ogrodij in Kross je prenehal pravilno delovati,

144
00:14:29,400 --> 00:14:35,760
zato smo opustili podporo zanja. Na srečo je imela takrat Krita zelo malo uporabnikov, zato nihče ni bil prizadet.

145
00:14:35,760 --> 00:14:43,920
Nato smo Krito prenesli na drugo verzijo Qt in KDE, kar je bilo veliko dela.

146
00:14:43,920 --> 00:14:51,440
Eno poletje na strešni terasi, ko je bilo res toplo, 
sem iz besedilnega urejevalnika KDE Kate vzel kodo za

147
00:14:51,440 --> 00:14:58,280
skriptanje v pythonu, jo kopiral in popravil, da je Krita podpirala skripte. To je osnova našega

148
00:14:58,280 --> 00:15:05,760
trenutni API-ja za izvajanje skriptov v pythonu, ki je nekoliko omejen, vendar raste.

149
00:15:05,760 --> 00:15:11,920
V njem lahko naredite vse z uporabniškim vmesnikom in skriptate, kakor želite. In potem, seveda,

150
00:15:11,920 --> 00:15:19,200
smo imeli ogromen projekt Summer of Code (GSOC), kjer smo 
implementirali podporo za Disneyjev SeExpr.

151
00:15:19,200 --> 00:15:25,699
Resnično omogoča pisanje skriptov, ustvarjanje stvari, ki bodo napolnile vaše ... vaše plasti s čimerkoli.

152
00:15:25,699 --> 00:15:31,240
Pravzaprav nismo prepričani, koliko se uporablja, vendar je izjemno sposoben. Torej, da! Krita je zelo

153
00:15:31,240 --> 00:15:37,120
prilagodljiva in ob tem je večina Krite iz vstavkov. Vsako orodje je vstavek. Če poznate

154
00:15:37,120 --> 00:15:43,560
malo C ++, mislim, sam nisem znal C ++, ko sem začel kodirati Krito in sem se dobro odrezal, ko mi ni uspelo

155
00:15:43,560 --> 00:15:50,560
narediti nič koristnega, kar je pritegnilo druge sodelavce, in Krita je vzletela, zato začnimo hekati!

156
00:15:50,880 --> 00:15:56,560
Za zaključek najlepša hvala, ker ste bili na voljo za ta intervju.

157
00:15:56,560 --> 00:16:02,240
Vem, da ste res zaposleni, zato to zelo cenim.

158
00:16:02,240 --> 00:16:05,440
Kakšno sporočilo želite poslati uporabnikom Krite?

159
00:16:05,440 --> 00:16:09,000
Hvala Ramónu za intervju. Zadnje sporočilo za vse, ki uporabljate Krito.

160
00:16:09,000 --> 00:16:13,520
Še več uporabljajte Krito! Ustvarjajte, ustvarjajte! Rad vidim vse slikarije, izdelane s Krito.

161
00:16:13,520 --> 00:16:18,116
Vse, kar počnete s Krito, je zame super. Adijo!

162
00:16:18,116 --> 00:16:24,384
Z nami praznujte 25. obletnico razvoja Krite

163
00:16:24,384 --> 00:16:32,312
Vsaka povratna informacija je res dobrodošla. Ramon tukaj, kot ponavadi, poslavljam se, želim lep dan … ali večer. Hvala za ogled. ❤
