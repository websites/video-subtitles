1
00:00:00,130 --> 00:00:02,040
Tout d'abord, bonjour à tout le monde

2
00:00:02,040 --> 00:00:07,140
Je voudrais vous dire que cette vidéo est dédiée aux personnes débutant dans la peinture numérique et

3
00:00:07,330 --> 00:00:13,620
choisissent <b><font color=#ff0000>Krita</b></font> comme première option pour apprendre. Donc, si vous êtes un utilisateur de niveau intermédiaire ou avancé

4
00:00:14,139 --> 00:00:18,839
<i>Vous verrez probablement des choses dans cette vidéo que vous connaissez déjà. Dans cette vidéo, </i>

5
00:00:18,840 --> 00:00:24,719
nous allons parler de : téléchargement de Krita, pilotes, comment installer Krita correctement,

6
00:00:24,789 --> 00:00:28,079
l'écran de bienvenue, comment créer notre première image,

7
00:00:28,420 --> 00:00:35,790
brosses, l'espace de travail et la couleur, comment enregistrer vos images et travailler en toute sécurité. Alors êtes-vous prêt ?

8
00:00:36,850 --> 00:00:38,850
Commençons

9
00:00:44,560 --> 00:00:47,940
Tout a beaucoup changé depuis les premiers programmes de peinture numérique

10
00:00:48,550 --> 00:00:56,430
De nos jours, nous vivons des moments passionnants parce que nous sommes entourés d'applications de création. Aujourd'hui plus que jamais, nous pouvons

11
00:00:56,430 --> 00:00:59,159
peindre avec beaucoup d'outils différents et c'est super.

12
00:00:59,160 --> 00:01:03,360
Mais, parfois, nous sommes submergés par la quantité d'options dont nous disposons.

13
00:01:03,790 --> 00:01:11,069
Si vous êtes ici, c'est parce que vous êtes intéressés par Krita et que vous voulez commencer à peindre et à créer votre propre monde

14
00:01:11,260 --> 00:01:16,259
Peut-être que vous avez vu une belle image faite avec Krita et qui

15
00:01:16,570 --> 00:01:19,410
augmente votre curiosité concernant le logiciel Krita

16
00:01:19,660 --> 00:01:23,160
Eh bien, cette vidéo est faite pour vous faciliter votre première

17
00:01:23,380 --> 00:01:28,499
expérience avec Krita. Depuis les premières étapes, Krita est un programme avec une interface moyennement

18
00:01:28,780 --> 00:01:34,619
complexe sans trop de boutons qui vous distrairaient ou vous intimideraient, ce qui est toujours apprécié

19
00:01:35,259 --> 00:01:38,489
Parce que nous ne devons pas apprendre une interface complexe

20
00:01:38,490 --> 00:01:44,580
Mais, nous pouvons l'ajuster en fonction de nos besoins. Si vous n'avez pas installé le programme, vous avez ici quelques conseils.

21
00:01:44,619 --> 00:01:45,520
tout d'abord,

22
00:01:45,520 --> 00:01:47,520
Où puis-je obtenir le logiciel Krita ?

23
00:01:47,680 --> 00:01:53,610
Beaucoup de gens recherchent des logiciels dans des pages, proposant beaucoup de logiciels. Ce n'est pas recommandé

24
00:01:53,800 --> 00:01:58,559
Parce que nous pouvons avoir de mauvaises surprises comme des logiciels malveillants ou des logiciels espions.

25
00:01:59,170 --> 00:02:01,170
Donc, si vous voulez peindre en toute sécurité

26
00:02:01,390 --> 00:02:08,970
le meilleur site pour télécharger Krita est le site officiel « krita.org ». Les liens seront dans la

27
00:02:09,280 --> 00:02:13,830
description ci-dessous. Nous pouvons rechercher le bouton de téléchargement ou simplement appuyer sur le gros bouton

28
00:02:14,549 --> 00:02:20,909
« Obtenez Krita maintenant », un bouton très descriptif. Ensuite, nous pouvons choisir la plate-forme que nous voulons pour Krita.

29
00:02:21,010 --> 00:02:23,010
C'est-à-dire si j'utilise par exemple

30
00:02:23,500 --> 00:02:28,139
Linux, Windows ou Macintosh. Nous pouvons télécharger Krita et pendant qu'il se télécharge

31
00:02:28,540 --> 00:02:35,249
Jetons un coup d'œil à ce que nous avons ici. Comme vous pouvez le voir, nous pouvons également aller sur la boutique « Windows » ou à une plate-forme « Steam »

32
00:02:35,590 --> 00:02:39,479
Si nous voulons obtenir Krita en tant qu'application. C'est une option payante

33
00:02:39,480 --> 00:02:46,259
pour aider à payer le principal développeur de Krita, Boudewijn, ceci est vraiment important. Vous pouvez également voir qu'il y a quelque chose appelé

34
00:02:46,630 --> 00:02:51,540
versions de développement. Krita existe en deux versions différentes. Alors, laquelle préférez-vous ?

35
00:02:52,380 --> 00:02:54,859
version stable ou version de développement ?

36
00:02:55,500 --> 00:02:58,130
Quel Krita est le meilleur pour moi ?

37
00:02:58,740 --> 00:03:00,380
Question étrange, n'est-ce pas ?

38
00:03:00,380 --> 00:03:00,870
Bien...

39
00:03:00,870 --> 00:03:03,740
Si vous avez besoin de stabilité pour terminer votre projet

40
00:03:04,140 --> 00:03:08,509
confortablement, le choix le plus habituel est le téléchargement de la version stable

41
00:03:08,640 --> 00:03:12,830
C'est celui possédant les principales fonctionnalités pour terminer le travail

42
00:03:12,890 --> 00:03:20,330
Mais si vous voulez essayer les dernières fonctionnalités et même donner vos commentaires après cela, vous pouvez télécharger

43
00:03:21,030 --> 00:03:27,020
la version de développement. Et oui, la version de développement peut planter. Et oui, c'est bien

44
00:03:30,060 --> 00:03:36,229
C'est bien parce que cela nous permet d'affiner le développement et de laisser Krita s'améliorer autant que

45
00:03:36,360 --> 00:03:40,190
possible pour la prochaine version et la bonne chose est que je peux télécharger

46
00:03:40,560 --> 00:03:46,970
les versions de développement et ne pas supprimer ma version stable. Vous choisissez donc. Nous avons déjà téléchargé Krita

47
00:03:47,120 --> 00:03:49,120
Et maintenant ? Pilotes

48
00:03:50,280 --> 00:03:56,179
Il est pratique de télécharger les derniers pilotes pour notre tablette graphique. Avoir notre équipement

49
00:03:56,430 --> 00:04:02,810
à jour est toujours une bonne idée. Nous pouvons éviter de nombreux problèmes à la fois dans Krita et dans d'autres logiciels

50
00:04:03,299 --> 00:04:06,739
Alors investissez un peu de temps pour tout préparer.

51
00:04:06,739 --> 00:04:11,449
Je vous attends. Une fois Krita téléchargé et vous avez mis à jour les pilotes,

52
00:04:11,450 --> 00:04:18,169
vous pouvez installer Krita. Si vous trouvez des problèmes avec la tablette, veuillez vérifier que celle-ci est bien compatible avec Krita

53
00:04:18,810 --> 00:04:24,649
Krita couvre un large éventail de périphériques, mais parfois nous pouvons trouver des comportements étranges

54
00:04:24,690 --> 00:04:28,910
Ainsi, ceci est le lien pour le vérifier. Également dans la description ci-dessous

55
00:04:29,370 --> 00:04:35,419
Certains utilisateurs ont signalé des problèmes avec des « anneaux » autour du curseur dans Windows 10

56
00:04:35,419 --> 00:04:38,299
Alors, veuillez regarder cette vidéo si vous êtes dans cette situation

57
00:04:39,120 --> 00:04:44,929
L'installation de Krita n'est pas compliquée une fois qu'il est téléchargé. Veuillez cliquer sur le fichier pour installer

58
00:04:45,810 --> 00:04:52,070
De plus, veuillez également vous assurer que Windows Shell est sélectionné afin de voir les fichiers de Krita dans notre navigateur de fichiers sous Windows

59
00:04:52,919 --> 00:04:56,809
Si vous êtes un utilisateur sous Linux, vous pouvez télécharger un fichier « AppImage »

60
00:04:57,570 --> 00:05:00,830
Une fois qu'il est téléchargé, alors, vous devez rendre le fichier exécutable

61
00:05:01,740 --> 00:05:06,569
Vérification des autorisations de contrôle. Faites un clic droit sur votre fichier « AppImage » et

62
00:05:06,879 --> 00:05:09,869
sélectionnez les propriétés, puis rendez le exécutable

63
00:05:10,509 --> 00:05:16,619
D'accord, veuillez ouvrir Krita et l'écran de démarrage apparaît durant le chargement des ressources. Après quelques secondes

64
00:05:16,620 --> 00:05:23,969
Vous pouvez maintenant voir l'interface du programme. Nous avons une sorte d'écran de bienvenue. Cet écran donne beaucoup d'informations importantes

65
00:05:24,610 --> 00:05:29,550
Par exemple, nous pouvons voir les dernières nouvelles de Krita. Grâce à cette boîte de dialogue

66
00:05:30,069 --> 00:05:32,968
liée au compte officiel concernant la peinture sous Krita

67
00:05:33,400 --> 00:05:38,159
Et aussi des liens très intéressants pour vous aider à en savoir plus sur

68
00:05:38,319 --> 00:05:45,028
Le monde de Krita et de sa communauté. Nous pouvons également aller directement au manuel et en apprendre beaucoup en lisant

69
00:05:45,159 --> 00:05:50,729
son contenu, créé par l'équipe de développement de Krita. Nous pouvons aller directement sur le forum et chercher de l'aide.

70
00:05:50,849 --> 00:05:55,199
Enregistrez simplement un compte, connectez-vous et vous pouvez parler directement à

71
00:05:55,569 --> 00:06:00,929
l'équipe de développement et partager vos idées ou ajouter des illustrations pour obtenir des commentaires des autres utilisateurs.

72
00:06:01,210 --> 00:06:06,060
Contactez direct avec l'équipe de programmation et l'équipe des artistes. Super ! Ici, sur la première colonne

73
00:06:06,060 --> 00:06:10,259
nous voyons un « nouveau fichier » avec le raccourci standard « CTRL » + « N »

74
00:06:10,539 --> 00:06:15,869
Si je clique dessus, une nouvelle fenêtre apparaît et je vois de nombreuses options. Vous

75
00:06:16,270 --> 00:06:22,259
pouvez sélectionner un modèle ou créer votre propre fichier avec la taille dont vous avez besoin. Si vous ne créez pas d'image

76
00:06:22,479 --> 00:06:26,098
certaines parties de l'interface de Krita ne fonctionnent pas, comme...

77
00:06:26,589 --> 00:06:31,199
la sélection des brosses, sélection des couleurs, etc. Seuls les menus sont disponibles.

78
00:06:31,629 --> 00:06:34,619
Ceci est logique car il n'y a rien à travailler.

79
00:06:34,719 --> 00:06:40,889
Ainsi, faisons donc notre première image avec Krita. Allez vers un document personnalisé et choisissez la taille du fichier

80
00:06:40,889 --> 00:06:44,758
que vous souhaitez créer. Ne vous occupez pas de toutes les options.

81
00:06:44,759 --> 00:06:49,679
Nous allons y jeter un œil dans une autre vidéo. Donnez une taille et cliquez sur le bouton « Créer »

82
00:06:49,960 --> 00:06:53,279
Une fois que vous avez créé une image, vous pouvez commencer à y travailler.

83
00:06:53,589 --> 00:06:58,919
Il y a des interfaces qui sont très complexes et nous submergent.

84
00:06:59,469 --> 00:07:01,469
Cela m'est aussi arrivé.

85
00:07:01,569 --> 00:07:07,528
Mais de quoi avons-nous vraiment besoin pour la peinture ? Nous avons besoin de couleurs de pinceaux et un confortable

86
00:07:08,050 --> 00:07:09,189
espace de travail

87
00:07:09,189 --> 00:07:11,549
Brosses, espace de travail et couleur

88
00:07:12,250 --> 00:07:13,509
Donc des brosses...

89
00:07:13,509 --> 00:07:19,809
où sont les brosses et comment peindre et effacer les choses, comment modifier leur taille rapidement et

90
00:07:19,970 --> 00:07:23,470
Comment ne pas se perdre parmi tant d'options de brosses.

91
00:07:24,110 --> 00:07:28,330
L'ensemble des brosses de Krita est conçu pour répondre aux besoins basiques de quiconque

92
00:07:28,460 --> 00:07:29,289
tout d'abord

93
00:07:29,289 --> 00:07:32,229
Il est bon de s'assurer que tout est correct

94
00:07:32,570 --> 00:07:40,329
parce qu'il peut arriver que nous activions le « mode de gomme » par erreur et que nous ne nous en souvenons pas et soudain

95
00:07:40,639 --> 00:07:45,099
les brosses ne peignent pas et nous pouvons être perturbés. Donc

96
00:07:45,740 --> 00:07:48,820
Vérifiez que le bouton « Mode gomme » est désactivé

97
00:07:48,949 --> 00:07:53,348
D'accord ! Les brosses sont sur le côté droit de l'interface par défaut

98
00:07:53,690 --> 00:08:01,479
Mais, vous pouvez modifier l'endroit où ils se trouvent simplement en faisant glisser le panneau. Si vous fermez le panneau « Brosses », vous pouvez le restaurer facilement. Veuillez aller dans

99
00:08:01,759 --> 00:08:08,589
Configuration / Panneaux / Préréglages de brosse et Krita se souvient de son dernier emplacement à afficher.

100
00:08:10,400 --> 00:08:12,519
Les brosses peuvent se ressembler beaucoup

101
00:08:13,340 --> 00:08:17,440
Mais, vous en voudrez encore plus peut-être, dans certains jours ou certaines semaines.

102
00:08:18,470 --> 00:08:25,209
Avons-nous dit que, dans la boutique de Krita, il y a tout un ensemble pour imiter l'huile, le pastel et l'aquarelle

103
00:08:27,500 --> 00:08:31,720
Krita comprend que beaucoup de brosses peuvent submerger l'utilisateur.

104
00:08:32,180 --> 00:08:37,839
Ainsi, il a donc créé un système d'étiquettes pouvant être utilisées pour ne voir que certaines brosses

105
00:08:38,479 --> 00:08:40,479
orientées vers un flux de travail spécifique

106
00:08:41,120 --> 00:08:45,039
Voyons comment cela fonctionne. Par exemple, nous avons la « Peinture »

107
00:08:45,709 --> 00:08:47,029
« Esquisser »

108
00:08:47,029 --> 00:08:54,789
« Texture » et ainsi de suite. Chaque étiquette possède quelques brosses concernant cette tâche. Par exemple, l'étiquette « Textures »

109
00:08:54,980 --> 00:09:02,380
Possède quelques brosses pour vous aider à créer rapidement des effets de texture. Dans les futures vidéos, nous verrons plus de choses concernant les brosses.

110
00:09:03,440 --> 00:09:08,020
Une autre façon de chercher des brosses est d'utiliser le filtrage. Nous pouvons aller plus loin

111
00:09:08,570 --> 00:09:11,770
pour explorer des brosses à l'aide de l'éditeur de brosses, par exemple,

112
00:09:12,230 --> 00:09:19,480
Pour en savoir plus sur les moteurs de brosses. Nous couvrirons cela dans une vidéo à venir. Mais peut-être que vous n'aimez pas avoir les brosses

113
00:09:19,480 --> 00:09:23,980
là, toujours visible, comme vous pouvez l'observer. Dans ce cas, vous pouvez utiliser le

114
00:09:24,350 --> 00:09:31,410
raccourci clavier « F6 » ou accédez à la fenêtre à liste déroulante en haut qui ouvrira la fenêtre avec toutes vos brosses

115
00:09:32,410 --> 00:09:37,439
Vous pouvez utiliser ici le système d'étiquettes. Vous avez encore une autre option très intéressante.

116
00:09:38,140 --> 00:09:41,009
Parfois, nous n'utilisons pas plus de 10 brosses

117
00:09:41,830 --> 00:09:46,770
Imaginez par exemple que nous dessinons en plein écran sans aucune interface

118
00:09:47,050 --> 00:09:54,540
en appuyant sur le raccourci « Tab », puis vous appuyez sur le bouton droit de la souris ou le bouton de stylet que vous avez associé au clic droit

119
00:09:55,450 --> 00:10:00,150
Ceci vous permet de voir des choses comme les brosses, les couleurs, les dernières couleurs utilisées.

120
00:10:00,150 --> 00:10:06,720
Et aussi faire un miroir de l'image, faire pivoter l'image et plus encore. Ce menu rapide est vraiment puissant.

121
00:10:06,850 --> 00:10:12,119
Nous verrons cela aussi dans une autre vidéo. Vous avez choisi une brosse. Pour changer la taille

122
00:10:12,360 --> 00:10:20,320
appuyez sur la touche « Maj » et faites glisser le curseur vers la droite pour augmenter la taille ou vers la gauche pour diminuer la taille

123
00:10:20,520 --> 00:10:26,220
Nous pouvons également le faire grâce à des raccourcis clavier. Eh bien, ce n'est rien de confortable ou

124
00:10:26,920 --> 00:10:33,020
grâce au curseur en haut de l'interface, ce qui est une bonne idée. Une chose que vous allez remarquer,

125
00:10:33,030 --> 00:10:35,309
est-ce que si vous choisissez un pinceau et

126
00:10:35,890 --> 00:10:37,810
Modifier la taille, par exemple

127
00:10:37,810 --> 00:10:45,510
Vous faites quelques traits et vous dites que « Hum, cela semble une très bonne brosse et je l'aime telle quelle ».

128
00:10:46,090 --> 00:10:50,639
mais si vous sélectionnez une autre brosse et que vous re-sélectionnez la brosse précédente

129
00:10:51,400 --> 00:10:55,800
Vous verrez que la brosse est retournée à son état précédent

130
00:10:56,740 --> 00:10:59,820
Dans ce cas, vos modifications n'étaient pas trop importantes.

131
00:11:00,370 --> 00:11:07,650
Mais, imaginez que vous commencez à jouer avec les paramètres de brosses et à la fin, vous ne savez pas ce que vous avez modifié exactement,

132
00:11:07,650 --> 00:11:12,869
mais le résultat de la brosse est très agréable. Dans ce cas

133
00:11:12,880 --> 00:11:15,460
Cela peut être vraiment ennuyeux, n'est-ce pas ?

134
00:11:15,580 --> 00:11:22,300
Cela est fait pour les personnes préférant utiliser des brosses par défaut, telles qu'elles sont et sans les changer trop.

135
00:11:22,440 --> 00:11:29,240
C'est une bonne façon de faire au début car cela vous aide à mieux comprendre la différence entre eux et

136
00:11:29,680 --> 00:11:35,310
les possibilités que vous pouvez avoir avec une seule brosse et c'est la raison pour laquelle Krita a été créé

137
00:11:35,560 --> 00:11:42,859
les « brosses sales ». Des brosses pouvant se souvenir de vos modifications même si vous changez encore et encore de brosses,

138
00:11:43,350 --> 00:11:45,890
Pour utiliser cette fonctionnalité. Nous devons activer

139
00:11:46,410 --> 00:11:53,480
l'option « Enregistrer temporairement les ajustements aux préréglages » dans l'éditeur de brosses, Krita se souviendra des paramètres jusqu'à sa fermeture.

140
00:11:53,640 --> 00:11:58,849
D'accord, si vous souhaitez enregistrer une version modifiée, cliquez simplement sur le bouton d'écrasement

141
00:11:59,040 --> 00:12:01,040
Et c'est tout

142
00:12:01,140 --> 00:12:04,969
Vous avez votre premier pinceau modifié, prêt à être utilisé

143
00:12:05,269 --> 00:12:11,479
Même si vous fermez Krita. Pour être à l'aise dans un espace de travail, nous devons savoir comment réaliser une navigation basique

144
00:12:12,000 --> 00:12:17,779
Comme le zoom, le panoramique et la rotation et aussi le miroir horizontal peuvent être utiles

145
00:12:18,029 --> 00:12:18,890
dans Krita

146
00:12:18,890 --> 00:12:23,059
nous utilisons par défaut la touche « M » comme raccourci pour faire une copie en miroir de la vue

147
00:12:23,370 --> 00:12:30,200
C'est vraiment utile lorsque nous peignons pendant un long moment. Un raccourci simple, mais une fonctionnalité puissante. La couleur

148
00:12:31,110 --> 00:12:37,550
Lorsque nous peignons, nous devons choisir le sélecteur de couleurs. Un emplacement où vous pouvez sélectionner différentes couleurs.

149
00:12:37,920 --> 00:12:43,490
Par exemple, comment rendre ma couleur moins saturée ou plus lumineuse ? Par exemple,

150
00:12:44,190 --> 00:12:49,909
Nous pouvons utiliser « Sélecteur avancé de couleurs » ou utiliser une « palette » personnalisée. Pour l'instant

151
00:12:49,950 --> 00:12:56,390
Nous allons laisser cela tel qu'il est par défaut car nous traiterons la couleur dans les vidéos à venir.

152
00:12:57,240 --> 00:13:03,109
Enregistrement ou images et travail en toute sécurité. Une pratique hautement recommandée est d'enregistrer souvent

153
00:13:03,570 --> 00:13:08,539
Alors, faites-le. Tous les logiciels plantent parfois. Bien...

154
00:13:08,540 --> 00:13:12,740
Je suppose que vous avez dessiné et testé les brosses qui sont proposées par défaut

155
00:13:12,899 --> 00:13:17,659
mais que se passe-t-il si nous sommes tellement excités que nous oublions de l'enregistrer et

156
00:13:18,000 --> 00:13:21,859
soudain, la lumière s'éteint ? Il y a des années, c'était un gros problème

157
00:13:21,930 --> 00:13:27,770
Cela vous fait tout refaire à nouveau depuis la dernière fois que vous enregistrez votre travail

158
00:13:28,740 --> 00:13:35,990
Mais, ne sommes nous pas dans une époque de technologies, n'est-ce pas ? C'est pourquoi Krita va plus loin et nous aide sur le comment ?

159
00:13:36,750 --> 00:13:40,039
Chaque fois que vous ouvrez une image et vous y travaillez dessus

160
00:13:40,649 --> 00:13:42,889
Krita enregistre votre travail, de façon silencieuse

161
00:13:43,260 --> 00:13:50,179
Le programme réalise une copie du fichier et s'occupe d'enregistrer le travail toutes les 15 minutes. Est-ce que ceci est agréable ou pas ?

162
00:13:51,059 --> 00:13:58,439
Je pense que ça l'est. Si nous voulons changer cette durée, nous devons aller dans les paramètres de Configuration / Configurer Krita / Général / Section et

163
00:13:59,319 --> 00:14:02,218
 / Manipulation des fichiers et ici, nous pouvons changer la durée

164
00:14:02,889 --> 00:14:04,719
Faisons-le.

165
00:14:04,719 --> 00:14:11,489
Je considère que cinq minutes est une période non intrusive pour enregistrer dans le cas où pour une raison étrange

166
00:14:12,099 --> 00:14:18,959
Vous souhaitez désactiver cette option par un simple clic sur le bouton radio « Activer l'enregistrement automatique »

167
00:14:19,389 --> 00:14:22,108
Je ne le ferais pas. Je ne vais pas le faire.

168
00:14:22,109 --> 00:14:26,098
Je peux l'assurer. Et si je veux enregistrer différentes étapes de mon travail

169
00:14:26,529 --> 00:14:31,859
Ensuite, nous utilisons l'option de sauvegarde incrémentale, un nom simple pour une fonctionnalité très utile

170
00:14:32,319 --> 00:14:34,348
Et pourquoi est-ce que je pense que c'est si utile ?

171
00:14:34,719 --> 00:14:38,369
Parce que si vous remarquez, il possède un raccourci associé

172
00:14:38,529 --> 00:14:44,669
qui est « CTRL » + « ALT » + « S » et de cette façon, nous utilisons simplement ce raccourci et nous créons une

173
00:14:44,949 --> 00:14:51,478
version incrémentale du fichier sur lequel nous travaillons. Si vous aimez ce type d'outils aidant la productivité,

174
00:14:51,669 --> 00:14:56,759
faites-moi savoir dans les commentaires et donnez à Krita un « J'aime ». D'accord ! Voilà, c'est la fin pour l'instant !

175
00:14:56,759 --> 00:14:59,878
Vous avez les bases mais il y en a beaucoup plus.

176
00:15:00,189 --> 00:15:03,898
N'oubliez pas de vous abonner et de partager cette vidéo dans vos média sociaux

177
00:15:03,899 --> 00:15:09,719
Et si vous pensez que cela pourrait aider vos connaissances et plus d'utilisateurs de Krita, partagez-le.

178
00:15:10,179 --> 00:15:15,119
si vous êtes intéressés par un sujet spécifique ou si vous souhaitez suggérer de nouveaux sujets

179
00:15:15,879 --> 00:15:21,028
Faites-moi savoir dans les commentaires ci-dessous et peut-être que cela pourrait être la prochaine vidéo

180
00:15:21,969 --> 00:15:28,169
Merci beaucoup d'avoir regardé cette vidéo et soyez prêts pour de nouvelles vidéos où je traiterais l'utilisation de Krita

181
00:15:28,239 --> 00:15:30,239
avec des fonctionnalités plus avancées

182
00:15:30,789 --> 00:15:32,789
Au revoir
