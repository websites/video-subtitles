﻿1
00:00:01,120 --> 00:00:04,520
Hola a tothom, i us donem la benvinguda a este vídeo nou!

2
00:00:04,520 --> 00:00:08,200
Estem celebrant el 25è aniversari de Krita

3
00:00:08,200 --> 00:00:13,960
i hui! Ací! Tenim un convidat molt, molt, especial.

4
00:00:14,000 --> 00:00:16,600
La mantenidora de Krita… Halla Rempt

5
00:00:16,600 --> 00:00:21,600
I Halla ha sigut la mantenidora durant més de 20 anys!

6
00:00:21,600 --> 00:00:22,960
És sorprenent!

7
00:00:22,960 --> 00:00:25,880
Un dia molt especial, gaudix-lo!

8
00:00:25,880 --> 00:00:28,120
Hola Halla, com va?

9
00:00:28,400 --> 00:00:30,200
Hola Ramon, encantada de veure't!

10
00:00:30,440 --> 00:00:34,960
Celebrar 25 anys és una fita important.

11
00:00:34,960 --> 00:00:39,200
Què significa ser de codi obert per al desenvolupament de Krita?

12
00:00:39,800 --> 00:00:45,120
Què significa ser codi obert per al desenvolupament de Krita en els últims 25 anys? Bé, jo diria

13
00:00:45,120 --> 00:00:50,520
que és absolutament essencial i açò per dos motius. El primer, per descomptat, és que tractant-se de codi obert

14
00:00:50,520 --> 00:00:55,680
ha incorporat les col·laboracions de centenars de persones. N'hi ha més de 600

15
00:00:55,680 --> 00:01:00,440
a la llista de comissions de Krita i açò és només el desenvolupament. No els llocs web,

16
00:01:00,440 --> 00:01:05,120
ni la documentació, ni tota la resta i açò és impressionant! Sempre que ens quedem parats, en algun lloc,

17
00:01:05,120 --> 00:01:10,720
la gent ens pot ajudar! I molta gent ho ha fet. Finalment, potser encara més important, ara que

18
00:01:10,720 --> 00:01:17,200
Krita és programari lliure de codi obert d'acord amb la GPL, mai es podrà tancar. Durant

19
00:01:17,200 --> 00:01:22,800
anys hem vist desaparéixer competidors perquè els han comprat empreses com Adobe,

20
00:01:22,800 --> 00:01:27,440
sabíem que podíem confiar en el fet que Krita només continuaria tant de temps com hi haguera

21
00:01:27,440 --> 00:01:32,680
col·laboradors. Així que ser de codi obert és totalment fonamental, açò mai canviarà!

22
00:01:32,760 --> 00:01:35,240
Vaja! Esta és una informació molt interessant.

23
00:01:35,240 --> 00:01:40,240
Krita ha sigut creixent durant estos 25 anys,

24
00:01:40,240 --> 00:01:42,200
el qual és un viatge increïble.

25
00:01:42,200 --> 00:01:46,160
Com ha evolucionat Krita durant estos 25 anys?

26
00:01:46,200 --> 00:01:50,240
25 anys és un viatge bastant llarg… No he estat allà durant tot este viatge.

27
00:01:50,240 --> 00:01:54,840
Potser només 21 anys, el qual encara és molt de temps. Quan mire arrere

28
00:01:54,840 --> 00:01:58,960
com era Krita quan em vaig unir al projecte, ni tan sols es podia dibuixar.

29
00:01:58,960 --> 00:02:02,720
Podíeu carregar imatges afegint capes i moure-les i açò era tot.

30
00:02:02,720 --> 00:02:08,320
Així que vam començar a construir-lo. Es va reescriure tot el nucli de Krita. El nucli s'ha reescrit 4 vegades

31
00:02:08,320 --> 00:02:15,680
i cada vegada que Krita té més rendiment comencem a afegir eines, es comencen a afegir fases

32
00:02:15,680 --> 00:02:23,640
com que Krita admetera un llenç basat en openGL, acceleració de GPU abans que Photoshop. CMYK des de fa temps.

33
00:02:23,640 --> 00:02:28,040
Vam començar a implementar més i més coses divertides, més i més coses útils i després va arribar el

34
00:02:28,040 --> 00:02:33,800
gran descans. Vam començar a escoltar els nostres usuaris i vam tindre estes sessions. Esprints de desenvolupament,

35
00:02:33,800 --> 00:02:38,960
on es reunixen els desenvolupadors i col·laboradors de Krita, sobretot treballant i debatent conjuntament

36
00:02:38,960 --> 00:02:44,000
el que farem l'any vinent 
des del nostre esprint en Blender Studios… i

37
00:02:44,000 --> 00:02:49,560
després demanem als artistes que facen una «mostra» de la faena 
amb Krita i… durant una hora

38
00:02:49,560 --> 00:02:55,000
els veurem pintar, amb l'esperança que 
no els importe, i poden dir el que vullguen,

39
00:02:55,000 --> 00:03:00,320
queixar-se de tot allò de què han de queixar-se. 
Qualsevol cosa val, i nosaltres, els desenvolupadors, no tenim

40
00:03:00,320 --> 00:03:03,960
permís per a contestar. Tampoc podem ajudar-te, perquè volem veure quins

41
00:03:03,960 --> 00:03:11,080
són els problemes. I esta mena d'observació de l'usuari realment ens ajuda a millorar Krita a passos de gegant.

42
00:03:11,080 --> 00:03:18,000
Quin ha sigut l'ús més creatiu de Krita que vos ha fet pensar…

43
00:03:18,000 --> 00:03:20,680
Vaja! Mai hi he pensat!

44
00:03:20,680 --> 00:03:27,120
l'ús més estrany de Krita? Supose que és la persona que volia utilitzar les capacitats de profunditat

45
00:03:27,120 --> 00:03:34,120
del bit alt de Krita per a fer mapes d'estreles. S'utilitzen fotografies astronòmiques enormes i

46
00:03:34,120 --> 00:03:43,680
volia treballar amb elles i, sincerament, hi hauria… (bé, de fet ho vaig dir), d'utilitzar VIPS per a açò, però era un ús força estrany.

47
00:03:43,880 --> 00:03:49,440
Molts usuaris podrien preguntar-se, què motiva l'equip de Krita?

48
00:03:50,440 --> 00:03:56,520
Què ens motiva? Supose que una raó egoista i desinteressada. La raó egoista és

49
00:03:56,520 --> 00:04:02,320
esta és la faena més divertida que he fet en la meua vida. He treballat com a mínim en sis o set empreses

50
00:04:02,320 --> 00:04:09,000
de tota mena, i només en una d'estes empreses vam publicar el programari que jo estava desenvolupant,

51
00:04:09,000 --> 00:04:15,600
algunes empreses només desenvolupen programari per a gastar diners, és una bogeria, però Krita es posa en marxa

52
00:04:15,600 --> 00:04:21,920
de la mà dels usuaris i ací és on entra la part desinteressada, perquè estos usuaris ens diuen les

53
00:04:21,920 --> 00:04:29,200
coses més agradables, com aquella vegada que vaig rebre un correu electrònic d'algú de Malàisia. Els seus pares li van dir

54
00:04:29,200 --> 00:04:33,840
que «les xiques no necessiten dibuixar, així que no, no et facilitarem cap programa de dibuix». El seu germà

55
00:04:33,840 --> 00:04:39,520
li va donar una tauleta de dibuix, així que ella va descobrir 
Krita i va poder començar a fer Art

56
00:04:39,520 --> 00:04:46,120
i ella em va enviar en tons d'èxtasi absolut: «Puc fer 
art gràcies, gràcies, gràcies, gràcies» i açò és senzillament

57
00:04:46,120 --> 00:04:51,800
tan motivador i ens provoca una sensació increïble. Així que si t'agrada Krita, pensa a dir-nos-ho

58
00:04:51,800 --> 00:04:58,320
perquè normalment només veem informes d'error. Els informes d'error són útils, si en feu, gràcies!

59
00:04:58,320 --> 00:05:03,080
Però de vegades poden ser una mica desmotivadors perquè n'hi ha tants.

60
00:05:03,200 --> 00:05:09,400
Ens podeu explicar més sobre l'ús comercial de Krita?

61
00:05:09,400 --> 00:05:16,240
L'ús comercial de Krita. Per descomptat, està molt bé. Açò és el que diem explícitament en tota la nostra

62
00:05:16,240 --> 00:05:22,280
llista de preguntes freqüents (PMF) en el quadro d'explicacions a tot arreu. Però l'ús comercial

63
00:05:22,280 --> 00:05:29,160
realment succeïx. La primera vegada que vaig sentir parlar d'açò va ser quan presentàvem Krita al SIGGRAPH en Vancouver.

64
00:05:29,160 --> 00:05:35,694
Vam tindre un estand al costat de Blender gràcies a l'ajuda realment útil d'en Ton Roosendaal, va ser com…

65
00:05:35,694 --> 00:05:41,600
hi havia molt, molt d'interés, moltes persones volien parlar amb nosaltres i llavors va vindre aquell tipus

66
00:05:41,600 --> 00:05:49,680
cap a mi i em va dir que no li ho explicara a ningú més, almenys de moment, però (psst) hem estat utilitzant Krita

67
00:05:49,680 --> 00:05:55,200
per a la introducció de «Little, Big», la qual era una pel·lícula d'animació crec que de Disney,

68
00:05:55,200 --> 00:06:01,560
Ja no estic segura, així que va ser el primer que vaig sentir sobre l'ús comercial de Krita i era el 2014, pense.

69
00:06:01,560 --> 00:06:06,480
Des de llavors, hem estat patrocinats i hem estat en contacte amb la gent

70
00:06:06,480 --> 00:06:14,360
que utilitza Krita comercialment per a jocs, pel·lícules, llibres, i açò és força estimulant.

71
00:06:14,360 --> 00:06:23,240
D'acord, així que volem ajudar Krita. Com es finança Krita? Com de sostenible és de cara al futur?

72
00:06:23,240 --> 00:06:28,160
Ja hem parlat una mica d'açò. Bàsicament hi ha dues entitats. D'una banda hi ha la

73
00:06:28,160 --> 00:06:36,600
Krita Foundation sense ànim de lucre, d'altra banda hi ha el programari Halla Rempt amb ànim de lucre, soc jo.

74
00:06:36,600 --> 00:06:44,000
i dividim les activitats entre les dues. La fundació sol·licita donacions, cosa que significa que està exempta

75
00:06:44,000 --> 00:06:50,400
d'impostos, el programari Halla Rempt ven DVD i coses, però per descomptat ja no té ingressos

76
00:06:50,400 --> 00:06:58,080
hui en dia perquè tenim estes grans guies d'aprenentatge en YouTube ❤. Es ven Krita en diverses botigues d'aplicacions.

77
00:06:58,080 --> 00:07:06,400
Els dos fluxos d'ingressos combinats, les donacions puntuals al Fons, els ingressos de les botigues són suficients per a patrocinar

78
00:07:06,400 --> 00:07:13,000
el nostre equip de desenvolupadors i els vídeos d'en Ramon, actualment patrocinats, perquè açò siga bastant sostenible

79
00:07:13,000 --> 00:07:21,160
almenys hem estat fent açò des de 2015 i les botigues 
es van afegir el 2017, així que estem fent una bona faena.

80
00:07:21,160 --> 00:07:28,520
Com la comunitat ha sigut donant suport a Krita i com Krita ha sigut donant suport a la seua comunitat?

81
00:07:28,520 --> 00:07:33,360
Tornem-hi! Quan Krita era només una afició per a tothom que hi estava involucrat,

82
00:07:33,360 --> 00:07:39,120
la majoria de nosaltres no teníem accés a les tauletes digitals de dibuix. Vaig començar a treballar en Krita perquè va arribar-me

83
00:07:39,120 --> 00:07:47,362
una per al meu aniversari però va començar a fallar, així que vam fer una recaptació de fons per a aconseguir dues tauletes Wacom i llapis d'art!

84
00:07:47,362 --> 00:07:54,280
Així que va ser quan vam adonar-nos que en especial a la comunitat de programari lliure, ni tan sols hi havia artistes

85
00:07:54,280 --> 00:08:03,560
Tornant a aquells dies, vam ser realment solidaris. I una mica més tard, va arribar en Lukas, un estudiant de codi

86
00:08:03,560 --> 00:08:09,800
eslovac, que es va convertir en un col·laborador habitual, fins i tot va fer la seua tesi sobre els motors de pinzell

87
00:08:09,800 --> 00:08:15,400
de Krita. Va obtindre 10 punts sobre 10 perquè era un treball brillant i els seus professors

88
00:08:15,400 --> 00:08:21,520
van quedar sorpresos que el seu alumne va fer coses reals que s'utilitzaven en el món real!

89
00:08:21,520 --> 00:08:28,320
Al mateix temps, estàvem adaptant Krita des de les Qt3 a les Qt4, estàvem reescrivint-ho tot

90
00:08:28,320 --> 00:08:34,720
res funcionava, tot fallava massa, era realment horrible per a tothom! En aquell moment, en Lukas va dir:

91
00:08:34,720 --> 00:08:40,960
«Els meus tres últims mesos a la Universitat se suposa que són pràctiques. He de fer coses importants,

92
00:08:40,960 --> 00:08:47,160
útils, preferiria que se'm pagués una mica per açò». I com que Krita en aquell moment fallava

93
00:08:47,160 --> 00:08:55,720
per tot arreu, i estàvem lluitant tant com a codificadors voluntaris i aficionats per a aconseguir que Krita es convertira en alguna cosa útil, vam

94
00:08:55,720 --> 00:09:04,480
finançar en Lukas per a corregir errors durant 3 mesos. Va ser un gran èxit! Inicialment, en Lukas principalment volia alguns

95
00:09:04,480 --> 00:09:12,840
diners per a pagar el lloguer, però nosaltres li volíem pagar una quantitat de diners decent cada mes,

96
00:09:12,840 --> 00:09:20,400
amb el resultat de la recaptació de fons que molts artistes, moltes persones entusiastes, havien donat suport a Krita.

97
00:09:20,400 --> 00:09:27,520
Tres mesos es van convertir en sis mesos, i després una mica més tard, 
en Dmitri Kazakov ha sigut fent un treball brillant

98
00:09:27,520 --> 00:09:34,160
des del seu primer GSoC amb Krita. Anava a acabar la Universitat i jo no volia que ell

99
00:09:34,160 --> 00:09:39,960
agafara una altra faena i perdre'l per al projecte. Així que vam començar a fer algunes recaptacions de fons periòdiques

100
00:09:39,960 --> 00:09:46,400
que realment ens van fer molta publicitat i era esgotador, però era divertit fer-ho i en Dmitry encara

101
00:09:46,400 --> 00:09:49,880
treballa a temps complet en Krita, com a desenvolupador patrocinat, 
així que va funcionar.

102
00:09:50,000 --> 00:09:57,360
Podries recordar un moment en què la comunitat de Krita va vindre a ajudar al desenvolupament de Krita?

103
00:09:57,600 --> 00:10:01,600
El pitjor moment va ser quan l'oficina d'impostos neerlandesa…

104
00:10:01,600 --> 00:10:07,040
es va adonar que… saltem-nos les coses complicades que fan vindre mal de cap, de totes maneres

105
00:10:07,040 --> 00:10:10,720
volien tindre 24.000 € d'una vegada des de

106
00:10:10,720 --> 00:10:14,800
la Krita Foundation, uns diners que no teníem quan ho vam explicar

107
00:10:14,800 --> 00:10:21,000
al món. El món va córrer per a donar-nos suport! 
Va ser genial adonar-nos realment des del

108
00:10:21,000 --> 00:10:27,320
moment en què mai havíem mirat arrere, que teníem una comunitat de suport i que tenim un fons de desenvolupament

109
00:10:27,320 --> 00:10:35,280
al qual vos insto a unir-vos, ho sentim per la publicitat, venem Krita en diverses botigues d'aplicacions. Estes botigues

110
00:10:35,280 --> 00:10:43,480
són un infern per a treballar-hi, però ens donen la nostra paga mensual, gràcies a la comunitat per donar-nos suport!

111
00:10:43,480 --> 00:10:50,840
Com ajuda Krita als usuaris, a aprendre el programari o fins i tot a codificar el programari?

112
00:10:51,280 --> 00:10:56,520
Estem intentant que Krita siga accessible o fins i tot fàcil d'aprendre per als nouvinguts,

113
00:10:56,520 --> 00:11:03,640
però, en primer lloc, per descomptat, només intentem mantindre els 
estàndards d'interfície d'usuari existents. No estem intentant fer

114
00:11:03,640 --> 00:11:09,488
coses noves estranyes. Només volem que la gent puga trobar-ho tot en el lloc on està acostumada.

115
00:11:09,488 --> 00:11:15,920
Si heu utilitzat un ordinador durant els últims 5, 10, 20 o 30 anys, realment res en Krita

116
00:11:15,920 --> 00:11:22,600
sorprén que siga utilitzable en si mateix. Per descomptat, també estem treballant conjuntament amb el nostre

117
00:11:22,600 --> 00:11:29,840
expert en interfície d'usuari (UI) Scott Petrovic i ell està ajudant 
a dissenyar característiques noves. Ara, si aneu més enllà

118
00:11:29,840 --> 00:11:36,560
el nivell de l'aplicació mateixa, llavors la documentació és realment important. Passem una mica de temps

119
00:11:36,560 --> 00:11:44,440
i esforç en la nostra pàgina web de documentació: «docs.krita.org» no només llista totes les opcions de menú, també oferix

120
00:11:44,440 --> 00:11:51,600
guies d'aprenentatge sobre elements del flux de treball. Coses que potser no són aparents si eres un usuari d'oficina

121
00:11:51,600 --> 00:11:57,280
d'aplicacions de pintura digital perquè, siguem clars en açò, la pintura digital és una eina

122
00:11:57,840 --> 00:12:05,720
separada de tota la resta. Vull dir que sí. Jo també pinte analògic

123
00:12:05,720 --> 00:12:12,240
utilitzant pintures a l'oli i dibuixe un esbós i esculpeixo i després quan intente dibuixar utilitzant Krita, sé

124
00:12:12,240 --> 00:12:18,600
que la majoria dels hàbits apresos no es transferixen a una aplicació de pintura digital, així que tenim

125
00:12:18,600 --> 00:12:24,920
una extensa web amb guies d'aprenentatge, directius, 
consells, també tenim una gran comunitat i gràcies

126
00:12:24,960 --> 00:12:34,202
a Raghavendra Kamath (raghukamath), tenim un lloc web de comunitat d'usuaris que es diu: krita-artists.org, igual que Blender Artists

127
00:12:34,202 --> 00:12:38,120
està totalment basat en açò. És un tema recurrent. Sempre que crec que necessitem

128
00:12:38,120 --> 00:12:43,320
una cosa nova, primer mire què ha sigut fent en Ton a Blender i després només ho copie i no

129
00:12:43,320 --> 00:12:49,040
m'avergonyisc d'admetre-ho. El nostre fòrum d'artistes de Krita és un recurs enorme on la gent pot fer preguntes

130
00:12:49,040 --> 00:12:54,640
però també trobar respostes a les preguntes que s'han plantejat anteriorment. Finalment, invertim molt

131
00:12:54,640 --> 00:13:01,280
temps i diners, en la sèrie de vídeos d'en Ramón del nostre canal Krita, així que gràcies Ramón per treballar amb nosaltres.

132
00:13:01,360 --> 00:13:11,160
Com de personalitzable és Krita i quin tipus de connectors o extensions poden crear o utilitzar els usuaris?

133
00:13:11,840 --> 00:13:18,760
Quan Krita era força jove, en Cyrille Berger va crear per primera vegada, la primera interfície de creació de scripts per a Krita

134
00:13:18,760 --> 00:13:24,960
per a fer scripts en JavaScript, Ruby i Python. Espera! En realitat, esta no és la primera interfície de creació de scripts

135
00:13:24,960 --> 00:13:30,120
Interfície de JavaScript de KDE. Vull dir que saps 
que quasi tots els navegadors excepte

136
00:13:30,120 --> 00:13:39,640
Firefox d'hui en dia es basa en el treball de KDE en KHTML, un navegador web basat en KDE. Per descomptat, este venia amb un motor JavaScript.

137
00:13:39,640 --> 00:13:47,720
I este motor JavaScript era una cosa separada que podíem utilitzar per a fer que Krita poguera crear scripts,

138
00:13:47,720 --> 00:13:52,560
així que açò va ser el primer. De manera que primer havíem tingut JavaScript i després el substituirem

139
00:13:52,560 --> 00:14:02,720
per Kross, que és el que va fer possible 
Ruby, Python, etc. Després vàrem afegir Open GTL

140
00:14:02,720 --> 00:14:09,360
molt oblidats estos dies, un clon idèntic d'alguna cosa utilitzada en Hollywood es va tornar a implementar o

141
00:14:09,360 --> 00:14:15,920
en Cyrille Berger va tornar a implementar açò i es podria utilitzar 
per a moltes coses, inclosa la creació d'espais de color completament nous.

142
00:14:15,920 --> 00:14:22,920
Va ser genial, però en Cyrille es va casar i va deixar el projecte 
i bàsicament no es podia mantindre i, per tant, ho vam deixar estar.

143
00:14:22,920 --> 00:14:29,400
Al mateix temps, vam adaptar Krita a una versió nova dels Frameworks de KDE, i Kross va deixar de funcionar correctament

144
00:14:29,400 --> 00:14:35,760
per tant, vàrem retirar el nostre suport. Afortunadament, llavors Krita tenia molt pocs usuaris, de manera que ningú es va veure realment afectat.

145
00:14:35,760 --> 00:14:43,920
Després vam adaptar Krita a una altra versió de les Qt i de KDE, el qual va ser molta faena

146
00:14:43,920 --> 00:14:51,440
Un estiu en el terrat quan era realment càlid, 
vaig agafar el codi de creació de scripts en Python de l'editor de text

147
00:14:51,440 --> 00:14:58,280
Kate de KDE i vaig copiar-lo, solucionar-ho i fent que Krita torne a poder crear scripts. Esta és la base de la nostra

148
00:14:58,280 --> 00:15:05,760
API actual de creació de scripts en Python, la qual està una mica limitada, però està creixent i encara hi és.

149
00:15:05,760 --> 00:15:11,920
Podeu fer qualsevol cosa que vos vinga de gust amb la interfície d'usuari i els scripts. I llavors, és clar,

150
00:15:11,920 --> 00:15:19,200
vam tindre aquell enorme projecte de codi d'estiu (GSoC) on nosaltres 
vam implementar el suport per al SeExpr de Disney.

151
00:15:19,200 --> 00:15:25,699
Fa realment possible escriure un script, generar coses que ompliran les teues… les vostres capes amb el que vulgueu

152
00:15:25,699 --> 00:15:31,240
De fet, no estem segurs de si s'utilitza molt, però és extremadament potent. Així que sí! Krita és molt

153
00:15:31,240 --> 00:15:37,120
personalitzable, i inclús a part d'açò, la majoria de Krita són connectors. Cada eina és un connector. Si coneixes

154
00:15:37,120 --> 00:15:43,560
una mica de C++, vull dir que jo no coneixia C++ quan vaig començar a modificar Krita i em va paréixer bé en

155
00:15:43,560 --> 00:15:50,560
no arribar a fer res útil, ja que va atraure altres col·laboradors, i Krita es va impulsar. Per tant, fem hacking!

156
00:15:50,880 --> 00:15:56,560
Com a últimes paraules. Moltes gràcies per estar disponible per a esta entrevista,

157
00:15:56,560 --> 00:16:02,240
Sé que estàs molt ocupada, així que ho agraïsc molt.

158
00:16:02,240 --> 00:16:05,440
Quin missatge vols enviar als usuaris de Krita?

159
00:16:05,440 --> 00:16:09,000
Gràcies Ramón per l'entrevista. Un últim missatge per a tothom que utilitza Krita.

160
00:16:09,000 --> 00:16:13,520
Utilitza més Krita! Fes art, fes art, fes art! M'encanta veure tot l'art fet amb Krita.

161
00:16:13,520 --> 00:16:18,116
Qualsevol cosa que feu amb Krita està bé per mi. Adeu!

162
00:16:18,116 --> 00:16:24,384
Celebra amb nosaltres el 25è aniversari del desenvolupament de Krita

163
00:16:24,384 --> 00:16:32,312
Qualsevol comentari és molt benvingut. Ací, Ramon com sempre, dient-vos adeu, passeu un bon dia… o nit. Moltes gràcies per veure'l. ❤
