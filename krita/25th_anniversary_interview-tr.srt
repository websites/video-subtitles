﻿1
00:00:01,120 --> 00:00:04,520
Herkese merhaba ve bu yeni videoya hoş geldiniz!

2
00:00:04,520 --> 00:00:08,200
Krita’nın 25. yıldönümünü kutluyoruz

3
00:00:08,200 --> 00:00:13,960
ve bugün burada çok çok özel bir konuğumuz var.

4
00:00:14,000 --> 00:00:16,600
Krita’nın bakımcısı… Halla Rempt!

5
00:00:16,600 --> 00:00:21,600
Ve Halla 20 yılı aşkın süredir Krita’nın bakımcısı olarak görev yapıyor!

6
00:00:21,600 --> 00:00:22,960
Bu harika!

7
00:00:22,960 --> 00:00:25,880
Çok özel bir gün, keyfini çıkarın!

8
00:00:25,880 --> 00:00:28,120
Merhaba Halla, nasılsın?

9
00:00:28,400 --> 00:00:30,200
Merhaba Ramon, seni gördüğüme sevindim!

10
00:00:30,440 --> 00:00:34,960
25. yılı kutlamak önemli bir dönüm noktası.

11
00:00:34,960 --> 00:00:39,200
Krita’nın gelişimi için açık kaynak olmak ne anlama geliyor?

12
00:00:39,800 --> 00:00:45,120
Son 25 yılda Krita’nın gelişimi açısından açık kaynak olmak ne anlama geliyordu?

13
00:00:45,120 --> 00:00:50,520
Bunun kesinlikle gerekli olduğunu söyleyebilirim ve bunun iki nedeni var.

14
00:00:50,520 --> 00:00:55,680
Birincisi elbette açık kaynak olmanın yüzlerce kişinin katkılarından yararlanmış olmasıdır.

15
00:00:55,680 --> 00:01:00,440
Krita’nın işleme listesinde 600’den fazla kişi var ve bu yalnızca bir alan.

16
00:01:00,440 --> 00:01:05,120
Web siteleri değil, belgeler değil, yalnızca geliştirme ve bu harika! Ne zaman bir yerde

17
00:01:05,120 --> 00:01:10,720
sıkışıp kalsak, insanlar bize yardım edebilirdi ve pek çok insan da bunu yaptı. Son olarak belki de daha da önemlisi,

18
00:01:10,720 --> 00:01:17,200
Krita artık GPL kapsamında açık kaynak kodlu özgür bir yazılım olduğundan asla kapatılamaz. Yıllar boyunca rakiplerin

19
00:01:17,200 --> 00:01:22,800
Adobe gibi şirketler tarafından satın alındıkları için ortadan kaybolduğunu gördüğümüzde,

20
00:01:22,800 --> 00:01:27,440
katkıda bulunanlar olduğu sürece Krita’nın devam edeceğine güvenebileceğimizi biliyorduk.

21
00:01:27,440 --> 00:01:32,680
Yani açık kaynak olmak tamamen temel bir değerimiz, bu asla değişmeyecek!

22
00:01:32,760 --> 00:01:35,240
Vay! bu çok ilginç bir bilgi.

23
00:01:35,240 --> 00:01:40,240
Krita bu 25 yıldır ortalıkta

24
00:01:40,240 --> 00:01:42,200
ve bu inanılmaz bir yolculuktu.

25
00:01:42,200 --> 00:01:46,160
Krita bu 25 yıl boyunca nasıl evrildi?

26
00:01:46,200 --> 00:01:50,240
25 yıl, oldukça uzun bir yolculuk… Bu yolculuğun tamamı boyunca orada değildim.

27
00:01:50,240 --> 00:01:54,840
Belki yalnızca 21 yıl ki bu hala uzun bir süre. Projeye katıldığımda

28
00:01:54,840 --> 00:01:58,960
Krita’nın nasıl göründüğüne baktığımda çizim bile yapamıyordun.

29
00:01:58,960 --> 00:02:02,720
Görselleri yükleyebilir, katman ekleyebilir ve katmanları taşıyabilirdin, hepsi bu.

30
00:02:02,720 --> 00:02:08,320
Biz de bunun üzerine yapmaya başladık. Krita’nın tüm çekirdeğini yeniden yazdık. Çekirdek 4 kez yeniden yazıldı

31
00:02:08,320 --> 00:02:15,680
ve Krita her seferinde daha başarımlı hale geldiğinde araçlar eklemeye başladık, Krita’nın openGL tabanlı bir tuvali

32
00:02:15,680 --> 00:02:23,640
desteklemesi, GPU’nun Photoshop’tan önce ivmelendirilmesi gibi aşamalar eklemeye başladık. CMYK bizde çoktan vardı.

33
00:02:23,640 --> 00:02:28,040
Giderek daha eğlenceli, daha yararlı şeyler uygulamaya başladık ve sonra büyük atılım geldi.

34
00:02:28,040 --> 00:02:33,800
Bu oturumları yaptığımızda kullanıcılarımızı dinlemeye başladık. Krita’nın geliştiricilerinin

35
00:02:33,800 --> 00:02:38,960
ve katkıda bulunanlarının bir araya geldiği geliştirme hız koşuları, Blender Studios’taki hız koşumuzdan

36
00:02:38,960 --> 00:02:44,000
bu yana çoğunlukla birlikte çalışıyoruz ve gelecek yıl ne yapacağımızı

37
00:02:44,000 --> 00:02:49,560
tartışıyoruz ve ardından sanatçılardan Krita ile çalışarak “Demo” yapmalarını istiyoruz.

38
00:02:49,560 --> 00:02:55,000
Ne yapmamızı istiyorsunuz diye sordular ve işte sana bir saat, resim yapmanı izleyeceğiz, umarım sıkıntı yoktur,

39
00:02:55,000 --> 00:03:00,320
istediğin her şeyi söyleyebilirsin, şikayet edebileceğin her şeyden şikayet edebilirsin. Her şey yolunda ve biz

40
00:03:00,320 --> 00:03:03,960
geliştiricilerin karşılık vermesine izin vermedik. Sana yardım etmemize de izin verilmiyor;

41
00:03:03,960 --> 00:03:11,080
çünkü sorunların ne olduğunu görmek istiyoruz. Ve bu tür bir kullanıcı gözlemi, yaratımı hızla geliştirmemize gerçekten yardımcı oldu.

42
00:03:11,080 --> 00:03:18,000
Krita’nın aklına gelen en yaratıcı kullanımı neydi?

43
00:03:18,000 --> 00:03:20,680
Vay! Bunu hiç düşünmemiştim!

44
00:03:20,680 --> 00:03:27,120
Krita’nın en değişik amaçlı kullanımı? Sanırım yıldız haritaları yapmak için

45
00:03:27,120 --> 00:03:34,120
Krita’nın yüksek bit derinliği yeteneklerini kullanmak isteyen kişi olabilir. Muazzam astronomik fotoğraflar kullanıyordu

46
00:03:34,120 --> 00:03:43,680
ve onlarla çalışmak istedim ve açıkçası ben bunun için VIPS kullanırdım (aslında bunu söyledim ona). Ama oldukça farklı bir bakış açısıydı.

47
00:03:43,880 --> 00:03:49,440
Birçok kullanıcı Krita’nın arkasındaki takımı neyin motive ettiğini merak edebilir.

48
00:03:50,440 --> 00:03:56,520
Bizi ne motive ediyor? Sanırım bir bencil ve bir tane de bencil olmayan neden var. Bencil neden,

49
00:03:56,520 --> 00:04:02,320
bunun hayatımda yaptığım en eğlenceli iş olması. En az altı ya da yedi farklı şirkette çalıştım

50
00:04:02,320 --> 00:04:09,000
ve bu şirketlerin yalnızca birinde üzerinde geliştirdiğim yazılımı piyasaya sürdüm, bazı şirketler yalnızca

51
00:04:09,000 --> 00:04:15,600
para harcamak için yazılım geliştiriyor, bu çılgınlık; ancak Krita kullanıcıların

52
00:04:15,600 --> 00:04:21,920
eline geçiyor. İşte bencil olmayan kısım da burada devreye giriyor; çünkü bu kullanıcılar bize

53
00:04:21,920 --> 00:04:29,200
en tatlı şeyleri anlatıyorlar, mesela Malezya’daki birinden bir e-posta aldığımda olduğu gibi. Ailesi ona

54
00:04:29,200 --> 00:04:33,840
“kızların çizim yapmasına gerek yok, o yüzden sana çizim yazılımı almayacağız” demiş.

55
00:04:33,840 --> 00:04:39,520
Kardeşi ona bir çizim tableti vermiş, o da Krita ile resim çizmeye başlamış ve

56
00:04:39,520 --> 00:04:46,120
bana coşkulu bir e-posta gönderdi: “Artık resim yapabiliyorum, teşekkür ederim, teşekkür ederim, teşekkürler” ve bu o kadar

57
00:04:46,120 --> 00:04:51,800
motive edici ki bize inanılmaz bir his veriyor, eğer Krita’yı seviyorsanız bize onu beğendiğinizi söylemeyi düşünün;

58
00:04:51,800 --> 00:04:58,320
çünkü normalde sadece hata raporlarını görürüz. Hata raporları yararlıdır, eğer hata bildirimi yapıyorsanız teşekkür ederiz!

59
00:04:58,320 --> 00:05:03,080
Ancak bazen çok fazla oldukları için biraz moral bozucu olabiliyorlar.

60
00:05:03,200 --> 00:05:09,400
Krita’nın ticari kullanımı hakkında bize daha fazla bilgi verebilir misin?

61
00:05:09,400 --> 00:05:16,240
Krita’nın ticari kullanımı. Tabii ki sıkıntı yok. Tüm Sık Sorulan Sorular listelerimizde (SSS)

62
00:05:16,240 --> 00:05:22,280
tüm Hakkında kutularımızda bunu açıkça söylüyoruz. Ancak ticari kullanım aslında oluyor.

63
00:05:22,280 --> 00:05:29,160
Bunu ilk kez Vancouver’daki SIGGRAPH’ta Krita’yı tanıttığımız zaman duymuştum.

64
00:05:29,160 --> 00:05:35,694
Ton Roosendaal’ın gerçekten faydalı yardımı sayesinde Blender’ın yanında bir standımız vardı,

65
00:05:35,694 --> 00:05:41,600
sanki çok çok ilgi var gibiydi, pek çok insan bizimle konuşmak istiyordu ve sonra bir adam yanıma geldi

66
00:05:41,600 --> 00:05:49,680
ve bana şimdilik benden duymuş olma; ancak -şşşş- Disney tarafından sanırım bir canlandırma olan

67
00:05:49,680 --> 00:05:55,200
“Little, Big”in tanıtımı için Krita’yı kullanıyorduk, artık emin değilim,

68
00:05:55,200 --> 00:06:01,560
o yüzden bu benim Krita’nın ticari kullanımını ilk duyduğum zamandı ve sanırım 2014 yılıydı.

69
00:06:01,560 --> 00:06:06,480
Sponsorluk almaya başladığımız ilk zamandan bu yana insanlarla iletişim halindeyiz ve

70
00:06:06,480 --> 00:06:14,360
insanların Krita’yı oyunlar, filmler, kitaplar vb. için ticari amaçla kullandığını duyuyoruz ve bu gerçekten mutluluk verici.

71
00:06:14,360 --> 00:06:23,240
Peki o zaman, Krita’ya yardım etmek istiyoruz, Krita nasıl finanse ediliyor, Gelecek için ne kadar sürdürülebilir?

72
00:06:23,240 --> 00:06:28,160
Bundan zaten biraz söz etmiştik. Temelde iki varlık var. Bir yanda

73
00:06:28,160 --> 00:06:36,600
kâr amacı gütmeyen Krita Vakfı var, diğer yanda kâr amaçlı “Halla Rempt Software” var, o da benim.

74
00:06:36,600 --> 00:06:44,000
Aktiviteleri ikisi arasında paylaştırdık. Vakıf bağış topluyor, bu da vergiden muaf olduğu anlamına geliyor,

75
00:06:44,000 --> 00:06:50,400
Halla Rempt Software DVD’ler ve benzeri şeyler satıyor, ancak elbette bu günlerde bunlardan artık neredeyse hiç gelirimiz olmuyor;

76
00:06:50,400 --> 00:06:58,080
çünkü YouTube’da harika öğreticilerimiz var ❤. Krita’yı çeşitli uygulama mağazalarında satıyoruz.

77
00:06:58,080 --> 00:07:06,400
İki gelir akışının birleşimi, fonun tek seferlik bağışları, mağazalardan elde edilen gelir, şu anda sponsor olduğumuz

78
00:07:06,400 --> 00:07:13,000
geliştirici ekibimize ve Ramon’un videolarına sponsor olmak için yeterli, yani bu oldukça sürdürülebilir,

79
00:07:13,000 --> 00:07:21,160
en azından bunu 2015’ten beri yapıyoruz, 2017’de eklenen mağazalarla birlikte iyi bir iş çıkarıyoruz.

80
00:07:21,160 --> 00:07:28,520
Topluluk Krita’yı nasıl destekliyor ve Krita da topluluğunu nasıl destekliyor?

81
00:07:28,520 --> 00:07:33,360
Krita’nın herkes için sadece bir hobi olduğu zamanlarda

82
00:07:33,360 --> 00:07:39,120
çoğumuzun sayısal çizim tabletlerine erişimi yoktu. Krita’da çalışmaya başladım; çünkü

83
00:07:39,120 --> 00:07:47,362
doğum günümde bir tane aldım; ancak bozuldu ve iki Wacom tableti ve sanat kalemi almak için bağış toplama etkinliği yaptık!

84
00:07:47,362 --> 00:07:54,280
İşte o zaman, özellikle özgür yazılım topluluğunun gerçekten destekleyici olduğunu fark ettik,

85
00:07:54,280 --> 00:08:03,560
o zamanlar sanatçılar bile yoktu ve daha sonra, düzenli olarak katkıda bulunan Slovakyalı kod öğrencimiz Lukas geldi;

86
00:08:03,560 --> 00:08:09,800
tezini Krita’nın fırça işletkeleri üzerinde yazdı.

87
00:08:09,800 --> 00:08:15,400
Bunun için 10 üzerinden 10 aldı; çünkü harika bir çalışmaydı ve profesörleri

88
00:08:15,400 --> 00:08:21,520
öğrencilerinin gerçek dünyada kullanılan bir şey ile çalışmasına o kadar şaşırmışlardı ki!

89
00:08:21,520 --> 00:08:28,320
Aynı zamanda Krita’yı Qt3’ten Qt4’e taşıyorduk, hiçbir şeyin işe yaramıyordu; her şeyi yeniden yazıyorduk,

90
00:08:28,320 --> 00:08:34,720
her şey kelimelerle anlatılamayacak kadar “bozuktu”, bu gerçekten korkunç bir şeydi! Bu noktada Lukas dedi ki:

91
00:08:34,720 --> 00:08:40,960
“Üniversitedeki son üç ayımın staj olarak geçmesi gerekiyor. İlgili, faydalı şeyler yapmam gerekiyor,

92
00:08:40,960 --> 00:08:47,160
biraz da cep harçlığı olsa iyi olurdu.” Ve o zamanlar Krita inanılmaz derecede

93
00:08:47,160 --> 00:08:55,720
hatalı olduğundan ve Krita’yı kullanışlı bir duruma getirmek için hobi kodlayıcıları olarak mücadele ettiğimizden,

94
00:08:55,720 --> 00:09:04,480
hadi Lukas’a hataları düzeltmesi için 3 ay boyunca fon sağlayalım. BÜYÜK bir başarıydı! Başlangıçta Lukas kirasını ödemek

95
00:09:04,480 --> 00:09:12,840
için biraz para istiyordu; ancak o zaman ona her ay makul bir miktarda ödeme yapabildik,

96
00:09:12,840 --> 00:09:20,400
bağış toplama etkinliğinin sonucunda pek çok sanatçı, pek çok coşkulu insan Krita’yı destekledi.

97
00:09:20,400 --> 00:09:27,520
Üç ay altı ay oldu ve kısa bir süre sonra Dmitriy Kazakov, Krita ile yaptığı ilk GSOC’den

98
00:09:27,520 --> 00:09:34,160
bu yana harika işler yapmayı sürdürüyordu. Üniversiteyi bitirecekti ve ben onun başka bir

99
00:09:34,160 --> 00:09:39,960
işe girmesini ve proje yüzünden onu kaybetmesini istemedim. Böylece, bize çok fazla tanıtım kazandıran ve yorucu olan bazı düzenli

100
00:09:39,960 --> 00:09:46,400
bağış toplama etkinlikleri yapmaya başladık; ancak bunu yapmak eğlenceliydi ve Dmitriy ücretli geliştirici olarak

101
00:09:46,400 --> 00:09:49,880
hâlâ tam zamanlı olarak Krita üzerinde çalışıyor; yani her şey çok güzel oldu.

102
00:09:50,000 --> 00:09:57,360
Krita topluluğunun Krita’nın gelişimine yardım etmek için geldiği bir zamanı hatırlıyor musun?

103
00:09:57,600 --> 00:10:01,600
En kötü an, Hollanda vergi dairesinin şunu fark etmesiydi…

104
00:10:01,600 --> 00:10:07,040
hadi bu başımı ağrıtıyor karmaşık şeyleri geçelim, Krita vakfından tek seferde

105
00:10:07,040 --> 00:10:10,720
24.000 € almak istediler, biz söylediğimizde bu para bizde yoktu,

106
00:10:10,720 --> 00:10:14,800
dünya bizi desteklemek için koştu,

107
00:10:14,800 --> 00:10:21,000
bu harikaydı aslında o andan itibaren asla arkamıza bakmadık. Destekleyici bir topluluğa sahibiz

108
00:10:21,000 --> 00:10:27,320
ve bir kalkınma fonumuz var, sizi de katılmaya davet ediyorum,

109
00:10:27,320 --> 00:10:35,280
fiş için özür dilerim, Krita’yı çeşitli uygulama mağazalarında satıyoruz. Uygulama mağazaları

110
00:10:35,280 --> 00:10:43,480
üzerinde çalışmak kesinlikle çok zamn alıyor; ancak bize aylık olarak ödeme yapıyor, bizi destekledikleri için topluluğumuza çok teşekkürler!

111
00:10:43,480 --> 00:10:50,840
Krita, yazılımı öğrenmede ve hatta yazılımı kodlamada kullanıcılara nasıl yardımcı oluyor?

112
00:10:51,280 --> 00:10:56,520
Krita’yı yeni başlayanlar için erişilebilir ve hatta öğrenilebilir hale getirmeye çalışıyoruz;

113
00:10:56,520 --> 00:11:03,640
ancak elbette ilk etapta var olan kullanıcı arayüzü standartlarını korumaya çalışıyoruz. Tuhaf yeni şeyler yapmaya çalışmıyoruz.

114
00:11:03,640 --> 00:11:09,488
İnsanların her şeyi alıştıkları yerde bulabilmelerini istiyoruz.

115
00:11:09,488 --> 00:11:15,920
Son 5, 10, 20 veya 30 yıldır bilgisayar kullandıysanız Krita’da Krita’yı kullanılabilir

116
00:11:15,920 --> 00:11:22,600
kılan hiçbir şey sizi gerçekten şaşırtmayacaktır. Elbette kullanıcı arayüzü (UI) uzmanımız Scott Petrovic

117
00:11:22,600 --> 00:11:29,840
ile de birlikte çalışıyoruz ve kendisi yeni özellikler tasarlamamıza yardımcı oluyor. Şimdi, uygulamanın kendi düzeyinin

118
00:11:29,840 --> 00:11:36,560
ötesine geçerseniz o zaman belgelendirme gerçekten önemlidir. Belgelendirme web sitemize oldukça fazla zaman ve

119
00:11:36,560 --> 00:11:44,440
çaba harcıyoruz: “docs.krita.org” yalnızca tüm menü seçeneklerini listelemiyor, aynı zamanda size iş akışıyla ilgili

120
00:11:44,440 --> 00:11:51,600
şeyler hakkında eğitimler de veriyor. Sayısal boyama uygulamalarını kullanan bir ofis kullanıcısıysanız göze

121
00:11:51,600 --> 00:11:57,280
çarpmayabilecek şeyler çünkü bu konuda açık olalım, Sayısal Boyama her şeyden ayrı bir araçtır.

122
00:11:57,840 --> 00:12:05,720
Yani evet. Ben de yağlı boya kullanarak analog resim yapıyorum ve eskiz ve heykel çiziyorum

123
00:12:05,720 --> 00:12:12,240
ve ardından Krita’yı kullanarak çizmeye çalıştığımda öğrenilen alışkanlıkların

124
00:12:12,240 --> 00:12:18,600
çoğunun sayısal boyama uygulamalarına aktarılmadığını biliyorum, bu nedenle eğitimlerin

125
00:12:18,600 --> 00:12:24,920
olduğu kapsamlı bir web sitemiz var, yönergeler, ipuçları, aynı zamanda harika bir topluluğa sahibiz ve

126
00:12:24,960 --> 00:12:34,202
Raghavendra Kamath (raghukamath) sayesinde, krita-artists.org adında bir kullanıcı formu web sitemiz var, tıpkı Blender Artists gibi,

127
00:12:34,202 --> 00:12:38,120
tamamen buna dayanıyor. Bu yinelenen bir tema. Ne zaman yeni bir şeye ihtiyacımız olduğunu düşünsem

128
00:12:38,120 --> 00:12:43,320
önce Ton’un Blender için ne yaptığına bakıyorum, sonra bunu kopyalıyorum ve bunu

129
00:12:43,320 --> 00:12:49,040
itiraf etmekten utanmıyorum. Krita sanatçı forumumuz, insanların soru sorabileceği ve

130
00:12:49,040 --> 00:12:54,640
aynı zamanda daha önce sorulan soruların yanıtlarını da bulabileceği devasa bir kaynaktır.

131
00:12:54,640 --> 00:13:01,280
Ve Krita kanalımız için videolara çok fazla zaman ve para yatırıyoruz, bu yüzden bizimle çalıştığın için teşekkürler Ramón!

132
00:13:01,360 --> 00:13:11,160
Krita ne kadar özelleştirilebilir ve kullanıcılar ne tür eklentiler veya uzantılar oluşturabilir veya kullanabilir?

133
00:13:11,840 --> 00:13:18,760
Krita gerçekten gençken Cyrille Berger, Krita için JavaScript, Ruby ve Python

134
00:13:18,760 --> 00:13:24,960
ile yazılan ilk komut dosyası arayüzünü ilk kez yarattı. Beklemek! bu aslında ilk komut dosyası arayüzümüz değil.

135
00:13:24,960 --> 00:13:30,120
KDE’nin JavaScript arayüzü. Demek istediğim, bugünlerde Firefox dışındaki hemen hemen

136
00:13:30,120 --> 00:13:39,640
her tarayıcının KDE’nin KDE tabanlı bir web tarayıcısı olan KHTML üzerindeki çalışmasına dayandığını biliyorsunuz. Bu elbette JavaScript işletkesiyle geldi.

137
00:13:39,640 --> 00:13:47,720
Ve bu JavaScript işletkesi, Krita’yı betik yazılabilir hale getirmek için kullanabileceğimiz ayrı bir şeydi,

138
00:13:47,720 --> 00:13:52,560
yani bu ilk şeydi. yani önce JavaScript’imiz vardı, sonra onu Kross’la değiştirdik ki

139
00:13:52,560 --> 00:14:02,720
bu da Ruby, Python vb.’yi olanaklı kılan şeydi. Sonra bu günlerde çoğunlukla unutulan OpenGTL’yi ekledik, Hollywood’da

140
00:14:02,720 --> 00:14:09,360
kullanılan bir şeyin bire bir klonu yeniden uygulandı, daha doğrusu Cyrille Berger bunu yeniden uyguladı

141
00:14:09,360 --> 00:14:15,920
ve tamamen yeni renk alanları oluşturmak da dahil olmak üzere birçok şey için kullanılabilir.

142
00:14:15,920 --> 00:14:22,920
Gerçekten harikaydı ama Cyrille evlendi ve projeden ayrıldı ve bu sürdürülemez bir durumdu, biz de bu işi bıraktık.

143
00:14:22,920 --> 00:14:29,400
Aynı zamanda Krita’yı KDE Frameworks’ün yeni bir sürümüne taşıdık ve Kross düzgün çalışmayı bıraktığından desteği bıraktık.

144
00:14:29,400 --> 00:14:35,760
Neyse ki o zamanlar Krita’nın çok az kullanıcısı vardı, dolayısıyla kimse gerçekten etkilenmedi.

145
00:14:35,760 --> 00:14:43,920
Daha sonra Krita’yı Qt ve KDE’nin başka bir sürümüne taşıdık ki bu çok iş gerektiriyordu.

146
00:14:43,920 --> 00:14:51,440
Bir yaz, hava gerçekten sıcakken çatı terasında, KDE’nin metin düzenleyicisi

147
00:14:51,440 --> 00:14:58,280
Kate’den Python komut dosyası kodunu aldım ve bunların hepsini kopyalayıp Krita’yı betik yapılabilir hale getirmek için düzelttim.

148
00:14:58,280 --> 00:15:05,760
Bu, biraz sınırlı olan şu anki Python betik oluşturma API’mizin temelidir, ancak büyüyor ve hâlâ orada.

149
00:15:05,760 --> 00:15:11,920
İçinde kullanıcı arayüzü benzeri her şeyi yapabilir ve istediğiniz gibi betikler oluşturabilirsiniz.

150
00:15:11,920 --> 00:15:19,200
Ve sonra elbette Disney’in SeExpr için destek eklediğimiz devasa bir Google Summer of Code projemiz (GSOC) vardı.

151
00:15:19,200 --> 00:15:25,699
Betik yazmayı, katmanlarınızı istediğiniz herhangi bir şeyle dolduracak şeyler oluşturmayı gerçekten olanaklı kılar.

152
00:15:25,699 --> 00:15:31,240
Aslında ne kadar kullanıldığından emin değiliz ama son derece güçlü. Yani evet! Krita çok

153
00:15:31,240 --> 00:15:37,120
özelleştirilebilir ve hatta bunun dışında Krita’nın çoğu parçası eklentidir. Her araç bir eklentidir.

154
00:15:37,120 --> 00:15:43,560
Eğer biraz C++ biliyorsanız, yani ben Krita’yı hacklemeye başladığımda C++ bilmiyordum ve yararlı bir şey yapma konusunda başarısız oldum,

155
00:15:43,560 --> 00:15:50,560
bu da diğer katkıda bulunanların ilgisini çekti ve Krita başarıya ulaştı, o yüzden oynamaya başlayalım!

156
00:15:50,880 --> 00:15:56,560
Son sözler: Bu röportaj için çok teşekkür ederim,

157
00:15:56,560 --> 00:16:02,240
gerçekten meşgul olduğunu biliyorum, bu yüzden bunu çok takdir ediyorum.

158
00:16:02,240 --> 00:16:05,440
Krita kullanıcılarına hangi mesajı göndermek istersin?

159
00:16:05,440 --> 00:16:09,000
Ramón, röportaj için çok teşekkür ederim. Krita kullanan herkese son bir mesajım:

160
00:16:09,000 --> 00:16:13,520
Krita’yı daha fazla kullanın! Sanat yapın, sanat yapın, sanat yapın! Krita ile yapılan tüm sanat eserlerini görmeyi seviyorum.

161
00:16:13,520 --> 00:16:18,116
Krita ile yaptığınız sürece ne olduğu önemli değil. Hoşça kalın!

162
00:16:18,116 --> 00:16:24,384
Krita’nın geliştirilmesinin 25. yıldönümünü bizimle kutlayın.

163
00:16:24,384 --> 00:16:32,312
Herhangi bir geri bildirimi her zaman memnuniyetle karşılıyoruz. Ramon her zamanki gibi burada, veda ediyor, iyi günler veya geceler. İzlediğiniz için çok teşekkürler. ❤
