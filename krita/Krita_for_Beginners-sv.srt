1
00:00:00,130 --> 00:00:02,040
Först av allt, hej allesamman

2
00:00:02,040 --> 00:00:07,140
Jag vill berätta att den här videon är till för personer som just börjat med digital målning och

3
00:00:07,330 --> 00:00:13,620
valt <b><font color=#ff0000>Krita</b></font> som först alternativ att lära sig. Så om du är på medelnivå eller en avancerad användare

4
00:00:14,139 --> 00:00:18,839
<i>ser du nog saker i den här videon som du redan kan. I videon</i>

5
00:00:18,840 --> 00:00:24,719
pratar vi om: nerladdning av Kirta, drivrutiner, hur man installerar Krita på rätt sätt,

6
00:00:24,789 --> 00:00:28,079
välkomstskärmen, hur vi skapar vår första bild,

7
00:00:28,420 --> 00:00:35,790
penslar, arbetsytan och färger, hur bilder och arbete sparas säkert. Så är du redo?

8
00:00:36,850 --> 00:00:38,850
Låt oss börja

9
00:00:44,560 --> 00:00:47,940
Allt har förändrats mycket sedan de första digitala målarprogrammen

10
00:00:48,550 --> 00:00:56,430
Nuförtiden lever vi i spännande tider eftersom vi är omgivna av kreativa program. Idag kan vi mer än någonsin

11
00:00:56,430 --> 00:00:59,159
måla med många olika verktyg och det är jättebra.

12
00:00:59,160 --> 00:01:03,360
Men ibland blir vi överväldigade av mängden alternativ som är tillgängliga.

13
00:01:03,790 --> 00:01:11,069
Om du är här, är det för att du är intresserad av Krita och vill börja måla och skapa din egna värld

14
00:01:11,260 --> 00:01:16,259
Kanske har du sett en vacker bild gjord med Krita och det

15
00:01:16,570 --> 00:01:19,410
ökade din nyfikenhet på Kritas programvara

16
00:01:19,660 --> 00:01:23,160
Ja, den här videon är till för att göra din första

17
00:01:23,380 --> 00:01:28,499
erfarenhet av Krita enklare. Redan från början har Krita varit ett program med ett

18
00:01:28,780 --> 00:01:34,619
gränssnitt på mellannivå utan för många knappar som distraherar eller skrämmer dig. Det är alltid uppskattat

19
00:01:35,259 --> 00:01:38,489
eftersom vi inte behöver lära oss ett komplext gränssnitt

20
00:01:38,490 --> 00:01:44,580
Men vi kan justera det att passa våra behov. Om du inte har installerat programmet följer några tips.

21
00:01:44,619 --> 00:01:45,520
För det första,

22
00:01:45,520 --> 00:01:47,520
Var hämtar man Kritas programvara?

23
00:01:47,680 --> 00:01:53,610
Många letar efter programvara på sidor som erbjuder många program. Det rekommenderas inte

24
00:01:53,800 --> 00:01:58,559
eftersom man kan få otrevliga överraskningar som skadliga program och spionprogram.

25
00:01:59,170 --> 00:02:01,170
Så om du vill måla säkert

26
00:02:01,390 --> 00:02:08,970
är den bästa platsen för nerladdning av Krita den officiella webbplatsen, Krita.org. Länkarna finns i

27
00:02:09,280 --> 00:02:13,830
beskrivningen nedan. Vi kan leta efter nerladdningsknappen eller bara klicka på den stora knappen

28
00:02:14,549 --> 00:02:20,909
"Get Krita Now", en mycket beskrivande knapp. Sedan kan vi välja plattformen vi vill ha för Krita.

29
00:02:21,010 --> 00:02:23,010
Det vill säga, om jag exempelvis använder

30
00:02:23,500 --> 00:02:28,139
Linux, Windows eller Macintosh. Vi kan ladda ner Krita och medan nerladdningen pågår

31
00:02:28,540 --> 00:02:35,249
Låt oss ta en titt på vad vi har här. Som du kan se, kan vi också gå till Windows Store eller en Steam-plattform

32
00:02:35,590 --> 00:02:39,479
Om vi vill hämta Krita som en app. Det kräver betalning

33
00:02:39,480 --> 00:02:46,259
men hjälper till att betala huvudutvecklaren av Krita, Boudewijn, mycket viktigt. Här kan du också se något som heter

34
00:02:46,630 --> 00:02:51,540
nattliga byggen. Krita har två olika versioner. Så vad föredrar du?

35
00:02:52,380 --> 00:02:54,859
den stabila version eller utvecklingsversionen?

36
00:02:55,500 --> 00:02:58,130
Vilken Krita är bäst för mig?

37
00:02:58,740 --> 00:03:00,380
Konstig fråga, inte sant?

38
00:03:00,380 --> 00:03:00,870
nåja ...

39
00:03:00,870 --> 00:03:03,740
Om du behöver stabilitet för att slutföra ett projekt

40
00:03:04,140 --> 00:03:08,509
bekvämt, är det vanliga valet att ladda ner den stabila versionen

41
00:03:08,640 --> 00:03:12,830
Det är den som har huvudfunktionerna för att göra färdigt jobbet

42
00:03:12,890 --> 00:03:20,330
Men om du vill prova de senaste funktionerna och till och med sedan ge återkoppling, då kan du ladda ner

43
00:03:21,030 --> 00:03:27,020
utvecklingsversionen. Och ja, utvecklingsutgåvan kan krascha, och ja, det är bra

44
00:03:30,060 --> 00:03:36,229
Det är bra för att det låter oss förfina utvecklingen och gör Krita så polerat som

45
00:03:36,360 --> 00:03:40,190
möjligt för nästa utgåva och det som är bra är att man kan ladda ner

46
00:03:40,560 --> 00:03:46,970
nattliga byggen och inte ta bort den stabila utgåvan. Så man kan välja. Vi har redan laddat ner Krita

47
00:03:47,120 --> 00:03:49,120
Och vad gäller nu? Drivrutiner

48
00:03:50,280 --> 00:03:56,179
Det är bekvämt att ladda ner de senaste drivrutinerna för vår grafiska ritplatta. Att hålla utrustningen

49
00:03:56,430 --> 00:04:02,810
uppdaterad är alltid en bra idé och vi kan undvika många problem både i Krita och annan programvara

50
00:04:03,299 --> 00:04:06,739
Så investera en del tid på att göra allting klart.

51
00:04:06,739 --> 00:04:11,449
Jag väntar på dig. När väl Krita har laddats ner och du har uppdaterat drivrutinerna,

52
00:04:11,450 --> 00:04:18,169
kan du installera Krita. Om du stöter på problem med ritplattan, kontrollera att den är kompatibel med Krita

53
00:04:18,810 --> 00:04:24,649
Krita täcker ett stort antal enheter, men ibland kan vi stöta på konstigt beteende

54
00:04:24,690 --> 00:04:28,910
Så här är länken för att kolla det. Också i beskrivningen nedan

55
00:04:29,370 --> 00:04:35,419
Vissa användare har rapporterat problem med "ringar" runt markören på Windows 10

56
00:04:35,419 --> 00:04:38,299
Så titta på den här videon om du befinner dig i den situationen

57
00:04:39,120 --> 00:04:44,929
Att installera Krita är inte komplicerat när det väl är nerladdat klicka på filen för att installera

58
00:04:45,810 --> 00:04:52,070
Säkerställ också att Windows shell är markerat för att se krita filer i filbläddraren i Windows.

59
00:04:52,919 --> 00:04:56,809
Om du använder Linux kan du ladda ner en appimage

60
00:04:57,570 --> 00:05:00,830
När den väl är nerladdad måste du göra filen körbar

61
00:05:01,740 --> 00:05:06,569
Titta på kontrollrättigheterna. Högerklicka bara på appimage-filen och

62
00:05:06,879 --> 00:05:09,869
välj egenskaper och gör den sedan körbar

63
00:05:10,509 --> 00:05:16,619
Okej, öppna Krita så dyker startskärmen upp och läser in resurserna. Efter några sekunder

64
00:05:16,620 --> 00:05:23,969
Nu kan du se programmets gränssnitt. Vi har en sorts välkomstskärm. Skärmen ger dig en mängd viktig information

65
00:05:24,610 --> 00:05:29,550
Vi kan exempelvis se de senaste nyheterna från Krita. Tack vara den här rutan

66
00:05:30,069 --> 00:05:32,968
Länkad till det officiella kontot för Krita målning

67
00:05:33,400 --> 00:05:38,159
Och också mycket intressanta länkar som hjälper dig ta reda på mer om

68
00:05:38,319 --> 00:05:45,028
Kritas värld och gemenskap. Vi kan också gå direkt till handboken och lära oss mycket genom att läsa, bra

69
00:05:45,159 --> 00:05:50,729
innehåll, skapat av Kritas utvecklare. Vi kan gå direkt till forumet och leta efter hjälp.

70
00:05:50,849 --> 00:05:55,199
Registrera bara ett konto, logga in så kan du prata direkt med

71
00:05:55,569 --> 00:06:00,929
utvecklare och dela med dig av dina idéer eller lägga till konstverk för att få återkoppling från andra användare.

72
00:06:01,210 --> 00:06:06,060
Direktkontakt med programmerare och konstnärer, så trevligt! Här i första kolumnen

73
00:06:06,060 --> 00:06:10,259
ser vi "ny fil" med den vanliga genvägen "Ctrl+N"

74
00:06:10,539 --> 00:06:15,869
Om jag klickar på den dyker ett nytt fönster upp och jag ser många alternativ, du

75
00:06:16,270 --> 00:06:22,259
kan välja en mall eller skapa din egen fil med storleken du behöver. Om du inte skapar en bild

76
00:06:22,479 --> 00:06:26,098
fungerar inte vissa delar av Kritas gränssnitt, som ...

77
00:06:26,589 --> 00:06:31,199
penselval, färgval, och så vidare. Bara menyer är tillgängliga.

78
00:06:31,629 --> 00:06:34,619
Det är logiskt eftersom det inte finns något att arbeta på.

79
00:06:34,719 --> 00:06:40,889
Så låt oss skapa vår första bild i Krita. Gå till eget dokument och välj filstorleken

80
00:06:40,889 --> 00:06:44,758
du vill skapa och bekymra dig inte om alla andra alternativ.

81
00:06:44,759 --> 00:06:49,679
Vi tar en titt på den i en annan video. Ange en storlek och klicka på knappen Skapa

82
00:06:49,960 --> 00:06:53,279
När du väl har skapat en bild kan du börja arbeta med den.

83
00:06:53,589 --> 00:06:58,919
Det finns gränssnitt som är mycket komplexa som överväldigar oss.

84
00:06:59,469 --> 00:07:01,469
Det har hänt mig också

85
00:07:01,569 --> 00:07:07,528
Men vad behöver vi verkligen för att måla? Vi behöver penselfärger och en bekväm

86
00:07:08,050 --> 00:07:09,189
arbetsyta

87
00:07:09,189 --> 00:07:11,549
Penslar, arbetsyta och färg

88
00:07:12,250 --> 00:07:13,509
Så penslar...

89
00:07:13,509 --> 00:07:19,809
var finns penslarna, och hur målar man och suddar saker, hur ändrar man deras storlek snabbt och

90
00:07:19,970 --> 00:07:23,470
hur undviker man att gå vilse bland så många penselalternativ.

91
00:07:24,110 --> 00:07:28,330
Kritas penseluppsättning är konstruerad att passa allas grundbehov

92
00:07:28,460 --> 00:07:29,289
för det första

93
00:07:29,289 --> 00:07:32,229
det är bra att försäkra sig om att allt är riktigt

94
00:07:32,570 --> 00:07:40,329
eftersom det kan hända att vi aktiverar "raderingsläge" av misstag och inte kommer ihåg det och plötsligt

95
00:07:40,639 --> 00:07:45,099
målar inte penslarna och vi kan bli förvirrade. Så

96
00:07:45,740 --> 00:07:48,820
verifiera att raderingsknappen är av

97
00:07:48,949 --> 00:07:53,348
Ok! Penslar är normalt på höger sida av gränssnittet

98
00:07:53,690 --> 00:08:01,479
Men du kan ändra var de är placerade genom att bra dra panelen. Om du stänger penselpanelen kan du enkelt återställa den. Gå till

99
00:08:01,759 --> 00:08:08,589
inställningar/paneler/penselförinställning så kommer Krita ihåg den senaste platsen den visades.

100
00:08:10,400 --> 00:08:12,519
Penslarna kan se mycket lika ut

101
00:08:13,340 --> 00:08:17,440
men du vill kanske ha ännu fler om några dagar eller veckor.

102
00:08:18,470 --> 00:08:25,209
Sa jag att i Kritas butik finns en hel uppsättning för att emulera oljemålning, pastell och akvarell.

103
00:08:27,500 --> 00:08:31,720
Krita vet att många penslar kan överväldiga användare.

104
00:08:32,180 --> 00:08:37,839
Så ett system med beteckningar har skapats som kan användas för att bara se vissa penslar

105
00:08:38,479 --> 00:08:40,479
orienterade för ett specifikt arbetsflöde

106
00:08:41,120 --> 00:08:45,039
Låt oss se hur det fungerar. Vi har exempelvis "Måla"

107
00:08:45,709 --> 00:08:47,029
"Skiss"

108
00:08:47,029 --> 00:08:54,789
"Struktur", och så vidare. Varje beteckning har några penslar kopplade till aktiviteten. Exempelvis beteckningen "Struktur"

109
00:08:54,980 --> 00:09:02,380
Har några penslar som hjälper dig skapa snabba struktureffekter. I framtida videor ser vi mer saker om penslar.

110
00:09:03,440 --> 00:09:08,020
Ett annat sätt att leta efter penslar är att använda filtrering, vi kan gå djupare

111
00:09:08,570 --> 00:09:11,770
utforska penslar med penseleditorn, till exempel,

112
00:09:12,230 --> 00:09:19,480
för att lära oss mer om penselgränssnitt. Vi täcker det i en kommande video. Men kanske du inte tycker om att ha penslarna

113
00:09:19,480 --> 00:09:23,980
där, alltid synliga, som om de observerar dig. I så fall kan du använda

114
00:09:24,350 --> 00:09:31,410
snabbtangenten F6 eller gå till kombinationsfönstret längst upp som öppnar fönstret med alla dina penslar

115
00:09:32,410 --> 00:09:37,439
Här kan du använda beteckningssystemet. Du har också ett annat mycket intressant alternativ.

116
00:09:38,140 --> 00:09:41,009
Ibland använder vi inte mer än 10 penslar

117
00:09:41,830 --> 00:09:46,770
Föreställ dig till exempel att vi tecknar med fullskärmsläge utan något gränssnitt

118
00:09:47,050 --> 00:09:54,540
tryck på genvägen "Tabulator", och tryck sedan på höger musknapp, eller pennknappen som du har kopplat till högerklick

119
00:09:55,450 --> 00:10:00,150
Det låter dig se saker som penslar, färger, senast använda färger.

120
00:10:00,150 --> 00:10:06,720
Och också spegla bilden, rotera bilden, med mera. Denna snabbmeny är riktigt kraftfull

121
00:10:06,850 --> 00:10:12,119
Vi ser det också i en annan video. Du har valt en pensel. För att ändra storleken

122
00:10:12,360 --> 00:10:20,320
tryck på skifttangenten och dra markören till höger för att öka storleken eller till vänster för att minska storleken

123
00:10:20,520 --> 00:10:26,220
Vi kan också göra det med snabbtangenter. Nå, det är ingenting bekvämt eller

124
00:10:26,920 --> 00:10:33,020
via skjutreglaget längst upp i gränssnittet, vilket är en bra idé. Något du kommer att märka.

125
00:10:33,030 --> 00:10:35,309
är att om du väljer en pensel och

126
00:10:35,890 --> 00:10:37,810
Ändra storleken, till exempel

127
00:10:37,810 --> 00:10:45,510
Du gör några penseldrag och du säger, hmm, det här ser mycket bra ut, en mycket bra pensel och jag gillar den som den är.

128
00:10:46,090 --> 00:10:50,639
men om du väljer en annan pensel och väljer bort den föregående penseln

129
00:10:51,400 --> 00:10:55,800
Ser du att penseln har återgått till sitt tidigare tillstånd

130
00:10:56,740 --> 00:10:59,820
I så fall var inte dina ändringar för mycket.

131
00:11:00,370 --> 00:11:07,650
Men föreställ dig att du börjar leka med penselparametrarna och till slut inte vet exakt vad du har ändrat.

132
00:11:07,650 --> 00:11:12,869
men penselresultatet är mycket häftigt i detta fall

133
00:11:12,880 --> 00:11:15,460
Det kan vara riktigt irriterande, inte sant?

134
00:11:15,580 --> 00:11:22,300
Det görs för personer som föredrar att använda standardpenslar som de är utan att ändra dem alltför mycket.

135
00:11:22,440 --> 00:11:29,240
Det är bra praxis i början eftersom det hjälper till att bättre förstå skillnaden mellan dem och

136
00:11:29,680 --> 00:11:35,310
möjligheterna man kan få med en enda pensel, och det är orsaken Krita skapade

137
00:11:35,560 --> 00:11:42,859
de "Smutsiga penslarna". Penslar som kan komma ihåg dina ändringar även om du byter penslar om och om igen.

138
00:11:43,350 --> 00:11:45,890
För att använda funktionen måste vi aktivera

139
00:11:46,410 --> 00:11:53,480
"Spara finjusteringar av förinställningar tillfälligt" i Kritas penseleditor för att komma ihåg inställningarna tills det avslutas.

140
00:11:53,640 --> 00:11:58,849
Okej, om du vill spara en ändrad version klicka bara på överskrivningsknappen

141
00:11:59,040 --> 00:12:01,040
Och det är allt

142
00:12:01,140 --> 00:12:04,969
Du har din första ändrade pensel redo att använda

143
00:12:05,269 --> 00:12:11,479
Även om du avslutar Krita. För att använda en bekväm arbetsyta måste vi veta hur grundnavigeringen fungerar

144
00:12:12,000 --> 00:12:17,779
Som zooma, panorera och rotera. Även horisontell spegling kan vara användbart

145
00:12:18,029 --> 00:12:18,890
i Krita

146
00:12:18,890 --> 00:12:23,059
använder vi normalt tangenten "M" som genväg för att spegla vyn

147
00:12:23,370 --> 00:12:30,200
Det är mycket användbart när vi målar under en lång tid. En enkel genväg, men en kraftfull funktion. Färgen

148
00:12:31,110 --> 00:12:37,550
När vi målar måste vi placera formuläret för att välja färger. En plats där man kan välja olika färger.

149
00:12:37,920 --> 00:12:43,490
Hur gör man till exempel sina färger mindre mättade eller ljusare. Exempelvis

150
00:12:44,190 --> 00:12:49,909
Vi kan använda "Avancerad färgväljare" eller en egen "Palett". För närvarande

151
00:12:49,950 --> 00:12:56,390
lämnar vi den som den normalt är eftersom vi kommer att täcka färger i kommande videor.

152
00:12:57,240 --> 00:13:03,109
Spara bilder och arbeta säkert. En högt rekommenderad praxis är att spara ofta

153
00:13:03,570 --> 00:13:08,539
Så gör det. All programvara kraschar någon gång, Nåja ...

154
00:13:08,540 --> 00:13:12,740
Jag antar att du har ritat och testat penslarna som normalt levereras

155
00:13:12,899 --> 00:13:17,659
men vad händer om vi är så begeistrade att vi glömmer spara och

156
00:13:18,000 --> 00:13:21,859
plötsligt slocknar ljuset? För många år sedan var det ett stort problem

157
00:13:21,930 --> 00:13:27,770
Det leder till att du måste göra om allting igen sedan senaste gången du sparade ditt arbete

158
00:13:28,740 --> 00:13:35,990
Men vi lever väl i en teknologisk tidsålder? Så det är därför Krita går längre och hjälper oss, hur då?

159
00:13:36,750 --> 00:13:40,039
Varje gång du öppnar en bild och arbetar på den

160
00:13:40,649 --> 00:13:42,889
Sparar Krita ditt arbete i det tysta

161
00:13:43,260 --> 00:13:50,179
Programmet gör en kopia av filen och tar hand om att spara arbetet var 15:e minut. Det är väl häftigt?

162
00:13:51,059 --> 00:13:58,439
Jag tycker det är det. Om vi vill ändra tiden, måste vi gå till menyn Inställningar/Anpassa Krita sidan Allmänt och

163
00:13:59,319 --> 00:14:02,218
Filhantering och där ändrar vi tiden

164
00:14:02,889 --> 00:14:04,719
Låt oss göra det.

165
00:14:04,719 --> 00:14:11,489
Jag anser att fem minuter är inte så påträngande tid för att spara i fall av någon underlig anledning

166
00:14:12,099 --> 00:14:18,959
du vill inaktivera alternativet, klicka helt enkelt på alternativknappen "Aktivera spara automatiskt"

167
00:14:19,389 --> 00:14:22,108
Jag skulle  inte göra det. Jag tänker inte göra det.

168
00:14:22,109 --> 00:14:26,098
Jag kan försäkra det. Vad händer om jag vill spara olika stadier av mitt arbete

169
00:14:26,529 --> 00:14:31,859
Då använder vi alternativet spara inkrementell ett bra namn på en mycket användbar funktion

170
00:14:32,319 --> 00:14:34,348
Och varför tycker jag att den är så användbar?

171
00:14:34,719 --> 00:14:38,369
Eftersom som du kanske märkt har den en tillhörande genväg

172
00:14:38,529 --> 00:14:44,669
vilket är "Ctrl+Alt+S" och på så sätt trycker vi bara på genvägen så skapar vi

173
00:14:44,949 --> 00:14:51,478
en inkrementell version av filen vi arbetar på. Om du tycker om den här sortens verktyg som hjälper med produktivitet

174
00:14:51,669 --> 00:14:56,759
tala om det för mig i kommentarerna och gilla Krita. Okej! Det är slutet för denna gången.

175
00:14:56,759 --> 00:14:59,878
Du har fått grunderna men det finns mycket mer.

176
00:15:00,189 --> 00:15:03,898
Glöm inte att prenumerera och dela videon i sociala medier

177
00:15:03,899 --> 00:15:09,719
Och om du tror att det kan hjälpa dina vänner och fler användare av Krita, dela det.

178
00:15:10,179 --> 00:15:15,119
Om du är intresserad av något särskilt ämne eller om du vill föreslå nya ämnen

179
00:15:15,879 --> 00:15:21,028
Tala om det i kommentarerna nedan så kanske det kan bli nästa video

180
00:15:21,969 --> 00:15:28,169
Tack så mycket för att du tittade och var redo för nya videor där jag täcker hur man använder Krita

181
00:15:28,239 --> 00:15:30,239
med mer avancerade funktioner

182
00:15:30,789 --> 00:15:32,789
Hej då
