1
00:00:00,130 --> 00:00:02,040
Hello everybody first of all

2
00:00:02,040 --> 00:00:07,140
I would like to tell you this video is for the people who are beginning with digital painting and

3
00:00:07,330 --> 00:00:13,620
choose <b><font color=#ff0000>Krita</b></font> as the first option to learn. So if you are a mid-level or an advanced user

4
00:00:14,139 --> 00:00:18,839
<i>Probably you will see things in this video that you already know. in this video,</i>

5
00:00:18,840 --> 00:00:24,719
we are going to talk about:  Downloading Krita, drivers, how to install Krita properly,

6
00:00:24,789 --> 00:00:28,079
the welcome screen, how to create our first image,

7
00:00:28,420 --> 00:00:35,790
brushes, the workspace and color, how to save your images and work safe. So are you ready?

8
00:00:36,850 --> 00:00:38,850
Let's get started

9
00:00:44,560 --> 00:00:47,940
Everything has changed a lot since the first digital painting programs

10
00:00:48,550 --> 00:00:56,430
Nowadays, we are living exciting times because we are surrounded by creative applications. Today more than ever we can

11
00:00:56,430 --> 00:00:59,159
paint with lot of different tools and that's great.

12
00:00:59,160 --> 00:01:03,360
But sometimes we are saturated by the amount of options we have available.

13
00:01:03,790 --> 00:01:11,069
If you are here, it is because you are interested in Krita and you want to start painting and creating your own world

14
00:01:11,260 --> 00:01:16,259
Maybe you have seen a beautiful image done with Krita and that

15
00:01:16,570 --> 00:01:19,410
increases your curiosity about the Krita software

16
00:01:19,660 --> 00:01:23,160
Well, this video is to make easier your first

17
00:01:23,380 --> 00:01:28,499
experience with Krita. Since the earliest stages Krita has been a program with a mid-level

18
00:01:28,780 --> 00:01:34,619
Interface without too much buttons that distract or intimidate you, that is always appreciated

19
00:01:35,259 --> 00:01:38,489
Because we don't have to learn a complex interface

20
00:01:38,490 --> 00:01:44,580
But we can adjust it to fit our needs. if you haven't installed the program here you have some tips.

21
00:01:44,619 --> 00:01:45,520
first of all,

22
00:01:45,520 --> 00:01:47,520
Where do I get the Krita software?

23
00:01:47,680 --> 00:01:53,610
Many people look for software in pages that offer a lot of software. This is not recommended

24
00:01:53,800 --> 00:01:58,559
because we can find bad surprises like malware or spyware.

25
00:01:59,170 --> 00:02:01,170
So if you want to paint safe

26
00:02:01,390 --> 00:02:08,970
the best site for downloading Krita is the official website Krita.org. The links will be in the

27
00:02:09,280 --> 00:02:13,830
description below. We can look for the download button or just press the big button

28
00:02:14,549 --> 00:02:20,909
"Get Krita now" a very descriptive button. Then we can choose the platform we want for Krita.

29
00:02:21,010 --> 00:02:23,010
That is, if i use for example

30
00:02:23,500 --> 00:02:28,139
Linux, Windows or Macintosh. We can download Krita and while it's downloading

31
00:02:28,540 --> 00:02:35,249
Let's take a look what we have here. As you can see, we can also go to Windows Store or a steam platform

32
00:02:35,590 --> 00:02:39,479
If we want to get Krita as an app. It's paid content

33
00:02:39,480 --> 00:02:46,259
but helps to pay the main developer of Krita Boudewijn, really important. Also you can see there are something called

34
00:02:46,630 --> 00:02:51,540
nightly builds. Krita has two different versions. So what do you prefer?

35
00:02:52,380 --> 00:02:54,859
stable version or development version?

36
00:02:55,500 --> 00:02:58,130
What Krita is the best for me?

37
00:02:58,740 --> 00:03:00,380
Strange question, isn't it?

38
00:03:00,380 --> 00:03:00,870
well...

39
00:03:00,870 --> 00:03:03,740
If you need stability to finish your project

40
00:03:04,140 --> 00:03:08,509
comfortably, the most usual choice is downloading the stable version

41
00:03:08,640 --> 00:03:12,830
It is the one that has the main features to finish the job

42
00:03:12,890 --> 00:03:20,330
But if you want to be trying the latest features and even give your feedback after that, then you can download

43
00:03:21,030 --> 00:03:27,020
Development version. And yes, the development release can crash, and yes, that's good

44
00:03:30,060 --> 00:03:36,229
It is good because this allow us to refine the development and let Krita as polish it as

45
00:03:36,360 --> 00:03:40,190
Possible for the next release and the good thing is that I can download

46
00:03:40,560 --> 00:03:46,970
Nightly builds and don't delete my stable release. So you choose. We have already downloaded Krita

47
00:03:47,120 --> 00:03:49,120
And now what? Drivers

48
00:03:50,280 --> 00:03:56,179
It is convenient to download the latest drivers for our graphic tablet. Having our equipment

49
00:03:56,430 --> 00:04:02,810
updated is always a good idea and we can save many problems both in Krita and in other software

50
00:04:03,299 --> 00:04:06,739
So invest a little time in getting everything ready.

51
00:04:06,739 --> 00:04:11,449
I wait for you. Once Krita is downloaded and you have updated the drivers,

52
00:04:11,450 --> 00:04:18,169
you can install Krita. If you find issues with tablet verify that your tablet is compatible with Krita

53
00:04:18,810 --> 00:04:24,649
Krita covers a wide range of devices, but sometimes we can find weird behaviors

54
00:04:24,690 --> 00:04:28,910
So here is the link to check it out. Also in the description below

55
00:04:29,370 --> 00:04:35,419
Some users have reported problems with "rings" around the cursor in Windows 10

56
00:04:35,419 --> 00:04:38,299
So, please see this video if you are in this situation

57
00:04:39,120 --> 00:04:44,929
Installing Krita is not complicated once it is downloaded do click on the file to install

58
00:04:45,810 --> 00:04:52,070
Also, make sure Windows shell is marked in order to see the krita files in our file browser in Windows

59
00:04:52,919 --> 00:04:56,809
If you are in Linux user you can download  an appimage

60
00:04:57,570 --> 00:05:00,830
Once it is downloaded, then you have to make the file executable

61
00:05:01,740 --> 00:05:06,569
Checking the control permissions. Just right click in your appimage file and

62
00:05:06,879 --> 00:05:09,869
select properties and then make it executable

63
00:05:10,509 --> 00:05:16,619
Okay, open Krita and the splash screen appears loading the resources. After a few seconds

64
00:05:16,620 --> 00:05:23,969
Now you can see the program interface. We have a kind of welcome screen. This screen gives a lot of important information

65
00:05:24,610 --> 00:05:29,550
For example, we can see the latest news from Krita. Thanks to this box

66
00:05:30,069 --> 00:05:32,968
Linked to the official account of Krita painting

67
00:05:33,400 --> 00:05:38,159
And also very interesting links that helps you to learn more about

68
00:05:38,319 --> 00:05:45,028
The world of Krita and his community. Also we can go directly to the manual and learning a lot reading good

69
00:05:45,159 --> 00:05:50,729
contents, created by Krita developers. We can go directly to the forum and look for help.

70
00:05:50,849 --> 00:05:55,199
Just register an account, log in and you can talk directly to

71
00:05:55,569 --> 00:06:00,929
developers and share your ideas or add artwork to get feedback from another users.

72
00:06:01,210 --> 00:06:06,060
Direct contact with programmers and artists. nice! Here on the first column

73
00:06:06,060 --> 00:06:10,259
we see "new file" with the common shortcut "Control+N"

74
00:06:10,539 --> 00:06:15,869
If I click on it a new window appears and I see many options. you

75
00:06:16,270 --> 00:06:22,259
can choose a template or create your own file with the size you need. If you don't create an image

76
00:06:22,479 --> 00:06:26,098
some parts of the Krita's interface doesn't work, like...

77
00:06:26,589 --> 00:06:31,199
Brush selection, color selection, and so on. only menus are available.

78
00:06:31,629 --> 00:06:34,619
This is logical because there is nothing to work with.

79
00:06:34,719 --> 00:06:40,889
So let's make our first image in Krita. go to custom document and choose the file size

80
00:06:40,889 --> 00:06:44,758
you want to create and don't worry about all the options.

81
00:06:44,759 --> 00:06:49,679
We will take a look to them in another video. give a size and click on create button

82
00:06:49,960 --> 00:06:53,279
Once you create an image you can start working on it.

83
00:06:53,589 --> 00:06:58,919
There are interfaces that are very complex and overwhelm us.

84
00:06:59,469 --> 00:07:01,469
it has happened to me too

85
00:07:01,569 --> 00:07:07,528
But what do we really need for painting? We need brushes colors and a comfortable

86
00:07:08,050 --> 00:07:09,189
workspace

87
00:07:09,189 --> 00:07:11,549
Brushes, workspace, and color

88
00:07:12,250 --> 00:07:13,509
So brushes...

89
00:07:13,509 --> 00:07:19,809
where are the brushes, and how to paint and erase things, how to change their size quickly and

90
00:07:19,970 --> 00:07:23,470
How not to get lost among so many brush options.

91
00:07:24,110 --> 00:07:28,330
The Krita brush set is designed to fit the basic needs of anyone

92
00:07:28,460 --> 00:07:29,289
first of all

93
00:07:29,289 --> 00:07:32,229
It is good to make sure that everything is correct

94
00:07:32,570 --> 00:07:40,329
because it can happen that we activate the "eraser mode" by mistake and we don't remember it and suddenly

95
00:07:40,639 --> 00:07:45,099
the brushes don't paint and we can be confused. So

96
00:07:45,740 --> 00:07:48,820
Verify that the eraser mode button is off

97
00:07:48,949 --> 00:07:53,348
Ok! brushes are on the right side of the interface by default

98
00:07:53,690 --> 00:08:01,479
But you can change where they are located just dragging the docker. if you close the brushes docker you can restore it easily. Go to

99
00:08:01,759 --> 00:08:08,589
settings/dockers/brush preset and Krita remembers his last location to be shown.

100
00:08:10,400 --> 00:08:12,519
The brushes may look alike a lot

101
00:08:13,340 --> 00:08:17,440
But you will want even more maybe in some days or weeks.

102
00:08:18,470 --> 00:08:25,209
Did i say that in Krita's shop there is a whole set to emulate oil, pastel and watercolour

103
00:08:27,500 --> 00:08:31,720
Krita knows that a lot of brushes can overwhelm the user.

104
00:08:32,180 --> 00:08:37,839
So it has created a system of labels that can be used to see only some brushes

105
00:08:38,479 --> 00:08:40,479
oriented to a specific workflows

106
00:08:41,120 --> 00:08:45,039
Let's see how this works. For example, we have the "Paint"

107
00:08:45,709 --> 00:08:47,029
"Sketch"

108
00:08:47,029 --> 00:08:54,789
"Texture", and so on. Each label has a few brushes related to that task. For example the label "Textures"

109
00:08:54,980 --> 00:09:02,380
Has some brushes to help you to create fast textural effects. in future videos. We will see more things about brushes.

110
00:09:03,440 --> 00:09:08,020
Another way to look for brushes is using filtering, we can go deeper

111
00:09:08,570 --> 00:09:11,770
exploring brushes using the brush editor, for example,

112
00:09:12,230 --> 00:09:19,480
To learn about brushengines more. We will cover that in a coming video. But maybe you don't like to have the brushes

113
00:09:19,480 --> 00:09:23,980
there, always visible, like observing you. In that case you can use the

114
00:09:24,350 --> 00:09:31,410
F6 keyboard shortcut or go to the drop-down window at the top that will open the window with all your brushes

115
00:09:32,410 --> 00:09:37,439
Here you can use the label system. You still have another very interesting option.

116
00:09:38,140 --> 00:09:41,009
Sometimes we don't use more than 10 brushes

117
00:09:41,830 --> 00:09:46,770
Imagine for example that we are drawing in full screen without any interface

118
00:09:47,050 --> 00:09:54,540
pressing "Tab" shortcut, then you press the right mouse button, or the stylus button you have associated with right click

119
00:09:55,450 --> 00:10:00,150
This allows you to see things like brushes, color, latest used colors.

120
00:10:00,150 --> 00:10:06,720
And also mirror the image, rotate the image and more. This quick menu is really powerful.

121
00:10:06,850 --> 00:10:12,119
We will see that in another video too. You have chosen a brush. To change the size

122
00:10:12,360 --> 00:10:20,320
press the "shift" key and drag the cursor to the right for increasing the size or to the left for decreasing the size

123
00:10:20,520 --> 00:10:26,220
We can also do it throughout keyboard shortcuts. Well, it's nothing comfortable or

124
00:10:26,920 --> 00:10:33,020
trough the slider at the top of the interface, which is a good idea. One thing you are going to notice,

125
00:10:33,030 --> 00:10:35,309
is that if you choose a brush and

126
00:10:35,890 --> 00:10:37,810
Change the size, for example

127
00:10:37,810 --> 00:10:45,510
You make some strokes and you say hmm this look very good a very good brush and I like it as it is.

128
00:10:46,090 --> 00:10:50,639
but if you select another brush and you reselect the previous brush

129
00:10:51,400 --> 00:10:55,800
You will see that the brush it has returned to its previous state

130
00:10:56,740 --> 00:10:59,820
In this case your changes were not too much.

131
00:11:00,370 --> 00:11:07,650
But imagine that you start playing with the brush parameters and at the end you don't know what you have changed exactly,

132
00:11:07,650 --> 00:11:12,869
but the brush result is very cool. In this case

133
00:11:12,880 --> 00:11:15,460
This can be really annoying, isn't it?

134
00:11:15,580 --> 00:11:22,300
This is done for people who prefer to use  default brushes such as they are and without changing them too much.

135
00:11:22,440 --> 00:11:29,240
This is a good practice at the beginning because it helps you to understand better the difference between them and

136
00:11:29,680 --> 00:11:35,310
the possibilities you can have with a single brush and that's the reason why Krita created

137
00:11:35,560 --> 00:11:42,859
the "Dirty brushes". Brushes that can remember your changes even if you change brushes again and again,

138
00:11:43,350 --> 00:11:45,890
To use this feature. We have to activate

139
00:11:46,410 --> 00:11:53,480
"Temporarily save tweaks to presets" in the brush editor Krita will remember the settings until it closes.

140
00:11:53,640 --> 00:11:58,849
Okay, if you want to save a modified version just click on overwrite button

141
00:11:59,040 --> 00:12:01,040
And that's it

142
00:12:01,140 --> 00:12:04,969
You have your first modified brush ready to be used

143
00:12:05,269 --> 00:12:11,479
Even if you close Krita. To be in a comfortable workspace, we need to know how to make the basic navigation

144
00:12:12,000 --> 00:12:17,779
Like zoom, pan, and rotate and also horizontal mirror can be useful

145
00:12:18,029 --> 00:12:18,890
in Krita

146
00:12:18,890 --> 00:12:23,059
we use by default the "M" key as a shortcut to mirror the view

147
00:12:23,370 --> 00:12:30,200
That is really useful when we paint for a long time. A simple shortcut, but a powerful feature. The color

148
00:12:31,110 --> 00:12:37,550
When we are painting, we need to place the pick colors form. A place where you can choose different colors.

149
00:12:37,920 --> 00:12:43,490
For example, how do I make my color less saturated or brighter? For example

150
00:12:44,190 --> 00:12:49,909
We can use "Advanced color selector" or use a custom "Palette". For now

151
00:12:49,950 --> 00:12:56,390
We're going to leave that as it is by default because we will cover color in upcoming videos.

152
00:12:57,240 --> 00:13:03,109
Saving or images and work safe. A highly recommended practice is to save often

153
00:13:03,570 --> 00:13:08,539
So please do it. All the software crashes sometimes. Well...

154
00:13:08,540 --> 00:13:12,740
I guess you have been drawing and testing the brushes that came by default

155
00:13:12,899 --> 00:13:17,659
but what happens if we are so excited that we forget to save and

156
00:13:18,000 --> 00:13:21,859
suddenly the light goes out? Years ago that was a big problem

157
00:13:21,930 --> 00:13:27,770
It makes you to redo everything again since the last time you save your work

158
00:13:28,740 --> 00:13:35,990
But we are in a technological era right? So that's why Krita goes further and help us how?

159
00:13:36,750 --> 00:13:40,039
Everytime you open an image and you are working on it

160
00:13:40,649 --> 00:13:42,889
Quietly Krita is saving your work

161
00:13:43,260 --> 00:13:50,179
The program makes a copy of the file and takes care of saving the work every 15 minutes. is that cool or not?

162
00:13:51,059 --> 00:13:58,439
I think it is. if we want to change that time, we have to go to the menu settings/ configure Krita/General/section and

163
00:13:59,319 --> 00:14:02,218
/File handling and there we change the time

164
00:14:02,889 --> 00:14:04,719
Let's do it.

165
00:14:04,719 --> 00:14:11,489
I consider that five minutes is a non-intrusive time for saving in the case that for some strange reason

166
00:14:12,099 --> 00:14:18,959
You want to deactivate this option simple click on the radio button "Enable auto saving"

167
00:14:19,389 --> 00:14:22,108
I wouldn't do it. I'm not gonna do it.

168
00:14:22,109 --> 00:14:26,098
I can assure that. What if I want to save different stages of my work

169
00:14:26,529 --> 00:14:31,859
Then we use the incremental save option a cool name for a very useful feature

170
00:14:32,319 --> 00:14:34,348
And why do I think is so useful?

171
00:14:34,719 --> 00:14:38,369
Because if you notice it has a shortcut associated with it

172
00:14:38,529 --> 00:14:44,669
which is "Ctrl+Alt+S" and that way we just press the shortcut and we create an

173
00:14:44,949 --> 00:14:51,478
Incremental version of the file we are working on. If you like this type of tools that help productivity,

174
00:14:51,669 --> 00:14:56,759
let me know in the comments and give Krita a like. Ok! That's the end for now!

175
00:14:56,759 --> 00:14:59,878
You have the basics but there is much much more.

176
00:15:00,189 --> 00:15:03,898
Don't forget to subscribe and share this video in your social media

177
00:15:03,899 --> 00:15:09,719
And if you think this could help your friends and more Krita users, then share it.

178
00:15:10,179 --> 00:15:15,119
If you are interested in some specific topic or you want to suggest new topics

179
00:15:15,879 --> 00:15:21,028
Let me know in the comments below and maybe this could be the next video

180
00:15:21,969 --> 00:15:28,169
Thanks a lot for watching and be ready for new videos where I will be covering how to use Krita

181
00:15:28,239 --> 00:15:30,239
with more advanced features

182
00:15:30,789 --> 00:15:32,789
Bye

