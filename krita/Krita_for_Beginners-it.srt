1
00:00:00,130 --> 00:00:02,040
Prima di tutto, ciao a tutti/e!

2
00:00:02,040 --> 00:00:07,140
Tengo a precisare che questo video è stato pensato per le persone che si stanno avvicinando alla pittura digitale e

3
00:00:07,330 --> 00:00:13,620
scelgono <b><font color=#ff0000>Krita</b></font> come prima opzione per imparare. Se sei dunque un utente di livello medio o avanzato

4
00:00:14,139 --> 00:00:18,839
<i>Probabilmente in questo video vedrai cose che già conosci. In questo video,</i>

5
00:00:18,840 --> 00:00:24,719
parleremo di:  come scaricare Krita, driver, come installare correttamente il programma,

6
00:00:24,789 --> 00:00:28,079
schermata di benvenuto, come creare la nostra prima immagine,

7
00:00:28,420 --> 00:00:35,790
pennelli, spazio di lavoro e colore, come salvare le immagine e lavorare in sicurezza. Pronti?

8
00:00:36,850 --> 00:00:38,850
Iniziamo

9
00:00:44,560 --> 00:00:47,940
Dai primi programmi di pittura digitale sono cambiate un sacco di cose

10
00:00:48,550 --> 00:00:56,430
Oggi viviamo tempi emozionanti, perché siamo circondati da applicazioni creative. Oggi più che mai possiamo

11
00:00:56,430 --> 00:00:59,159
dipingere con un sacco di strumenti differenti e questa è una grande cosa.

12
00:00:59,160 --> 00:01:03,360
Ma a volte abbiamo a disposizione una quantità eccessiva di opzioni.

13
00:01:03,790 --> 00:01:11,069
Se sei qui è perché sei interessato a Krita e vuoi iniziare a disegnare e creare il tuo mondo

14
00:01:11,260 --> 00:01:16,259
Magari hai notato un'immagine bellissima fatta con Krita e che

15
00:01:16,570 --> 00:01:19,410
aumenta la tua curiosità sul programma

16
00:01:19,660 --> 00:01:23,160
Questo video è perciò fatto per facilitare la tua prima

17
00:01:23,380 --> 00:01:28,499
esperienza con Krita. Sin dai primi rilasci, Krita è sempre stato un programma con un'interfaccia

18
00:01:28,780 --> 00:01:34,619
di medio livello, senza troppi pulsanti che distraggono e intimoriscono, cosa, questa, sempre apprezzata

19
00:01:35,259 --> 00:01:38,489
perché non dobbiamo imparare un'interfaccia complessa

20
00:01:38,490 --> 00:01:44,580
ma possiamo regolarla in base alle nostre esigenze. Se non hai installato il programma, ecco alcuni suggerimenti.

21
00:01:44,619 --> 00:01:45,520
Per prima cosa

22
00:01:45,520 --> 00:01:47,520
dove recupero il programma?

23
00:01:47,680 --> 00:01:53,610
Molte persone cercano software in pagine che offrono un sacco di programmi. Questo non è consigliato

24
00:01:53,800 --> 00:01:58,559
perché è possibile trovarci al loro interno cattive sorprese, tipo malware o spyware.

25
00:01:59,170 --> 00:02:01,170
Se vuoi dunque disegnare in sicurezza

26
00:02:01,390 --> 00:02:08,970
il sito migliore per scaricare Krita è il sito web ufficiale Krita.org. Troverai i collegamenti

27
00:02:09,280 --> 00:02:13,830
nella descrizione sottostante. Possiamo cercare il pulsante del download oppure premere semplicemente il grande pulsante

28
00:02:14,549 --> 00:02:20,909
«Scarica», che è molto descrittivo. Possiamo quindi scegliere la piattaforma in cui dobbiamo utilizzare il programma.

29
00:02:21,010 --> 00:02:23,010
Cioè, se uso per esempio

30
00:02:23,500 --> 00:02:28,139
Linux, Windows o Macintosh. Possiamo scaricare Krita e, mentre sta scaricando,

31
00:02:28,540 --> 00:02:35,249
diamo un'occhiata a quello che c'è qui. Come puoi vedere, possiamo anche andare al Windows Store o sulla piattaforma Steam

32
00:02:35,590 --> 00:02:39,479
se vogliamo ottenere Krita come app. È contenuto a pagamento

33
00:02:39,480 --> 00:02:46,259
ma aiuta a finanziare lo sviluppatore principale di Krita, Boudewijn, ed è davvero importante. Inoltre puoi osservare che ci sono alcune cose chiamate

34
00:02:46,630 --> 00:02:51,540
nightly build. Krita presenta due versioni diverse. Qual è quella da scegliere?

35
00:02:52,380 --> 00:02:54,859
La versione stabile o quella di sviluppo?

36
00:02:55,500 --> 00:02:58,130
Qual è la versione di Krita migliore per me?

37
00:02:58,740 --> 00:03:00,380
Strana domanda, vero?

38
00:03:00,380 --> 00:03:00,870
Bene...

39
00:03:00,870 --> 00:03:03,740
Se hai bisogno di stabilità per terminare il tuo progetto

40
00:03:04,140 --> 00:03:08,509
senza problemi, la scelta più comune è scaricare la versione stabile

41
00:03:08,640 --> 00:03:12,830
È quella che contiene le funzionalità principali per terminare il lavoro

42
00:03:12,890 --> 00:03:20,330
Ma se desideri provare le ultime funzionalità e commentarne i risultati dopo averle provate, allora puoi scaricare

43
00:03:21,030 --> 00:03:27,020
la versione di sviluppo. Sì, questa versione può bloccarsi e sì, questa è una buona cosa

44
00:03:30,060 --> 00:03:36,229
È buono perché ci permette di migliorare lo sviluppo e ripulire Krita il più

45
00:03:36,360 --> 00:03:40,190
possibile per il prossimo rilascio. La cosa buona è che posso scaricare le

46
00:03:40,560 --> 00:03:46,970
nightly build e non eliminare il mio rilascio stabile. Scegli, dunque. Abbiamo già scaricato Krita

47
00:03:47,120 --> 00:03:49,120
e ora che faccio? I driver

48
00:03:50,280 --> 00:03:56,179
È opportuno scaricare gli ultimi driver per la nostra tavoletta grafica. Tenere la nostra attrezzatura

49
00:03:56,430 --> 00:04:02,810
aggiornata è sempre una buona idea, ed evitiamo un sacco di problemi sia con Krita, sia con gli altri software

50
00:04:03,299 --> 00:04:06,739
Investi dunque un po' di tempo affinché sia tutto pronto.

51
00:04:06,739 --> 00:04:11,449
Ti aspetto. Una volta che Krita è scaricato e i driver sono aggiornati,

52
00:04:11,450 --> 00:04:18,169
puoi installare Krita. Se riscontri problemi con la tavoletta, verifica che sia compatibile con Krita

53
00:04:18,810 --> 00:04:24,649
Krita supporta un'ampia gamma di dispositivi, ma a volte è possibile riscontrare comportamenti strani,

54
00:04:24,690 --> 00:04:28,910
dunque ecco il collegamento per verificare. Inoltre nella descrizione sotto

55
00:04:29,370 --> 00:04:35,419
alcuni utenti ha segnalato problemi con «rings» attorno al cursore in Windows 10

56
00:04:35,419 --> 00:04:38,299
Guarda dunque questo video se ti trovi nella stessa situazione

57
00:04:39,120 --> 00:04:44,929
L'installazione di Krita non è complicata, una volta scaricato fai clic sul file per installarlo

58
00:04:45,810 --> 00:04:52,070
Assicurati, inoltre, che la shell di Windows sia spuntata in modo da poter vedere i file di Krita nel nostro navigatore di file in Windows

59
00:04:52,919 --> 00:04:56,809
Se sei un utente di Linux puoi scaricare una appimage

60
00:04:57,570 --> 00:05:00,830
Una volta scaricata devi rendere il file eseguibile

61
00:05:01,740 --> 00:05:06,569
controllando i permessi sul file. Fai clic col pulsante destro del mouse sul file appimage,

62
00:05:06,879 --> 00:05:09,869
seleziona le proprietà e rendi il file eseguibile

63
00:05:10,509 --> 00:05:16,619
Bene, apri Krita: apparirà la schermata di avvio mentre vengono caricate le risorse. Dopo alcuni secondi

64
00:05:16,620 --> 00:05:23,969
potrai osservare l'interfaccia utente. Ti trovi davanti una specie di schermata di benvenuto. Questa schermata fornisce un sacco di informazioni importanti

65
00:05:24,610 --> 00:05:29,550
Per esempio, possiamo leggere le ultime notizie su Krita. Grazie a questo riquadro

66
00:05:30,069 --> 00:05:32,968
collegato all'account ufficiale di Krita painting

67
00:05:33,400 --> 00:05:38,159
e anche collegamenti molto interessanti che ti aiutano a saperne di più sul

68
00:05:38,319 --> 00:05:45,028
mondo di Krita e la sua comunità. Possiamo anche andare direttamente al manuale e imparare molto leggendo

69
00:05:45,159 --> 00:05:50,729
degli ottimo contenuti creati dagli sviluppatori di Krita. Possiamo andare direttamente al forum per cercare aiuto.

70
00:05:50,849 --> 00:05:55,199
Basta registrarti, accedere, e puoi parlare direttamente con

71
00:05:55,569 --> 00:06:00,929
gli sviluppatori, condividere le tue idee o aggiungere i tuoi lavori per ottenere commenti dagli altri utenti.

72
00:06:01,210 --> 00:06:06,060
Contatto diretto con i programmatori e gli artisti, ottimo! Ecco, sulla prima colonna

73
00:06:06,060 --> 00:06:10,259
vediamo «nuovo file» con la normale scorciatoia «Ctrl+N»

74
00:06:10,539 --> 00:06:15,869
Se ci faccio sopra clic si apre una nuova finestra e vedo un sacco di opzioni. Puoi

75
00:06:16,270 --> 00:06:22,259
scegliere un modello o creare il tuo file personalizzato della dimensione che preferisci. Se non crei un'immagine

76
00:06:22,479 --> 00:06:26,098
alcune parti dell'interfaccia di Krita non funzionano, tipo...

77
00:06:26,589 --> 00:06:31,199
la selezione dei pennelli, la selezione dei colori, e via discorrendo. Sono disponibili solo i menu.

78
00:06:31,629 --> 00:06:34,619
Questo è logico, poiché non nulla su cui lavorare.

79
00:06:34,719 --> 00:06:40,889
Creiamo, dunque, la nostra prima immagine. Vai al documento personalizzato e scegli la dimensione del file

80
00:06:40,889 --> 00:06:44,758
che vuoi creare, non preoccuparti di tutte le opzioni.

81
00:06:44,759 --> 00:06:49,679
Daremo loro uno sguardo in un altro video. Attribuisci una dimensione e fai clic sul pulsante Crea

82
00:06:49,960 --> 00:06:53,279
Una volta creata l'immagine puoi iniziare a lavorarci sopra.

83
00:06:53,589 --> 00:06:58,919
Ci sono interfacce che sono molto complesse e ci sopraffanno.

84
00:06:59,469 --> 00:07:01,469
È successo anche a me.

85
00:07:01,569 --> 00:07:07,528
Ma cosa abbiamo bisogno veramente per disegnare? Abbiamo bisogno di pennelli, colori e uno spazio di lavoro

86
00:07:08,050 --> 00:07:09,189
confortevole

87
00:07:09,189 --> 00:07:11,549
Pennelli, spazio di lavoro e colore

88
00:07:12,250 --> 00:07:13,509
Pennelli, dunque...

89
00:07:13,509 --> 00:07:19,809
dove sono i pennelli, come disegno e cancello le cose, come cambio rapidamente le loro dimensioni e

90
00:07:19,970 --> 00:07:23,470
come non mi perdo in mezzo alle molte opzioni dei pennelli.

91
00:07:24,110 --> 00:07:28,330
Il set di pennelli di Krita è progettato per adattarsi alle esigenze di base di ognuno

92
00:07:28,460 --> 00:07:29,289
Per prima cosa

93
00:07:29,289 --> 00:07:32,229
è bene accertarsi che tutto sia a posto

94
00:07:32,570 --> 00:07:40,329
perché può succedere che attiviamo la «modalità gomma» per sbaglio, non lo ricordiamo e improvvisamente

95
00:07:40,639 --> 00:07:45,099
i pennelli non dipingono e possiamo confonderci. Perciò

96
00:07:45,740 --> 00:07:48,820
verifica che il pulsante della modalità gomma sia spento

97
00:07:48,949 --> 00:07:53,348
Ok! Per impostazione predefinita, i pennelli sono nella parte destra dell'interfaccia,

98
00:07:53,690 --> 00:08:01,479
ma puoi cambiare la loro posizione semplicemente trascinando l'area di aggancio. Se chiudi l'area di aggancio dei pennelli, puoi ripristinarla facilmente. Vai alle

99
00:08:01,759 --> 00:08:08,589
impostazioni/aree di aggancio/preimpostazione dei pennelli e Krita ricorda l'ultima posizione mostrata.

100
00:08:10,400 --> 00:08:12,519
I pennelli potrebbero assomigliarsi molto

101
00:08:13,340 --> 00:08:17,440
ma in alcuni giorni o settimane potresti volerne persino di più.

102
00:08:18,470 --> 00:08:25,209
Ho detto che nel negozio di Krita esiste un intero set per emulare oli, pastelle e acquerelli

103
00:08:27,500 --> 00:08:31,720
Krita sa che molti pennelli possono sopraffare l'utente.

104
00:08:32,180 --> 00:08:37,839
Ha creato dunque un sistema di etichette che possono essere utilizzate per vedere solo alcuni pennelli

105
00:08:38,479 --> 00:08:40,479
orientate a flussi di lavoro specifici

106
00:08:41,120 --> 00:08:45,039
Vediamo come funziona. Per esempio, abbiamo «Pittura»

107
00:08:45,709 --> 00:08:47,029
«Schizzo»

108
00:08:47,029 --> 00:08:54,789
«Trama», e via discorrendo. Ogni livello contiene alcuni pennelli collegati a quell'attività. Per esempio, l'etichetta «Trame»

109
00:08:54,980 --> 00:09:02,380
ha dei pennelli che ti aiutano a creare rapidi effetti di trama. In video futuri, vedremo ulteriori informazioni sui pennelli.

110
00:09:03,440 --> 00:09:08,020
Un altro modo per cercare i pennelli è l'uso dei filtri; possiamo approfondire la ricerca

111
00:09:08,570 --> 00:09:11,770
esplorando i pennelli tramite l'editor dei pennelli, per esempio

112
00:09:12,230 --> 00:09:19,480
per saperne di più sui motori dei pennelli. Tratteremo l'argomento in un video di prossima uscita. È possibile anche che non ti piaccia avere i pennelli

113
00:09:19,480 --> 00:09:23,980
lì, sempre visibili, come se ti stessero osservando. In questo caso puoi usare la

114
00:09:24,350 --> 00:09:31,410
scorciatoia da tastiera F6 keyboard o andare alla finestra a tendina in alto, che apre la finestra contenente tutti i tuoi pennelli.

115
00:09:32,410 --> 00:09:37,439
Qui puoi usare il sistema di etichette. E hai ancora un'altra opzione molto interessante.

116
00:09:38,140 --> 00:09:41,009
A volte non utilizziamo più di dieci pennelli

117
00:09:41,830 --> 00:09:46,770
Immagina, per esempio, di disegnare a schermo intero senza alcuna interfaccia,

118
00:09:47,050 --> 00:09:54,540
premendo la scorciatoia «Tab», poi premi il pulsante destro del mouse, o il pulsante dello stilo che hai associato col clic destro

119
00:09:55,450 --> 00:10:00,150
Questo ti permette di vedere gli elementi quali i pennelli, i colori, gli ultimi colori utilizzati.

120
00:10:00,150 --> 00:10:06,720
E anche di rispecchiare, ruotare l'immagine e altro ancora. Questo menu rapido è davvero forte.

121
00:10:06,850 --> 00:10:12,119
Anche questo lo vedremo in un altro video. Hai scelto un pennello. Per cambiare la dimensione

122
00:10:12,360 --> 00:10:20,320
premi il tasto «Maiusc» e trascina il cursore verso destra per aumentare la dimensione o verso sinistra per diminuirla

123
00:10:20,520 --> 00:10:26,220
Possiamo anche farlo tramite le scorciatoie da tastiera. Be', non è niente comodo, oppure

124
00:10:26,920 --> 00:10:33,020
tramite il cursore in cima dell'interfaccia, che è una buona idea. Una cosa che noterai

125
00:10:33,030 --> 00:10:35,309
è che se scegli un pennello e

126
00:10:35,890 --> 00:10:37,810
cambi la sua dimensione, per esempio

127
00:10:37,810 --> 00:10:45,510
esegui dei tratteggi e dici ok va molto bene, un ottimo pennello, mi piace il suo aspetto.

128
00:10:46,090 --> 00:10:50,639
Ma se scegli un altro pennello e riselezioni il pennello precedente

129
00:10:51,400 --> 00:10:55,800
noterai che il pennello è tornato al suo stato precedente

130
00:10:56,740 --> 00:10:59,820
In questo caso le tue modifiche non erano tante.

131
00:11:00,370 --> 00:11:07,650
Ma immagina di iniziare a giocare con i parametri del pennello e di non sapere cosa hai cambiato esattamente, alla fine, 

132
00:11:07,650 --> 00:11:12,869
ma il pennello risulta molto figo. In questo caso

133
00:11:12,880 --> 00:11:15,460
questo potrebbe risultare molto fastidioso, non trovi?

134
00:11:15,580 --> 00:11:22,300
Questo è studiato per quelle persone che preferiscono usare i pennelli predefiniti, senza modificarli troppo.

135
00:11:22,440 --> 00:11:29,240
È una buona abitudine all'inizio, perché ti aiuta a capire meglio le loro differenze e

136
00:11:29,680 --> 00:11:35,310
le possibilità che hai con un singolo pennello. Questa è la ragione per cui Krita ha creato

137
00:11:35,560 --> 00:11:42,859
i cosiddetti «pennelli sporchi». Pennelli in grado di ricordare le modifiche apportate, anche se li hai cambiati più e più volte.

138
00:11:43,350 --> 00:11:45,890
Per utilizzare questa funzionalità dobbiamo attivare

139
00:11:46,410 --> 00:11:53,480
l'opzione «Salva temporaneamente le regolazioni nelle preimpostazioni» nell'editor dei pennelli: Krita ricorderà le impostazioni fino alla sua chiusura.

140
00:11:53,640 --> 00:11:58,849
Ok, se vuoi salvare una versione modificata, basta fare clic sul pulsante Sovrascrivi

141
00:11:59,040 --> 00:12:01,040
ed è fatto

142
00:12:01,140 --> 00:12:04,969
hai modificato il tuo primo pennello, pronto da usare,

143
00:12:05,269 --> 00:12:11,479
anche se chiudi Krita. Per lavorare in uno spazio confortevole, dobbiamo sapere come creare la navigazione di base

144
00:12:12,000 --> 00:12:17,779
quale zoom, spostamento e rotazione; anche il rispecchiamento orizzontale può essere utile

145
00:12:18,029 --> 00:12:18,890
in Krita

146
00:12:18,890 --> 00:12:23,059
Utilizziamo, come impostazione predefinita, il tasto «M» come scorciatoia per rispecchiare la vista

147
00:12:23,370 --> 00:12:30,200
Questo è molto utile quando dipingiamo per molto tempo. Una scorciatoia semplice per una funzionalità avanzata. Il colore

148
00:12:31,110 --> 00:12:37,550
Quando stiamo dipingendo, abbiamo bisogno di un posto da cui attingere i colori. Un posto in cui poter scegliere i diversi colori.

149
00:12:37,920 --> 00:12:43,490
Per esempio, come rendo il mio colore meno saturato o più brillante? Per esempio

150
00:12:44,190 --> 00:12:49,909
possiamo utilizzare il «Selettore dei colori avanzato» oppure usare una «Tavolozza» personalizzata. Per il momento

151
00:12:49,950 --> 00:12:56,390
la lasceremo così com'è, nel suo stato predefinito, perché tratteremo il colore in futuri video.

152
00:12:57,240 --> 00:13:03,109
Salvare le immagini e lavorare in sicurezza. Una pratica fortemente consigliata è salvare spesso

153
00:13:03,570 --> 00:13:08,539
Quindi fallo. Tutti i programmi a volte si bloccano. Bene...

154
00:13:08,540 --> 00:13:12,740
Immagino che hai disegnato e provato i pennelli predefiniti forniti col programma

155
00:13:12,899 --> 00:13:17,659
ma cosa succede se siamo così concentrati che ci dimentichiamo di salvare e

156
00:13:18,000 --> 00:13:21,859
improvvisamente manca la corrente? Anni addietro sarebbe stato un grosso problema,

157
00:13:21,930 --> 00:13:27,770
avresti dovuto rifare tutto daccapo, ripartendo dal tuo ultimo salvataggio

158
00:13:28,740 --> 00:13:35,990
Ma adesso siamo in un'era tecnologica, giusto? Dunque, ecco perché Krita va avanti, e come ci aiuta?

159
00:13:36,750 --> 00:13:40,039
Ogni volta che apriamo un'immagine e ci lavoriamo sopra

160
00:13:40,649 --> 00:13:42,889
Krita salva il tuo lavoro in silenzio

161
00:13:43,260 --> 00:13:50,179
Il programma crea una copia del file e si preoccupa di salvare il lavoro ogni 15 minuti. Figo, no?

162
00:13:51,059 --> 00:13:58,439
Io penso di sì. Se vogliamo cambiare il tempo, basta andare nel menu Impostazioni/Configura Krita/sezione Generale/

163
00:13:59,319 --> 00:14:02,218
Gestione file e qui cambiamo il tempo

164
00:14:02,889 --> 00:14:04,719
Facciamolo.

165
00:14:04,719 --> 00:14:11,489
Ritengo che un tempo di salvataggio di cinque minuti non sia invasivo; nel caso in cui, per qualunque strana ragione

166
00:14:12,099 --> 00:14:18,959
volessi disattivare questa opzione, basta fare clic sul pulsante radio «Abilita il salvataggio automatico»

167
00:14:19,389 --> 00:14:22,108
Io non lo farei. Non lo farò.

168
00:14:22,109 --> 00:14:26,098
Lo posso garantire. Immagina se volessi salvare diversi passaggi del mio lavoro

169
00:14:26,529 --> 00:14:31,859
In tal caso utilizziamo l'opzione di salvataggio incrementale, un bel nome per una funzionalità molto utile.

170
00:14:32,319 --> 00:14:34,348
Perché penso che sia così utile?

171
00:14:34,719 --> 00:14:38,369
Perché, se noti, ha una scorciatoia associata,

172
00:14:38,529 --> 00:14:44,669
la quale è «Ctrl+Alt+S», in tal modo, basta che la premiamo e creiamo una

173
00:14:44,949 --> 00:14:51,478
versione incrementale del file su cui stiamo lavorando. Se ti piace questo tipo di strumenti che aiutano la produttività,

174
00:14:51,669 --> 00:14:56,759
scrivilo nei commenti e metti un 'Mi piace' a Krita. Ok! Siamo alla fine, per adesso!

175
00:14:56,759 --> 00:14:59,878
Ora hai le basi, ma c'è molto altro.

176
00:15:00,189 --> 00:15:03,898
Non dimenticare di iscriverti e condividere questo video nei tuoi social.

177
00:15:03,899 --> 00:15:09,719
Se pensi che possa aiutare i tuoi amici e altri utenti di Krita, allora condividilo.

178
00:15:10,179 --> 00:15:15,119
Se sei interessato a qualche argomento specifico o vuoi suggerirne di nuovi,

179
00:15:15,879 --> 00:15:21,028
scrivi nei commenti sotto, può darsi che venga trattato nel prossimo video

180
00:15:21,969 --> 00:15:28,169
Grazie per la visione, ci saranno nuovi video che tratteranno l'uso di Krita

181
00:15:28,239 --> 00:15:30,239
con funzionalità più avanzate

182
00:15:30,789 --> 00:15:32,789
Ciao!
