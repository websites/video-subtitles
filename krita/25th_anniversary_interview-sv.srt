﻿1
00:00:01,120 --> 00:00:04,520
Hej allesamman, och välkomna till den här nya videon!

2
00:00:04,520 --> 00:00:08,200
Vi firar Kritas 25:e årsdag

3
00:00:08,200 --> 00:00:13,960
Och idag! Här! Har vi en mycket, mycket speciell gäst.

4
00:00:14,000 --> 00:00:16,600
Kritas underhållsansvariga ... Halla Rempt

5
00:00:16,600 --> 00:00:21,600
Och Halla har varit underhållsansvarig i mer än 20 år!

6
00:00:21,600 --> 00:00:22,960
Det är enastående!

7
00:00:22,960 --> 00:00:25,880
En mycket speciell dag, njut av den!

8
00:00:25,880 --> 00:00:28,120
Hej Halla, hur står det till?

9
00:00:28,400 --> 00:00:30,200
Hi Ramón, trevligt att träffas!

10
00:00:30,440 --> 00:00:34,960
Att fira 25 år är en viktig milstolpe.

11
00:00:34,960 --> 00:00:39,200
Vad har det inneburit att vara öppen källkod för utvecklingen av Krita?

12
00:00:39,800 --> 00:00:45,120
Vad betydde öppen källkod för utvecklingen av Krita under de senaste 25 åren? Tja, skulle jag säga

13
00:00:45,120 --> 00:00:50,520
det är absolut nödvändigt och det är av två anledningar. Det första är naturligtvis att vara öppen källkod

14
00:00:50,520 --> 00:00:55,680
har dragit nytta av bidrag från hundratals människor. Det finns över 600

15
00:00:55,680 --> 00:01:00,440
personer i Kritas incheckningslista och det är bara utvecklingen. Inte webbplatserna,

16
00:01:00,440 --> 00:01:05,120
inte dokumentationen, inte allt annat och det är fantastiskt! När vi fastnade någonstans,

17
00:01:05,120 --> 00:01:10,720
kunde folk hjälpa oss, och många gjorde det. Slutligen, och kanske ännu viktigare, nu när

18
00:01:10,720 --> 00:01:17,200
Krita är fri programvara med öppen källkod under GPL, kan det aldrig ändras. Under

19
00:01:17,200 --> 00:01:22,800
åren har vi sett konkurrenter försvinna därför att de eftersom de köptes ut av företag som Adobe,

20
00:01:22,800 --> 00:01:27,440
visste vi att vi kunde lita på att Krita bara skulle fortsätta så länge det finns

21
00:01:27,440 --> 00:01:32,680
bidragsgivare. Så att vara öppen källkod är helt grundläggande, det kommer aldrig att förändras!

22
00:01:32,760 --> 00:01:35,240
Minsann! Det är mycket intressant information.

23
00:01:35,240 --> 00:01:40,240
Krita har funnits under 25 år,

24
00:01:40,240 --> 00:01:42,200
det har varit en enastående resa.

25
00:01:42,200 --> 00:01:46,160
Hur har Krita utvecklats under dessa 25 år?

26
00:01:46,200 --> 00:01:50,240
25 år, det är en riktigt lång resa ... Jag har inte varit där under hela resan.

27
00:01:50,240 --> 00:01:54,840
Kanske bara 21 år som ändå är lång tid. När jag ser tillbaka på

28
00:01:54,840 --> 00:01:58,960
hur Krita såg ut när jag gick med i projektet, kunde man inte ens rita

29
00:01:58,960 --> 00:02:02,720
Man kunde läsa in bilder, lägga till lager och flytta omkring lager och det var allt.

30
00:02:02,720 --> 00:02:08,320
Så vi började bygga vidare på det. Vi skrev om hela kärnan i Krita. Kärnan har skrivits om fyra gånger

31
00:02:08,320 --> 00:02:15,680
och varje gång ökade Kritas prestanda. Vi började lägga till verktyg, vi började lägga till faser

32
00:02:15,680 --> 00:02:23,640
i Krita som stöd för en duk baserad på OpenGL, acceleration med grafikprocessor innan Photoshop, CMYK i evighet. Vi

33
00:02:23,640 --> 00:02:28,040
började implementera mer och mer roliga saker, mer och mer användbara saker och sedan kom

34
00:02:28,040 --> 00:02:33,800
ett stort genombrott. Vi började lyssna på våra användare, vi hade sessioner. Utvecklingssprintar,

35
00:02:33,800 --> 00:02:38,960
där Kritas utvecklare och bidragsgivare samlades, oftast arbetar vi tillsammans och diskuterar

36
00:02:38,960 --> 00:02:44,000
vad vi ska göra nästa år ändra
sedan vår sprint hos Blender Studios... ... och

37
00:02:44,000 --> 00:02:49,560
sedan bad vi konstnärerna "demonstrera" hur de arbetar
med Krita, vi bad dem bara och ... här har du en timme

38
00:02:49,560 --> 00:02:55,000
och vi tittar på medan du målar, hoppas att du
inte har något emot det, du kan säga vad du vill,

39
00:02:55,000 --> 00:03:00,320
klaga på allt som du vill klaga på.
Allt går bra, och vi utvecklare får inte

40
00:03:00,320 --> 00:03:03,960
får inte gå i svaromål. Vi får inte heller hjälpa dig, för vi vill se vad

41
00:03:03,960 --> 00:03:11,080
problemen är. Och den typen av användarobservationer hjälpte oss verkligen att förbättra Krita med stormsteg.

42
00:03:11,080 --> 00:03:18,000
Vilken har varit den mest kreativa användningen av Krita som fick dig att tänka ...

43
00:03:18,000 --> 00:03:20,680
Oj! Det har jag aldrig tänkt på!

44
00:03:20,680 --> 00:03:27,120
den mest extrema användnigen av Krita? Jag gissar att det var personen som ville använda Kritas stora

45
00:03:27,120 --> 00:03:34,120
bitdjupskapacitet för att skapa stjärnkartor. Använda enorma astronomiska fotografier och

46
00:03:34,120 --> 00:03:43,680
ville arbeta med dem och ärligt talat, skulle jag ha, (ja jag sa faktiskt det), använt VIPS för det. Men det var en rätt extremt användning.

47
00:03:43,880 --> 00:03:49,440
Många användare kanske undrar vad som motiverar teamet bakom Krita?

48
00:03:50,440 --> 00:03:56,520
Vad motiverar oss? Jag tror, en självisk och en osjälvisk anledning. Den själviska anledningen är

49
00:03:56,520 --> 00:04:02,320
att det är det roligaste jobbet jag någonsin haft i mitt liv. Jag har jobbat för alla typer av företag åtminstone

50
00:04:02,320 --> 00:04:09,000
sex eller sju, och bara i ett av de företagen har vi faktiskt gett ut programvaran jag utvecklade,

51
00:04:09,000 --> 00:04:15,600
vissa företag utvecklar bara programvara för att spendera pengar. Det är galet, men Krita hamnar hos

52
00:04:15,600 --> 00:04:21,920
användarna och det är där den osjälviska delen kommer in, eftersom användare berättar de mest 

53
00:04:21,920 --> 00:04:29,200
underbara saker för oss, som den gången jag fick ett e-postmeddelande från någon i Malaysia. Hon fick veta av sina föräldrar

54
00:04:29,200 --> 00:04:33,840
att "flickor behöver inte rita, så nej, vi skaffar dig inte något ritprogram", hennes bror

55
00:04:33,840 --> 00:04:39,520
gav henne en ritplatta, så hon upptäckte Krita
och kunde börja skapa konstverk och hon

56
00:04:39,520 --> 00:04:46,120
skickade brev till mig i fullkomlig extas: "Jag kan skapa
konst, tack, tack, tack" och det är bara

57
00:04:46,120 --> 00:04:51,800
så motiverande, ger oss en otrolig känsla, så om du gillar Krita tänk på att berätta det för oss

58
00:04:51,800 --> 00:04:58,320
eftersom vi normalt bara ser felrapporter. Felrapporter är användbara, om du gör felrapporter, tack!

59
00:04:58,320 --> 00:05:03,080
Men ibland kan de minska motivationen lite eftersom det finns så många av dem.

60
00:05:03,200 --> 00:05:09,400
Kan du berätta mer om den kommersiella användningen av Krita?

61
00:05:09,400 --> 00:05:16,240
Kommersiell användning av Krita. Det är såklart helt okej. Det är vad vi uttryckligen säger i alla våra

62
00:05:16,240 --> 00:05:22,280
listor över vanliga frågor (FAQ) i informationsrutorna överallt. Men kommersiell

63
00:05:22,280 --> 00:05:29,160
användning förekommer faktiskt. Första gången jag hörde om det var när vi presenterade Krita på SIGGRAPH i Vancouver.

64
00:05:29,160 --> 00:05:35,694
Vi hade ett bås bredvid Blender tack vare Ton Roosendaals verkligt användbara hjälp, vi var som ...

65
00:05:35,694 --> 00:05:41,600
det fanns så mycket, så mycket intresse, så många människor ville prata med oss ​​och sedan kom den här killen

66
00:05:41,600 --> 00:05:49,680
fram till mig och han sa, berätta inte för någon annan åtminstone nu men - psst - vi har använt Krita

67
00:05:49,680 --> 00:05:55,200
i introduktionen till "Little, Big", som var en animerad film tror jag av Disney,

68
00:05:55,200 --> 00:06:01,560
Jag är inte säker längre, så det var första gången jag hörde talas om kommersiell användning av Krita och det var 2014, tror jag.

69
00:06:01,560 --> 00:06:06,480
Ända sedan dess har vi fått sponsring och vi har varit i kontakt med folk

70
00:06:06,480 --> 00:06:14,360
som använder Krita kommersiellt för spel, för filmer, för böcker, och det är väldigt stimulerande.

71
00:06:14,360 --> 00:06:23,240
Ok, så vi vill hjälpa Krita, Hur finansieras Krita, Hur hållbart är det för framtiden?

72
00:06:23,240 --> 00:06:28,160
Vi har redan pratat om det lite. I grund och botten finns det två delar. Å ena sidan finns den

73
00:06:28,160 --> 00:06:36,600
ideella Krita-stiftelsen, å andra sidan finns det vinstdrivande Halla Rempt software, det är jag.

74
00:06:36,600 --> 00:06:44,000
och vi delar upp aktiviteter mellan de två. Stiftelsen ber om donationer vilket betyder att det är

75
00:06:44,000 --> 00:06:50,400
skattebefriat, Halla Rempt software säljer dvd:er och andra saker, men det är förstås knappast någon inkomst längre

76
00:06:50,400 --> 00:06:58,080
nuförtiden eftersom vi har sådana fantastiska handledningar på YouTube ❤. Vi säljer Krita i olika programbutiker.

77
00:06:58,080 --> 00:07:06,400
De två inkomstströmmarna tillsammans, fondens engångsdonationer, intäkter från butiker räcker för att sponsra

78
00:07:06,400 --> 00:07:13,000
vårt för närvarande sponsrade utvecklingsteam och Ramóns videor så det är ganska hållbart

79
00:07:13,000 --> 00:07:21,160
åtminstone har vi gjort det sedan 2015 med
butiker tillagda 2017 så vi gör ett bra jobb.

80
00:07:21,160 --> 00:07:28,520
Hur har gemenskapen stöttat Krita och hur har Krita stöttat gemenskapen?

81
00:07:28,520 --> 00:07:33,360
Sedan länge! När Krita bara var en hobby för alla inblandade,

82
00:07:33,360 --> 00:07:39,120
hade inte de flesta av oss tillgång till digitala ritplattor. Jag började jobba med Krita för att jag fick

83
00:07:39,120 --> 00:07:47,362
en på min födelsedag men den började fallera så vi gjorde en insamling för att få två Wacom ritplattor, pennor för konst.

84
00:07:47,362 --> 00:07:54,280
Så det var då vi märkte att särskilt i gemenskapen för fri programvara, fanns det inte ens konstnärer

85
00:07:54,280 --> 00:08:03,560
då, som var verkligen stöttande, och lite senare, Det var Lukas, vi hade denna slovakiska

86
00:08:03,560 --> 00:08:09,800
kodningsstudent, som gick vidare till att bli en regelbunden bidragsgivare, han gjorde till och med sin avhandling om Kritas

87
00:08:09,800 --> 00:08:15,400
penselgränssnitt. Han fick 10 poäng av 10 för den, eftersom den var ett lysande arbete och hans lärare

88
00:08:15,400 --> 00:08:21,520
blev så förvånade över att deras elev faktiskt gjorde saker som användes i verkligheten!

89
00:08:21,520 --> 00:08:28,320
Samtidigt var vi, som vi, vi konverterade Krita från Qt3 till Qt4, vi skrev om allt

90
00:08:28,320 --> 00:08:34,720
ingenting fungerade, allt var extremt fullt med fel, det var verkligen hemskt! Då sa Lukas:

91
00:08:34,720 --> 00:08:40,960
"Mina sista tre månader på universitetet är tänkt att vara praktik. Jag måste göra saker som är relevanta,

92
00:08:40,960 --> 00:08:47,160
och användbara, jag skulle föredra att få lite betalt för det."
Och för att Krita på den tiden var så otroligt

93
00:08:47,160 --> 00:08:55,720
fullt med fel, och vi kämpade, fortfarande som frivilliga, som hobbykodare för att fåKrita till ett användbart tillstånd, låt oss

94
00:08:55,720 --> 00:09:04,480
finansiera Lukas för att bara fixa fel i 3 månader. Det var en STOR succé!. Från början ville Lukas främst ha en del

95
00:09:04,480 --> 00:09:12,840
pengar för att betala sin hyra men vi kunde betala honom för vid den tiden, för oss, en anständig summa pengar varje månad,

96
00:09:12,840 --> 00:09:20,400
med resultatet av insamlingen. Så många artister, så många entusiastiska människor, stödde Krita.

97
00:09:20,400 --> 00:09:27,520
Tre månader blev sex månader, och därefter lite senare, har
Dmitrij Kazakov gjort enastående arbete alltsedan

98
00:09:27,520 --> 00:09:34,160
hans första GSOC med Krita. Han skulle avsluta universitetet och jag ville inte att han skulle

99
00:09:34,160 --> 00:09:39,960
ta ett annat jobb och gå förlorad för projektet. Så vi började göra några regelbundna insamlingar

100
00:09:39,960 --> 00:09:46,400
som faktiskt gav oss mycket publicitet och var ansträngande, men roliga att göra, och Dmitry arbetar fortfarande

101
00:09:46,400 --> 00:09:49,880
heltid med Krita, som en sponsrad utvecklare,
så det fungerade.

102
00:09:50,000 --> 00:09:57,360
Kan du minnas en tid då Kritas gemenskap ställde upp för att hjälpa utvecklingen av Krita?

103
00:09:57,600 --> 00:10:01,600
Det värsta ögonblicket var när det holländska skatteverket ...

104
00:10:01,600 --> 00:10:07,040
märkte att ... låt oss hoppa över de komplicerade sakerna som i alla fall ger mig huvudvärk,

105
00:10:07,040 --> 00:10:10,720
de ville ha 24 000 euro på en gång från

106
00:10:10,720 --> 00:10:14,800
Krita-stiftelsen, pengar som vi inte hade, när vi berättade

107
00:10:14,800 --> 00:10:21,000
det för omvärlden. Hela världen skyndade sig att
stödja oss! Vilket var fantastiskt, vi faktiskt, faktiskt från

108
00:10:21,000 --> 00:10:27,320
det ögonblicket och framåt har vi aldrig blickat tillbaka, vi har en stöttande gemenskap och vi har en

109
00:10:27,320 --> 00:10:35,280
utvecklingsfond, som jag uppmanar alla att gå med i, ursäkta reklamen, ja, vi säljer Krita i diverse programbutiker, programbutikerna

110
00:10:35,280 --> 00:10:43,480
är helvetiska att arbeta med, men de betalar oss varje månad, tack till gemenskapen för att ni stöttar oss!

111
00:10:43,480 --> 00:10:50,840
Hur hjälper Krita användare att lära sig programvaran eller till och med koda programvaran?

112
00:10:51,280 --> 00:10:56,520
Vi försöker göra Krita tillgänglig eller till och med möjligt att lära sig för nykomlingar,

113
00:10:56,520 --> 00:11:03,640
men i första hand försöker vi helt enkelt behålla befintliga
användargränssnittsstandarder. Vi försöker inte göra

114
00:11:03,640 --> 00:11:09,488
konstiga nya grejer. Vi vill bara att folk ska kunna hitta allt på den plats de är vana vid.

115
00:11:09,488 --> 00:11:15,920
Om du har använt en dator de senaste 5, 10, 20 eller 30 åren, då kommer ingenting i Krita verkligen att

116
00:11:15,920 --> 00:11:22,600
överraska dig vilket gör Krita i sig användbar. Vi samarbetar givetvis också med vår

117
00:11:22,600 --> 00:11:29,840
användargränssnittsexpert Scott Petrovic och han hjälper
oss konstruera nya funktioner. Nu om du går bortom

118
00:11:29,840 --> 00:11:36,560
själva programmets nivå, då är dokumentation verkligen viktig. Vi spenderar ganska mycket tid

119
00:11:36,560 --> 00:11:44,440
och ansträngning på vår dokumentationswebbplats: "docs.krita.org"
listar inte bara alla menyalternativ, utan ger också

120
00:11:44,440 --> 00:11:51,600
du handledningar om arbetsflöden. Saker som kanske inte är uppenbara om du är en kontorsanvändare av

121
00:11:51,600 --> 00:11:57,280
digitala målarprogram, för låt oss vara tydliga med det, digital målning är ett verktyg

122
00:11:57,840 --> 00:12:05,720
separerat från allt annat. Jag menar, jo, Jag målar själv analogt också

123
00:12:05,720 --> 00:12:12,240
med användning av oljefärger och jag ritar en skiss och skulpterar och sedan när jag försöker rita med Krita vet jag

124
00:12:12,240 --> 00:12:18,600
att de flesta av de inlärda vanorna inte överförs till ett digitalt målarprogram så vi har

125
00:12:18,600 --> 00:12:24,920
en omfattande webbplats med handledningar, riktlinjer, 
tips, vi har också en bra gemenskap och tack

126
00:12:24,960 --> 00:12:34,202
vare Raghavendra Kamath (raghukamat) har vi en webbplats med användarformulär som heter: krita-artists.org , precis som Blender Artists

127
00:12:34,202 --> 00:12:38,120
den är helt baserat på det. Det är ett återkommande tema. När jag tror att vi behöver

128
00:12:38,120 --> 00:12:43,320
något nytt, tittar jag först på vad Ton har gjort för Blender och sedan kopierar jag bara det och jag

129
00:12:43,320 --> 00:12:49,040
skäms inte över att erkänna det. Vårt artistforum för Krita är en enorm resurs där folk kan ställa frågor

130
00:12:49,040 --> 00:12:54,640
men också hitta svar på frågor som har ställts tidigare. Slutligen investerar vi mycket

131
00:12:54,640 --> 00:13:01,280
tid och pengar på Ramóns videoserie för vår kanal om Krita, så tack Ramón för att du arbetar med oss!

132
00:13:01,360 --> 00:13:11,160
Hur anpassningsbar är Krita, och vilken typ av insticksprogram eller tillägg kan användare skapa eller använda?

133
00:13:11,840 --> 00:13:18,760
När Krita var riktigt nytt skapade Cyrille Berger först det första skriptgränssnittet för Krita

134
00:13:18,760 --> 00:13:24,960
att skrivas i Javascript, Ruby och Python. Vänta! det är faktiskt inte det första, vårt första

135
00:13:24,960 --> 00:13:30,120
skriptgränssnitt. KDE:s Javascript-gränssnitt, Jag menar
du vet att nästa varenda webbläsare utom

136
00:13:30,120 --> 00:13:39,640
Firefox idag är baserade på KDE:s arbete på KHTML en KDE-baserad webbläsare. Den levererades naturligtvis med ett gränssnitt för Javascript.

137
00:13:39,640 --> 00:13:47,720
Och det Javascript-gränssnittet var en separat sak som vi kunde använda för att göra Krita skriptbart,

138
00:13:47,720 --> 00:13:52,560
så det var det allra första. så vi hade först Javascript och sedan bytte vi ut

139
00:13:52,560 --> 00:14:02,720
det mot Kross, som är det som gjorde
Ruby, Python, etc. ... möjliga. Sedan lade vi till Open GTL

140
00:14:02,720 --> 00:14:09,360
mestadels glömt nuförtiden, en ett-till-ett kopia av något som används i Hollywood, omkonstruerade det eller

141
00:14:09,360 --> 00:14:15,920
rättare sagt omkonstruerade Cyrille Berger det och det kunde användas
för många saker inklusive att skapa helt nya färgrymder.

142
00:14:15,920 --> 00:14:22,920
Det var riktigt häftigt, men Cyrille gifte sig och lämnade projektet
och det var i princip ohållbart så vi övergav det.

143
00:14:22,920 --> 00:14:29,400
Samtidigt konverterade vi Krita till en ny version av KDE Ramverk, och Kross slutade fungera som det skulle

144
00:14:29,400 --> 00:14:35,760
så vi lade ner stödet för det. Lyckligtvis hade Krita då väldigt få användare, så ingen påverkades i verkligheten.

145
00:14:35,760 --> 00:14:43,920
Sedan konverterade vi Krita till en annan version av Qt och KDE, vilket var mycket jobb

146
00:14:43,920 --> 00:14:51,440
En sommar ute på takterrassen när det var riktigt varmt, 
tog Jag skriptkoden för Python från KDE:s

147
00:14:51,440 --> 00:14:58,280
texteditor Kate och kopierade allt det och rättade det för att göra Krita skriptbart. Det är grunden för vårt

148
00:14:58,280 --> 00:15:05,760
nuvarande Python skriptprogrammeringsgränssnitt, vilket är lite begränsat, men det växer och finns fortfarande där.

149
00:15:05,760 --> 00:15:11,920
Man kan göra vad som helst med användargränssnitt i den och skriva vad du än vill. Och sedan, naturligtvis,

150
00:15:11,920 --> 00:15:19,200
hade vi ett enormt Summer of code (GSOC) projekt där vi
implementerade stöd för Disneys SeExpr.

151
00:15:19,200 --> 00:15:25,699
Det gör det verkligen möjligt att skriva skript, skapa saker som fyller dina ... dina lager med vad du vill

152
00:15:25,699 --> 00:15:31,240
Vi är faktiskt inte säkra på hur mycket det används men det är extremt kraftfullt. Så ja! Krita är väldigt

153
00:15:31,240 --> 00:15:37,120
anpassningsbart, och även bortsett från det är det mesta av Krita insticksprogram. Varje verktyg är ett insticksprogram. om du kan

154
00:15:37,120 --> 00:15:43,560
en del C++, jag menar jag kunde inte C++ när jag började knappa på Krita och jag var bra på att misslyckas

155
00:15:43,560 --> 00:15:50,560
göra något användbart, vilket i sin tur attraherade andra bidragsgivare, och Krita lyfte, så låt oss börja knappa!

156
00:15:50,880 --> 00:15:56,560
Som sista ord, Tack så mycket för att du var tillgänglig för intervjun,

157
00:15:56,560 --> 00:16:02,240
Jag vet att du är riktigt upptagen, så jag uppskattar det mycket.

158
00:16:02,240 --> 00:16:05,440
Vilket meddelande vill du skicka till Kritas användare?

159
00:16:05,440 --> 00:16:09,000
Tack Ramón för intervjun. Ett sista meddelande till alla som använder Krita.

160
00:16:09,000 --> 00:16:13,520
Använd Krita mer! Skapa konst, skapa konst, skapa konst! Jag älskar att se alla konstverk framställda med Krita.

161
00:16:13,520 --> 00:16:18,116
Allt ni gör med Krita tycker jag om. Hej då!

162
00:16:18,116 --> 00:16:24,384
Fira ​​25-årsdagen av Kritas utveckling med oss

163
00:16:24,384 --> 00:16:32,312
All återkoppling är verkligt välkommen. Ramón här som vanligt, säger adjö, ha en trevlig dag ... eller natt. Tack så mycket för att du tittade. ❤
