﻿1
00:00:01,120 --> 00:00:04,520
Hallo iedereen! en welkom bij deze nieuwe video!

2
00:00:04,520 --> 00:00:08,200
We vieren de 25ste verjaardag van Krita

3
00:00:08,200 --> 00:00:13,960
en vandaag! Hier! hebben we een erg, erg, speciale gast.

4
00:00:14,000 --> 00:00:16,600
De onderhouder van Krita...Halla Rempt

5
00:00:16,600 --> 00:00:21,600
En Halla is de onderhouder geweest gedurende meer dat 20 jaar!

6
00:00:21,600 --> 00:00:22,960
Het is verbazend!

7
00:00:22,960 --> 00:00:25,880
een erg speciale dag, geniet ervan!

8
00:00:25,880 --> 00:00:28,120
Hi Halla, hoe gaat het met je?

9
00:00:28,400 --> 00:00:30,200
Hi Ramon, fijn je te zien!

10
00:00:30,440 --> 00:00:34,960
25 jaar vieren is een belangrijke mijlpaal.

11
00:00:34,960 --> 00:00:39,200
Wat open-source zijn betekent voor de ontwikkeling van Krita?

12
00:00:39,800 --> 00:00:45,120
Wat open-source zijn betekent voor de ontwikkeling van Krita in de voorbije 25 jaar? Wel, ik zou zeggen

13
00:00:45,120 --> 00:00:50,520
het is absoluut essentieel en dat is om twee redenen. Als eerste natuurlijk dat het open-source is

14
00:00:50,520 --> 00:00:55,680
heeft bijdragen aangetrokken van honderden mensen. Ere zijn meer dan 600

15
00:00:55,680 --> 00:01:00,440
mensen in de lijst met commits van Krita en dat is alleen de ontwikkeling. Niet de websites,

16
00:01:00,440 --> 00:01:05,120
niet de documentatie, niet al het andere en dat is geweldig! Toen we vastzaten, ergens,

17
00:01:05,120 --> 00:01:10,720
konden mensen ons helpen! en veel mensen deden dat. Tenslotte misschien nog belangrijker, nu dat

18
00:01:10,720 --> 00:01:17,200
Krita open-source is, vrije software onder de GPL, het kan nooit gesloten worden. Wanneer over de

19
00:01:17,200 --> 00:01:22,800
jaren we mededingers hebben zien verdwijnen omdat ze uitgekocht zijn door bedrijven zoals Adobe,

20
00:01:22,800 --> 00:01:27,440
we gewoon wisten dat we erop konden vertrouwen dat Krita gewoon door zou gaan zolang als er

21
00:01:27,440 --> 00:01:32,680
bijdragers zijn. Dus open-source zijn is totaal fundamenteel, dat gaat nooit veranderen!

22
00:01:32,760 --> 00:01:35,240
Wow! dat is erg interessante informatie.

23
00:01:35,240 --> 00:01:40,240
Krita is er geweest gedurende deze 25 jaren,

24
00:01:40,240 --> 00:01:42,200
wat een ongelooflijke reis is geweest.

25
00:01:42,200 --> 00:01:46,160
Hoe Krita is geëvolueerd gedurende deze 25 jaar?

26
00:01:46,200 --> 00:01:50,240
25 jaar dat is een behoorlijk lange reis... Ik ben daar niet de gehele   reis geweest.

27
00:01:50,240 --> 00:01:54,840
Misschien slechts 21 jaar wat nog steeds een lange tijd is. Wanneer ik terugkijk

28
00:01:54,840 --> 00:01:58,960
hoe Krita er uitzag toen ik mee ging doen met het project, je kon zelfs niet tekenen.

29
00:01:58,960 --> 00:02:02,720
Je kon afbeeldingen laden en lagen toevoegen en die verplaatsen en dat was het.

30
00:02:02,720 --> 00:02:08,320
Daarop zijn we begonnen te bouwen. We herschreven de gehele kern van Krita. De kern is 4 keer herschreven

31
00:02:08,320 --> 00:02:15,680
en elke keer dat Krita meer kon presteren begonnen we hulpmiddelen toe te voegen, we begonnen fasen toe te voegen

32
00:02:15,680 --> 00:02:23,640
zoals het ondersteunen door Krita van een op openGL gebaseerd werkveld, GPU versneld voordat Photoshop dat deed. CMYK al jaren. We

33
00:02:23,640 --> 00:02:28,040
begonnen meer en meer plezierige zaken te implementeren, meer en meer nuttige zaken en toen kwam de

34
00:02:28,040 --> 00:02:33,800
grote doorbraak. We begonnen naar onze gebruikers te luisteren tijdens deze sessies. Development Sprints,

35
00:02:33,800 --> 00:02:38,960
waar ontwikkelaars en medewerkers van Krita samen komen, meestal werken we samen om te bespreken

36
00:02:38,960 --> 00:02:44,000
wat we gaan doen in het volgende jaar ooit 
sinds onze sprint in Blender Studios... ...en

37
00:02:44,000 --> 00:02:49,560
toen vroegen we de artiesten om hun werkwijze met Krita te 
"Demoën" we vroegen het hen gewoon en... hier is een uur

38
00:02:49,560 --> 00:02:55,000
voor jou en we gaan bekijken hoe je schildert, we hopen 
dat je het niet erg vindt, je mag alles zeggen,

39
00:02:55,000 --> 00:03:00,320
Klagen over alles waarover je wilt klagen. 
Alles mag, en wij de ontwikkelaars, mogen niets

40
00:03:00,320 --> 00:03:03,960
terug praten. We mogen je ook niet helpen, omdat we willen zien wat

41
00:03:03,960 --> 00:03:11,080
de problemen zijn. En dat soort van observeren van de gebruiker hielp ons stap voor stap dingen te verbeteren.

42
00:03:11,080 --> 00:03:18,000
Wat is het meest creatieve gebruik van Krita geweest dat je deed denken...

43
00:03:18,000 --> 00:03:20,680
Wow! daar had ik nooit aan gedacht!

44
00:03:20,680 --> 00:03:27,120
het meeste gebruik dat er is van Krita? Ik vermoed dat dat de persoon is die het hoogste beetje uit Krita wil gebruiken

45
00:03:27,120 --> 00:03:34,120
diepte mogelijkheden voor het maken van sterrenkaarten. Gebruikt enorme astronomische foto's en

46
00:03:34,120 --> 00:03:43,680
er mee wil werken en eerlijk, ik zou willen hebben--,  (wel ik heb dat echt gezegd), gebruik er VIPS voor, maar het was behoorlijk buiten de gebruiksmogelijkheden.

47
00:03:43,880 --> 00:03:49,440
Veel gebruikers zullen zich afvragen wat het team achter Krita motiveert?

48
00:03:50,440 --> 00:03:56,520
Wat ons motiveert? Ik neem aan een zelfzuchtige en onbaatzuchtige reden. De zelfzuchtige reden is

49
00:03:56,520 --> 00:04:02,320
dit is het leukste werk dat ik ooit in mijn leven heb gedaan. Ik heb voor allerlei soorten bedrijven gewerkt minstens

50
00:04:02,320 --> 00:04:09,000
zes of zeven en bij slechts een van deze bedrijven hebben we echt de software waaraan ik ontwikkelde uitgegeven,

51
00:04:09,000 --> 00:04:15,600
sommige bedrijven ontwikkelen software om geld uit te geven het is raar maar Krita komt

52
00:04:15,600 --> 00:04:21,920
in de handen van gebruikers en dat is waar het onbaatzuchtige deel inkomt, omdat deze gebruikers ons

53
00:04:21,920 --> 00:04:29,200
de liefste dingen vertellen, zoals die tijd dat ik een e-mail van iemand in Malaysia kreeg. Haar was verteld door haar ouders

54
00:04:29,200 --> 00:04:33,840
Dat "meisjes hoeven niet te tekenen, dus nee, we geven je geen tekensoftware" haar broer

55
00:04:33,840 --> 00:04:39,520
gaf haar een tekentablet, dus ze ontdekte 
Krita en ze kon beginnen met Kunst te maken en ze

56
00:04:39,520 --> 00:04:46,120
e-mailde me in tonen van uiterste opwinding: " Ik kan kunst 
maken dankzij jou, dank je, dank je "en dat is gewoon

57
00:04:46,120 --> 00:04:51,800
zo motiverend en geeft ons een ongelooflijk gevoel dus als u Krita fijn vindt denk eraan ons te vertellen dat u het heerlijk vindt

58
00:04:51,800 --> 00:04:58,320
omdat we normaal alleen bugrapporten zien. Bugrapporten zijn nuttig, als u bugrapporten maakt, hartelijk dank!

59
00:04:58,320 --> 00:05:03,080
Maar soms kunnen ze een klein beetje demotiverend zijn omdat er zo veel van zijn.

60
00:05:03,200 --> 00:05:09,400
Kun je ons meer over het commerciële gebruik van Krita vertellen?

61
00:05:09,400 --> 00:05:16,240
Commercieel gebruik van Krita. Het is natuurlijk heel fijn. Dat is wat we expliciet zeggen in al onze

62
00:05:16,240 --> 00:05:22,280
Frequently Asked question lijsten (FAQ) in het vak Info over. Maar commercieel

63
00:05:22,280 --> 00:05:29,160
gebruik gebeurt echt. De eerste keer dat ik er van hoorde was toen we Krita presenteerden op SIGGRAPH in Vancouver.

64
00:05:29,160 --> 00:05:35,694
We hadden een booth naast blender dankzij de echt nuttige hulp van Ton Roosendaal, we waren zoals...

65
00:05:35,694 --> 00:05:41,600
er zoveel is, zoveel interesse, zoveel mensen wilden ons spreken en toen kwam deze persoon

66
00:05:41,600 --> 00:05:49,680
naar me toe en hij zei vertel het niemand anders ten minste niet nu maar -psst- we zijn Krita aan het gebruiken

67
00:05:49,680 --> 00:05:55,200
voor de introductie voor "Little, Big", wat een animatiefilm was denk ik door Disney,

68
00:05:55,200 --> 00:06:01,560
Ik ben er niet meer zeker van, dus dat was de eerste keer dat ik hoorde van het commerciële gebruik van Krita en dat was 2014, denk ik.

69
00:06:01,560 --> 00:06:06,480
Sindsdien hebben sponsoring gekregen en hebben we contact gehouden met mensen

70
00:06:06,480 --> 00:06:14,360
die Krita commercieel gebruiken voor games, voor films, voor boeken en dat is gewoon erg stimulerend.

71
00:06:14,360 --> 00:06:23,240
Ok, we willen Krita helpen, hoe wordt Krita gefinancierd, hoe staat het met de toekomst?

72
00:06:23,240 --> 00:06:28,160
We hebben er al een beetje over gesproken. in de basis zijn er twee entiteiten. Aan de ene kant is er de

73
00:06:28,160 --> 00:06:36,600
nonprofit stichting Krita, aan de andere kant is er de Halla Rempt software voor winst, dat ben ik.

74
00:06:36,600 --> 00:06:44,000
en we splitsen de activiteiten tussen de twee. De stichting vraagt donaties wat betekent dat het aftrekbaar is

75
00:06:44,000 --> 00:06:50,400
van de belasting, Halla Rempt software verkoopt DVD's en dingen, maar dat levert natuurlijk nog nauwelijks enige inkomsten

76
00:06:50,400 --> 00:06:58,080
vandaag de dag, omdat we deze geweldige tutorials op YouTube ❤ hebben. Het verkoopt Krita in verschillende app-stores.

77
00:06:58,080 --> 00:07:06,400
De twee inkomstenstromen gecombineerd, het fonds eenmalige donaties, inkomen uit stores is genoeg om 

78
00:07:06,400 --> 00:07:13,000
ons huidige team van ontwikkelaars en Ramon's videos te sponsoren dus dat is tamelijk duurzaam

79
00:07:13,000 --> 00:07:21,160
we doen dat minstens al sinds 2015 met 
stores toegevoegd in 2017 dus we doen het goed.

80
00:07:21,160 --> 00:07:28,520
Hoe heeft de gemeenschap Krita ondersteund en hoe heeft Krita zijn gemeenschap ondersteund?

81
00:07:28,520 --> 00:07:33,360
Terug in de tijd! toen Krita gewoon een hobby was voor iedereen die meedeed,

82
00:07:33,360 --> 00:07:39,120
hadden de meesten van ons geen digitale tekentabletten. Ik begon in Krita te werken omdat ik

83
00:07:39,120 --> 00:07:47,362
er een kreeg voor mijn verjaardag maar het ging kapot dus gingen fondsen werven om twee Wacom tabletten en een tekenpen!

84
00:07:47,362 --> 00:07:54,280
dat was dus toen we opmerkten dat speciaal in de vrije software gemeenschap, er eerder zelfs geen kunstenaars waren

85
00:07:54,280 --> 00:08:03,560
zijn echt ondersteunend en even later, dat was Lukas, hadden we deze Slowaak

86
00:08:03,560 --> 00:08:09,800
student die codeert, die door ging om een reguliere bijdrager te worden, hij deed zelfs zijn proefschrift over Krita's

87
00:08:09,800 --> 00:08:15,400
penseelengines. Hij kreeg er een 10 uit 10 voor omdat het briljant werk was en zijn professoren

88
00:08:15,400 --> 00:08:21,520
verbaasd waren! dat hun student in het echt iets deed dat in de echte wereld werd gebruikt!

89
00:08:21,520 --> 00:08:28,320
Tegelijk waren we Krita over te zetten van Qt3 naar Qt4 en waren we alles aan het herschrijven

90
00:08:28,320 --> 00:08:34,720
niets werkt, alles was te "buggy" voor woorden het was werkelijk verschrikkelijk! op dat punt zei Lukas:

91
00:08:34,720 --> 00:08:40,960
"Mijn laatste drie maanden aan de universiteit zijn bedoeld voor een stage. Ik moet relevante zaken doen,

92
00:08:40,960 --> 00:08:47,160
nuttig, ik geeft er de voorkeur aan om een beetje betaald te worden."
En omdat Krita op dat moment zo ongelooflijk

93
00:08:47,160 --> 00:08:55,720
buggy was en we nog steeds als vrijwilligers, als hobby codeerders aan het vechten om Krita in een bruikbare status te krijgen, laten

94
00:08:55,720 --> 00:09:04,480
Lukas voor 3 maanden betalen om alleen bugs te repareren. Dat was een ENORM succes!. Initieel wilde Lukas hoofdzakelijk enige

95
00:09:04,480 --> 00:09:12,840
geld om zijn huur te betalen maar we konden hem toen, voor ons doen, een nette hoeveelheid geld elke maand betalen,

96
00:09:12,840 --> 00:09:20,400
met het resultaat van de fondsenwerving, zoveel kunstenaars, zoveel enthousiaste mensen, ondersteunden Krita.

97
00:09:20,400 --> 00:09:27,520
Drie maanden werden zes maanden en toen iets later, 
heeft Dmitry Kazakov briljant, zoals nooit, werk gedaan

98
00:09:27,520 --> 00:09:34,160
sinds zijn eerste GSOC met Krita. He ging zijn universitaire studie afronden en ik wilde dat hij

99
00:09:34,160 --> 00:09:39,960
een andere baan nam en hem voor het project verloor. Dus begonnen we enige reguliere fondsenwerving te doen

100
00:09:39,960 --> 00:09:46,400
die ons werkelijk heel wat publiciteit en uitputting bracht, maar leuk om te doen en nu is Dmitry er nog steeds

101
00:09:46,400 --> 00:09:49,880
als full-time werkend aan Krita, als gesponsorde ontwikkelaar, 
dat werkte dus.

102
00:09:50,000 --> 00:09:57,360
Kun je je een tijd herinneren waar de gemeenschap van Krita kwam helpen met de ontwikkeling van Krita?

103
00:09:57,600 --> 00:10:01,600
Het slechtste moment was toen de Nederlandse belastingdienst...

104
00:10:01,600 --> 00:10:07,040
merkte dat... laten die gehele zaak overslaan het geeft me hoofdpijn, in elk geval

105
00:10:07,040 --> 00:10:10,720
ze wilden €24,000 in een keer hebben van de

106
00:10:10,720 --> 00:10:14,800
stichting Krita, geld dat we niet hadden toen we de wereld

107
00:10:14,800 --> 00:10:21,000
er over vertelden. De wereld schoot ons te hulp! 
Wat geweldig was, vanaf dat moment

108
00:10:21,000 --> 00:10:27,320
hebben we nooit meer teruggekeken we kregen een ondersteunende gemeenschap en kregen een ontwikkelfonds

109
00:10:27,320 --> 00:10:35,280
die ik aanbeveel om aan mee te doen, sorry voor de plug, uh we verkopen Krita in verschillende App-stores. App-stores

110
00:10:35,280 --> 00:10:43,480
zijn een absolute hel om mee te werken, maar het betaalt ons maandelijks, dank gemeenschap voor het ondersteunen van ons!

111
00:10:43,480 --> 00:10:50,840
Hoe Krita gebruikers helpt, in het leren van de software of zelfs het coderen van de software?

112
00:10:51,280 --> 00:10:56,520
We proberen Krita toegankelijk te maken of zelfs te leren voor nieuwkomers,

113
00:10:56,520 --> 00:11:03,640
maar in de eerste plaats natuurlijk, proberen we het bestaande 
standaarden voor gebruikersinterface te behouden. We proberen geen

114
00:11:03,640 --> 00:11:09,488
vreemde zaken. We willen juist dat mensen in staat zijn alles te vinden op de plaats waar ze gebruikt worden.

115
00:11:09,488 --> 00:11:15,920
Als u een computer voor de laatste 5, 10, 20 of 30 jaar hebt gebruikt, dan zal niets in Krita u werkelijk

116
00:11:15,920 --> 00:11:22,600
verbazen, wat Krita in zichzelf bruikbaar maakt. We werken natuurlijk ook samen met onze

117
00:11:22,600 --> 00:11:29,840
gebruikersinterface (UI) expert Scott Petrovic en hij helpt ons
om nieuwe mogelijkheden te ontwerpen. Als u nu over het

118
00:11:29,840 --> 00:11:36,560
niveau van de toepassing zelf gaat, dan is documentatie echt belangrijk. We hebben er heel wat tijd aan besteed

119
00:11:36,560 --> 00:11:44,440
en inspanning op onze website voor documentatie: "docs.krita.org" 
laat niet alleen menu-opties zien, maar geeft u ook

120
00:11:44,440 --> 00:11:51,600
handleidingen over werkmethoden. Dingen die niet vanzelf sprekend zijn als je een kantoorgebruiker bent van

121
00:11:51,600 --> 00:11:57,280
digitale toepassingen voor schilderen omdat, laten we daar helder over zijn, digitaal schilderen is een werktuig

122
00:11:57,840 --> 00:12:05,720
anders dan al het andere. I bedoel ja. Ik schilder zelf ook analoog

123
00:12:05,720 --> 00:12:12,240
met olieverf en ik teken een schets en beeldhouw en daarna wanneer ik het probeer te tekenen met Krita weet ik

124
00:12:12,240 --> 00:12:18,600
dat de meeste van de geleerde gewoonten niet overgaan naar digitale toepassing voor schilderen dus hebben we

125
00:12:18,600 --> 00:12:24,920
een uitgebreide website met handleidingen, richtlijnen, 
tips, we hebben ook een geweldige gemeenschap en dankzij

126
00:12:24,960 --> 00:12:34,202
Raghavendra Kamath (raghukamath) hebben we een gebruikswebsite genaamd: krita-artists.org , net als Blender Artists

127
00:12:34,202 --> 00:12:38,120
deze is totaal daarop gebaseerd. Dat is een zich herhalend thema. Wanneer ik denk we hebben

128
00:12:38,120 --> 00:12:43,320
iets nieuws nodig, dan kijk ik eerst naar wat Ton aan het doen is voor Blender en daarna kopieer ik dat gewoon en heb ik geen

129
00:12:43,320 --> 00:12:49,040
schaamte dat toe te geven. Ons Krita kunstenaarsforum is een geweldige bron waar mensen vragen kunnen stellen

130
00:12:49,040 --> 00:12:54,640
maar ook antwoorden kunnen vinden op vragen die eerder zijn gesteld. Tenslotte investeren we heel wat

131
00:12:54,640 --> 00:13:01,280
tijd en geld, in de videoserie van Ramón voor ons Krita kanaal, dus bedankt Ramón voor het met ons werken!.

132
00:13:01,360 --> 00:13:11,160
Hoe aanpasbaar is Krita en welk soort of plug-ins of uitbreidingen kunnen gebruikers maken of gebruiken?

133
00:13:11,840 --> 00:13:18,760
Toen Krita nog echt jong was heeft Cyrille Berger als eerste het eerste interface voor scripts voor Krita gemaakt

134
00:13:18,760 --> 00:13:24,960
om script te maken in JavaScript, Ruby en Python. Wacht! dat is niet echt de eerste, ons eerste scriptinterface.

135
00:13:24,960 --> 00:13:30,120
Het JavaScript-interface van KDE. Ik bedoel weet je 
dat bijna elke browser behalve voor

136
00:13:30,120 --> 00:13:39,640
Firefox vandaag is gebaseerd op het werk van KDE op KHTML een op KDE gebaseerd webbrowser. Die kwam natuurlijk met een JavaScript-engine.

137
00:13:39,640 --> 00:13:47,720
En die JavaScript-engine was een apart dingetje dat we konden gebruiken om scripts in Krita te maken,

138
00:13:47,720 --> 00:13:52,560
dus was dat het allereerste. We hadden dus eerst JavaScript daarna hebben we het vervangen

139
00:13:52,560 --> 00:14:02,720
met die van Kross, wat het ding was dat Ruby, 
Python, etc... mogelijk maakte. Daarna voegden we Open GTL toe

140
00:14:02,720 --> 00:14:09,360
het meest vergeten deze dagen, een een-op-een kloon van iets gebruikt in Hollywood opnieuw dat geïmplementeerd of

141
00:14:09,360 --> 00:14:15,920
eigenlijk Cyrille Berger implementeerde dat weer en het kon gebruikt worden 
voor heel wat zaken inclusief aanmaken van geheel nieuwe kleurruimten.

142
00:14:15,920 --> 00:14:22,920
Het was echt cool, maar Cyrille trouwde en verliet het project 
en het was in de basis niet te onderhouden dus hebben we dat laten vallen.

143
00:14:22,920 --> 00:14:29,400
Tegelijk brachten we Krita over naar een nieuwe versie van het KDE Frameworks en Kross stopte met juist te werken

144
00:14:29,400 --> 00:14:35,760
dus lieten we ondersteuning daarvoor vallen. Gelukkig had Krita toen zeer weinig gebruikers, dus niemand was echt getroffen.

145
00:14:35,760 --> 00:14:43,920
Toen hebben we Krita overgezet naar andere versie van Qt en KDE, wat veel werk was

146
00:14:43,920 --> 00:14:51,440
Een zomer buiten op het dakterras toen het echt warm was, 
nam ik de python scripting code uit de tekst van de

147
00:14:51,440 --> 00:14:58,280
KDE-bewerker van Kate en kopieerde dat en repareerde het om scripts in Krita mogelijk te maken. Dat is de basis van onze

148
00:14:58,280 --> 00:15:05,760
huidige python scripting API die een klein beetje beperkt is, maar het groeit en is er nog steeds.

149
00:15:05,760 --> 00:15:11,920
Je kunt er allerlei gebruikersinterface-achtigs doen en het scripten hoe je wilt. En, natuurlijk,

150
00:15:11,920 --> 00:15:19,200
hadden we dit geweldige Summer of code project (GSOC) waar we 
ondersteuning implementeerden voor Disney's SeExpr.

151
00:15:19,200 --> 00:15:25,699
Het maakt het echt mogelijk om scripts te schrijven, zaken te genereren die zullen passen in jouw... jouw lage met alles wat je wilt

152
00:15:25,699 --> 00:15:31,240
We zijn er niet echt zeker van hoeveel het wordt gebruikt maar het is extreem krachtig. Dus ja! Krita is erg

153
00:15:31,240 --> 00:15:37,120
goed aan te passen en zelfs los daarvan, het meeste van Krita is plug-ins. Elk hulpmiddel is een plug-in. Als je iets weet van

154
00:15:37,120 --> 00:15:43,560
van C++, I bedoel ik wist niets van C++ toen ik begon met hacken van Krita en ik voelde me goed toe het faalde

155
00:15:43,560 --> 00:15:50,560
om iets nuttigs te doen wat uitdraaide op het aantrekken van andere bijdragers, en Krita kwam van de grond laten we dus beginnen met hacken!

156
00:15:50,880 --> 00:15:56,560
Als laatste, heel veel dank voor het beschikbaar zijn voor dit interview,

157
00:15:56,560 --> 00:16:02,240
Ik weet dat je het echt druk hebt, dus waardeer het heel erg.

158
00:16:02,240 --> 00:16:05,440
Wat zou willen zeggen tegen de gebruikers van Krita?

159
00:16:05,440 --> 00:16:09,000
Dank je Ramón voor het interview. Een laatste bericht voor iedereen die Krita gebruikt.

160
00:16:09,000 --> 00:16:13,520
Gebruik Krita meer! maak kunst, maak kunst, maak kunst! Ik kijk graag naar alle kunst die met Krita is gemaakt.

161
00:16:13,520 --> 00:16:18,116
Alles wat je doet met Krita is wat mij betreft OK. bye bye!

162
00:16:18,116 --> 00:16:24,384
Vier met ons de 25ste verjaardag van ontwikkelen voor Krita

163
00:16:24,384 --> 00:16:32,312
Elke terugkoppeling is echt welkom. Ramon hier zoals gewoonlijk, zegt gedag, nog een fijne dag... of avond. Hartelijk dank voor het bekijken. ❤
