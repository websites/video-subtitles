﻿1
00:00:01,120 --> 00:00:04,520
Ciao a tutti! E benvenuti in questo nuovo video!

2
00:00:04,520 --> 00:00:08,200
Celebriamo il 25° anniversario di Krita

3
00:00:08,200 --> 00:00:13,960
e oggi! Qui! abbiamo un ospite molto, molto speciale.

4
00:00:14,000 --> 00:00:16,600
Il responsabile di Krita...Halla Rempt

5
00:00:16,600 --> 00:00:21,600
E Halla è stato il responsabile per più di 20 anni!

6
00:00:21,600 --> 00:00:22,960
È incredibile!

7
00:00:22,960 --> 00:00:25,880
un giorno davvero speciale, goditelo!

8
00:00:25,880 --> 00:00:28,120
Ciao Halla, come stai?

9
00:00:28,400 --> 00:00:30,200
Ciao Ramon, piacere di vederti!

10
00:00:30,440 --> 00:00:34,960
Festeggiare 25 anni è un traguardo significativo.

11
00:00:34,960 --> 00:00:39,200
Cosa ha significato essere open source per lo sviluppo di Krita?

12
00:00:39,800 --> 00:00:45,120
Cosa ha significato essere open source per lo sviluppo di Krita negli ultimi 25 anni? Beh, direi

13
00:00:45,120 --> 00:00:50,520
è assolutamente essenziale e questo per due motivi. Il primo, ovviamente, è che essendo open source

14
00:00:50,520 --> 00:00:55,680
ha attinto al contributo di centinaia di persone. Ci sono oltre 600

15
00:00:55,680 --> 00:01:00,440
persone nell'elenco dei commit di Krita e questo è solo lo sviluppo. Non i siti web,

16
00:01:00,440 --> 00:01:05,120
non la documentazione, non tutto il resto e questo è fantastico! Ogni volta che rimanevamo bloccati, da qualche parte,

17
00:01:05,120 --> 00:01:10,720
le persone potevano aiutarci! e molte persone lo hanno fatto. Infine, forse ancora più importante, ora che

18
00:01:10,720 --> 00:01:17,200
Krita è un software libero open source sotto GPL, non potrà mai essere chiuso. Quando con il passare

19
00:01:17,200 --> 00:01:22,800
degli anni abbiamo visto i concorrenti scomparire perché rilevati da aziende come Adobe,

20
00:01:22,800 --> 00:01:27,440
eravamo consapevoli che potevamo confidare nel fatto che Krita sarebbe continuato finché ci fossero stati

21
00:01:27,440 --> 00:01:32,680
collaboratori. Quindi essere open source è assolutamente fondamentale e questo non cambierà mai!

22
00:01:32,760 --> 00:01:35,240
Oh! Questa è un'informazione molto interessante

23
00:01:35,240 --> 00:01:40,240
Krita è stato in circolazione in questi 25 anni,

24
00:01:40,240 --> 00:01:42,200
che è un viaggio incredibile.

25
00:01:42,200 --> 00:01:46,160
Come si è evoluto Krita in questi 25 anni?

26
00:01:46,200 --> 00:01:50,240
25 anni sono un viaggio piuttosto lungo... Non sono stato lì per tutto quel viaggio.

27
00:01:50,240 --> 00:01:54,840
Forse solo 21 anni, il che è comunque molto tempo. Quando guardo indietro

28
00:01:54,840 --> 00:01:58,960
all'aspetto di Krita quando mi sono unito al progetto, non potevi nemmeno disegnare.

29
00:01:58,960 --> 00:02:02,720
Potevi caricare immagini, aggiungere livelli, spostare livelli e basta.

30
00:02:02,720 --> 00:02:08,320
Quindi, abbiamo iniziato a costruire su questo. Abbiamo riscritto l'intero nucleo di Krita. Il core è stato riscritto 4 volte

31
00:02:08,320 --> 00:02:15,680
e ogni volta che Krita diventava più prestante abbiamo iniziato ad aggiungere strumenti, abbiamo iniziato ad aggiungere fasi

32
00:02:15,680 --> 00:02:23,640
come il supporto di un canvas basato su openGL, l'accelerazione della GPU prima di Photoshop. CMYK per anni.

33
00:02:23,640 --> 00:02:28,040
Abbiamo iniziato a implementare cose sempre più divertenti, cose sempre più utili e poi è arrivata la

34
00:02:28,040 --> 00:02:33,800
grande occasione. Abbiamo iniziato ad ascoltare i nostri utenti durante queste sessioni. Sprint di sviluppo,

35
00:02:33,800 --> 00:02:38,960
dove gli sviluppatori e i collaboratori di Krita si incontrano, per lo più lavoriamo insieme e discutiamo

36
00:02:38,960 --> 00:02:44,000
su cosa faremo nel prossimo anno
dal nostro sprint ai Blender Studios... ...e

37
00:02:44,000 --> 00:02:49,560
poi chiediamo agli artisti di preparare delle dimostrazioni lavorando con 
Krita glielo abbiamo semplicemente chiesto e... ecco un'ora

38
00:02:49,560 --> 00:02:55,000
per voi e vi guarderemo dipingere, spero
non vi dispiaccia, potete dire quello che volete,

39
00:02:55,000 --> 00:03:00,320
lamentarvi di tutto ciò che avete di cui lamentarvi.
Tutto va bene, e a noi sviluppatori non è

40
00:03:00,320 --> 00:03:03,960
permesso di rispondere. Non ci è consentito nemmeno aiutarvi, perché vogliamo vedere quali

41
00:03:03,960 --> 00:03:11,080
sono i problemi. E questo tipo di osservazione degli utenti ci ha davvero aiutato a migliorare Krita a passi da gigante.

42
00:03:11,080 --> 00:03:18,000
Qual è stato l'uso più creativo di Krita che ti ha fatto pensare...

43
00:03:18,000 --> 00:03:20,680
Oh! Non ci avevo mai pensato!

44
00:03:20,680 --> 00:03:27,120
l'uso più creativo di Krita? Immagino che sia la persona che voleva usare le capacità

45
00:03:27,120 --> 00:03:34,120
di elevata profondità di bit di Krita per creare mappe stellari. Utilizzato enormi fotografie astronomiche e

46
00:03:34,120 --> 00:03:43,680
voleva lavorare con loro e, onestamente, avrei... (beh, in realtà l'ho detto), usato VIPS per quello, ma era un uso piuttosto fuori dal comune.

47
00:03:43,880 --> 00:03:49,440
Molti utenti potrebbero chiedersi: cosa motiva la squadra che sta dietro a Krita?

48
00:03:50,440 --> 00:03:56,520
Cosa ci motiva? Immagino che sia una ragione egoistica e altruista. La ragione egoistica è

49
00:03:56,520 --> 00:04:02,320
questo è il lavoro più divertente che abbia mai fatto in vita mia. Ho lavorato almeno per tutti i tipi di aziende

50
00:04:02,320 --> 00:04:09,000
sei o sette e solo in una di quelle aziende abbiamo effettivamente rilasciato il software su cui stavo sviluppando,

51
00:04:09,000 --> 00:04:15,600
alcune aziende sviluppano software solo per spendere soldi, è pazzesco ma Krita finisce

52
00:04:15,600 --> 00:04:21,920
nelle mani degli utenti ed è qui che entra in gioco la parte altruistica, perché questi utenti ci dicono il

53
00:04:21,920 --> 00:04:29,200
cose più carine, come quella volta che ho ricevuto un'email da qualcuno in Malesia. Le era stato detto dai suoi genitori

54
00:04:29,200 --> 00:04:33,840
che «le ragazze non hanno bisogno di disegnare, quindi no, non ti prenderemo un software di disegno», suo fratello

55
00:04:33,840 --> 00:04:39,520
le regalò una tavoletta da disegno, così lei scoprì 
Krita e iniziò a creare arte, mi

56
00:04:39,520 --> 00:04:46,120
mi scrisse in tono di assoluta estasi:«Posso fare arte
grazie, grazie, grazie» e ciò è così

57
00:04:46,120 --> 00:04:51,800
motivante, ci dà una sensazione incredibile. Per cui se vi piace Krita ricordatevi di dirci che vi piace

58
00:04:51,800 --> 00:04:58,320
perché normalmente vediamo solo segnalazioni di bug. Le segnalazioni di bug sono utili, se fate segnalazioni di bug, grazie!

59
00:04:58,320 --> 00:05:03,080
Ma a volte possono essere un po' demotivanti perché ce ne sono così tante.

60
00:05:03,200 --> 00:05:09,400
Puoi dirci di più sull'utilizzo commerciale di Krita?

61
00:05:09,400 --> 00:05:16,240
Utilizzo commerciale di Krita. Ovviamente va benissimo. Questo è quello che diciamo esplicitamente in tutte le nostre

62
00:05:16,240 --> 00:05:22,280
domande frequenti (FAQ), nella casella informativa, ovunque. Ma l'uso commerciale

63
00:05:22,280 --> 00:05:29,160
avviene effettivamente. La prima volta che ne ho sentito parlare è stato mentre presentavamo Krita al SIGGRAPH di Vancouver.

64
00:05:29,160 --> 00:05:35,694
Avevamo un banchetto accanto a blender grazie all'aiuto davvero utile di Ton Roosendaal, eravamo come...

65
00:05:35,694 --> 00:05:41,600
c'è così tanto, così tanto interesse, così tante persone volevano parlare con noi e poi si è avvicinato questo ragazzo

66
00:05:41,600 --> 00:05:49,680
e mi ha detto: «Non dirlo a nessun altro almeno per ora ma -psst- stiamo usando Krita

67
00:05:49,680 --> 00:05:55,200
per l'introduzione di "Little, Big"», che era un film d'animazione credo della Disney,

68
00:05:55,200 --> 00:06:01,560
Non ne sono più sicuro, quindi quella è stata la prima volta che ho sentito parlare dell'uso commerciale di Krita e credo che fosse il 2014.

69
00:06:01,560 --> 00:06:06,480
Da allora, abbiamo ricevuto sponsorizzazioni e siamo rimasti in contatto con le persone

70
00:06:06,480 --> 00:06:14,360
che utilizzano Krita a livello commerciale per giochi, film, libri, e questo è semplicemente molto stimolante.

71
00:06:14,360 --> 00:06:23,240
Ok, quindi vogliamo aiutare Krita, come viene finanziato Krita, quanto è sostenibile per il futuro?

72
00:06:23,240 --> 00:06:28,160
Ne abbiamo già parlato un po'. Fondamentalmente ci sono due entità. Da un lato c'è la

73
00:06:28,160 --> 00:06:36,600
Krita Foundation senza fini di lucro, d'altra parte c'è il software a scopo di lucro Halla Rempt, che sono io.

74
00:06:36,600 --> 00:06:44,000
e abbiamo diviso le attività tra le due. La fondazione sollecita donazioni, il che significa che è esentata dalle tasse, 

75
00:06:44,000 --> 00:06:50,400
Halla Rempt software vende DVD e roba del genere, ma ovviamente non genera quasi più entrate

76
00:06:50,400 --> 00:06:58,080
di questi tempi perché abbiamo questi fantastici tutorial su YouTube ❤. Vende Krita in vari store di applicazioni.

77
00:06:58,080 --> 00:07:06,400
I due flussi di entrate combinati, le donazioni una tantum della fondazione e le entrate dagli store, sono sufficienti per sostenere

78
00:07:06,400 --> 00:07:13,000
la nostra squadra di sviluppatori attualmente sponsorizzati e i video di Ramon, quindi è abbastanza sostenibile

79
00:07:13,000 --> 00:07:21,160
almeno lo facciamo dal 2015 con 
gli store aggiunti nel 2017, per cui stiamo facendo un buon lavoro.

80
00:07:21,160 --> 00:07:28,520
In che modo la comunità ha sostenuto Krita e in che modo Krita ha sostenuto la sua comunità?

81
00:07:28,520 --> 00:07:33,360
Molto indietro! Quando Krita era solo un hobby per tutti coloro che ne erano coinvolti,

82
00:07:33,360 --> 00:07:39,120
la maggior parte di noi non aveva accesso alle tavolette da disegno digitali. Ho iniziato a lavorare a Krita perché ne ho

83
00:07:39,120 --> 00:07:47,362
ricevuta una per il mio compleanno, ma ha iniziato ad avere problemi, quindi abbiamo fatto una raccolta fondi per ottenere due tavolette Wacom e una penna artistica!

84
00:07:47,362 --> 00:07:54,280
è stato allora che abbiamo notato che, soprattutto nella comunità del software libero, non c'erano nemmeno artisti

85
00:07:54,280 --> 00:08:03,560
allora, erano davvero di supporto, e un po' più tardi, quello era Lukas, avevamo questo 

86
00:08:03,560 --> 00:08:09,800
studente di programmazione slovacco, che divenne poi un collaboratore regolare, fece anche la sua tesi sui

87
00:08:09,800 --> 00:08:15,400
motori dei pennelli di Krita. Ottenne 10 punti su 10 perché era un lavoro brillante e i suoi professori

88
00:08:15,400 --> 00:08:21,520
furono molto stupiti che il loro studente avesse effettivamente fatto cose che venivano usate nel mondo reale!

89
00:08:21,520 --> 00:08:28,320
Allo stesso tempo stavamo convertendo Krita da Qt3 a Qt4, stavamo riscrivendo tutto

90
00:08:28,320 --> 00:08:34,720
non funzionava niente, pieno di bug per descriverlo a parole, era davvero orribile! A quel punto Lukas disse:

91
00:08:34,720 --> 00:08:40,960
«I miei ultimi tre mesi all'università dovrebbero essere uno stage. Devo fare cose rilevanti,

92
00:08:40,960 --> 00:08:47,160
utili, preferirei essere pagato qualcosa per questo."
. E perché Krita a quel tempo era così incredibilmente

93
00:08:47,160 --> 00:08:55,720
pieno di bug, e stavamo lottando come volontari e programmatori per hobby per avere Krita in uno stato utile, decidemmo

94
00:08:55,720 --> 00:09:04,480
di finanziare Lukas solo per correggere i bug per 3 mesi. È stato un ENORME successo! Inizialmente Lukas voleva qualcosa

95
00:09:04,480 --> 00:09:12,840
per pagare l'affitto ma potevamo pagargli allora, per noi, una discreta somma di denaro ogni mese,

96
00:09:12,840 --> 00:09:20,400
con il risultato della raccolta fondi, tanti artisti, tante persone entusiaste, ha sostenuto Krita.

97
00:09:20,400 --> 00:09:27,520
Tre mesi sono diventati sei mesi e poi, un po' qualche tempo dopo, 
Dmitry Kazakov ha svolto un lavoro brillante

98
00:09:27,520 --> 00:09:34,160
sin dal suo primo GSOC con Krita. Stava per finire l'università e io non volevo che 

99
00:09:34,160 --> 00:09:39,960
accettasse un altro lavoro e perderlo per il progetto. Quindi abbiamo iniziato a fare alcune raccolte fondi regolari

100
00:09:39,960 --> 00:09:46,400
il che in realtà ci ha procurato molta pubblicità ed è stato estenuante, ma divertente da fare e bene, Dmitry è ancora

101
00:09:46,400 --> 00:09:49,880
impiegato a tempo pieno su Krita ,come sviluppatore sponsorizzato,
per cui ha funzionato.

102
00:09:50,000 --> 00:09:57,360
Potresti ricordare un momento in cui la comunità di Krita è venuta in aiuto allo sviluppo di Krita?

103
00:09:57,600 --> 00:10:01,600
Il momento peggiore è stato quando l'ufficio delle imposte olandese...

104
00:10:01,600 --> 00:10:07,040
ci notificò che... tralasciamo le cose complicate, mi fa venire il mal di testa, comunque

105
00:10:07,040 --> 00:10:10,720
volevano 24.000 € in una volta sola dalla

106
00:10:10,720 --> 00:10:14,800
Krita Foundation, denaro che non avevamo. Prendemmo la decisione di comunicarlo

107
00:10:14,800 --> 00:10:21,000
al mondo. E il mondo corse a sostenerci! 
Il che fu fantastico e da

108
00:10:21,000 --> 00:10:27,320
quel momento in poi non ci siamo mai guardati indietro, abbiamo una comunità solidale e abbiamo uno fondo di sviluppo,

109
00:10:27,320 --> 00:10:35,280
al quale vi invito a unirvi, scusate per la divagazione, vendiamo Kritain vari store di applicazioni. Gli store di applicazioni

110
00:10:35,280 --> 00:10:43,480
sono un vero inferno su cui lavorare, ma ci pagano mensilmente, graziealla comunità per averci supportato!

111
00:10:43,480 --> 00:10:50,840
In che modo Krita aiuta gli utenti, nell'apprendimento del software o addirittura nella scrittura del codice del programma?

112
00:10:51,280 --> 00:10:56,520
Stiamo cercando di rendere Krita accessibile o addirittura apprendibile per i nuovi arrivati,

113
00:10:56,520 --> 00:11:03,640
ma in primo luogo, ovviamente, cerchiamo semplicemente di mantenere gli standard 
esistenti dell'interfaccia utente. Non cerchiamo di fare

114
00:11:03,640 --> 00:11:09,488
cose nuove e strane. Vogliamo solo che le persone siano in grado di trovare tutto nel posto a cui sono abituati.

115
00:11:09,488 --> 00:11:15,920
Se hai usato un computer negli ultimi 5, 10, 20 o 30 anni, allora non c'è niente in Krita che vi

116
00:11:15,920 --> 00:11:22,600
sorprenderà, cosa che rende Krita di per sé utilizzabile. Naturalmente stiamo anche lavorando insieme al nostro

117
00:11:22,600 --> 00:11:29,840
esperto di interfaccia utente (UI), Scott Petrovic, che ci sta aiutando 
per progettare nuove funzionalità. Ora, se si va oltre il

118
00:11:29,840 --> 00:11:36,560
livello dell'applicazione stessa, la documentazione è davvero importante. Noi spendiamo un bel po' di tempo

119
00:11:36,560 --> 00:11:44,440
e sforzi sul nostro sito web di documentazione: «docs.krita.org» 
non elenca solo tutte le opzioni del menu, fornisce anche

120
00:11:44,440 --> 00:11:51,600
tutorial sugli aspetti del flusso di lavoro. Cose che potrebbero non essere evidenti se sei un utente d'ufficio di

121
00:11:51,600 --> 00:11:57,280
applicazioni di pittura digitale perché parliamoci chiaro, la pittura  digitale è uno strumento

122
00:11:57,840 --> 00:12:05,720
separato da tutto il resto. Voglio dire, sì. Anch'io dipingo in modo analogico

123
00:12:05,720 --> 00:12:12,240
usando colori ad olio e disegno uno schizzo e scolpisco e poi quando provo a disegnare usando Krita, so

124
00:12:12,240 --> 00:12:18,600
che la maggior parte delle abitudini apprese non si trasferiscono in un'applicazione di pittura digitale, per cui abbiamo

125
00:12:18,600 --> 00:12:24,920
un ampio sito web con tutorial, linee guida, 
suggerimenti, abbiamo anche una grande comunità e grazie

126
00:12:24,960 --> 00:12:34,202
a Raghavendra Kamath (raghukamath) abbiamo un sito web con moduli utente chiamato: krita-artists.org, proprio come Blender Artists

127
00:12:34,202 --> 00:12:38,120
è totalmente basato su quello. Questo è un tema ricorrente. Ogni volta che penso che ne abbiamo bisogno

128
00:12:38,120 --> 00:12:43,320
qualcosa di nuovo, prima guardo cosa Ton sta facendo per Blender e poi  lo copio e non lo sono

129
00:12:43,320 --> 00:12:49,040
mi vergogno di ammetterlo. Il nostro forum di artisti Krita è un'enorme risorsa in cui le persone possono porre domande

130
00:12:49,040 --> 00:12:54,640
ma trovare anche le risposte alle domande che sono state poste in precedenza. Infine investiamo molto

131
00:12:54,640 --> 00:13:01,280
tempo e denaro, nella serie di video Ramón per il nostro canale Krita, quindi grazie Ramón per il tuo lavoro!

132
00:13:01,360 --> 00:13:11,160
Quanto è personalizzabile Krita e che tipo di plugin o estensioni possono creare o utilizzare gli utenti?

133
00:13:11,840 --> 00:13:18,760
Quando Krita era molto giovane, Cyrille Berger creò per la prima volta la prima interfaccia di creazione script per Krita

134
00:13:18,760 --> 00:13:24,960
per poter utilizzare JavaScript, Ruby e Python. Aspetta! in realtà non è quella la prima interfaccia di

135
00:13:24,960 --> 00:13:30,120
creazione script. L'interfaccia JavaScript di KDE. Voglio dire, sapete
che praticamente tutti i browser tranne

136
00:13:30,120 --> 00:13:39,640
Firefox al giorno d'oggi si basano sul lavoro di KDE su KHTML, un browser web basato su KDE. Ovviamente è arrivato con il motore JavaScript.

137
00:13:39,640 --> 00:13:47,720
E quel motore JavaScript era una cosa separata che potevamo usare per creare script per Krita,

138
00:13:47,720 --> 00:13:52,560
quindi questa è stata la primissima cosa. Perciò prima avevamo JavaScript e poi lo abbiamo sostituito

139
00:13:52,560 --> 00:14:02,720
con Kross, che è ciò che ha reso 
Ruby, Python, ecc... possibili. Poi abbiamo aggiunto Open GTL

140
00:14:02,720 --> 00:14:09,360
per lo più dimenticato di questi tempi, un clone uno a uno di qualcosa usato a Hollywood, lo ha re-implementato o

141
00:14:09,360 --> 00:14:15,920
piuttosto Cyrille Berger lo ha re-implementato e poteva essere usato 
per molte cose, inclusa la creazione di spazi di colore completamente nuovi.

142
00:14:15,920 --> 00:14:22,920
È stato davvero bello, ma Cyrille si è sposato e ha lasciato il progetto
ed era praticamente ingestibile, per cui l'abbiamo abbandonato.

143
00:14:22,920 --> 00:14:29,400
Allo stesso tempo, abbiamo portato Krita su una nuova versione di KDE Frameworks e Kross ha smesso di funzionare correttamente

144
00:14:29,400 --> 00:14:35,760
quindi abbiamo interrotto il supporto per questo. Fortunatamente all'epoca Krita aveva pochissimi utenti, quindi nessuno fu realmente interessato.

145
00:14:35,760 --> 00:14:43,920
Poi abbiamo portato Krita su un'altra versione di Qt e KDE, il che ha richiesto molto lavoro

146
00:14:43,920 --> 00:14:51,440
Un'estate fuori sulla terrazza sul tetto quando faceva davvero caldo, 
Ho preso il codice di creazione python dal testo dell'editor di testo di KDE

147
00:14:51,440 --> 00:14:58,280
Kate e copiato tutto e corretto per supportare gli script in Krita. Questa è la base del nostro

148
00:14:58,280 --> 00:15:05,760
attuale API di creazione script python che è un po' limitata, ma sta crescendo ed è ancora lì.

149
00:15:05,760 --> 00:15:11,920
Puoi fare qualsiasi cosa relativa all'interfaccia utente e creare script come preferisci. E poi, ovviamente,

150
00:15:11,920 --> 00:15:19,200
abbiamo avuto questo enorme progetto Summer of code (GSOC) in cui
implementato il supporto per SeExpr della Disney.

151
00:15:19,200 --> 00:15:25,699
Rende davvero possibile scrivere script, generare materiale che riempirà i tuoi... i tuoi livelli con tutto ciò che desideri

152
00:15:25,699 --> 00:15:31,240
In realtà non siamo sicuri di quanto venga utilizzato, ma è estremamente potente. Quindi sì! Krita è molto

153
00:15:31,240 --> 00:15:37,120
personalizzabile e, a parte questo, la maggior parte di Krita è composta da estensioni. Ogni strumento è un'estensione. se conosci un

154
00:15:37,120 --> 00:15:43,560
un po' di C++, voglio dire, non conoscevo il C++ quando ho iniziato a scrivere il codice Krita e ho fatto bene

155
00:15:43,560 --> 00:15:50,560
per fare qualcosa di utile che a sua volta abbia attirato altri contributori, e Krita è decollato, quindi iniziamo a scrivere codice!

156
00:15:50,880 --> 00:15:56,560
Per le ultime parole, grazie mille per essere stato disponibile per questa intervista,

157
00:15:56,560 --> 00:16:02,240
So che sei molto impegnato, quindi lo apprezzo molto.

158
00:16:02,240 --> 00:16:05,440
Quale messaggio vorresti inviare agli utenti di Krita?

159
00:16:05,440 --> 00:16:09,000
Grazie Ramón per l'intervista. Un ultimo messaggio per tutti coloro che usano Krita.

160
00:16:09,000 --> 00:16:13,520
Usate di più Krita! Fate arte, fate arte, fate arte! Adoro vedere tutta l'arte realizzata con Krita.

161
00:16:13,520 --> 00:16:18,116
Qualunque cosa tu faccia con Krita per me va bene. Ciao ciao!

162
00:16:18,116 --> 00:16:24,384
Festeggia con noi il 25° anniversario dello sviluppo di Krita

163
00:16:24,384 --> 00:16:32,312
Qualsiasi riscontro è davvero benvenuto. Ramon è qui come al solito, vi saluta e vi augura una buona giornata... o buona notte. Grazie mille per la visione. ❤
