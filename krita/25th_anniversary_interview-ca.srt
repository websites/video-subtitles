﻿1
00:00:01,120 --> 00:00:04,520
Hola a tothom, i us donem la benvinguda a aquest vídeo nou!

2
00:00:04,520 --> 00:00:08,200
Estem celebrant el 25è aniversari del Krita

3
00:00:08,200 --> 00:00:13,960
i avui! Aquí! Tenim un convidat molt, molt, especial.

4
00:00:14,000 --> 00:00:16,600
La mantenidora del Krita... Halla Rempt

5
00:00:16,600 --> 00:00:21,600
I Halla ha estat la mantenidora durant més de 20 anys!

6
00:00:21,600 --> 00:00:22,960
És sorprenent!

7
00:00:22,960 --> 00:00:25,880
Un dia molt especial, gaudeix-lo!

8
00:00:25,880 --> 00:00:28,120
Hola Halla, com va?

9
00:00:28,400 --> 00:00:30,200
Hola Ramon, encantat de veure't!

10
00:00:30,440 --> 00:00:34,960
Celebrar 25 anys és una fita important.

11
00:00:34,960 --> 00:00:39,200
Què significa ser de codi obert per al desenvolupament del Krita?

12
00:00:39,800 --> 00:00:45,120
Què significa ser codi obert per al desenvolupament del Krita en els últims 25 anys? Bé, jo diria

13
00:00:45,120 --> 00:00:50,520
és absolutament essencial i això per dos motius. El primer, per descomptat, és que tractant-se de codi obert

14
00:00:50,520 --> 00:00:55,680
ha incorporat les col·laboracions de centenars de persones. Hi ha més de 600

15
00:00:55,680 --> 00:01:00,440
persones a la llista de comissions del Krita i això és només el desenvolupament. No els llocs web,

16
00:01:00,440 --> 00:01:05,120
ni la documentació, ni tota la resta i això és impressionant! Sempre que ens quedem aturats, en algun lloc,

17
00:01:05,120 --> 00:01:10,720
la gent ens pot ajudar! I molta gent ho ha fet. Finalment, potser encara més important, ara que

18
00:01:10,720 --> 00:01:17,200
el Krita és programari lliure de codi obert d'acord amb la GPL, mai es podrà tancar. Durant

19
00:01:17,200 --> 00:01:22,800
anys hem vist desaparèixer competidors perquè els han comprat empreses com Adobe,

20
00:01:22,800 --> 00:01:27,440
sabíem que podíem confiar en el fet que el Krita només continuaria tant de temps com hi hagués

21
00:01:27,440 --> 00:01:32,680
col·laboradors. Així que ser de codi obert és totalment fonamental, això mai canviarà!

22
00:01:32,760 --> 00:01:35,240
Vaja! Aquesta és una informació molt interessant.

23
00:01:35,240 --> 00:01:40,240
El Krita ha estat creixent durant aquests 25 anys,

24
00:01:40,240 --> 00:01:42,200
i això és un viatge increïble.

25
00:01:42,200 --> 00:01:46,160
Com ha evolucionat el Krita durant aquests 25 anys?

26
00:01:46,200 --> 00:01:50,240
25 anys és un viatge bastant llarg... No he estat allà durant tot aquest viatge.

27
00:01:50,240 --> 00:01:54,840
Potser només 21 anys, que encara és molt de temps. Quan miro enrere

28
00:01:54,840 --> 00:01:58,960
com era el Krita quan em vaig unir al projecte, ni tan sols es podia dibuixar.

29
00:01:58,960 --> 00:02:02,720
Podíeu carregar imatges afegint capes i moure-les i això era tot.

30
00:02:02,720 --> 00:02:08,320
Així que vam començar a construir-ho. Es va reescriure tot el nucli del Krita. El nucli s'ha reescrit 4 vegades

31
00:02:08,320 --> 00:02:15,680
i cada vegada que el Krita té més rendiment comencem a afegir eines, comencen a afegir fases

32
00:02:15,680 --> 00:02:23,640
com que el Krita admetés un llenç basat en openGL, acceleració de GPU abans del Photoshop. El CMYK des de fa temps.

33
00:02:23,640 --> 00:02:28,040
Vam començar a implementar més i més coses divertides, més i més coses útils i després va arribar el

34
00:02:28,040 --> 00:02:33,800
gran descans. Vam començar a escoltar els nostres usuaris i vam tenir aquestes sessions. Esprints de desenvolupament,

35
00:02:33,800 --> 00:02:38,960
on es reuneixen els desenvolupadors i col·laboradors del Krita, sobretot treballant i debatent conjuntament

36
00:02:38,960 --> 00:02:44,000
el que farem l'any vinent 
des del nostre esprint a Blender Studios... i

37
00:02:44,000 --> 00:02:49,560
després demanem als artistes que facin una «mostra» de la feina 
amb el Krita i... durant una hora

38
00:02:49,560 --> 00:02:55,000
els veurem pintar, amb l'esperança que 
no els importi, i poden dir el que vulguin,

39
00:02:55,000 --> 00:03:00,320
queixar-se de tot allò de què han de queixar-se. 
Qualsevol cosa val, i nosaltres, els desenvolupadors, no tenim

40
00:03:00,320 --> 00:03:03,960
permís per a contestar. Tampoc podem ajudar-te, perquè volem veure quins

41
00:03:03,960 --> 00:03:11,080
són els problemes. I aquesta mena d'observació de l'usuari realment ens ajuda a millorar el Krita a passos de gegant.

42
00:03:11,080 --> 00:03:18,000
Quin ha estat l'ús més creatiu del Krita que us ha fet pensar...

43
00:03:18,000 --> 00:03:20,680
Vaja! Mai hi he pensat!

44
00:03:20,680 --> 00:03:27,120
l'ús més estrany del Krita? Suposo que és la persona que volia utilitzar les capacitats de profunditat

45
00:03:27,120 --> 00:03:34,120
del bit alt del Krita per a fer mapes d'estrelles. S'utilitzen fotografies astronòmiques enormes i

46
00:03:34,120 --> 00:03:43,680
volia treballar amb elles i, sincerament, hi hauria... (bé, de fet ho vaig dir), d'utilitzar el VIPS per a això, però era un ús força estrany.

47
00:03:43,880 --> 00:03:49,440
Molts usuaris podrien preguntar-se, què motiva l'equip del Krita?

48
00:03:50,440 --> 00:03:56,520
Què ens motiva? Suposo que una raó egoista i desinteressada. La raó egoista és

49
00:03:56,520 --> 00:04:02,320
aquesta és la feina més divertida que he fet en la meva vida. He treballat com a mínim en sis o set empreses

50
00:04:02,320 --> 00:04:09,000
de tota mena, i només en una d'aquestes empreses vam publicar el programari que jo estava desenvolupant,

51
00:04:09,000 --> 00:04:15,600
algunes empreses només desenvolupen programari per a gastar diners, és una bogeria, però el Krita es posa en marxa

52
00:04:15,600 --> 00:04:21,920
de la mà dels usuaris i aquí és on entra la part desinteressada, perquè aquests usuaris ens diuen les

53
00:04:21,920 --> 00:04:29,200
coses més agradables, com aquella vegada que vaig rebre un correu electrònic d'algú de Malàisia. Els seus pares li van dir

54
00:04:29,200 --> 00:04:33,840
que «les noies no necessiten dibuixar, així que no, no et facilitarem cap programa de dibuix». El seu germà

55
00:04:33,840 --> 00:04:39,520
li va donar una tauleta de dibuix, així que ella va descobrir 
el Krita i va poder començar a fer Art

56
00:04:39,520 --> 00:04:46,120
i ella em va enviar en tons d'èxtasi absolut: «Puc fer 
art gràcies, gràcies, gràcies, gràcies» i això és només

57
00:04:46,120 --> 00:04:51,800
tan motivador i ens provoca una sensació increïble. Així que si t'agrada el Krita, pensa a dir-nos que t'agrada

58
00:04:51,800 --> 00:04:58,320
perquè normalment només veiem informes d'errors. Els informes d'error són útils, si feu informes d'error, gràcies!

59
00:04:58,320 --> 00:05:03,080
Però de vegades poden ser una mica desmotivadors perquè n'hi ha tants.

60
00:05:03,200 --> 00:05:09,400
Ens podeu explicar més sobre l'ús comercial del Krita?

61
00:05:09,400 --> 00:05:16,240
Ús comercial del Krita. Per descomptat, està molt bé. Això és el que diem explícitament en tota la nostra

62
00:05:16,240 --> 00:05:22,280
llista de preguntes freqüents (PMF) en el quadre d'explicacions a tot arreu. Però l'ús comercial

63
00:05:22,280 --> 00:05:29,160
realment succeeix. La primera vegada que vaig sentir parlar d'això va ser quan presentàvem el Krita al SIGGRAPH a Vancouver.

64
00:05:29,160 --> 00:05:35,694
Vam tenir un estand al costat del Blender gràcies a l'ajuda realment útil de Ton Roosendaal, va ser com...

65
00:05:35,694 --> 00:05:41,600
hi havia molt, molt d'interès, moltes persones volien parlar amb nosaltres i llavors va venir aquell tipus

66
00:05:41,600 --> 00:05:49,680
cap a mi i em va dir que no li ho expliques a ningú més, almenys de moment, però (psst) hem estat utilitzant el Krita

67
00:05:49,680 --> 00:05:55,200
per a la introducció de «Little, Big», que era una pel·lícula d'animació crec que de Disney,

68
00:05:55,200 --> 00:06:01,560
Ja no estic segura, així que va ser el primer que vaig sentir sobre l'ús comercial del Krita i era el 2014, penso.

69
00:06:01,560 --> 00:06:06,480
Des de llavors, hem estat patrocinats i hem estat en contacte amb la gent

70
00:06:06,480 --> 00:06:14,360
que utilitzar el Krita comercialment per a jocs, pel·lícules, llibres, i això és força estimulant.

71
00:06:14,360 --> 00:06:23,240
D'acord, així que volem ajudar el Krita. Com es finança el Krita? Com de sostenible és de cara al futur?

72
00:06:23,240 --> 00:06:28,160
Ja hem parlat una mica d'això. Bàsicament hi ha dues entitats. D'una banda hi ha la

73
00:06:28,160 --> 00:06:36,600
Fundació Krita sense ànim de lucre, d'altra banda hi ha el programari Halla Rempt amb ànim de lucre, soc jo.

74
00:06:36,600 --> 00:06:44,000
i dividim les activitats entre les dues. La fundació sol·licita donacions, cosa que significa que és exempta

75
00:06:44,000 --> 00:06:50,400
d'impostos, el programari Halla Rempt ven DVD i coses, però per descomptat ja no té ingressos

76
00:06:50,400 --> 00:06:58,080
avui en dia perquè tenim aquestes grans guies d'aprenentatge a YouTube ❤. Ven el Krita en diverses botigues d'aplicacions.

77
00:06:58,080 --> 00:07:06,400
Els dos fluxos d'ingressos combinats, les donacions puntuals al Fons, els ingressos de les botigues són suficients per a patrocinar

78
00:07:06,400 --> 00:07:13,000
el nostre equip de desenvolupadors i els vídeos d'en Ramon, actualment patrocinats, perquè això sigui bastant sostenible

79
00:07:13,000 --> 00:07:21,160
almenys hem estat fent això des de 2015 i les botigues 
es van afegir el 2017, així que estem fent una bona feina.

80
00:07:21,160 --> 00:07:28,520
Com la comunitat ha estat donant suport al Krita i com el Krita ha estat donant suport a la seva comunitat?

81
00:07:28,520 --> 00:07:33,360
Tornem-hi! Quan el Krita era només una afició per a tothom que hi estava involucrat,

82
00:07:33,360 --> 00:07:39,120
la majoria de nosaltres no teníem accés a les tauletes digitals de dibuix. Vaig començar a treballar en el Krita perquè va arribar-me

83
00:07:39,120 --> 00:07:47,362
una per al meu aniversari però va començar a fallar, així que vam fer una recaptació de fons per a aconseguir dues tauletes Wacom i bolígrafs d'art!

84
00:07:47,362 --> 00:07:54,280
Així que va ser quan vam adonar-nos que en especial la comunitat de programari lliure, ni tan sols hi havia artistes

85
00:07:54,280 --> 00:08:03,560
Tornant a aquells dies, vam ser realment solidaris. I una mica més tard, va arribar en Lukas, un estudiant de codi

86
00:08:03,560 --> 00:08:09,800
eslovac, que es va convertir en un col·laborador habitual, fins i tot va fer la seva tesi sobre els motors de pinzell

87
00:08:09,800 --> 00:08:15,400
del Krita. Va obtenir 10 punts sobre 10 perquè era un treball brillant i els seus professors

88
00:08:15,400 --> 00:08:21,520
van quedar sorpresos que el seu alumne va fer coses reals que s'utilitzaven en el món real!

89
00:08:21,520 --> 00:08:28,320
Al mateix temps, estàvem adaptant el Krita des de les Qt3 a les Qt4, estàvem reescrivint-ho tot

90
00:08:28,320 --> 00:08:34,720
res funcionava, tot fallava massa, era realment horrible per a tothom! En aquell moment Lukas va dir:

91
00:08:34,720 --> 00:08:40,960
«Els meus tres últims mesos a la Universitat se suposa que són pràctiques. He de fer coses importants,

92
00:08:40,960 --> 00:08:47,160
útils, preferiria que se'm pagués una mica per això». I com que el Krita en aquell moment fallava

93
00:08:47,160 --> 00:08:55,720
per tot arreu, i estàvem lluitant tant com a codificadors voluntaris i aficionats per a aconseguir que el Krita es convertís en quelcom útil, vam

94
00:08:55,720 --> 00:09:04,480
finançar en Lukas per a corregir errors durant 3 mesos. Va ser un gran èxit! Inicialment Lukas principalment volia alguns

95
00:09:04,480 --> 00:09:12,840
diners per a pagar el lloguer, però nosaltres li volíem pagar una quantitat de diners decent cada mes,

96
00:09:12,840 --> 00:09:20,400
amb el resultat de la recaptació de fons que molts artistes, moltes persones entusiastes, havien donat suport al Krita.

97
00:09:20,400 --> 00:09:27,520
Tres mesos es van convertir en sis mesos, i després una mica més tard, 
en Dmitri Kazakov ha estat fent un treball brillant

98
00:09:27,520 --> 00:09:34,160
des del seu primer GSOC amb el Krita. Anava a acabar la Universitat i jo no volia que ell

99
00:09:34,160 --> 00:09:39,960
agafés una altra feina i perdre'l pel projecte. Així que vam començar a fer algunes recaptacions de fons periòdiques

100
00:09:39,960 --> 00:09:46,400
que realment ens van fer molta publicitat i era esgotador, però era divertit fer-ho i en Dmitry encara

101
00:09:46,400 --> 00:09:49,880
treballa a temps complet en el Krita, com a desenvolupador patrocinat, 
així que va funcionar.

102
00:09:50,000 --> 00:09:57,360
Podries recordar un moment en què la comunitat del Krita va venir a ajudar al desenvolupament del Krita?

103
00:09:57,600 --> 00:10:01,600
El pitjor moment va ser quan l'oficina d'impostos neerlandesa...

104
00:10:01,600 --> 00:10:07,040
es va adonar que... saltem-nos les coses complicades que fan venir mal de cap, de totes maneres

105
00:10:07,040 --> 00:10:10,720
volien tenir 24.000 € d'una vegada des de

106
00:10:10,720 --> 00:10:14,800
la fundació Krita, diners que no teníem quan això ho vam explicar

107
00:10:14,800 --> 00:10:21,000
al món. El món va córrer per a donar-nos suport! 
Això va ser genial adonar-nos realment des del

108
00:10:21,000 --> 00:10:27,320
moment en què mai havíem mirat enrere, que teníem una comunitat de suport i tenim un fons de desenvolupament

109
00:10:27,320 --> 00:10:35,280
al qual us insto a unir-vos, ho sento per la publicitat, venem el Krita en diverses botigues d'aplicacions. Botigues d'aplicacions

110
00:10:35,280 --> 00:10:43,480
són un infern per a treballar-hi, però ens donen la nostra paga mensual, gràcies a la comunitat per donar-nos suport!

111
00:10:43,480 --> 00:10:50,840
Com ajuda el Krita als usuaris, a aprendre el programari o fins i tot a codificar el programari?

112
00:10:51,280 --> 00:10:56,520
Estem intentant que el Krita sigui accessible o fins i tot fàcil d'aprendre per als nouvinguts,

113
00:10:56,520 --> 00:11:03,640
però, en primer lloc, per descomptat, només intentem mantenir els 
estàndards d'interfície d'usuari existents. No estem intentant fer

114
00:11:03,640 --> 00:11:09,488
coses noves estranyes. Només volem que la gent pugui trobar-ho tot en el lloc on està acostumada.

115
00:11:09,488 --> 00:11:15,920
Si heu utilitzat un ordinador durant els últims 5, 10, 20 o 30 anys, realment res en el Krita

116
00:11:15,920 --> 00:11:22,600
sorprèn que sigui usable en si mateix. Per descomptat, també estem treballant conjuntament amb el nostre

117
00:11:22,600 --> 00:11:29,840
expert en interfície d'usuari (UI) Scott Petrovic i ell està ajudant 
a dissenyar funcionalitats noves. Ara, si vas més enllà de

118
00:11:29,840 --> 00:11:36,560
el nivell de l'aplicació mateixa, llavors la documentació és realment important. Passem una mica de temps

119
00:11:36,560 --> 00:11:44,440
i esforç a la nostra pàgina web de documentació: «docs.krita.org» no només llista totes les opcions de menú, també ofereix

120
00:11:44,440 --> 00:11:51,600
guies d'aprenentatge sobre elements del flux de treball. Coses que potser no són aparents si ets un usuari d'oficina

121
00:11:51,600 --> 00:11:57,280
d'aplicacions de pintura digital perquè, siguem clars en això, la pintura digital és una eina

122
00:11:57,840 --> 00:12:05,720
separada de tota la resta. Vull dir que sí. Jo també pinto analògic

123
00:12:05,720 --> 00:12:12,240
utilitzant pintures a l'oli i dibuixo un esbós i esculpeixo i després quan intento dibuixar utilitzant el Krita, sé

124
00:12:12,240 --> 00:12:18,600
que la majoria dels hàbits apresos no es transfereixen a una aplicació de pintura digital, així que tenim

125
00:12:18,600 --> 00:12:24,920
una extensa web amb guies d'aprenentatge, directius, 
consells, també tenim una gran comunitat i gràcies

126
00:12:24,960 --> 00:12:34,202
a Raghavendra Kamath (raghukamath), tenim un lloc web de comunitat d'usuaris que es diu: krita-artists.org, igual que Blender Artists

127
00:12:34,202 --> 00:12:38,120
està totalment basat en això. És un tema recurrent. Sempre que crec que necessitem

128
00:12:38,120 --> 00:12:43,320
una cosa nova, primer miro què ha estat fent en Ton al Blender i després només ho copio i no

129
00:12:43,320 --> 00:12:49,040
m'avergonyeixo d'admetre-ho. El nostre fòrum d'artistes del Krita és un recurs enorme on la gent pot fer preguntes

130
00:12:49,040 --> 00:12:54,640
però també trobar respostes a les preguntes que s'han plantejat anteriorment. Finalment, invertim molt

131
00:12:54,640 --> 00:13:01,280
temps i diners, a la sèrie de vídeos del Ramón del nostre canal Krita, així que gràcies Ramón per treballar amb nosaltres.

132
00:13:01,360 --> 00:13:11,160
Com de personalitzable és el Krita i quin tipus de connectors o extensions poden crear o utilitzar els usuaris?

133
00:13:11,840 --> 00:13:18,760
Quan el Krita era força jove, Cyrille Berger va crear el primer cop la primera interfície de creació de scripts per al Krita

134
00:13:18,760 --> 00:13:24,960
per a fer scripts en JavaScript, Ruby i Python. Espera! En realitat, això no és la primera interfície de creació de scripts

135
00:13:24,960 --> 00:13:30,120
Interfície de JavaScript de KDE. Vull dir que saps 
que gairebé tots els navegadors excepte

136
00:13:30,120 --> 00:13:39,640
el Firefox d'avui en dia es basa en el treball de KDE en el KHTML, un navegador web basat en KDE. Per descomptat, això venia amb un motor JavaScript.

137
00:13:39,640 --> 00:13:47,720
I aquest motor JavaScript era una cosa separada que podíem utilitzar per a fer que el Krita pogués crear scripts,

138
00:13:47,720 --> 00:13:52,560
així que això va ser el primer. De manera que primer havíem tingut el JavaScript i després el substituirem

139
00:13:52,560 --> 00:14:02,720
pel Kross, que és el que va fer possible 
Ruby, Python, etc. Després vàrem afegir l'Open GTL

140
00:14:02,720 --> 00:14:09,360
molt oblidats aquests dies, un clon idèntic d'alguna cosa utilitzada a Hollywood es va tornar a implementar o

141
00:14:09,360 --> 00:14:15,920
en Cyrille Berger va tornar a implementar això i es podria utilitzar 
per a moltes coses, inclosa la creació d'espais de color completament nous.

142
00:14:15,920 --> 00:14:22,920
Va ser genial, però Cyrille es va casar i va deixar el projecte 
i bàsicament no es podia mantenir i, per tant, ho vam deixar estar.

143
00:14:22,920 --> 00:14:29,400
Al mateix temps, vam adaptar el Krita a una versió nova dels Frameworks de KDE, i el Kross va deixar de funcionar correctament

144
00:14:29,400 --> 00:14:35,760
per tant, vàrem retirar el nostre suport. Afortunadament, llavors el Krita tenia molt pocs usuaris, de manera que ningú es va veure realment afectat.

145
00:14:35,760 --> 00:14:43,920
Després vam adaptar el Krita a una altra versió de les Qt i del KDE, que va ser molta feina

146
00:14:43,920 --> 00:14:51,440
Un estiu al terrat quan era realment càlid, 
vaig agafar el codi de creació de scripts Python de l'editor de text

147
00:14:51,440 --> 00:14:58,280
Kate de KDE i vaig copiar-ho, solucionar-ho i fent que el Krita torni a poder crear scripts. Aquesta és la base de la nostra

148
00:14:58,280 --> 00:15:05,760
API actual de creació de scripts de Python, que és una mica limitada, però està creixent i encara hi és.

149
00:15:05,760 --> 00:15:11,920
Podeu fer qualsevol cosa que us vingui de gust amb la interfície d'usuari i els scripts. I llavors, és clar,

150
00:15:11,920 --> 00:15:19,200
vam tenir aquell enorme projecte de codi d'estiu (GSOC) on nosaltres 
vam implementar el suport per al SeExpr de Disney.

151
00:15:19,200 --> 00:15:25,699
Fa realment possible escriure un script, generar coses que ompliran les teves... les teves capes amb el que vulguis

152
00:15:25,699 --> 00:15:31,240
De fet, no estem segurs de si s'utilitza molt, però és extremadament potent. Així que sí! El Krita és molt

153
00:15:31,240 --> 00:15:37,120
personalitzable, i inclús a part d'això, la majoria del Krita són connectors. Cada eina és un connector. Si coneixes

154
00:15:37,120 --> 00:15:43,560
una mica de C++, vull dir que jo no coneixia C++ quan vaig començar a modificar el Krita i em va semblar bé en

155
00:15:43,560 --> 00:15:50,560
no arribar a fer res útil, ja que va atreure altres col·laboradors, i el Krita es va impulsar. Per tant, fem hacking!

156
00:15:50,880 --> 00:15:56,560
Com a últimes paraules, moltes gràcies per estar disponible per aquesta entrevista,

157
00:15:56,560 --> 00:16:02,240
Sé que estàs molt ocupada, així que ho agraeixo molt.

158
00:16:02,240 --> 00:16:05,440
Quin missatge vols enviar als usuaris del Krita?

159
00:16:05,440 --> 00:16:09,000
Gràcies Ramón per l'entrevista. Un últim missatge per a tothom que fa servir el Krita.

160
00:16:09,000 --> 00:16:13,520
Usa més el Krita! Fes art, fes art, fes art! M'encanta veure tot l'art fet amb el Krita.

161
00:16:13,520 --> 00:16:18,116
Qualsevol cosa que feu amb el Krita està bé per mi. Adeu!

162
00:16:18,116 --> 00:16:24,384
Celebra amb nosaltres el 25è aniversari del desenvolupament del Krita

163
00:16:24,384 --> 00:16:32,312
Qualsevol comentari és molt benvingut. Aquí, Ramon com sempre, dient-vos adeu, passeu un bon dia... o nit. Moltes gràcies per veure-ho. ❤
