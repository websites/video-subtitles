#!/usr/bin/python3
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: MIT

"""
**upload_youtube** allows you to push all the subtitles for a video.
"""
import os
import argparse
import time
import pickle

import google_auth_oauthlib.flow
import googleapiclient.discovery
import googleapiclient.errors

from googleapiclient.http import MediaFileUpload

import common

verbose = False
dry_run = False

def get_existing_subs_for_video(youtube, video_id):
    if not hasattr(get_existing_subs_for_video, "all_subs"):
        get_existing_subs_for_video.all_subs = {}
    # Fast lookup, there is probably a cleaner way to do this
    if video_id in get_existing_subs_for_video.all_subs:
        if verbose:
            print(f"Found fast lookup for {video_id}: {get_existing_subs_for_video.all_subs[video_id]}")
        return get_existing_subs_for_video.all_subs[video_id]

    locale_to_id = {}
    request = youtube.captions().list(
        part="snippet",
        videoId=video_id
    )
    try:
        response = request.execute()
    except googleapiclient.errors.HttpError as error:
        print(error)
        return locale_to_id
    existing_subtitles = response["items"]
    
    if verbose:
        print(f"Requested caption list for {video_id}: {existing_subtitles}")

    for sub in existing_subtitles:
        locale_to_id[sub["snippet"]["language"]] = sub["id"]
    get_existing_subs_for_video.all_subs[video_id] = locale_to_id
    return locale_to_id


def upload_video(youtube, srt_filename, locale, video_id):
    # For now, all languages seem supported in Youtube
    if locale in [""]:
        if verbose:
            print(f"Ignoring locale {locale} as not supported YouTube for now")
        return
    if not dry_run:
        locale_to_id = get_existing_subs_for_video(youtube, video_id)
        if locale in locale_to_id:
            print(f"Update existing subtitles for {video_id} and language {locale}")
            request = youtube.captions().update(
                part="snippet,id",
                body={
                    "id": locale_to_id[locale],
                    "snippet": {
                        "id": locale_to_id[locale],
                        "language": locale,
                        "name": locale
                    }
                },
                media_body=MediaFileUpload(srt_filename)
            )
        else:
            print(f"Insert new subtitles for {video_id} and language {locale}")
            request = youtube.captions().insert(
                part="snippet",
                body={
                    "snippet": {
                        "videoId": video_id,
                        "language": locale,
                        "name": locale
                    }
                },
                media_body=MediaFileUpload(srt_filename)
            )
        response = request.execute()
        if verbose:
            print(response)
        time.sleep(1)


def handle_video(youtube, file_to_upload):
    if verbose:
        print("handle", file_to_upload)

    no_extension_file = file_to_upload.replace(".srt", "")
    if (locale_index := no_extension_file.find("-")) != -1:
        no_extension_file = no_extension_file[0:locale_index]
    json_file = no_extension_file + ".json"

    if not os.path.exists(json_file):
        if verbose:
            print(f"{json_file} does not exist, skip {file_to_upload} file")
            return
    youtube_id = common.get_video_id(json_file, "yt_id")

    if youtube_id == "":
        if verbose:
            print(f"Skip subtitle {file_to_upload} because it has no youtube video")
        return

    # Check if it is the original srt (English) or a translated one
    if (locale_index := file_to_upload.find("-")) != -1:
        locale = file_to_upload[locale_index+1:].removesuffix(".srt")
        upload_video(youtube, file_to_upload, locale, youtube_id)
    else:
        upload_video(youtube, file_to_upload, "en", youtube_id)

def handle_folder(youtube, folder):
    """
    Browse recursively all the files in the folder.
    For each srt file, if the filename contains a locale and if there is a json file
    for the original srt file, we upload the file to youtube
    """
    if verbose:
        print(f"Recursive find in {folder} folder")
    for root, dirs, files in os.walk(folder):
        dirs[:] = [d for d in dirs if d not in ['.git', 'po', 'poqm']]
        for name in files:
            if name.endswith("srt"):
                handle_video(youtube, os.path.join(root, name))


def get_authenticated_service(secret_file):
    api_service_name = "youtube"
    api_version = "v3"
    scopes = ["https://www.googleapis.com/auth/youtubepartner", "https://www.googleapis.com/auth/youtube.force-ssl"]
    if os.path.exists("CREDENTIALS_PICKLE_FILE"):
        with open("CREDENTIALS_PICKLE_FILE", 'rb') as pickle_file:
            credentials = pickle.load(pickle_file)
    else:
        client_secrets_file = secret_file
        flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(client_secrets_file, scopes)
        credentials = flow.run_local_server()
        with open("CREDENTIALS_PICKLE_FILE", 'wb') as pickle_file:
            pickle.dump(credentials, pickle_file)
    return googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", "-i", help="srt input folders",
                        nargs='*', default=["."])
    parser.add_argument("--verbose", "-v", help="verbose mode",
                        action="store_true")
    parser.add_argument(
        "--secret", "-s", help="secret file path (default to env{API_SECRET})", default=os.environ["API_SECRET"])
    parser.add_argument("--dry-run", "-d",
                        help="dry run mode (no upload)", action="store_true")

    args = parser.parse_args()
    verbose = args.verbose
    dry_run = args.dry_run
    secret_file = args.secret

    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    youtube = get_authenticated_service(secret_file)

    for input_folder in args.input:
        if os.path.isdir(input_folder):
            handle_folder(youtube, input_folder)
        elif os.path.isfile(input_folder):
            handle_video(youtube, input_folder)

if __name__ == "__main__":
    main()
