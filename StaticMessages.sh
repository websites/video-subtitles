#!/usr/bin/env bash
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
# SPDX-License-Identifier: MIT

EXPORTS_POT_DIR=1

FILE_PREFIX=video_subtitles
function export_pot_dir # First parameter will be the path of the directory where we have to store the pot files
{
    potdir=$1
    for file in */*.srt; do
        # Ignore srt files containing a dash. They are translated files, not the ones we want to translate
        [[ $file =~ - ]] && continue
        dirname=`dirname $file`
        filename=`basename $file`
        sub2po --progress=names -P -i $file -o $potdir/${FILE_PREFIX}--${dirname}--${filename%.srt}.pot
    done
}

function import_po_dirs # First parameter will be a path that will be a directory to the dirs for each lang and then all the .po files inside
{
    podir=$1
    for lang in `ls $podir`
    do
        for file in `ls $podir/$lang`
        do
            moduleName=$(echo $file | awk -F'[-]{2}' '{print $2}' )
            templateFile=$moduleName/$(echo $file | awk -F'[-]{2}' '{print $3}' )
            templateFile=${templateFile%.po}.srt #replace .po with .srt

            pofile=$podir/$lang/${file}
            srt=${templateFile%.srt}-${lang}.srt
            po2sub -t ${templateFile} -i ${pofile} -o ${srt}.tmp --threshold=100
            if [ -f "${srt}.tmp" ]; then
                # In case an srt exists, we don't want to erase it if we can't
                # manage to create a new one
                mv ${srt}.tmp ${srt}
            fi
        done
    done
}
