﻿1
00:00:11,080 --> 00:00:13,720
V tem izobraževalnem videu bomo videli, kako nastaviti selenium.

2
00:00:14,800 --> 00:00:18,360
Selenium je orodje, ki se uporablja za GUI testiranje aplikacij.

3
00:00:18,360 --> 00:00:23,360
Vodnika za namestitev najdete na community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
V tem videu ga bomo nastavili na neon OS. Pa začnimo.

5
00:00:35,000 --> 00:00:44,080
(Te pakete že imam nameščene, ta korak bo potreboval veliko več časa)

6
00:00:47,320 --> 00:00:53,360
Ko imamo potrebne pakete, bomo zdaj klonirali orodje Selenium in sledili naslednjim korakom.

7
00:00:56,560 --> 00:01:01,560
(te ukaze lahko preprosto kopirate in prilepite)

8
00:01:29,520 --> 00:01:39,440
Ko so ti koraki uspešno opravljeni, lahko začnemo pisati začetne teste. Tukaj lahko sledite tem korakom, če želite izvedeti, kako napisati nekaj osnovnih testov za selenium.

9
00:01:40,000 --> 00:01:45,000
Poskusimo zagnati primer testa. Preverite, ali imate v sistemu nameščen kcalc.

10
00:01:47,200 --> 00:01:52,200
kcalc je v osnovni pripomoček kalkulator iz KDE.

11
00:02:00,960 --> 00:02:04,200
Kot lahko vidite, lahko izvajamo teste s seleniumom.

12
00:02:12,720 --> 00:02:21,360
Upoštevajte, da je selenium premikajoča se tarča in pri izdelavi tega videoposnetka ni nobenih napak in napak pri gradnji, vendar se lahko takšne napake pojavijo v prihodnosti.

13
00:02:21,920 --> 00:02:26,360
Ne pozabite prijaviti morebitnih napak skupnosti KDE. In hvala za ogled videa!
