﻿1
00:00:10,480 --> 00:00:18,880
Dans cette vidéo, nous verrons ce qu'est Accerciser et comment nous pouvons l'utiliser pour tester automatiquement les éléments de l'interface graphique utilisateur.

2
00:00:19,760 --> 00:00:27,560
Accerciser est ouvert ici. Laissez-moi ouvrir l'utilitaire KCalc.

3
00:00:28,560 --> 00:00:31,800
Donc, comme vous pouvez le voir, il reconnaît l'utilité de la calculatrice.

4
00:00:32,159 --> 00:00:33,840
Si j'appuie deux fois ici

5
00:00:38,800 --> 00:00:41,720
Il mettra en surbrillance la fenêtre

6
00:00:42,680 --> 00:00:49,840
Nous pouvons aller dans l'afficheur d'interface pour afficher les éléments.

7
00:00:51,240 --> 00:00:53,520
Donc, par exemple

8
00:00:54,520 --> 00:00:58,480
Ceci est un cadre.

9
00:00:58,480 --> 00:01:04,760
Et si je veux appuyer sur un quelconque bouton, je devrais trouver cette valeur. Disons « 9 ».

10
00:01:09,640 --> 00:01:17,240
Comme vous pouvez le voir ici, c'est la touche « 9 ». Si je veux appuyer sur ce bouton, nous devrons passer aux actions.

11
00:01:18,800 --> 00:01:22,320
Comme nous pouvons le voir, il y a eu un appui sur la touche « 9 ».

12
00:01:23,320 --> 00:01:26,200
De la même façon, nous pouvons également trouver une valeur pour l'addition.

13
00:01:28,040 --> 00:01:32,400
Si j'appuie dessus, il y aura un appui sur la touche « + ».

14
00:01:33,520 --> 00:01:36,640
Vérifions-le avec « 9 + 6 »

15
00:01:48,440 --> 00:01:51,080
Comme nous pouvons le voir « 15 »

16
00:01:51,920 --> 00:01:58,080
C'est un moyen de trouver les éléments qui sont associés à une action particulière.

17
00:01:59,800 --> 00:02:09,479
Vous pouvez voir ici la description qui dit « plus ». Elle nous donne également l'identifiant de l'élément.

18
00:02:10,360 --> 00:02:15,360
Ceci serait utile pour écrire un test reposant sur Selenium qui viendrait dans la dernière partie de la vidéo.
