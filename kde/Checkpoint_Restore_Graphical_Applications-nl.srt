1
00:00:00,000 --> 00:00:08,000
Controlepunt herstellen in de gebruikersruimte is een manier om een actieve bestaande toepassing op te slaan naar schijf in zijn huidige status.

2
00:00:08,000 --> 00:00:14,520
Het klink alsof het verbazingwekkend kan zijn, maar vanuit een grafische toepassing werkt het niet, wordt in de website gezegd,

3
00:00:14,520 --> 00:00:17,520
Elke X11 toepassing wordt niet ondersteund.

4
00:00:17,520 --> 00:00:24,520
Het werk dat we hebben gedaan op Wayland om te verplaatsen tussen compositors lost dit impliciet op.

5
00:00:25,000 --> 00:00:32,520
We zullen een demo doen. Ik heb KolourPaint open, ik doe er iets mee, plakken in dit Wayland logo, make een heerlijk rommeltje.

6
00:00:33,520 --> 00:00:38,520
En het ziet er leuk uit, maar het gebruikt een maar liefst 45 megabytes RAM.

7
00:00:38,520 --> 00:00:45,520
Misschien moet ik nu hiermee stoppen en iets anders doen, zoals WebKit compileren en heb ik nu nodig elke beschikbare...

8
00:00:45,520 --> 00:00:47,000
...Megabyte.

9
00:00:47,000 --> 00:00:51,520
Dus voer ik nu dit commando uit, criu dump, en het is weg. Opgeslagen naar schijf.

10
00:00:52,000 --> 00:01:00,520
Ik kan op nieuw starten, ik kan de kernel bijwerken, ik kan zelfs deze map, die is aangemaakt, naar een andere laptop verplaatsen,

11
00:01:00,520 --> 00:01:04,519
en zolang als zijn binaire bestanden er zijn, ze vandaar herstellen.

12
00:01:04,519 --> 00:01:09,520
Als ik criu restore uitvoer, komt het terug en het komt onmiddellijk terug.

13
00:01:09,520 --> 00:01:15,520
Dus kan dit mechanisme ook gebruikt worden om een langzaam startend proces ook te starten.

14
00:01:16,520 --> 00:01:20,520
En ik kan het rond het logo slepen, zodat u kunt zien dat het is exact in de oorspronkelijke status is opgeslagen.

15
00:01:20,520 --> 00:01:27,520
Dit zou niet mogelijk zijn als ik de afbeelding naar schijf had opgeslagen, omdat KolourPaint geen concept voor lagen of zoiets heeft.

16
00:01:29,520 --> 00:01:32,520
En het is exact waar het was.

17
00:01:33,520 --> 00:01:44,520
Maar we kunnen meer doen dan gewoon opslaan vanuit herstel... ik open KMines, een kloon van een mijnenopruimer die we in KDE hebben, als ik het goed heb gespeld.

18
00:01:45,520 --> 00:01:48,520
Niet erg goed in KMines, doe een zet.

19
00:01:48,520 --> 00:01:54,520
Maar deze keer wanneer ik het dump, ga ik dit extra argument toevoegen, laten draaien.

20
00:01:54,520 --> 00:01:59,520
En nu kan ik de status opslaan, een zet doen.

21
00:01:59,520 --> 00:02:03,520
Okay, geen erg goede zet, teleurstellend.

22
00:02:03,520 --> 00:02:12,520
En nu, als ik KMines sluit, kan ik het herstellen, exact waar ik was en een andere poging doen.

23
00:02:13,520 --> 00:02:18,520
En oh, nog steeds mis in KMines, ik kan het sluiten, en kan een andere poging doen.

24
00:02:18,520 --> 00:02:26,520
En misschien is dit niet de beste manier op mijnenruimer te spelen, maar het zou allerlei soorten van andere implicaties kunnen hebben voor debugging.

25
00:02:29,520 --> 00:02:41,520
Gebruik van Crio is niet op hetzelfde niveau als de rest van de werk aan de compositor overdracht, maar het ontgrendelt heel wat opties voor toekomstige ontwikkelaars om op te bouwen en er iets groots van te maken.
