﻿1
00:00:06,320 --> 00:00:10,160
Dans cette vidéo, nous verrons comment nous pouvons écrire des tests reposant sur Selenium.

2
00:00:11,040 --> 00:00:14,880
Ainsi, une condition préalable à cette vidéo sera la vidéo « Configuration de Selenium ».

3
00:00:14,880 --> 00:00:21,640
Donc, vous avez vraiment besoin d'avoir Selenium installé et en cours d'exécution sur votre système afin que vous puissiez commencer à écrire un test.

4
00:00:22,400 --> 00:00:30,520
Nous pouvons suivre le guide de configuration ici. Nous pouvons utiliser les trucs standards qui sont déjà mentionnés dans la section « Écriture de tests ».

5
00:00:31,560 --> 00:00:39,200
Donc, ici, j'ai le code VS ouvert. Comme vous pouvez le voir, j'ai tout le code standard écrit.

6
00:00:39,200 --> 00:00:47,280
Et puis, j'ai écrit un code pour « 9 + 6 » qui devrait faire « 15 », ce qui a également été montré dans la vidéo de Accerciser.

7
00:00:48,600 --> 00:00:49,760
Passons le en revue

8
00:00:54,280 --> 00:01:01,720
Nous essayons de trouver la valeur « 9 ». Si je ouvre Accerciser une fois de plus et que je trouve « 9 » ici

9
00:01:03,320 --> 00:01:14,120
Nous avons vu que la valeur du bouton-poussoir ici est « 9 », ce qui est également utilisé ici. De même pour « + », nous pouvons voir que la valeur du bouton-poussoir est le signe « + », ce qui est également mentionné ici.

10
00:01:14,920 --> 00:01:25,720
Ce que nous faisons ici, c'est déclencher un clic. On clique donc sur « 9 » ici, puis sur le symbole « + », puis sur « 6 », et enfin sur le symbole « = ».

11
00:01:29,360 --> 00:01:31,920
Le symbole « Égal à » est également « = ».

12
00:01:32,200 --> 00:01:39,240
Et puis, nous essayons d'extraire le texte de la section d'affichage des résultats.

13
00:01:40,120 --> 00:01:47,720
Donc, si nous allons Accerciser et essayons de trouver une description pour la zone de texte d'affichage.

14
00:01:55,200 --> 00:02:04,680
Il s'agit de la zone de texte d'affichage. Comme nous pouvons le voir, la description indique « Affichage des résultats ». C'est donc ce que nous faisons ici.

15
00:02:04,680 --> 00:02:10,880
Nous utilisons « Affichage des résultats » et nous essayons d'extraire le texte de ce qui est affiché.

16
00:02:12,200 --> 00:02:15,600
Il s'agit d'un test de base. Alors, essayons de l'exécuter.

17
00:02:28,520 --> 00:02:33,520
Comme nous pouvons le voir, le test a été réussi et nous obtenons un résultat « Ok ».

18
00:02:34,600 --> 00:02:48,240
C'est ainsi que vous pouvez écrire plusieurs tests. Cela peut également être écrit pour la division, la soustraction et tout ce que vous pouvez trouver avec l'utilitaire Accerciser.

19
00:02:49,840 --> 00:02:59,800
Une chose à noter est que certaines applications peuvent ne pas avoir du code pour l'interface graphique utilisateur pour l'accessibilité. Dans ce cas, vous devrez ajouter manuellement du code pour l'accessibilité.

20
00:03:00,640 --> 00:03:04,720
Après cela, vous pourrez accéder à ces éléments avec Selenium.

21
00:03:05,800 --> 00:03:09,640
Merci de votre attention !
