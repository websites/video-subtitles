﻿1
00:00:00,000 --> 00:00:05,200
In questo video vedremo cos'è selenium e come KDE lo utilizza per creare software sostenibile

2
00:00:05,200 --> 00:00:11,040
E anche come aiuta a raggiungere altri obiettivi di KDE che sono l'accessibilità e il test del sistema.

3
00:00:12,200 --> 00:00:20,120
L'utilizzo del software è in rapido aumento e con questo aumento aumentiamo il suo impatto ambientale come ad esempio maggiori emissioni di CO2

4
00:00:20,120 --> 00:00:24,360
Se vogliamo minimizzare questo impatto dobbiamo ridurre al minimo il consumo di energia.

5
00:00:24,360 --> 00:00:28,400
E per ridurre al minimo il consumo di energia dobbiamo essere in grado di misurare questo consumo.

6
00:00:29,240 --> 00:00:34,360
Per poter misurare il consumo energetico abbiamo bisogno di un meccanismo in grado di imitare il comportamento dell'utente

7
00:00:34,920 --> 00:00:39,240
per riprodurre ciò che l'utente farebbe con il mouse e la tastiera.

8
00:00:39,320 --> 00:00:43,920
Oltre a ciò, abbiamo anche bisogno di uno strumento hardware per misurare effettivamente i consumi energetici.

9
00:00:43,920 --> 00:00:47,160
Questo meccanismo sarà fornito da Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI è uno strumento creato da KDE che utilizza selenium per l'automazione dei driver web e il protocollo di accessibilità AT-SPI di Linux.

11
00:00:55,360 --> 00:01:00,360
Con l'aiuto di selenium possiamo creare script di scenari di utilizzo che imitano il comportamento dell'utente.

12
00:01:01,520 --> 00:01:05,680
Come puoi vedere, può aiutare a creare software sostenibile, ma non è tutto.

13
00:01:05,680 --> 00:01:14,400
Selenium ci aiuta anche a raggiungere l'obiettivo di KDE per tutti, consentendo di migliorare l'accessibilità per tutti, comprese le persone con disabilità.

14
00:01:14,400 --> 00:01:18,400
Aiuta anche ad automatizzare e sistematizzare i processi interni.

15
00:01:18,720 --> 00:01:24,680
Aiuta a creare test funzionali che garantiscono che il nuovo codice rimanga invariato come prima degli aggiornamenti.

16
00:01:25,560 --> 00:01:31,800
Nella prossima parte del video vedremo come configurare selenium localmente e come scrivere alcuni test funzionali di base.
