1
00:00:00,000 --> 00:00:08,000
Check Point Restore në hapësirën e përdoruesit është një rrugë për të marrë një aplikacion teksa xhiron dhe për të ruajtur gjendjen e tij në disk.

2
00:00:08,000 --> 00:00:14,520
Duket sikur të ishte mahnitëse, por nga aplikacione grafike, nuk funksionon, thuhet te sajti,

3
00:00:14,520 --> 00:00:17,520
Çfarëdo aplikacioni X11 nuk mbulohet.

4
00:00:17,520 --> 00:00:24,520
Puna që kemi bërë në Wayland për kalim nga një hartues në tjetrin, e zgjidh këtë si pasojë.

5
00:00:25,000 --> 00:00:32,520
Do të bëjmë një demonstrim. Kam të hapur KolourPaint-in, po vizatoj diçka, ngjita këtë stemë të Wayland-it, përzierje e bukur.

6
00:00:33,520 --> 00:00:38,520
Dhe duket bukur, por po përdor një goxha vlerë prej 45MB RAM.

7
00:00:38,520 --> 00:00:45,520
Ndoshta tani ju duhet të shkëputeni dhe të bëni diçka tjetër, të përpiloni WebKit-in, ta zëmë dhe më duhet çdo…

8
00:00:45,520 --> 00:00:47,000
….Megabajt i lirë.

9
00:00:47,000 --> 00:00:51,520
Pra, mund të jap këtë urdhër, “criu dump” dhe mbaroi. U ruajt në disk.

10
00:00:52,000 --> 00:01:00,520
Mund të bëj rinisje, të kryej një përditësim kerneli, madje ta marr këtë dosje që u krijuar, ta kaloj në një tjetër portativ,

11
00:01:00,520 --> 00:01:04,519
dhe sa kohë që dyorët janë atje, ta rikthej prej andej.

12
00:01:04,519 --> 00:01:09,520
Nëse xhiroj “criu restore”, rikthehet, rikthehet aty për aty.

13
00:01:09,520 --> 00:01:15,520
Pra, ky mekanizëm mund të përdoret edhe për nisje “bootstrap” një procesi që mezi fillon.

14
00:01:16,520 --> 00:01:20,520
Dhe mund ta tërheq përreth stemës, mund ta shihni të ruajtur saktësisht te gjendja.

15
00:01:20,520 --> 00:01:27,520
S’do të qe e mundur, nëse ruaja vetëm figurën në disk, ngaqë KolourPaint s’e ka idenë e shtresave, e më the të thashë.

16
00:01:29,520 --> 00:01:32,520
Dhe gjendet saktësisht atje ku qe.

17
00:01:33,520 --> 00:01:44,520
Por mund të bëjmë më tepër se sa thjesht të ruajmë që prej rikthimit… Po hap KMines, një klonim i Minesweeper-it që kemi në KDE, po qe se e shqiptoj saktë.

18
00:01:45,520 --> 00:01:48,520
S’jam gjë në KMines, po bëj një lëvizje.

19
00:01:48,520 --> 00:01:54,520
Por, këtë herë, kur të kryej “dump”, do të shtoj këtë argument shtesë, “leave running”.

20
00:01:54,520 --> 00:01:59,520
Dhe tani mund ta ruaj gjendjen, si thoni.

21
00:01:59,520 --> 00:02:03,520
OK, s’thatë gjë, më zhgënjyet.

22
00:02:03,520 --> 00:02:12,520
Tani, po të mbyll KMines, mund ta rikthej, saktësisht atje ku isha dhe të bëj një tjetër lëvizje.

23
00:02:13,520 --> 00:02:18,520
Dhe, os, prapë s’jam gjë në KMines, mund ta mbyll, të vazhdoj me një tjetër lëvizje.

24
00:02:18,520 --> 00:02:26,520
Ndoshta kjo s’është mënyra më e mirë për të luajtur Minesweeper, por mund të ketë krejt llojet e implikimeve për diagnostikimin.

25
00:02:29,520 --> 00:02:41,520
Përdorimi i Crio-s s’është në të njëjtin nivel me pjesën tjetër të funksionimit “handoff” të jartuesit, por shkyç mjaft mundësi për zhvillues të ardhshëm për ta përdorur dhe krijuar diçka të fuqishme.
