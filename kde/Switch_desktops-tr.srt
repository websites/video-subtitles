1
00:00:00,000 --> 00:00:04,880
Şimdi güvenilir Plasma'mız ile başlıyoruz.

2
00:00:04,880 --> 00:00:10,480
CS:GO, top ve yaptığım bu özel uygulama açık.

3
00:00:10,480 --> 00:00:13,720
Bir düğmeye tıklayalım ve bum.

4
00:00:13,720 --> 00:00:15,240
Tüm uygulamalar hayatta kaldı.

5
00:00:15,240 --> 00:00:17,040
Bu ek bir katman değil.

6
00:00:17,040 --> 00:00:20,880
Bu, uygulamaların bileşikleştiriciler arasında taşınması.

7
00:00:20,880 --> 00:00:23,120
Bir düğmeye tıklayıp Plasma'ya geri dönelim.

8
00:00:23,120 --> 00:00:28,320
Ve ilginç olarak, istemci tarafındaki dekorasyonlardan sunucu tarafına geçtik ve yeniden geri döndük.

9
00:00:28,320 --> 00:00:32,520
Farklı sürümlerin ve ayrımların çoğu, araç kümesi içinde

10
00:00:32,520 --> 00:00:33,520
örtülü olarak ele alınmaktadır.

11
00:00:33,520 --> 00:00:36,800
GNOME'a atlayalım.

12
00:00:36,800 --> 00:00:38,760
Bu güzel genel görünüm efektiyle başlayalım.

13
00:00:38,760 --> 00:00:43,040
Tüm istemcilerim hâlâ orada, boyutlarını koruyorlar.

14
00:00:43,040 --> 00:00:44,040
Hyprland'a gidebiliriz.

15
00:00:44,040 --> 00:00:47,640
Yüklenmesini bekleyelim.

16
00:00:47,640 --> 00:00:53,440
Yüklendikten sonra güzel bir canlandırmaya geldik. Şuna bak!

17
00:00:53,440 --> 00:00:56,120
CS:GO'da dolanmayı sürdürebiliriz.

18
00:00:56,120 --> 00:00:57,760
Olması gerektiği gibi davranıyor.

19
00:00:57,760 --> 00:00:59,400
Her şey çalışıyor.

20
00:00:59,400 --> 00:01:00,400
Sway'a atlayalım.

21
00:01:00,400 --> 00:01:04,959
Bir uygulamayı Sway'de nasıl başlatacağımı yalnız böyle anlayabildim.

22
00:01:04,959 --> 00:01:07,280
Pencereler hâlâ orada.
