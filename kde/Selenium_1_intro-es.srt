﻿1
00:00:00,000 --> 00:00:05,200
Es este vídeo veremos qué es Selenium y cómo lo usa KDE para crear software sostenible

2
00:00:05,200 --> 00:00:11,040
Y también cómo ayuda a conseguir otros objetivos de KDE, como la accesibilidad y las pruebas del sistema.

3
00:00:12,200 --> 00:00:20,120
El uso de software está aumentando rápidamente, lo que también aumenta su impacto ambiental como, por ejemplo, más emisión de CO2.

4
00:00:20,120 --> 00:00:24,360
Si queremos minimizar este impacto, necesitamos minimizar el consumo de energía.

5
00:00:24,360 --> 00:00:28,400
Y para minimizar el consumo de energía debemos ser capaces de medirlo.

6
00:00:29,240 --> 00:00:34,360
Para poder medir el consumo de energía, necesitamos un mecanismo que pueda imitar el comportamiento del usuario

7
00:00:34,920 --> 00:00:39,240
para reproducir lo que haría el usuario con el ratón y con el teclado.

8
00:00:39,320 --> 00:00:43,920
Además de esto, necesitamos una herramienta de hardware para medir el consumo real de energía.

9
00:00:43,920 --> 00:00:47,160
Este mecanismo lo proporcionará Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI es una herramienta creada por KDE que usa selenium para la automatización del controlador web y el protocolo de accesibilidad AT-SPI de linux.

11
00:00:55,360 --> 00:01:00,360
Con la ayuda de selenium, podemos crear un guion de escenario de uso que imita el comportamiento del usuario.

12
00:01:01,520 --> 00:01:05,680
Como puede ver, puede ayudar a crear software sostenible. Pero eso no es todo.

13
00:01:05,680 --> 00:01:14,400
Selenium también nos ayuda a lograr el objetivo de KDE para todos, permitiendo mejorar la accesibilidad para todos, incluidas las personas con discapacidades.

14
00:01:14,400 --> 00:01:18,400
También ayuda a automatizar y sistematizar los procesos internos.

15
00:01:18,720 --> 00:01:24,680
Ayuda a crear pruebas funcionales que garantizan que el código nuevo siga siendo tan bueno como antes de las actualizaciones.

16
00:01:25,560 --> 00:01:31,800
En la siguiente parte del vídeo veremos cómo configurar selenium de forma local y cómo escribir algunas pruebas funcionales básicas.
