﻿1
00:00:00,000 --> 00:00:05,200
In deze video zullen we zien wat selenium is en hoe KDE het gebruikt om duurzame software te bouwen

2
00:00:05,200 --> 00:00:11,040
En ook hoe het helpt andere doelen van KDE te bereiken die toegankelijkheid en testen van systeem zijn.

3
00:00:12,200 --> 00:00:20,120
Software gebruik is snel aan het toenemen en met deze toename verhogen wij zijn invloed op de omgeving zoals bijvoorbeeld meer CO2 uitstoot.

4
00:00:20,120 --> 00:00:24,360
Als we deze invloed willen minimaliseren, moeten we energieconsumptie minimaliseren.

5
00:00:24,360 --> 00:00:28,400
En om energieconsumptie te minimaliseren moeten we in staat zijn deze consumptie te meten.

6
00:00:29,240 --> 00:00:34,360
Om in staat te zijn energieconsumptie te meten hebben we een mechanisme nodig die gebruikersgedrag nadoet

7
00:00:34,920 --> 00:00:39,240
om te reproduceren wat de gebruiker zou doen met de muis en het toetsenbord.

8
00:00:39,320 --> 00:00:43,920
Daarmee samen hebben we ook een hardware hulpmiddel nodig om actueel het energiegebruik te meten.

9
00:00:43,920 --> 00:00:47,160
Dit mechanisme zal geleverd worden door Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI is een hulpmiddel gemaakt door KDE dat selenium voor automatisering van webdriver gebruikt en het linux AT-SPI toegankelijkheidsprotocol.

11
00:00:55,360 --> 00:01:00,360
Met de hulp van selenium kunnen we een gebruiksscenarioscript maken dat het gedrag van de gebruiker nadoet.

12
00:01:01,520 --> 00:01:05,680
Dus zoals u kunt zien kan het helpen bij het bouwen van duurzame software, maar dat is niet alles.

13
00:01:05,680 --> 00:01:14,400
Selenium helpt ons ook alle doelen voor KDE te bereiken, ons in staat stellen om toegankelijkheid voor iedereen te verbeteren inclusief mensen met handicaps.

14
00:01:14,400 --> 00:01:18,400
Het helpt ook het automatiseren en systematiseren van interne processen.

15
00:01:18,720 --> 00:01:24,680
Het helpt functionele testen te maken die verzekeren dat nieuwe code zo goed blijft als het was voordat het werd bijgewerkt.

16
00:01:25,560 --> 00:01:31,800
In het volgende deel van de video zullen we zien hoe selenium lokaal op te zetten en hoe enige basis functionele testen te schrijven.
