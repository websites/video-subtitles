﻿1
00:00:11,080 --> 00:00:13,720
I den här handledningsvideon kommer vi att se hur man ställer in selenium.

2
00:00:14,800 --> 00:00:18,360
Selenium är ett verktyg som används för testning av grafiska användargränssnitt i program.

3
00:00:18,360 --> 00:00:23,360
Du hittar den här installationsguiden på community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
I den här videon kommer vi att ställa in det på neon OS. Så låt oss börja.

5
00:00:35,000 --> 00:00:44,080
(Jag har redan paketen installerade, det kommer att ta mycket mer tid för det här steget)

6
00:00:47,320 --> 00:00:53,360
När vi har nödvändiga paket, laddar vi ner Selenium-verktyget och följer de efterföljande stegen.

7
00:00:56,560 --> 00:01:01,560
(du kan helt enkelt kopiera och klistra in kommandona)

8
00:01:29,520 --> 00:01:39,440
När de här stegen har utförts framgångsrikt kan vi börja skriva inledande tester. Du kan följa stegen här för att veta hur du skriver några grundläggande tester för selenium.

9
00:01:40,000 --> 00:01:45,000
Låt oss försöka köra ett exempeltest. Se till att du har miniräknaren installerat på systemet.

10
00:01:47,200 --> 00:01:52,200
Miniräknaren är i princip ett miniräknarprogram från KDE.

11
00:02:00,960 --> 00:02:04,200
Som du kan se kan vi köra tester med selenium.

12
00:02:12,720 --> 00:02:21,360
Tänk på att selenium är ett rörligt mål och när den här videon gjordes fanns inga fel och misslyckade byggen, men sådana fel kan uppstå i framtiden.

13
00:02:21,920 --> 00:02:26,360
Se till att rapportera eventuella fel till KDE-gemenskapen. Och tack för att du tittade på videon!
