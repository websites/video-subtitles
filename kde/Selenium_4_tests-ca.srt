﻿1
00:00:06,320 --> 00:00:10,160
En aquest vídeo veurem com podem escriure proves basades en el Selenium.

2
00:00:11,040 --> 00:00:14,880
Un requisit previ per a aquest vídeo seria el vídeo «Configuració del Selenium».

3
00:00:14,880 --> 00:00:21,640
Per tant, és necessari que tingueu instal·lat el Selenium i que s'executi en el vostre sistema, de manera que pugueu començar a escriure la prova.

4
00:00:22,400 --> 00:00:30,520
Podem seguir la guia de configuració aquí, podem utilitzar el material estàndard que ja s'esmenta a la secció «Escriure proves».

5
00:00:31,560 --> 00:00:39,200
Així que aquí tinc VS Code obert i com podeu veure, tinc escrit tot el codi estàndard

6
00:00:39,200 --> 00:00:47,280
I després he escrit un codi per a «9 + 6» que hauria de ser «15» que també es va mostrar en el vídeo d'Accerciser.

7
00:00:48,600 --> 00:00:49,760
Ho repassarem

8
00:00:54,280 --> 00:01:01,720
Estem tractant de trobar el valor «9», si torno a obrir l'Accerciser, i cerco «9» aquí

9
00:01:03,320 --> 00:01:14,120
Hem vist que el valor del botó de pulsació aquí és «9», que també s'utilitza aquí. De la mateixa manera, per a més, podem veure que el valor del botó de prémer és el signe «+», que també s'esmenta aquí.

10
00:01:14,920 --> 00:01:25,720
El que estem fent aquí és activar un clic. Per tant, s'està fent clic a «9», després al símbol «+» i després al símbol «6» i després al símbol «=».

11
00:01:29,360 --> 00:01:31,920
El símbol igual també és «=»

12
00:01:32,200 --> 00:01:39,240
I després intentem extreure el text de la secció de visualització de resultats

13
00:01:40,120 --> 00:01:47,720
Així que si anem a l'Accerciser i intentem trobar la descripció de l'àrea de visualització del text.

14
00:01:55,200 --> 00:02:04,680
Aquesta és l'àrea de visualització de text i, com podem veure, la descripció diu «Visualització del resultat». Així que això és el que estem fent aquí.

15
00:02:04,680 --> 00:02:10,880
Estem utilitzant «Visualització del resultat» i estem intentant extreure text d'allò que s'està visualitzant.

16
00:02:12,200 --> 00:02:15,600
Això és una prova bàsica, així que intentem fer-ho.

17
00:02:28,520 --> 00:02:33,520
Com podem veure, la prova ha estat un èxit i ha anat bé.

18
00:02:34,600 --> 00:02:48,240
Així és com es poden escriure proves diverses, això també es pot escriure per a la divisió, resta i qualsevol altra cosa que es pugui trobar amb la utilitat Accerciser

19
00:02:49,840 --> 00:02:59,800
Una cosa a tenir en compte és que algunes aplicacions poden no tenir el codi de l'IGU per a l'accessibilitat, en aquest cas haureu d'afegir manualment el codi d'accessibilitat.

20
00:03:00,640 --> 00:03:04,720
Després d'això podreu accedir a aquests elements amb el Selenium.

21
00:03:05,800 --> 00:03:09,640
Gràcies per mirar-ho :)
