1
00:00:00,000 --> 00:00:08,000
Check Point Restore nello spazio utente è un modo per rendere in esecuzione un'applicazione esistente e salvarne lo stato su disco.

2
00:00:08,000 --> 00:00:14,520
Potrebbe essere sbalorditivo, ma con le applicazioni grafiche non funziona, dice nel sito web,

3
00:00:14,520 --> 00:00:17,520
Qualsiasi applicazione X11 non è supportata.

4
00:00:17,520 --> 00:00:24,520
Il lavoro che abbiamo svolto su Wayland per passare da un compositore all'altro risolve implicitamente questo problema.

5
00:00:25,000 --> 00:00:32,520
Faremo una dimostrazione. Ho KolourPaint aperto, sto realizzando alcuni disegni, incollato questo logo Wayland, creando una piacevole composizione.

6
00:00:33,520 --> 00:00:38,520
E ha un bell'aspetto, ma utilizza ben 45 megabyte di RAM.

7
00:00:38,520 --> 00:00:45,520
È possibile che ora debba interrompere e fare qualcos'altro, come compilare WebKit, e ora ho bisogno di tutti i...

8
00:00:45,520 --> 00:00:47,000
...Megabyte.

9
00:00:47,000 --> 00:00:51,520
Quindi eseguo questo comando, criu dump, ed è sparito. Salvato su disco.

10
00:00:52,000 --> 00:01:00,520
Posso riavviare, posso eseguire un aggiornamento del kernel, posso anche prendere questa cartella creata, spostarla su un altro laptop,

11
00:01:00,520 --> 00:01:04,519
e finché i suoi file binari sono presenti, ripristinarla da lì.

12
00:01:04,519 --> 00:01:09,520
Se eseguo criu restore, ritorna e ritorna immediatamente.

13
00:01:09,520 --> 00:01:15,520
Pertanto, questo meccanismo potrebbe essere utilizzato anche per avviare un processo di avvio lento.

14
00:01:16,520 --> 00:01:20,520
E posso trascinare il logo in giro, puoi vedere che è salvato nello stato esatto.

15
00:01:20,520 --> 00:01:27,520
Non sarebbe possibile se salvassi semplicemente l'immagine su disco, perché KolourPaint non ha il concetto di livelli o altro.

16
00:01:29,520 --> 00:01:32,520
Ed è esattamente dov'era.

17
00:01:33,520 --> 00:01:44,520
Ma possiamo fare di più che salvare semplicemente dal ripristino... Apro KMines, un clone di Campo Minato che abbiamo in KDE, se riesco a digitarlo correttamente.

18
00:01:45,520 --> 00:01:48,520
Non sono molto bravo con KMines, faccio la mia mossa.

19
00:01:48,520 --> 00:01:54,520
Ma questa volta quando lo salvo, aggiungerò questo argomento in più, lasciandolo in esecuzione.

20
00:01:54,520 --> 00:01:59,520
E ora posso salvare lo stato, faccio un tentativo.

21
00:01:59,520 --> 00:02:03,520
Ok, non proprio un buon tentativo, deludente.

22
00:02:03,520 --> 00:02:12,520
E ora, se chiudo KMines, posso ripristinarlo esattamente dove mi trovavo e riprovare.

23
00:02:13,520 --> 00:02:18,520
E oh, faccio ancora schifo con KMines, posso chiuderlo e continuare a fare un altro tentativo.

24
00:02:18,520 --> 00:02:26,520
E forse questo non è il modo migliore per giocare a Campo Minato, ma potrebbe avere tutta una serie di altre implicazioni per il debug.

25
00:02:29,520 --> 00:02:41,520
L'uso di Criu non è allo stesso livello del resto del lavoro di trasferimento del compositore, ma sblocca molte opzioni per i futuri sviluppatori con cui sviluppare e creare qualcosa di eccezionale.
