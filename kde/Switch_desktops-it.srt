1
00:00:00,000 --> 00:00:04,880
Iniziamo con il nostro fidato Plasma.

2
00:00:04,880 --> 00:00:10,480
Abbiamo CSGO aperto, in alto e questa piccola applicazione personalizzata che ho realizzato.

3
00:00:10,480 --> 00:00:13,720
Un clic su un pulsante e boom, siamo passati a Weston.

4
00:00:13,720 --> 00:00:15,240
Tutte le applicazioni sono sopravvissute.

5
00:00:15,240 --> 00:00:17,040
Non si tratta di livelli aggiuntivi.

6
00:00:17,040 --> 00:00:20,880
Sono quelle applicazioni che vengono spostate tra i compositori.

7
00:00:20,880 --> 00:00:23,120
Un clic su un pulsante e si torna su Plasma.

8
00:00:23,120 --> 00:00:28,320
E, cosa interessante, siamo passati dalle decorazioni lato client a quelle lato server e viceversa.

9
00:00:28,320 --> 00:00:32,520
E molte delle diverse versioni e differenze vengono gestite implicitamente all'interno del

10
00:00:32,520 --> 00:00:33,520
toolkit.

11
00:00:33,520 --> 00:00:36,800
Possiamo passare a Gnome.

12
00:00:36,800 --> 00:00:38,760
Inizia con questo delizioso effetto panoramica.

13
00:00:38,760 --> 00:00:43,040
Tutti i miei client sono ancora lì, mantenendo la loro dimensione.

14
00:00:43,040 --> 00:00:44,040
Posso passare a Hyprland.

15
00:00:44,040 --> 00:00:47,640
Aspetto che venga caricato.

16
00:00:47,640 --> 00:00:53,440
Otteniamo un'animazione molto bella quando lo fa. Whoo!

17
00:00:53,440 --> 00:00:56,120
Possiamo continuare a spostarci in CSGO.

18
00:00:56,120 --> 00:00:57,760
Si comporta come prima.

19
00:00:57,760 --> 00:00:59,400
Semplicemente funziona tutto.

20
00:00:59,400 --> 00:01:00,400
Passiamo a Sway.

21
00:01:00,400 --> 00:01:04,959
Questo è l'unico modo in cui ho capito come avviare un'applicazione in sway.

22
00:01:04,959 --> 00:01:07,280
Le finestre sono ancora lì.
