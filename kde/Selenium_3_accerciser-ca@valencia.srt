﻿1
00:00:10,480 --> 00:00:18,880
En este vídeo veurem què és Accerciser, i com podem utilitzar-lo per a provar automàticament els elements de la IGU

2
00:00:19,760 --> 00:00:27,560
Ací tinc obert Accerciser, deixa'm que òbriga la utilitat Kcalc

3
00:00:28,560 --> 00:00:31,800
Com podeu veure, reconeix la utilitat de la calculadora.

4
00:00:32,159 --> 00:00:33,840
Si prem dues vegades ací

5
00:00:38,800 --> 00:00:41,720
Ressaltarà la finestra

6
00:00:42,680 --> 00:00:49,840
Podem anar fins al visor d'interfície per a veure els elements.

7
00:00:51,240 --> 00:00:53,520
Per exemple

8
00:00:54,520 --> 00:00:58,480
Este és un marc

9
00:00:58,480 --> 00:01:04,760
I si vull prémer qualsevol botó llavors hauré de trobar este valor, diguem 9.

10
00:01:09,640 --> 00:01:17,240
Com podeu veure ací, açò és 9 i si vull prémer este botó, haurem d'anar fins a accions.

11
00:01:18,800 --> 00:01:22,320
Com podem veure, s'ha premut el 9.

12
00:01:23,320 --> 00:01:26,200
De la mateixa manera també podem trobar el valor per a l'addició.

13
00:01:28,040 --> 00:01:32,400
Si ho prem, es prem el «+».

14
00:01:33,520 --> 00:01:36,640
Comprovem-ho amb «9+6»

15
00:01:48,440 --> 00:01:51,080
Com podem veure «15»

16
00:01:51,920 --> 00:01:58,080
Esta és una manera de trobar els elements que estan associats a accions determinades.

17
00:01:59,800 --> 00:02:09,479
Ací podeu veure la descripció que diu «més», i també ens dona l'ID de l'element.

18
00:02:10,360 --> 00:02:15,360
Açò seria útil per a escriure una prova basada en Selenium que arribaria a l'última part del vídeo.
