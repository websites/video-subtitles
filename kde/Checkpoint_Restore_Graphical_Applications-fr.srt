1
00:00:00,000 --> 00:00:08,000
La vérification de la restauration du point dans l'espace utilisateur est un moyen de prendre une application existante en cours d'exécution et d'enregistrer son état sur le disque.

2
00:00:08,000 --> 00:00:14,520
Il semble que cela pourrait être incroyable, mais à partir des applications graphiques, cela ne fonctionne pas. Voilà ce qui dit sur le site Internet,

3
00:00:14,520 --> 00:00:17,520
Certaines applications « X11 » ne sont pas pas prises en charge.

4
00:00:17,520 --> 00:00:24,520
Le travail que nous avons fait sur Wayland pour se placer entre les compositeurs résout implicitement cela.

5
00:00:25,000 --> 00:00:32,520
Nous allons faire une démonstration. J'ai ouvert KolourPaint. Je fais quelques travaux d'infographie, avec une copie de ce logo Wayland pour faire une jolie composition.

6
00:00:33,520 --> 00:00:38,520
Et ça a l'air bien. Mais il utilise une énorme quantité de RAM de 45 mégaoctets.

7
00:00:38,520 --> 00:00:45,520
Peut-être que maintenant j'ai besoin de partir et de faire autre chose, comme compiler le module « WebKit ». Maintenant j'ai besoin que tout soit disponible...

8
00:00:45,520 --> 00:00:47,000
...Méga-octets.

9
00:00:47,000 --> 00:00:51,520
Donc, j'exécute cette commande « criu dump » et c'est parti. Tout est enregistré sur le disque.

10
00:00:52,000 --> 00:01:00,520
Je peux redémarrer, je peux effectuer une mise à jour du noyau, je peux même prendre ce dossier venant d'être créé, le déplacer vers un autre ordinateur portable,

11
00:01:00,520 --> 00:01:04,519
Et tant que ses binaires sont là, restaurez à partir de là.

12
00:01:04,519 --> 00:01:09,520
Si j'exécute la commande « criu restore », il revient de façon instantanée.

13
00:01:09,520 --> 00:01:15,520
Ainsi, ce mécanisme pourrait également être utilisé pour accélérer un processus démarrant lentement.

14
00:01:16,520 --> 00:01:20,520
Et je peux le faire glisser tout autour du logo. Vous pouvez voir qu'il est enregistré pour figer son état exactement.

15
00:01:20,520 --> 00:01:27,520
Ceci ne serait pas possible si je viens d'enregistrer l'image sur le disque, car KolourPaint n'a pas de concept de calque ou quoi que ce soit similaire.

16
00:01:29,520 --> 00:01:32,520
Et c'est exactement là où c'était.

17
00:01:33,520 --> 00:01:44,520
Mais nous pouvons faire plus que simplement enregistrer à partir de la restauration... J'ouvre KMines, un clone de jeu du démineur, présent dans KDE, si je peux l'épeler correctement.

18
00:01:45,520 --> 00:01:48,520
Pas très bon à KMines, fais mon déplacement.

19
00:01:48,520 --> 00:01:54,520
Mais cette fois où je le copie, je vais ajouter cet argument supplémentaire et arrêter l'exécution.

20
00:01:54,520 --> 00:01:59,520
Et maintenant, je peux enregistrer l'état, émettre une hypothèse.

21
00:01:59,520 --> 00:02:03,520
D'accord, ce n'est pas une très bonne hypothèse. Décevant.

22
00:02:03,520 --> 00:02:12,520
Et maintenant, si je ferme KMines, je peux le restaurer, exactement là où j'étais et démarrer un autre tour.

23
00:02:13,520 --> 00:02:18,520
Et oh, encore entrain de jouer à KMines, je peux le fermer, tout en lançant un autre tour.

24
00:02:18,520 --> 00:02:26,520
Et peut-être que ceci n'est pas la meilleure façon de jouer au démineur, mais cela pourrait avoir toutes sortes d'autres implications pour le débogage.

25
00:02:29,520 --> 00:02:41,520
L'utilisation de Crio n'est pas au même niveau que le reste du travail de transfert du compositeur. Cependant, cela autorise de nombreuses options sur lesquelles les futures équipes de développement pourront utiliser et réaliser quelque chose d'exceptionnel.
