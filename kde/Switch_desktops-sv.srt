1
00:00:00,000 --> 00:00:04,880
Så vi börjar i pålitliga Plasma.

2
00:00:04,880 --> 00:00:10,480
Vi har CSGO öppet, överst och ett litet eget program som jag har gjort.

3
00:00:10,480 --> 00:00:13,720
Klicka på en knapp och pang så är vi i Weston.

4
00:00:13,720 --> 00:00:15,240
Alla program har överlevt.

5
00:00:15,240 --> 00:00:17,040
Det finns inga extra lager.

6
00:00:17,040 --> 00:00:20,880
Det är programmen som flyttas mellan sammansättningarna.

7
00:00:20,880 --> 00:00:23,120
Klicka på en knapp och vi är tillbaka i Plasma.

8
00:00:23,120 --> 00:00:28,320
Och intressant nog har vi gått från klientsidans dekorationer till serversidan ochtillbaks igen.

9
00:00:28,320 --> 00:00:32,520
Och många av de olika versionerna och skillnaderna hanteras implicit inne i

10
00:00:32,520 --> 00:00:33,520
verktygslådan.

11
00:00:33,520 --> 00:00:36,800
Vi kan hoppa till Gnome.

12
00:00:36,800 --> 00:00:38,760
Börja med en underbar översiktseffekt.

13
00:00:38,760 --> 00:00:43,040
Alla mina klienter är kvar och behåller sin storlek.

14
00:00:43,040 --> 00:00:44,040
Kan gå till Hyprland.

15
00:00:44,040 --> 00:00:47,640
Vänta på att det ska laddas.

16
00:00:47,640 --> 00:00:53,440
Kom till en mycket trevlig animering när det görs. Tjo!

17
00:00:53,440 --> 00:00:56,120
Vi kan fortsätta flytta omkring CSGO.

18
00:00:56,120 --> 00:00:57,760
Det beter sig som förut.

19
00:00:57,760 --> 00:00:59,400
Allt funkar bara.

20
00:00:59,400 --> 00:01:00,400
Låt oss hoppa till Sway.

21
00:01:00,400 --> 00:01:04,959
Det här är enda sättet jag kommit på för att starta ett program i Sway.

22
00:01:04,959 --> 00:01:07,280
Fönster är fortfarande där.
