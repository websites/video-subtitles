﻿1
00:00:10,480 --> 00:00:18,880
En este vídeo veremos qué es Accerciser y cómo podemos usarlo para probar automáticamente los elementos de la interfaz gráfica.

2
00:00:19,760 --> 00:00:27,560
Aquí tengo Accerciser abierto. Voy a abrir la utilidad Kcalc.

3
00:00:28,560 --> 00:00:31,800
Como puede ver, reconoce la utilidad de la calculadora.

4
00:00:32,159 --> 00:00:33,840
Si pulso dos veces aquí

5
00:00:38,800 --> 00:00:41,720
Resaltará la ventana

6
00:00:42,680 --> 00:00:49,840
Podemos ir al visor Interfaz para ver los elementos.

7
00:00:51,240 --> 00:00:53,520
Por ejemplo

8
00:00:54,520 --> 00:00:58,480
Esto es un marco

9
00:00:58,480 --> 00:01:04,760
Y si quiero pulsar algún botón, tendré que encontrar su valor. Por ejemplo, 9.

10
00:01:09,640 --> 00:01:17,240
Como puede ver, este es el 9 y si quisiera pulsar este botón, tendremos que ir a las acciones.

11
00:01:18,800 --> 00:01:22,320
Como podemos ver, se ha pulsado 9.

12
00:01:23,320 --> 00:01:26,200
Del mismo modo, también podemos encontrar el valor para la suma.

13
00:01:28,040 --> 00:01:32,400
Si la pulso, se pulsará el '+'.

14
00:01:33,520 --> 00:01:36,640
Probemos con '9+6'

15
00:01:48,440 --> 00:01:51,080
Como podemos ver, '15'

16
00:01:51,920 --> 00:01:58,080
Esta es una forma de encontrar los elementos que están asociados con acciones particulares.

17
00:01:59,800 --> 00:02:09,479
Aquí puede ver que en la descripción pone 'más', además de darnos el ID del elemento.

18
00:02:10,360 --> 00:02:15,360
Esto será útil para escribir la prueba basada en Selenium que aparecerá en la última parte del vídeo.
