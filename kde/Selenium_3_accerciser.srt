﻿1
00:00:10,480 --> 00:00:18,880
In this video we will see what is Accerciser, and how we can use it to automatically test the GUI elements

2
00:00:19,760 --> 00:00:27,560
Here I have the Accerciser opened up, let me open the Kcalc utility

3
00:00:28,560 --> 00:00:31,800
So, as you can see it recognises the calculator utility.

4
00:00:32,159 --> 00:00:33,840
If I double press it here

5
00:00:38,800 --> 00:00:41,720
It will highlight the window

6
00:00:42,680 --> 00:00:49,840
We can go to Interface viewer to view the elements.

7
00:00:51,240 --> 00:00:53,520
So, for example

8
00:00:54,520 --> 00:00:58,480
This is a frame

9
00:00:58,480 --> 00:01:04,760
And if I want to press any buttons then i'll have to find that value, let's say 9.

10
00:01:09,640 --> 00:01:17,240
As you can see here, this is 9 and if I want to press this button, we'll have to go to actions.

11
00:01:18,800 --> 00:01:22,320
As we can see, 9 was pressed.

12
00:01:23,320 --> 00:01:26,200
Similarly we can also find value for addition.

13
00:01:28,040 --> 00:01:32,400
If I press it, the '+' would be pressed.

14
00:01:33,520 --> 00:01:36,640
Lets check it with '9+6'

15
00:01:48,440 --> 00:01:51,080
As we can see '15'

16
00:01:51,920 --> 00:01:58,080
This is a way to find the elements which are associated with a particular actions.

17
00:01:59,800 --> 00:02:09,479
You can see here the description it says 'plus',  and it also gives us the ID of the element.

18
00:02:10,360 --> 00:02:15,360
This would be helpful for writing Selenium based test which would come in the later part of the video.

