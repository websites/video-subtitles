﻿1
00:00:06,320 --> 00:00:10,160
In questo video vedremo come possiamo scrivere test basati su selenium.

2
00:00:11,040 --> 00:00:14,880
Quindi un prerequisito per questo video sarebbe il video «Configurazione di Selenium».

3
00:00:14,880 --> 00:00:21,640
Devi avere selenium installato e funzionante nel tuo sistema in modo da poter iniziare a scrivere i test.

4
00:00:22,400 --> 00:00:30,520
Possiamo seguire la guida di installazione qui, possiamo usare il materiale standard già menzionato nella sezione «Scrivere i test».

5
00:00:31,560 --> 00:00:39,200
Qui ho VS Code aperto e come puoi vedere ho scritto tutto il codice boilerplate

6
00:00:39,200 --> 00:00:47,280
E poi ho scritto un codice per «9 + 6» che dovrebbe essere «15», come mostrato anche nel video di accerciser.

7
00:00:48,600 --> 00:00:49,760
Esaminiamolo

8
00:00:54,280 --> 00:01:01,720
Stiamo cercando di trovare il valore «9», se apro nuovamente Accerciser e trovo «9» qui

9
00:01:03,320 --> 00:01:14,120
Abbiamo visto che il valore del pulsante qui è «9», utilizzato anche qui. Allo stesso modo per più, possiamo vedere che il valore del pulsante è il segno «+», menzionato anche qui.

10
00:01:14,920 --> 00:01:25,720
Ciò che stiamo facendo qui è attivare un clic. Qui viene fatto clic su «9», quindi sul simbolo «+», poi su «6» e quindi sul simbolo «=».

11
00:01:29,360 --> 00:01:31,920
Uguale al simbolo è anche «=»

12
00:01:32,200 --> 00:01:39,240
E poi stiamo provando a estrarre il testo dalla sezione di visualizzazione dei risultati

13
00:01:40,120 --> 00:01:47,720
Quindi, se andiamo su Accerciser e proviamo a trovare la descrizione per l'area di testo visualizzata.

14
00:01:55,200 --> 00:02:04,680
Questa è l'area del testo di visualizzazione e, come possiamo vedere, nella descrizione c'è scritto «Visualizzazione dei risultati». Questo è ciò che stiamo facendo qui.

15
00:02:04,680 --> 00:02:10,880
Stiamo utilizzando «Visualizzazione dei risultati» e stiamo cercando di estrarre il testo da ciò che viene visualizzato.

16
00:02:12,200 --> 00:02:15,600
Questo è un test di base, quindi proviamo a eseguirlo.

17
00:02:28,520 --> 00:02:33,520
Come possiamo vedere, il test ha avuto successo e otteniamo un OK.

18
00:02:34,600 --> 00:02:48,240
Ecco come puoi scrivere più test, questo può anche essere scritto per divisione, sottrazione e qualsiasi altra cosa che puoi trovare con l'utilità Accerciser

19
00:02:49,840 --> 00:02:59,800
Una cosa da notare è che alcune applicazioni potrebbero non avere il codice GUI per l'accessibilità, in tal caso dovrai aggiungere manualmente il codice di accessibilità.

20
00:03:00,640 --> 00:03:04,720
Successivamente sarai in grado di accedere a quegli elementi con Selenium.

21
00:03:05,800 --> 00:03:09,640
Grazie per la visione :)
