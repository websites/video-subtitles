1
00:00:00,000 --> 00:00:08,000
La restauració d'un punt de sincronisme en l'espai d'usuari és una manera de prendre una aplicació existent en execució i guardar el seu estat al disc.

2
00:00:08,000 --> 00:00:14,520
Pareix que podria ser sorprenent, però a les aplicacions gràfiques no funciona, diu en el lloc web,

3
00:00:14,520 --> 00:00:17,520
Cap aplicació X11 no és compatible.

4
00:00:17,520 --> 00:00:24,520
El treball que hem fet en Wayland per a moure's entre compositors resol implícitament açò.

5
00:00:25,000 --> 00:00:32,520
Farem una demostració. Tinc KolourPaint obert, estic fent un treball artístic, apegat en este logotip de Wayland, fent una maqueta encantadora.

6
00:00:33,520 --> 00:00:38,520
I està molt bé, però utilitza 45 megabytes de RAM.

7
00:00:38,520 --> 00:00:45,520
Potser ara he de marxar i fer alguna cosa més, com compilar WebKit, i ara necessite tots els megabytes…

8
00:00:45,520 --> 00:00:47,000
…disponibles.

9
00:00:47,000 --> 00:00:51,520
Així que execute esta ordre, «criu dump», i ha desaparegut. S'ha guardat en el disc.

10
00:00:52,000 --> 00:01:00,520
Puc reiniciar, puc fer una actualització del nucli, fins i tot puc agafar esta carpeta que s'ha creat, moure-la cap a un altre portàtil,

11
00:01:00,520 --> 00:01:04,519
i sempre que hi haja els seus executables, restaureu-los des d'ací.

12
00:01:04,519 --> 00:01:09,520
Si execute «criu restore», torna i ho fa a l'instant.

13
00:01:09,520 --> 00:01:15,520
Per tant, este mecanisme també es podria utilitzar per a arrancar un procés d'inici lent.

14
00:01:16,520 --> 00:01:20,520
I el puc arrossegar pel logotip, podeu veure que s'ha guardat exactament amb el seu estat.

15
00:01:20,520 --> 00:01:27,520
No seria possible si acabara de guardar la imatge al disc, perquè KolourPaint no té el concepte de capes o altres.

16
00:01:29,520 --> 00:01:32,520
I és exactament on era.

17
00:01:33,520 --> 00:01:44,520
Però podem fer alguna cosa més a part de guardar per a la restauració… Òbric KMines, un dragamines que tenim a KDE, si puc escriure-ho bé.

18
00:01:45,520 --> 00:01:48,520
No soc gaire bo a KMines, faç el meu moviment.

19
00:01:48,520 --> 00:01:54,520
Però este cop, quan el bolque, afegiré este argument addicional: «leave-running».

20
00:01:54,520 --> 00:01:59,520
I ara puc guardar l'estat, fent una suposició.

21
00:01:59,520 --> 00:02:03,520
D'acord, no és una suposició gaire bona, és decebedora.

22
00:02:03,520 --> 00:02:12,520
I ara, si tanque KMines, el puc restaurar, exactament on era, i tindre'n un altre.

23
00:02:13,520 --> 00:02:18,520
I encara estic apegat a KMines, puc tancar-lo, seguir tenint una altra oportunitat.

24
00:02:18,520 --> 00:02:26,520
I potser esta no és la millor manera de jugar al dragamines, però podria tindre tota mena d'altres implicacions per a la depuració.

25
00:02:29,520 --> 00:02:41,520
L'ús de Crio no està al mateix nivell que la resta de faena de transferència del compositor, però desbloqueja moltes opcions per a futurs desenvolupadors per a construir i fer alguna cosa genial.
