1
00:00:00,000 --> 00:00:04,000
D'acord, tenim algunes aplicacions obertes, reproduint una pel·lícula,

2
00:00:05,000 --> 00:00:11,000
jugant al Tetris a la cantonada, fent multitasca, i de sobte,

3
00:00:12,000 --> 00:00:14,000
El KWin ha fallat.

4
00:00:17,000 --> 00:00:22,000
I l'àudio no s'atura, el Tetris continuarà tan aviat com ho superi.

5
00:00:26,000 --> 00:00:29,000
I el nostre IDE encara està disponible.

6
00:00:29,000 --> 00:00:33,000
I puc executar això diverses vegades, simulant fallades diverses,

7
00:00:37,000 --> 00:00:39,000
i no perdem ni un fotograma.

8
00:00:40,000 --> 00:00:45,000
Fins i tot podem copiar text, copiar això, reiniciar el KWin.

9
00:00:46,000 --> 00:00:49,000
I ara, encara és al meu porta-retalls.

10
00:00:52,000 --> 00:00:53,000
Robust.
