1
00:00:00,000 --> 00:00:04,880
Así que comenzamos en el fiable Plasma.

2
00:00:04,880 --> 00:00:10,480
Tenemos CSGO abierto, arriba y esta pequeña aplicación personalizada que he creado.

3
00:00:10,480 --> 00:00:13,720
Hacemos clic en un botón y ¡boom!, estamos en Weston.

4
00:00:13,720 --> 00:00:15,240
Todas las aplicaciones han sobrevivido.

5
00:00:15,240 --> 00:00:17,040
No se trata de capas adicionales.

6
00:00:17,040 --> 00:00:20,880
Son esas aplicaciones las que se mueven entre los compositores.

7
00:00:20,880 --> 00:00:23,120
Pulsamos un botón y volvemos a Plasma.

8
00:00:23,120 --> 00:00:28,320
Y, curiosamente, hemos pasado de las decoraciones del lado cliente a las del lado servidor y viceversa.

9
00:00:28,320 --> 00:00:32,520
Y muchas de las distintas versiones y diferencias se manejan implícitamente dentro del

10
00:00:32,520 --> 00:00:33,520
toolkit.

11
00:00:33,520 --> 00:00:36,800
Podemos saltar a Gnome.

12
00:00:36,800 --> 00:00:38,760
Comenzar con este bonito efecto de vista general.

13
00:00:38,760 --> 00:00:43,040
Todos mis clientes siguen aquí, conservando su tamaño.

14
00:00:43,040 --> 00:00:44,040
Podemos ir a Hyprland.

15
00:00:44,040 --> 00:00:47,640
Esperamos a que se cargue.

16
00:00:47,640 --> 00:00:53,440
Conseguiremos una animación muy bonita cuando lo haga. ¡Guau!

17
00:00:53,440 --> 00:00:56,120
Podemos seguir moviéndonos por CSGO.

18
00:00:56,120 --> 00:00:57,760
Se está comportando como antes.

19
00:00:57,760 --> 00:00:59,400
Todo está funcionando.

20
00:00:59,400 --> 00:01:00,400
Vayamos a Sway.

21
00:01:00,400 --> 00:01:04,959
Esta es la única forma que descubrí para iniciar una aplicación en Sway.

22
00:01:04,959 --> 00:01:07,280
Las ventanas siguen ahí.
