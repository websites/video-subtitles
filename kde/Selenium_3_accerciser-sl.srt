﻿1
00:00:10,480 --> 00:00:18,880
V tem videoposnetku bomo videli, kaj je Accerciser in kako ga lahko uporabimo za samodejno preizkušanje elementov vmesnika.

2
00:00:19,760 --> 00:00:27,560
Tukaj imam odprt Accerciser, naj odprem pripomoček Kcalc.

3
00:00:28,560 --> 00:00:31,800
Torej, kot lahko vidite, prepozna pripomoček kalkulatorja.

4
00:00:32,159 --> 00:00:33,840
Če dvakrat pritisnem tukaj,

5
00:00:38,800 --> 00:00:41,720
s tem poudarim okno.

6
00:00:42,680 --> 00:00:49,840
Za ogled elementov lahko obiščemo ogledovalnik vmesnika.

7
00:00:51,240 --> 00:00:53,520
Na primer:

8
00:00:54,520 --> 00:00:58,480
to je okvir.

9
00:00:58,480 --> 00:01:04,760
Če želim pritisniti določen gumb, moram najti to vrednost, recimo 9.

10
00:01:09,640 --> 00:01:17,240
Kot lahko vidite tukaj, je to 9 in če želim pritisniti ta gumb, moram iti k dejanjem.

11
00:01:18,800 --> 00:01:22,320
Kot lahko vidimo, je bil pritisnjen gumb 9.

12
00:01:23,320 --> 00:01:26,200
Podobno lahko najdemo tudi vrednost za seštevanje.

13
00:01:28,040 --> 00:01:32,400
Če ga pritisnem, pritisnem »+«.

14
00:01:33,520 --> 00:01:36,640
Preverimo z »9+6«.

15
00:01:48,440 --> 00:01:51,080
Kot lahko vidimo: »15«.

16
00:01:51,920 --> 00:01:58,080
To je način, kako najti elemente, ki so povezani z določenimi dejanji.

17
00:01:59,800 --> 00:02:09,479
Tukaj lahko vidite opis, v katerem piše »plus« in nam poda tudi ID elementa.

18
00:02:10,360 --> 00:02:15,360
To bi bilo koristno za pisanje preizkusa na osnovi Seleniuma, ki sledi v kasnejšem delu videoposnetka.
