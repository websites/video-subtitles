﻿1
00:00:00,000 --> 00:00:05,200
I den här videon kommer vi att se vad selenium är och hur KDE använder det för att bygga hållbar programvara

2
00:00:05,200 --> 00:00:11,040
Och även hur det hjälper till att uppnå andra KDE-mål som tillgänglighet och systemtestning.

3
00:00:12,200 --> 00:00:20,120
Programvaruanvändning ökar snabbt och med ökningen ökar vi dess miljöpåverkan som till exempel mer CO2-utsläpp

4
00:00:20,120 --> 00:00:24,360
Om vi ​​vill minimera påverkan måste vi minimera energiförbrukningen.

5
00:00:24,360 --> 00:00:28,400
Och för att kunna minimera energianvändning måste vi kunna mäta användningen.

6
00:00:29,240 --> 00:00:34,360
För att kunna mäta energiförbrukning behöver vi en mekanism som kan efterlikna användarbeteende

7
00:00:34,920 --> 00:00:39,240
för att återge vad användaren skulle göra med sin mus och sitt tangentbord.

8
00:00:39,320 --> 00:00:43,920
Tillsammans med det behöver vi också ett maskinvaruverktyg för att faktiskt mäta energiförbrukningen.

9
00:00:43,920 --> 00:00:47,160
Den mekanismen tillhandahålls av Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI är ett verktyg skapat av KDE som använder selenium för automatisering av webbdrivrutiner och linux AT-SPI tillgänglighetsprotokoll.

11
00:00:55,360 --> 00:01:00,360
Med hjälp av selenium kan vi skapa skript med användningsscenarier som efterliknar användarbeteende.

12
00:01:01,520 --> 00:01:05,680
Så som du kan se kan det hjälpa till att bygga hållbar programvara, men det är inte allt.

13
00:01:05,680 --> 00:01:14,400
Selenium hjälper oss också att uppnå KDE:s alla mål, vilket gör det möjligt att förbättra tillgängligheten för alla inklusive personer med funktionshinder.

14
00:01:14,400 --> 00:01:18,400
Det hjälper också att automatisera och systematisera interna processer.

15
00:01:18,720 --> 00:01:24,680
Det hjälper till att skapa funktionella tester som säkerställer att ny kod förblir lika bra som den var innan uppdateringarna.

16
00:01:25,560 --> 00:01:31,800
I nästa del av videon kommer vi att se hur man ställer in selenium lokalt och hur man skriver några grundläggande funktionstester.
