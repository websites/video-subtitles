﻿1
00:00:06,320 --> 00:00:10,160
In deze video zullen we zien hoe we op selenium gebaseerde tests kunnen schrijven.

2
00:00:11,040 --> 00:00:14,880
Dus een voorwaarde voor deze video zou zijn de video `Setting up Selenium`.

3
00:00:14,880 --> 00:00:21,640
U moet dus echt selenium geïnstalleerd hebben en uitvoeren in uw systeem zodat u kunt beginnen met het schrijven van de test.

4
00:00:22,400 --> 00:00:30,520
We kunnen de gids voor opzetten hier volgen, we kunnen de recepten die al zijn genoemd in de sectie 'Writing Tests' gebruiken.

5
00:00:31,560 --> 00:00:39,200
Ik heb hier dus de VS-code geopend en zoals u kunt zien heb ik al code van het recept geschreven

6
00:00:39,200 --> 00:00:47,280
En daarna heb ik een code geschreven voor '9 + 6' wat '15' zou moeten zijn die ook getoond is in de Accerciser-video.

7
00:00:48,600 --> 00:00:49,760
Laten we er doorheen gaan

8
00:00:54,280 --> 00:01:01,720
We proberen de waarde '9' te vinden, als ik de Accerciser nog eens open, en hier '9' vindt

9
00:01:03,320 --> 00:01:14,120
We zagen dat de waarde van drukknop hier '9' is die hier ook is gebruikt. Evenzo voor plus, we kunnen zien dat de waarde van de drukknop het '+' teken is die hier ook wordt genoemd.

10
00:01:14,920 --> 00:01:25,720
Wat we hier gaan doen is een klik starten. Dus hier wordt op '9' geklikt, daarna op het symbool '+' en daarna '6' en daarna het symbool '='.

11
00:01:29,360 --> 00:01:31,920
Gelijk aan het symbool is ook '='

12
00:01:32,200 --> 00:01:39,240
En daarna proberen we de tekst uit de "Result Display" te extraheren

13
00:01:40,120 --> 00:01:47,720
Dus als we naar de Accerciser gaan en de beschrijving proberen te vinden voor het tekstgebied op het scherm.

14
00:01:55,200 --> 00:02:04,680
Dit is het tekstgebied op het scherm en zoals we kunnen zien, vertelt de beschrijving 'Result Display'. Dus dit is wat we hier aan het doen zijn.

15
00:02:04,680 --> 00:02:10,880
We gaan 'Result Display' gebruiken en we gaan proberen tekst van wat wordt getoond te extraheren.

16
00:02:12,200 --> 00:02:15,600
Dit is een basistest, dus laten we proberen het uit te voeren.

17
00:02:28,520 --> 00:02:33,520
Zoals we kunnen zien was de test succesvol en krijgen we een OK.

18
00:02:34,600 --> 00:02:48,240
Dit is hoe u meerdere testen kunt schrijven, dit kan ook geschreven worden voor delen, aftrekken en al het andere dat u kunt vind met het hulpmiddel Accerciser

19
00:02:49,840 --> 00:02:59,800
Nog iets op op te merken is dat sommige toepassingen niet de GUI-code hebben voor toegankelijkheid, in dat geval moet u handmatig de toegankelijkheidscode toevoegen.

20
00:03:00,640 --> 00:03:04,720
Daarna zult u in staat zijn om die elementen te benaderen met Selenium.

21
00:03:05,800 --> 00:03:09,640
Hartelijk dank voor het bekijken :)
