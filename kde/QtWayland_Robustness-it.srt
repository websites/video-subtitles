1
00:00:00,000 --> 00:00:04,000
Ok, abbiamo alcune applicazioni aperte, film in riproduzione,

2
00:00:05,000 --> 00:00:11,000
Tetris giocato in un angolo, multitasking, e poi all'improvviso,

3
00:00:12,000 --> 00:00:14,000
Il nostro KWin si arresta in modo inatteso.

4
00:00:17,000 --> 00:00:22,000
E l'audio non si è fermato, Tetris continua non appena riprende.

5
00:00:26,000 --> 00:00:29,000
E il nostro IDE è ancora disponibile.

6
00:00:29,000 --> 00:00:33,000
E posso eseguirlo più volte, simulare più arresti anomali,

7
00:00:37,000 --> 00:00:39,000
e non abbiamo saltato un fotogramma.

8
00:00:40,000 --> 00:00:45,000
Possiamo anche copiare del testo, copiare questo, riavviare KWin.

9
00:00:46,000 --> 00:00:49,000
E ora è ancora sulla mia bacheca.

10
00:00:52,000 --> 00:00:53,000
Robusto.
