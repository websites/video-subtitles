﻿1
00:00:11,080 --> 00:00:13,720
In questo video tutorial vedremo come configurare il selenio.

2
00:00:14,800 --> 00:00:18,360
Selenium è uno strumento utilizzato per il test delle interfacce delle applicazioni.

3
00:00:18,360 --> 00:00:23,360
Puoi trovare questa guida  d'installazione su community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
In questo video lo configureremo su neon OS. Perciò iniziamo.

5
00:00:35,000 --> 00:00:44,080
(Ho già installato questi pacchetti, servirà molto più tempo per questo passaggio)

6
00:00:47,320 --> 00:00:53,360
Una volta che avremo i pacchetti necessari, cloneremo ora lo strumento Selenium e seguiremo i passaggi successivi.

7
00:00:56,560 --> 00:01:01,560
(puoi semplicemente copiare e incollare questi comandi)

8
00:01:29,520 --> 00:01:39,440
Una volta eseguiti correttamente questi passaggi, possiamo iniziare a scrivere i test iniziali. Puoi seguire questi passaggi qui per sapere come scrivere alcuni test di base per selenium.

9
00:01:40,000 --> 00:01:45,000
Proviamo a eseguire un test di esempio. Assicurati di avere kcalc installato nel tuo sistema.

10
00:01:47,200 --> 00:01:52,200
kcalc è fondamentalmente un'applicazione di calcolatrice per KDE.

11
00:02:00,960 --> 00:02:04,200
Come puoi vedere siamo in grado di eseguire test con selenium.

12
00:02:12,720 --> 00:02:21,360
Tieni presente che selenium è un bersaglio mobile e durante la realizzazione di questo video non ci sono errori e errori di creazione, ma tali errori potrebbero verificarsi in futuro.

13
00:02:21,920 --> 00:02:26,360
Assicurati di segnalare eventuali bug alla comunità KDE. E grazie per aver guardato il video!
