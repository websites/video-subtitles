﻿1
00:00:10,480 --> 00:00:18,880
In deze video zullen we zien wat Accerciser is en hoe we het kunnen gebruiken om automatisch de GUI-elementen te testen

2
00:00:19,760 --> 00:00:27,560
Hier heb ik de Accerciser geopend, laat me het Kcalc hulpprogramma openen

3
00:00:28,560 --> 00:00:31,800
Dus, zoals u kunt zien herkent het het hulpprogramma rekenmachine.

4
00:00:32,159 --> 00:00:33,840
Als ik dit hier dubbelklik

5
00:00:38,800 --> 00:00:41,720
Zal het het venster doen oplichten

6
00:00:42,680 --> 00:00:49,840
We kunnen naar de interfaceweergave gaan om de elementen te zien.

7
00:00:51,240 --> 00:00:53,520
Dus, bijvoorbeeld

8
00:00:54,520 --> 00:00:58,480
Dit is een frame

9
00:00:58,480 --> 00:01:04,760
En als ik een knop wil indrukken dan moet ik die waarde vinden, laten we zeggen 9.

10
00:01:09,640 --> 00:01:17,240
Zoals u hier kunt zien, dit is 9 en als ik deze knop wil indrukken moeten we naar acties gaan.

11
00:01:18,800 --> 00:01:22,320
Zoals we kunnen zien, 9 was ingedrukt.

12
00:01:23,320 --> 00:01:26,200
Evenzo kunnen we de waarde voor optellen vinden.

13
00:01:28,040 --> 00:01:32,400
Als ik er op druk zal de '+' worden ingedrukt.

14
00:01:33,520 --> 00:01:36,640
Laten we het controleren met '9+6'

15
00:01:48,440 --> 00:01:51,080
Zoals we kunnen zie '15'

16
00:01:51,920 --> 00:01:58,080
Dit is een manier om de elementen te vinden die geassocieerd zijn met een specifieke actie.

17
00:01:59,800 --> 00:02:09,479
U kunt hier de beschrijving zien die 'plus' zegt en het ons ook geeft als het ID van het element.

18
00:02:10,360 --> 00:02:15,360
Dit zou behulpzaam zijn voor het schrijven van op Selenium gebaseerde testen die in een later deel van de video zouden komen.
