﻿1
00:00:11,080 --> 00:00:13,720
En este videotutorial veremos cómo configurar selenium.

2
00:00:14,800 --> 00:00:18,360
Selenium es una herramienta que se usa para probar la interfaz gráfica de las aplicaciones.

3
00:00:18,360 --> 00:00:23,360
Puede encontrar esta guía de instalación en community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
En este vídeo lo configuraremos en neon OS. Así que, ¡empecemos!

5
00:00:35,000 --> 00:00:44,080
(Ya tengo estos paquetes instalados, ya que llevaría mucho tiempo para este paso)

6
00:00:47,320 --> 00:00:53,360
Cuando tengamos los paquetes necesarios, clonaremos la herramienta Selenium y seguiremos los pasos posteriores.

7
00:00:56,560 --> 00:01:01,560
(solo tiene que copiar y pegar estas órdenes)

8
00:01:29,520 --> 00:01:39,440
Tras completar estos pasos con éxito, ya podemos empezar a escribir las pruebas iniciales. Puede seguir estos pasos para aprender a escribir algunas pruebas básicas para selenium.

9
00:01:40,000 --> 00:01:45,000
Vamos a intentar ejecutar una prueba de ejemplo. Asegúrese de que tiene kcalc instalada en el sistema.

10
00:01:47,200 --> 00:01:52,200
kcalc es básicamente la aplicación de calculadora de KDE.

11
00:02:00,960 --> 00:02:04,200
Como puede ver, podemos ejecutar pruebas con selenium.

12
00:02:12,720 --> 00:02:21,360
Tenga en cuenta que el selenium es un objetivo en movimiento y al realizar este vídeo no hay errores ni fallos de compilación, aunque dichos errores pueden ocurrir en el futuro.

13
00:02:21,920 --> 00:02:26,360
Asegúrese de informar de cualquier fallo a la comunidad KDE. ¡Y gracias por ver este vídeo!
