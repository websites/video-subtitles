1
00:00:00,000 --> 00:00:04,880
Beraz, Plasma fidagarria abiatzen ari gara.

2
00:00:04,880 --> 00:00:10,480
CSGO irekita dugu, «htop» eta nik neurrira egindako aplikazio txiki hori.

3
00:00:10,480 --> 00:00:13,720
Botoi batean klik egin eta, danba, «Weston»en gaude.

4
00:00:13,720 --> 00:00:15,240
Aplikazio guztiek biziraun dute.

5
00:00:15,240 --> 00:00:17,040
Hori ez da geruza gehigarri bat.

6
00:00:17,040 --> 00:00:20,880
Aplikazio horiek dira, konposatzaile artean mugitzen.

7
00:00:20,880 --> 00:00:23,120
Botoi bati klik eta atzera Plasman.

8
00:00:23,120 --> 00:00:28,320
Eta interesgarria dena, bezero aldeko apaingarrietatik zerbitzari aldekoetara joan eta berriz atzera egin dugu.

9
00:00:28,320 --> 00:00:32,520
Eta bertsio ezberdin asko eta ezberdintasunak inplizituki maneiatuak dira

10
00:00:32,520 --> 00:00:33,520
tresna-kutxan.

11
00:00:33,520 --> 00:00:36,800
Gnomera jauzi egin dezakegu.

12
00:00:36,800 --> 00:00:38,760
Ikuspegi-orokorreko efektu maitagarri hori abiatu.

13
00:00:38,760 --> 00:00:43,040
Nire bezero guztiak hor daude oraindik, haien neurria mantenduz.

14
00:00:43,040 --> 00:00:44,040
«Hyprland»era joan daiteke.

15
00:00:44,040 --> 00:00:47,640
Itxoin hura zamatu arte.

16
00:00:47,640 --> 00:00:53,440
Oso animazio atseginera iristen da egiten duenean. Aupa!

17
00:00:53,440 --> 00:00:56,120
CSGO inguruan mugitzen jarrai dezakegu.

18
00:00:56,120 --> 00:00:57,760
Lehengo jokabide bera du.

19
00:00:57,760 --> 00:00:59,400
Dena ondo dabil.

20
00:00:59,400 --> 00:01:00,400
«Sway»ra jauzi egin dezagun.

21
00:01:00,400 --> 00:01:04,959
Hori da «sway»n aplikazio bat abiarazteko bururatu zaidan modu bakarra.

22
00:01:04,959 --> 00:01:07,280
Leihoak oraindik hor daude.
