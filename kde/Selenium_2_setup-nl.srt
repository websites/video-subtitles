﻿1
00:00:11,080 --> 00:00:13,720
In deze videohandleiding zullen we zien hoe selenium in te stellen.

2
00:00:14,800 --> 00:00:18,360
Selenium is een hulpmiddel gebruikt om GUI's van toepassingen te testen.

3
00:00:18,360 --> 00:00:23,360
U kunt deze installatiegids vinden op community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
In deze video zullen we het neon OS opzetten. Laten we dus beginnen.

5
00:00:35,000 --> 00:00:44,080
(I heb al deze pakketten geïnstalleerd, het zal veel meer tijd nemen voor deze stap)

6
00:00:47,320 --> 00:00:53,360
Nadat we de noodzakelijke pakketten hebben zullen we nu het hulpmiddel Selenium klonen en de volgende stappen volgen.

7
00:00:56,560 --> 00:01:01,560
(u kunt eenvoudig deze commando's kopiëren en plakken)

8
00:01:29,520 --> 00:01:39,440
Nadat deze stappen met succes zijn gedaan kunne we beginnen met het schrijven van initiële testen. U kunt deze stappen hier volgen om te weten te komen hoe enige basistesten voor selenium te schrijven.

9
00:01:40,000 --> 00:01:45,000
Laten we proberen een voorbeeldtest uit te voeren. Ga na dat u kcalc hebt geïnstalleerd in uw systeem.

10
00:01:47,200 --> 00:01:52,200
kcalc is in de basis een rekenmachinehulpmiddel van KDE.

11
00:02:00,960 --> 00:02:04,200
Zoals u kunt zien zijn we in staat testen met selenium uit te voeren.

12
00:02:12,720 --> 00:02:21,360
Bedenk dat selenium een bewegend doel is en terwijl er bij het maken van deze video geen fouten waren en mislukkingen bij het bouwen, kunnen zulke fouten in de toekomst wel gebeuren.

13
00:02:21,920 --> 00:02:26,360
Rapporteer elke bug aan de KDE-gemeenschap. En hartelijk dank voor het bekijken van de video!
