﻿1
00:00:00,000 --> 00:00:05,200
En este vídeo veurem que és Selenium i com KDE l'utilitza per a construir programari sostenible

2
00:00:05,200 --> 00:00:11,040
I també com ajuda a aconseguir altres objectius de KDE que són l'accessibilitat i les proves del sistema.

3
00:00:12,200 --> 00:00:20,120
L'ús de programari està augmentant ràpidament i amb este augment fem augmentar el seu impacte ambiental com per exemple més emissions de CO2

4
00:00:20,120 --> 00:00:24,360
Si volem minimitzar este impacte, hem de minimitzar el consum d'energia.

5
00:00:24,360 --> 00:00:28,400
I per a minimitzar el consum d'energia necessitem ser capaços de mesurar este consum.

6
00:00:29,240 --> 00:00:34,360
Per a poder mesurar el consum d'energia necessitem mecanismes que puguen imitar el comportament de l'usuari

7
00:00:34,920 --> 00:00:39,240
reproduir el que l'usuari faria amb el ratolí i el teclat.

8
00:00:39,320 --> 00:00:43,920
A més d'açò, també necessitem una eina de maquinari per a mesurar realment els consums d'energia.

9
00:00:43,920 --> 00:00:47,160
Este mecanisme serà proporcionat per Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI és una eina creada per KDE que utilitza Selenium per a l'automatització del controlador web i el protocol d'accessibilitat linux AT-SPI.

11
00:00:55,360 --> 00:01:00,360
Amb l'ajuda de Selenium podem crear un script d'escenaris d'ús que imite el comportament de l'usuari.

12
00:01:01,520 --> 00:01:05,680
Així que, com es pot veure, pot ajudar a construir programari sostenible, però açò no és tot.

13
00:01:05,680 --> 00:01:14,400
Selenium també ens ajuda a aconseguir l'objectiu de KDE per a tothom, que permet millorar l'accessibilitat per a tothom, incloses les persones amb discapacitat.

14
00:01:14,400 --> 00:01:18,400
També ajuda a automatitzar i sistematitzar processos interns.

15
00:01:18,720 --> 00:01:24,680
Ajuda a crear proves funcionals que asseguren que el codi nou es manté tan correcte com abans de les actualitzacions.

16
00:01:25,560 --> 00:01:31,800
En la següent part del vídeo veurem com configurar localment Selenium i com escriure algunes proves funcionals bàsiques.
