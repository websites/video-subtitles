1
00:00:00,000 --> 00:00:08,000
Check Point Restore in user space is a way to take an existing application as running and save its state to disk.

2
00:00:08,000 --> 00:00:14,520
It sounds like it could be amazing, but from graphical applications, it doesn't work out, says in the website,

3
00:00:14,520 --> 00:00:17,520
Any X11 application is unsupported.

4
00:00:17,520 --> 00:00:24,520
The work we've done on Wayland to move between compositors implicitly solves this.

5
00:00:25,000 --> 00:00:32,520
We'll do a demo. I've got KolourPaint open, I'm doing some artwork, pasted in this Wayland logo, make a lovely mashup.

6
00:00:33,520 --> 00:00:38,520
And it's looking nice, but it's using a whopping 45 megabytes of RAM.

7
00:00:38,520 --> 00:00:45,520
Maybe now I need to go off and do something else, like compile WebKit, and now I need every available...

8
00:00:45,520 --> 00:00:47,000
...Megabyte.

9
00:00:47,000 --> 00:00:51,520
So, I run this command, criu dump, and it's gone. Saved to disk.

10
00:00:52,000 --> 00:01:00,520
I can reboot, I can perform a kernel update, I can even take this folder that it's created, move it to another laptop,

11
00:01:00,520 --> 00:01:04,519
and as long as its binaries are there, restore from there.

12
00:01:04,519 --> 00:01:09,520
If I run criu restore, it comes back, and it comes back instantly.

13
00:01:09,520 --> 00:01:15,520
So, this mechanism could be used to bootstrap a slow starting process as well.

14
00:01:16,520 --> 00:01:20,520
And I can drag it away the logo around, you can see it's saved to state exactly.

15
00:01:20,520 --> 00:01:27,520
It wouldn't be possible if I just saved the image to disk, because KolourPaint doesn't have a concept of layers or anything.

16
00:01:29,520 --> 00:01:32,520
And it's exactly where it was.

17
00:01:33,520 --> 00:01:44,520
But we can do more than just save from the restore... I open KMines, a Minesweeper of clone that we have in KDE, if I can spell it right.

18
00:01:45,520 --> 00:01:48,520
Not very good at KMines, make my move.

19
00:01:48,520 --> 00:01:54,520
But this time when I dump it, I'm going to add this extra argument, leave running.

20
00:01:54,520 --> 00:01:59,520
And now I can save the state, make a guess.

21
00:01:59,520 --> 00:02:03,520
Okay, not a very good guess, disappointing.

22
00:02:03,520 --> 00:02:12,520
And now, if I close KMines, I can restore it, exactly where I was, and have another go.

23
00:02:13,520 --> 00:02:18,520
And oh, still suck at KMines, I can close it, keep having another go.

24
00:02:18,520 --> 00:02:26,520
And maybe this isn't the best way to play Minesweeper, but it could have all sorts of other implications for debugging.

25
00:02:29,520 --> 00:02:41,520
Use of Crio isn't at the same level as the rest of the compositor handoff work, but it unlocks a lot of options for future developers to build on and make something great with.

