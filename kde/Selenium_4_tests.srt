﻿1
00:00:06,320 --> 00:00:10,160
In this video we will see how we can write selenium based tests.

2
00:00:11,040 --> 00:00:14,880
So a prerequisite to this video would be the `Setting up Selenium` video.

3
00:00:14,880 --> 00:00:21,640
So you really need to have selenium installed and running in you system so that you can start writing test.

4
00:00:22,400 --> 00:00:30,520
We can follow the setup guide here, we can use the boilerplate stuff which is already mentioned in the 'Writing Tests' section.

5
00:00:31,560 --> 00:00:39,200
So here I have VS Code opened and as you can see I have all the boilerplate code written

6
00:00:39,200 --> 00:00:47,280
And then I've written a code for '9 + 6' which should be '15' which was also shown in the accerciser video.

7
00:00:48,600 --> 00:00:49,760
Let's go through it

8
00:00:54,280 --> 00:01:01,720
We are trying to find the value '9', if I open the Accerciser once again, and find '9' here

9
00:01:03,320 --> 00:01:14,120
We saw the push button value here is '9' which is also used here. Similarly for plus, we can see the push button value is '+' sign which is also mentioned here.

10
00:01:14,920 --> 00:01:25,720
What we are doing here is triggering a click. So '9' is being clicked here, then the '+' symbol and then '6' and then '=' symbol.

11
00:01:29,360 --> 00:01:31,920
Equal to symbol is also '='

12
00:01:32,200 --> 00:01:39,240
And then we are trying to extract the text from the result display section

13
00:01:40,120 --> 00:01:47,720
So if we go to the Accerciser and try to find description for the display text area.

14
00:01:55,200 --> 00:02:04,680
This is the display text area and as we can see, description it says 'Result Display'. So this is what we are doing here.

15
00:02:04,680 --> 00:02:10,880
We are using 'Result Display' and we are trying to extract text from what is being displayed.

16
00:02:12,200 --> 00:02:15,600
This is a basic test, so let's just try to run it.

17
00:02:28,520 --> 00:02:33,520
As we can see, the test was successful and we get an OK.

18
00:02:34,600 --> 00:02:48,240
This is how you can write multiple tests, this can also be written for division, subtraction and anything else that you can find with the Accerciser utility

19
00:02:49,840 --> 00:02:59,800
One thing to note is some applications may not have the GUI code for accessibility, in that case you will have to manually add the accessibility code.

20
00:03:00,640 --> 00:03:04,720
After that you will be able to access those elements with Selenium.

21
00:03:05,800 --> 00:03:09,640
Thanks for watching :)

