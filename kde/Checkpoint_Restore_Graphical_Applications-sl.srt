1
00:00:00,000 --> 00:00:08,000
Check Point Restore v uporabniškem prostoru je način, da obstoječo aplikacijo vzamete za delujočo in njeno stanje shranite na disk.

2
00:00:08,000 --> 00:00:14,520
Sliši se, kot da bi lahko bilo neverjetno, toda iz grafičnih aplikacij se ne izide, piše na spletnem mestu,

3
00:00:14,520 --> 00:00:17,520
Nobena aplikacija X11 ni podprta.

4
00:00:17,520 --> 00:00:24,520
Delo, ki smo ga opravili na Waylandu za premikanje med upodobilniki, to implicitno rešuje.

5
00:00:25,000 --> 00:00:32,520
Naredili bomo demo. Imam odprt KolourPaint, delam nekaj umetnin, prilepljene v ta logotip Wayland, naredim čudovito mešanico.

6
00:00:33,520 --> 00:00:38,520
In videti je lepo, vendar uporablja ogromnih 45 megabajtov RAM-a.

7
00:00:38,520 --> 00:00:45,520
Mogoče moram zdaj iti in narediti nekaj drugega, na primer prevesti WebKit, zdaj pa potrebujem vse razpoložljive...

8
00:00:45,520 --> 00:00:47,000
...megabajtov.

9
00:00:47,000 --> 00:00:51,520
Torej, zaženem ta ukaz, criu dump, in ga ni več. Shranjeno na disk.

10
00:00:52,000 --> 00:01:00,520
Lahko znova zaženem, lahko izvedem posodobitev jedra, lahko celo vzamem to mapo, ki je bila ustvarjena, in jo premaknem na drug prenosnik,

11
00:01:00,520 --> 00:01:04,519
in dokler so njegove binarne datoteke tam, se obnovi od tam.

12
00:01:04,519 --> 00:01:09,520
Če zaženem criu restore, se vrne in se vrne takoj.

13
00:01:09,520 --> 00:01:15,520
Ta mehanizem bi torej lahko uporabili tudi za zagon počasnega zagonskega procesa.

14
00:01:16,520 --> 00:01:20,520
Lahko ga povlečem proč z logotipom, lahko vidite, da je shranjen za natančno navedbo.

15
00:01:20,520 --> 00:01:27,520
Ne bi bilo mogoče, če bi sliko samo shranil na disk, ker KolourPaint nima koncepta plasti ali česa podobnega.

16
00:01:29,520 --> 00:01:32,520
In točno tam, kjer je bilo.

17
00:01:33,520 --> 00:01:44,520
Ampak lahko storimo več kot samo shranimo iz obnovitve ... Odprem KMines, Minesweeper ali klon, ki ga imamo v KDE, če se prav zapišem.

18
00:01:45,520 --> 00:01:48,520
Nisem ravno dober v KMines, naredi svojo potezo.

19
00:01:48,520 --> 00:01:54,520
Toda ko bom tokrat opustil, bom dodal ta dodaten argument, pustil teči.

20
00:01:54,520 --> 00:01:59,520
In zdaj lahko rešim državo, ugibajte.

21
00:01:59,520 --> 00:02:03,520
V redu, ni ravno dobro ugibanje, razočaranje.

22
00:02:03,520 --> 00:02:12,520
In zdaj, če zaprem KMines, ga lahko obnovim, točno tam, kjer sem bil, in poskusim znova.

23
00:02:13,520 --> 00:02:18,520
In oh, KMines je še vedno zanič, lahko ga zaprem in nadaljujem.

24
00:02:18,520 --> 00:02:26,520
In morda to ni najboljši način za igranje Minesweeperja, vendar bi lahko imel številne druge posledice za odpravljanje napak.

25
00:02:29,520 --> 00:02:41,520
Uporaba Crio ni na enaki ravni kot ostalo delo predaje upodobilnika, vendar prihodnjim razvijalcem odklene veliko možnosti, na katerih lahko gradijo in naredijo nekaj odličnega.
