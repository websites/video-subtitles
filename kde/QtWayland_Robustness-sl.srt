1
00:00:00,000 --> 00:00:04,000
V redu, nekaj aplikacij se je odprlo, film se predvaja,

2
00:00:05,000 --> 00:00:11,000
Tetris, ki se igra v kotu, opravlja več nalog, nato pa nenadoma,

3
00:00:12,000 --> 00:00:14,000
Naš KWin se sesuje.

4
00:00:17,000 --> 00:00:22,000
In zvok se ni ustavil, Tetris se nadaljuje takoj, ko ga spet preboleva.

5
00:00:26,000 --> 00:00:29,000
Naš IDE je še vedno na voljo.

6
00:00:29,000 --> 00:00:33,000
To lahko zaženem večkrat, simuliram več sesutij,

7
00:00:37,000 --> 00:00:39,000
in nismo preskočili okvirja.

8
00:00:40,000 --> 00:00:45,000
Lahko celo kopiramo nekaj besedila, kopiramo to, znova zaženemo KWin.

9
00:00:46,000 --> 00:00:49,000
In zdaj je še vedno na moji plošči za zbiranje.

10
00:00:52,000 --> 00:00:53,000
Robusten.
