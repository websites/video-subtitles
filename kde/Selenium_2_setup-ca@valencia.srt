﻿1
00:00:11,080 --> 00:00:13,720
En esta guia d'aprenentatge en vídeo veurem com configurar Selenium.

2
00:00:14,800 --> 00:00:18,360
Selenium és una eina que s'utilitza per a les proves de la IGU de les aplicacions.

3
00:00:18,360 --> 00:00:23,360
Trobareu esta guia d'instal·lació a community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
En este vídeo el configurarem en «neon OS». Així que comencem.

5
00:00:35,000 --> 00:00:44,080
(Ja tinc estos paquets instal·lats, en este pas trigareu molt més temps)

6
00:00:47,320 --> 00:00:53,360
Una vegada tinguem els paquets necessaris, ara clonarem l'eina Selenium i seguirem els passos posteriors.

7
00:00:56,560 --> 00:01:01,560
(senzillament podeu copiar i apegar estes ordres)

8
00:01:29,520 --> 00:01:39,440
Amb estos passos realitzats amb èxit, podem començar a escriure les proves inicials. Podeu seguir estos passos ací per a saber com escriure algunes proves bàsiques per a Selenium.

9
00:01:40,000 --> 00:01:45,000
Intentem executar una prova d'exemple. Assegureu-vos que teniu KCalc instal·lat en el sistema.

10
00:01:47,200 --> 00:01:52,200
KCalc és bàsicament una aplicació d'utilitat de calculadora de KDE.

11
00:02:00,960 --> 00:02:04,200
Com podeu veure, podem fer proves amb Selenium.

12
00:02:12,720 --> 00:02:21,360
Cal tindre en compte que Selenium és un objectiu en moviment i mentre es fa este vídeo no hi ha errors ni fallades de construcció, però estos errors poden ocórrer en el futur.

13
00:02:21,920 --> 00:02:26,360
Assegureu-vos d'informar de qualsevol error a la comunitat KDE. I gràcies per veure el vídeo!
