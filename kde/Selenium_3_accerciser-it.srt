﻿1
00:00:10,480 --> 00:00:18,880
In questo video vedremo cos'è Accerciser e come possiamo usarlo per testare automaticamente gli elementi dell'interfaccia grafica

2
00:00:19,760 --> 00:00:27,560
Qui ho aperto Accerciser, lasciami aprire l'applicazione Kcalc

3
00:00:28,560 --> 00:00:31,800
Quindi, come puoi vedere, riconosce l'applicazione della calcolatrice.

4
00:00:32,159 --> 00:00:33,840
Se lo premo due volte qui

5
00:00:38,800 --> 00:00:41,720
Evidenzierà la finestra

6
00:00:42,680 --> 00:00:49,840
Possiamo andare al visualizzatore di interfaccia per visualizzare gli elementi.

7
00:00:51,240 --> 00:00:53,520
Quindi, per esempio

8
00:00:54,520 --> 00:00:58,480
Questo è un fotogramma

9
00:00:58,480 --> 00:01:04,760
E se voglio premere qualsiasi pulsante dovrò trovare quel valore, diciamo 9.

10
00:01:09,640 --> 00:01:17,240
Come puoi vedere qui, questo è 9 e se voglio premere questo pulsante, dovremo andare alle azioni.

11
00:01:18,800 --> 00:01:22,320
Come possiamo vedere, è stato premuto il 9.

12
00:01:23,320 --> 00:01:26,200
Allo stesso modo possiamo anche trovare valore per l'addizione.

13
00:01:28,040 --> 00:01:32,400
Se lo premo, sarà premuto il segno «+».

14
00:01:33,520 --> 00:01:36,640
Verifichiamolo con «9+6»

15
00:01:48,440 --> 00:01:51,080
Come possiamo vedere «15»

16
00:01:51,920 --> 00:01:58,080
Questo è un modo per trovare gli elementi associati a una particolare azione.

17
00:01:59,800 --> 00:02:09,479
Puoi vedere qui la descrizione che dice «più» e ci fornisce anche l'ID dell'elemento.

18
00:02:10,360 --> 00:02:15,360
Ciò sarebbe utile per scrivere il test basato su selenium che sarà fornito nella parte successiva del video.
