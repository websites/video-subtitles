﻿1
00:00:00,000 --> 00:00:05,200
V tem videu bomo videli, kaj je selenium in kako ga KDE uporablja za gradnjo trajnostne programske opreme

2
00:00:05,200 --> 00:00:11,040
In tudi, kako pomaga pri doseganju drugih ciljev KDE, kot sta dostopnost in testiranje sistema.

3
00:00:12,200 --> 00:00:20,120
Uporaba programske opreme hitro narašča in s tem povečanjem povečujemo njen vpliv na okolje, kot je na primer več emisij CO2

4
00:00:20,120 --> 00:00:24,360
Če želimo ta vpliv čim bolj zmanjšati, moramo zmanjšati porabo energije.

5
00:00:24,360 --> 00:00:28,400
Da bi zmanjšali porabo energije, moramo biti sposobni to porabo izmeriti.

6
00:00:29,240 --> 00:00:34,360
Da bi lahko izmerili porabo energije, potrebujemo mehanizme, ki lahko posnemajo vedenje uporabnikov

7
00:00:34,920 --> 00:00:39,240
reproducirati, kaj bi uporabnik naredil s svojo miško in tipkovnico.

8
00:00:39,320 --> 00:00:43,920
Poleg tega potrebujemo tudi strojno opremo za dejansko merjenje porabe energije.

9
00:00:43,920 --> 00:00:47,160
Ta mehanizem bo zagotovil Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI je orodje, ki ga je ustvarila KDE in uporablja selenium za avtomatizacijo spletnih gonilnikov in protokol dostopnosti linux AT-SPI.

11
00:00:55,360 --> 00:01:00,360
S pomočjo seleniuma lahko ustvarimo skript scenarija uporabe, ki posnema vedenje uporabnika.

12
00:01:01,520 --> 00:01:05,680
Kot lahko vidite, lahko pomaga zgraditi trajnostno programsko opremo, vendar to še ni vse.

13
00:01:05,680 --> 00:01:14,400
Selenium nam prav tako pomaga doseči cilj KDE za vse, kar omogoča izboljšanje dostopnosti za vse, vključno z osebami s posebnimi potrebami.

14
00:01:14,400 --> 00:01:18,400
Pomaga tudi pri avtomatizaciji in sistematizaciji notranjih procesov.

15
00:01:18,720 --> 00:01:24,680
Pomaga pri ustvarjanju funkcionalnih testov, ki zagotavljajo, da nova koda ostane tako dobra, kot je bila pred posodobitvami.

16
00:01:25,560 --> 00:01:31,800
V naslednjem delu videa bomo videli, kako lokalno nastavimo selenium in kako napišemo nekaj osnovnih funkcionalnih testov.
