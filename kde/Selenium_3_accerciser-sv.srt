﻿1
00:00:10,480 --> 00:00:18,880
I den här videon kommer vi att se vad Accerciser är, och hur vi kan använda det för att automatiskt testa elementen i det grafiska användargränssnittet

2
00:00:19,760 --> 00:00:27,560
Här har jag öppnat Accerciser, låt mig öppna Miniräknarverktyget

3
00:00:28,560 --> 00:00:31,800
Så som du kan se känner det igen miniräknaren.

4
00:00:32,159 --> 00:00:33,840
Om jag dubbeltrycker här

5
00:00:38,800 --> 00:00:41,720
Markeras fönstret

6
00:00:42,680 --> 00:00:49,840
Vi kan gå till gränssnittsvisaren för att titta på elementen.

7
00:00:51,240 --> 00:00:53,520
Så, exempelvis

8
00:00:54,520 --> 00:00:58,480
Det här är en ram

9
00:00:58,480 --> 00:01:04,760
Och om jag vill trycka på några knappar måste jag hitta det värdet, låt oss säga 9.

10
00:01:09,640 --> 00:01:17,240
Som du kan se här är det 9 och om jag vill trycka på knappen måste vi gå till åtgärder.

11
00:01:18,800 --> 00:01:22,320
Som vi kan se blev 9 nedtryckt.

12
00:01:23,320 --> 00:01:26,200
På liknande sätt kan vi också hitta värdet för addition.

13
00:01:28,040 --> 00:01:32,400
Om jag trycker på det, blir '+' nedtryckt.

14
00:01:33,520 --> 00:01:36,640
Låt oss kolla det med '9+6'

15
00:01:48,440 --> 00:01:51,080
Som vi kan se '15'

16
00:01:51,920 --> 00:01:58,080
Det är ett sätt att hitta de element som är associerade med en viss åtgärd.

17
00:01:59,800 --> 00:02:09,479
Här kan du se beskrivningen där det står "plus", och det ger oss också ID för elementet.

18
00:02:10,360 --> 00:02:15,360
Det är användbart för att skriva ett test baserat på Selenium, som kommer i den senare delen av videon.
