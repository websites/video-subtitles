﻿1
00:00:11,080 --> 00:00:13,720
In this tutorial video we will see how to setup selenium .

2
00:00:14,800 --> 00:00:18,360
Selenium is a tool used for GUI testing of applications.

3
00:00:18,360 --> 00:00:23,360
You can find this installation guide at community.kde.org/Selenium.

4
00:00:24,960 --> 00:00:28,880
In this video we will set it up on neon OS. So lets get started.

5
00:00:35,000 --> 00:00:44,080
(I already have these packages installed, it will take much more time for this step)

6
00:00:47,320 --> 00:00:53,360
Once we have necessesary packages, we will now clone the Selenium tool and follow the subsequent steps.

7
00:00:56,560 --> 00:01:01,560
(you can simply copy paste these commands)

8
00:01:29,520 --> 00:01:39,440
With these steps done successfully, we can start writing initial tests. You can follow these steps here to know how to write some basic tests for selenium.

9
00:01:40,000 --> 00:01:45,000
Lets try to run an example test. Make sure you have kcalc installed in your system.

10
00:01:47,200 --> 00:01:52,200
kcalc is basically a calculator utility application from KDE.

11
00:02:00,960 --> 00:02:04,200
As you can see we are able to run tests with selenium.

12
00:02:12,720 --> 00:02:21,360
Keep in mind that selenium is a moving target and while making this video there are no errors and build failures but such errors might occur in future.

13
00:02:21,920 --> 00:02:26,360
Make sure to report any bugs to the KDE community. And thank you for watching the video!

