1
00:00:00,000 --> 00:00:04,000
Okej, så vi har några program som öppnas, film som spelas.

2
00:00:05,000 --> 00:00:11,000
Tetris spelas i hörnet, flera saker samtidigt, och så plötsligt,

3
00:00:12,000 --> 00:00:14,000
Vår Kwin kraschar.

4
00:00:17,000 --> 00:00:22,000
Och ljudet slutade inte. Testris fortsätter så fort det kommer förbi det igen

5
00:00:26,000 --> 00:00:29,000
Och vår integrerade utvecklingsmiljö är fortfarande tillgänglig.

6
00:00:29,000 --> 00:00:33,000
Och jag kan göra det flera gånger, simulera flera krascher,

7
00:00:37,000 --> 00:00:39,000
och vi hoppade inte över någon bildruta.

8
00:00:40,000 --> 00:00:45,000
Vi kan till och med kopiera någon text, kopiera den, starta om Kwin.

9
00:00:46,000 --> 00:00:49,000
Och nu, finns den fortfarande på min insamlingstavla.

10
00:00:52,000 --> 00:00:53,000
Robust.
