﻿1
00:00:11,080 --> 00:00:13,720
Dans ce tutoriel vidéo, nous verrons comment configurer Selenium.

2
00:00:14,800 --> 00:00:18,360
Selenium est un outil utilisé pour les tests d'interface graphique utilisateur (GUI) des applications.

3
00:00:18,360 --> 00:00:23,360
Vous pouvez trouver ce guide d'installation sur le site « community.kde.org/Selenium ».

4
00:00:24,960 --> 00:00:28,880
Dans cette vidéo, nous allons le configurer sous Neon OS. Alors, commençons.

5
00:00:35,000 --> 00:00:44,080
(J'ai déjà installé ces paquets. Cela prendra beaucoup plus de temps pour cette étape)

6
00:00:47,320 --> 00:00:53,360
Une fois que nous aurons les paquets nécessaires, nous clonerons maintenant l'outil Selenium et suivrons les étapes suivantes.

7
00:00:56,560 --> 00:01:01,560
(Vous pouvez simplement faire un copier-coller de ces commandes)

8
00:01:29,520 --> 00:01:39,440
Avec ces étapes effectuées avec succès, nous pouvons commencer à écrire des tests initiaux. Vous pouvez suivre ces étapes ici pour savoir comment écrire quelques tests de base pour Selenium.

9
00:01:40,000 --> 00:01:45,000
Essayons d'exécuter un exemple de test. Veuillez-vous assurer que l'application KCalc est bien installée sur votre système.

10
00:01:47,200 --> 00:01:52,200
KCalc est essentiellement une application utilitaire de calculatrice de KDE.

11
00:02:00,960 --> 00:02:04,200
Comme vous pouvez le voir, nous sommes en mesure d'effectuer des tests avec Selenium.

12
00:02:12,720 --> 00:02:21,360
Veuillez garder à l'esprit que Selenium est une cible en mouvement et que lors de la réalisation de cette vidéo, il n'y a pas d'erreurs et d'échecs de compilation. Mais, de telles erreurs pourraient se produire à l'avenir.

13
00:02:21,920 --> 00:02:26,360
Veuillez-vous assurer de signaler tout bogue à la communauté de KDE. Et, merci d'avoir regardé la vidéo !
