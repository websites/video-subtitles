1
00:00:00,000 --> 00:00:04,880
So we're starting off in trusty Plasma.

2
00:00:04,880 --> 00:00:10,480
We've got CSGO open, top and this small custom application that I've made.

3
00:00:10,480 --> 00:00:13,720
Click a button and boom we're in Weston.

4
00:00:13,720 --> 00:00:15,240
All of the applications have survived.

5
00:00:15,240 --> 00:00:17,040
This is no additional layers.

6
00:00:17,040 --> 00:00:20,880
It's those applications being moved between the compositors.

7
00:00:20,880 --> 00:00:23,120
Click a button and back on Plasma.

8
00:00:23,120 --> 00:00:28,320
And interestingly we've gone from client side decorations to server side and back again.

9
00:00:28,320 --> 00:00:32,520
And many of the different versions and differences are being handled implicitly within the

10
00:00:32,520 --> 00:00:33,520
toolkit.

11
00:00:33,520 --> 00:00:36,800
We can jump to Gnome.

12
00:00:36,800 --> 00:00:38,760
Start off in this lovely overview effect.

13
00:00:38,760 --> 00:00:43,040
All my clients are still there, maintaining their size.

14
00:00:43,040 --> 00:00:44,040
Can go to Hyprland.

15
00:00:44,040 --> 00:00:47,640
Wait for that to load.

16
00:00:47,640 --> 00:00:53,440
Get to a very nice animation when it does. Whoo!

17
00:00:53,440 --> 00:00:56,120
We can continue moving around CSGO.

18
00:00:56,120 --> 00:00:57,760
It's behaving as before.

19
00:00:57,760 --> 00:00:59,400
Everything's just working.

20
00:00:59,400 --> 00:01:00,400
Let's jump to Sway.

21
00:01:00,400 --> 00:01:04,959
This is the only way I figured out how to launch an application in sway.

22
00:01:04,959 --> 00:01:07,280
Windows are still there.

