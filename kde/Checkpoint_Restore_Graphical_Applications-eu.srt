1
00:00:00,000 --> 00:00:08,000
Kontrol puntua lehengoratzea, erabiltzailearen espazioan, existitzen den aplikazio bat exekutatzen ari den bezala hartu eta bere egoera diskoan gordetzea da.

2
00:00:08,000 --> 00:00:14,520
Aparta dela ematen du, baina aplikazio grafikoetan, ez dabilela esaten du webgunean,

3
00:00:14,520 --> 00:00:17,520
X11rako aplikazio orok ez du euskarririk.

4
00:00:17,520 --> 00:00:24,520
«Wayland»en konposatzaileen artean mugitzeko egin dugun lanak, inplizituki, hori konpontzen du.

5
00:00:25,000 --> 00:00:32,520
Erakustaldi bat egingo dugu. «KolourPaint» irekita daukat, artelan batzuk egiten ari naiz, «Wayland»en logoa itsatsi dut, eta mashup eder bat egin.

6
00:00:33,520 --> 00:00:38,520
Eta itxura atsegina du, baina 45 megabyte RAM erabiltzen ditu.

7
00:00:38,520 --> 00:00:45,520
Agian orain beste gauza bat egitera joan behar naiz, «WebKit» konpilatzea adibidez, eta orain erabilgarri dauden...

8
00:00:45,520 --> 00:00:47,000
megabyte guztiak behar ditut.

9
00:00:47,000 --> 00:00:51,520
Beraz, komando hori exekutatzen dut, «criu dump», eta joan egin da. Diskoan gordeta.

10
00:00:52,000 --> 00:01:00,520
Berrabiarazi dezaket, muinaren eguneratze bat egin dezaket, sortutadagoen karpeta hori beste magaleko batera ere eraman dezaket,

11
00:01:00,520 --> 00:01:04,519
eta haren bitarrak hor dauden bitartean, handik lehengoratu.

12
00:01:04,519 --> 00:01:09,520
«criu restore» exekutatzen badut, itzuli egiten da, eta bat-batean itzultzen da.

13
00:01:09,520 --> 00:01:15,520
Beraz, mekanismo hori abiarazte motel bat abiarazteko ere erabil daiteke.

14
00:01:16,520 --> 00:01:20,520
Eta logoa inguruan arrastatu dezaket, egoera berberean gorde dela ikus dezakezu.

15
00:01:20,520 --> 00:01:27,520
Ezingo litzateke halakorik egin besterik gabe diskoan gorde izan banu, «KolourPaint»ek ez daukalako geruzen kontzepturik edo halakorik.

16
00:01:29,520 --> 00:01:32,520
Eta zehatz mehatz zegoen lekuan dago.

17
00:01:33,520 --> 00:01:44,520
Baino lehengoratzetik gordetzea baino gehiago egin dezakegu... «KMines» irekitzen dut, KDEn dugun Mina-bilatzailearen klon bat, ondo idatz badezaket.

18
00:01:45,520 --> 00:01:48,520
Ez naiz oso ona «KMines»ekin, nire mugimendua egin du.

19
00:01:48,520 --> 00:01:54,520
Baino oraingoan iraultzen dudanean, argumentu gehigarri hori gehitu behar diot, «leave-running» (utzi martxan).

20
00:01:54,520 --> 00:01:59,520
Eta orain egoera gorde dezaket, egin jokaldi bat.

21
00:01:59,520 --> 00:02:03,520
Ados, ez da oso saiakera ona izan, etsigarria.

22
00:02:03,520 --> 00:02:12,520
Eta orain, «KMines» ixten badut, lehengoratu dezaket, zehatz-mehatz zegoenen lekuan, eta beste aukera bat dut.

23
00:02:13,520 --> 00:02:18,520
Eta, ene, hala ere «KMines»ean txarra naiz, itxi dezaket, beste saiakera batekin jarraitu.

24
00:02:18,520 --> 00:02:26,520
Eta agian hori ez da izango Mina-bilatzailearekin jokatzeko erarik onena, baina arazteko era guztietako ondorioak izan ditzake.

25
00:02:29,520 --> 00:02:41,520
«criu»ren erabilera ez dago konposatzailearen gainerako transferentzia lanaren maila berean, baina etorkizuneko garatzaileentzako aukera asko irekitzen ditu, haren gainean eraiki eta zeozer aparta egiteko.
