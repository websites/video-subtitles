1
00:00:00,000 --> 00:00:08,000
La Restauració d'un punt de sincronisme en l'espai d'usuari és una manera de prendre una aplicació existent en execució i desar el seu estat al disc.

2
00:00:08,000 --> 00:00:14,520
Sembla que podria ser sorprenent, però a les aplicacions gràfiques no funciona, diu en el lloc web,

3
00:00:14,520 --> 00:00:17,520
Cap aplicació X11 no és compatible.

4
00:00:17,520 --> 00:00:24,520
El treball que hem fet en el Wayland per a moure's entre compositors resol implícitament això.

5
00:00:25,000 --> 00:00:32,520
Farem una demostració. Tinc el KolourPaint obert, estic fent un treball artístic, enganxat en aquest logotip del Wayland, fent una maqueta encantadora.

6
00:00:33,520 --> 00:00:38,520
I està molt bé, però fa servir 45 megabytes de RAM.

7
00:00:38,520 --> 00:00:45,520
Potser ara he de marxar i fer alguna cosa més, com compilar el WebKit, i ara necessito tots els megabytes...

8
00:00:45,520 --> 00:00:47,000
...disponibles.

9
00:00:47,000 --> 00:00:51,520
Així que executo aquesta ordre, «criu dump», i ha desaparegut. S'ha desat al disc.

10
00:00:52,000 --> 00:01:00,520
Puc reiniciar, puc fer una actualització del nucli, fins i tot puc agafar aquesta carpeta que s'ha creat, moure-la a un altre portàtil,

11
00:01:00,520 --> 00:01:04,519
i sempre que hi hagi els seus executables, restaureu-los des d'aquí.

12
00:01:04,519 --> 00:01:09,520
Si executo «criu restore», torna i ho fa a l'instant.

13
00:01:09,520 --> 00:01:15,520
Per tant, aquest mecanisme també es podria utilitzar per a arrencar un procés d'inici lent.

14
00:01:16,520 --> 00:01:20,520
I el puc arrossegar pel logotip, podeu veure que s'ha desat amb el seu estat exactament.

15
00:01:20,520 --> 00:01:27,520
No seria possible si acabés de desar la imatge al disc, perquè KolourPaint no té el concepte de capes o altres.

16
00:01:29,520 --> 00:01:32,520
I és exactament on era.

17
00:01:33,520 --> 00:01:44,520
Però podem fer alguna cosa més a part de desar per a la restauració... Obro el KMines, un dragamines que tenim a KDE, si puc escriure-ho bé.

18
00:01:45,520 --> 00:01:48,520
No soc gaire bo al KMines, faig el meu moviment.

19
00:01:48,520 --> 00:01:54,520
Però aquest cop, quan el bolqui, afegiré aquest argument addicional: «leave-running».

20
00:01:54,520 --> 00:01:59,520
I ara puc desar l'estat, fent una suposició.

21
00:01:59,520 --> 00:02:03,520
D'acord, no és una suposició gaire bona, és decebedora.

22
00:02:03,520 --> 00:02:12,520
I ara, si tanco el KMines, el puc restaurar, exactament on era, i tenir-ne un altre.

23
00:02:13,520 --> 00:02:18,520
I encara estic enganxat al KMines, puc tancar-lo, seguir tenint una altra oportunitat.

24
00:02:18,520 --> 00:02:26,520
I potser aquesta no és la millor manera de jugar al dragamines, però podria tenir tota mena d'altres implicacions per a la depuració.

25
00:02:29,520 --> 00:02:41,520
L'ús del Crio no està al mateix nivell que la resta de feina de transferència del compositor, però desbloqueja moltes opcions per a futurs desenvolupadors per a construir i fer quelcom genial.
