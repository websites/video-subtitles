﻿1
00:00:06,320 --> 00:00:10,160
V tem videoposnetku bomo videli, kako lahko napišemo teste na osnovi seleniuma.

2
00:00:11,040 --> 00:00:14,880
Torej bi bil predpogoj za ta videoposnetek videoposnetek `Namestitev seleniuma - Setting up Selenium`.

3
00:00:14,880 --> 00:00:21,640
Torej morate res imeti nameščen in delujoč selenium v ​​sistemu, da lahko začnete pisati test.

4
00:00:22,400 --> 00:00:30,520
Tukaj lahko sledimo navodilom za nastavitev, lahko uporabimo predloge, ki so že omenjene v razdelku 'Pisanje testov - Writing tests'.

5
00:00:31,560 --> 00:00:39,200
Tukaj imam odprto kodo VS in kot lahko vidite, imam napisano celotno kodo

6
00:00:39,200 --> 00:00:47,280
In potem sem napisal kodo za '9 + 6', ki bi morala biti '15', kar je bilo tudi prikazano v videu accerciser.

7
00:00:48,600 --> 00:00:49,760
Pojdimo skozi to

8
00:00:54,280 --> 00:01:01,720
Poskušamo najti vrednost '9', če še enkrat odprem Accerciser in tukaj najdem '9'

9
00:01:03,320 --> 00:01:14,120
Videli smo, da je vrednost gumba tukaj '9', ki se uporablja tudi tukaj. Podobno za plus lahko vidimo, da je vrednost gumba znak '+', ki je tudi omenjen tukaj.

10
00:01:14,920 --> 00:01:25,720
Kar počnemo tukaj, je proženje klika. Tukaj je torej kliknjeno '9', nato simbol '+' in nato '6' in nato simbol '='.

11
00:01:29,360 --> 00:01:31,920
Enak simbol je tudi '='

12
00:01:32,200 --> 00:01:39,240
Nato poskušamo izvleči besedilo iz razdelka za prikaz rezultatov

13
00:01:40,120 --> 00:01:47,720
Torej, če gremo v Accerciser in poskušamo najti opis za območje prikazanega besedila.

14
00:01:55,200 --> 00:02:04,680
To je besedilno območje prikaza in kot lahko vidimo, opis pravi 'Prikaz rezultata'. Torej to počnemo tukaj.

15
00:02:04,680 --> 00:02:10,880
Uporabljamo 'Prikaz rezultata' in poskušamo izluščiti besedilo iz tega, kar je prikazano.

16
00:02:12,200 --> 00:02:15,600
To je osnovni test, zato ga poskusimo zagnati.

17
00:02:28,520 --> 00:02:33,520
Kot lahko vidimo, je bil test uspešen in dobimo OK.

18
00:02:34,600 --> 00:02:48,240
Tako lahko napišete več testov, to lahko napišete tudi za deljenje, odštevanje in karkoli drugega, kar lahko najdete s pripomočkom Accerciser

19
00:02:49,840 --> 00:02:59,800
Upoštevati je treba, da nekatere aplikacije morda nimajo kode GUI za dostopnost; v tem primeru boste morali kodo za dostopnost dodati ročno.

20
00:03:00,640 --> 00:03:04,720
Po tem boste lahko dostopali do teh elementov s Seleniumom.

21
00:03:05,800 --> 00:03:09,640
Hvala za ogled :)
