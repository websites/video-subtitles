﻿1
00:00:00,000 --> 00:00:05,200
In this video we will see what is selenium and how KDE uses it to build sustainable software

2
00:00:05,200 --> 00:00:11,040
And also how it helps achieving other KDE goals which are accessibility and system testing.

3
00:00:12,200 --> 00:00:20,120
Software usage is rapidely increasing and with this increase we increase its environmental impact such as for example more CO2 emissions

4
00:00:20,120 --> 00:00:24,360
If we want to minimise this impact we need to minimise energy consumption.

5
00:00:24,360 --> 00:00:28,400
And to minimise energy consumption we need to be able to measure this consumption.

6
00:00:29,240 --> 00:00:34,360
To be able to measure energy consumption  we need a mecanisms  that can mimic user behaviour

7
00:00:34,920 --> 00:00:39,240
to reproduce what user would do with their mouse and their keyboard.

8
00:00:39,320 --> 00:00:43,920
Along with that, we also need a hardware tool to actually measure the energy consumptions.

9
00:00:43,920 --> 00:00:47,160
This mecanism will be provided by Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI is a tool created by KDE that uses selenium for web driver automation and  linux AT-SPI accessibility protocol.

11
00:00:55,360 --> 00:01:00,360
With the help of selenium we can create usage scenario script that mimic user behaviour.

12
00:01:01,520 --> 00:01:05,680
So as you can see it can help build sustainable software, but that's not all.

13
00:01:05,680 --> 00:01:14,400
Selenium also helps us achieve KDE for all goal, allowing to improve accessibility for everyone including people with disabilities.

14
00:01:14,400 --> 00:01:18,400
It also helps Automating and systematising internal processes.

15
00:01:18,720 --> 00:01:24,680
It helps create functional tests which ensures that new code stays as good as it was before the updates.

16
00:01:25,560 --> 00:01:31,800
In the next  part of the video we will see how to setup selenium locally and how to write some basic functional tests.

