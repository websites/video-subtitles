﻿1
00:00:00,000 --> 00:00:05,200
Dans cette vidéo, nous verrons ce qu'est Selenium et comment KDE l'utilise pour compiler des logiciels durables.

2
00:00:05,200 --> 00:00:11,040
Et aussi comment cela aide à atteindre d'autres objectifs de KDE que sont l'accessibilité et les tests du système.

3
00:00:12,200 --> 00:00:20,120
L'utilisation des logiciels augmente rapidement et, avec cette augmentation, nous augmentons leurs impacts environnementaux, comme par exemple, plus d'émissions de CO2.

4
00:00:20,120 --> 00:00:24,360
Si nous voulons minimiser cet impact, nous devons minimiser la consommation d'énergie.

5
00:00:24,360 --> 00:00:28,400
Et pour minimiser la consommation d'énergie, nous devons être capable de mesurer cette consommation.

6
00:00:29,240 --> 00:00:34,360
Pour pouvoir mesurer la consommation d'énergie, nous avons besoin d'un mécanisme capable d'imiter le comportement de l'utilisateur.

7
00:00:34,920 --> 00:00:39,240
pour reproduire ce que l'utilisateur ferait avec sa souris et son clavier.

8
00:00:39,320 --> 00:00:43,920
Parallèlement à cela, nous avons également besoin d'un outil matériel pour mesurer réellement les consommations d'énergie.

9
00:00:43,920 --> 00:00:47,160
Ce mécanisme sera mis à disposition par Selenium-AT-SPI.

10
00:00:47,160 --> 00:00:55,360
Selenium-AT-SPI est un outil créé et utilisé par KDE pour l'automatisation des pilotes Internet et le protocole d'accessibilité « Linux AT-SPI ».

11
00:00:55,360 --> 00:01:00,360
Avec l'aide de Selenium, nous pouvons créer un script de scénario d'utilisation imitant le comportement de l'utilisateur.

12
00:01:01,520 --> 00:01:05,680
Donc, comme vous pouvez le voir, cela peut aider à créer des logiciels durables. Mais, ce n'est pas tout.

13
00:01:05,680 --> 00:01:14,400
Selenium nous aide également à atteindre tous les objectifs de KDE en permettant d'améliorer l'accessibilité pour toute personne, y compris les personnes handicapées.

14
00:01:14,400 --> 00:01:18,400
Il aide également à automatiser et à systématiser les processus internes.

15
00:01:18,720 --> 00:01:24,680
Cela aide à la création de tests fonctionnels garantissant que le nouveau code reste aussi bon qu'avant les mises à jour.

16
00:01:25,560 --> 00:01:31,800
Dans la partie suivante de la vidéo, nous verrons comment configurer Selenium localement et comment écrire quelques tests fonctionnels de base.
