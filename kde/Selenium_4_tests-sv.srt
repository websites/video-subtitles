﻿1
00:00:06,320 --> 00:00:10,160
I den  här videon ser vi hur man kan skriva tester baserade på selenium.

2
00:00:11,040 --> 00:00:14,880
Så en nödvändig förutsättning för den här videon är videon  `Ställa in Selenium`.

3
00:00:14,880 --> 00:00:21,640
Så du behöver verkligen ha selenium installerat och klart att köra på systemet så att du kan börja skriva tester.

4
00:00:22,400 --> 00:00:30,520
Vi kan följa installationsguiden här, vi kan använda fördefinierade grejer som redan nämnts i avsnittet 'Skriva tester'.

5
00:00:31,560 --> 00:00:39,200
Så här har jag öppnat VS Code och som du kan se har jag all fördefinierad kod inskriven

6
00:00:39,200 --> 00:00:47,280
Och sedan har jag skrivit en kod för '9 + 6' som ska vara '15' som också visades i Accerciser-videon.

7
00:00:48,600 --> 00:00:49,760
Låt oss gå igenom det

8
00:00:54,280 --> 00:01:01,720
Vi försöker hitta värdet '9', om jag öppnar Accerciser igen och hittar '9' här

9
00:01:03,320 --> 00:01:14,120
Vi såg att tryckknappens värde här är '9' som också används här. På samma sätt för plus kan vi se att tryckknappens värde är tecknet '+' som också nämns här.

10
00:01:14,920 --> 00:01:25,720
Det vi gör här är att utföra ett klick. Så '9' klickas här, sedan '+'-symbolen och sedan '6' och sedan symbolen '='.

11
00:01:29,360 --> 00:01:31,920
Lika med symbolen är alltså '='

12
00:01:32,200 --> 00:01:39,240
Och sedan försöker vi extrahera texten från sektionen med resultatvisningen

13
00:01:40,120 --> 00:01:47,720
Så om vi går till Accerciser och försöker hitta en beskrivning för visningens textområde.

14
00:01:55,200 --> 00:02:04,680
Det är visningens textområdet och som vi kan se, i beskrivningen står det "Result Display". Så det är vad vi gör här.

15
00:02:04,680 --> 00:02:10,880
Vi använder 'Result Display' och vi försöker extrahera text från det som visas.

16
00:02:12,200 --> 00:02:15,600
Det är ett grundläggande test, så låt oss bara försöka köra det.

17
00:02:28,520 --> 00:02:33,520
Som vi kan se var testet lyckat och vi får ett OK.

18
00:02:34,600 --> 00:02:48,240
Så här kan du skriva flera tester, det kan också skrivas för division, subtraktion och allt annat som du kan hitta med Accerciser-verktyget

19
00:02:49,840 --> 00:02:59,800
En sak att notera är att vissa program kanske inte har det grafiska användargränssnittets kod för tillgänglighet, i så fall måste du lägga till kod för tillgänglighet manuellt.

20
00:03:00,640 --> 00:03:04,720
Efter det kan du komma åt elementen med Selenium.

21
00:03:05,800 --> 00:03:09,640
Tack för att du tittade :)
